#pragma once
#include "../inc/fmod.hpp"
#include "../inc/fmod.h"
#include "fmod_errors.h"
#include "../inc/fmod_dsp.h"
#include "../inc/fmod_output.h"
#include <cstring>
#include <iostream>
#include <vector>

#define SOUND SoundManager::Instance()

#pragma comment(lib, "fmodex_vc.lib")

using namespace std;

class SoundManager
{
	FMOD::System* system;
	SoundManager(void);
	FMOD::Sound* music;
	FMOD::Channel* musicChannel;
	FMOD::Reverb* reverb;
	vector<pair<FMOD::Sound*, float>> effects;
	float masterVolume;
public:
	static SoundManager* Instance();
	~SoundManager(void);
	void initialize();
	void playSoundEffect(string name);
	void playSoundEffect(string name, FMOD_VECTOR pos, bool loop);
	void startSong(string name);
	void setReverb(FMOD_REVERB_PROPERTIES reb);
	void activateReverb(bool in);
	void endSong();
	void update();
	void setMusicVolume(float volume);
	void setPosVel(FMOD_VECTOR pos, FMOD_VECTOR vel, FMOD_VECTOR fwd, FMOD_VECTOR up);
};


#pragma once
#include "../inc/d3dx9.h"

#define CAM Camera::Instance()

enum camstate{FREE, FIRST, THIRD};

class Camera
{

	float m_TPHeight;
	float m_TPDist;

	camstate state;

	D3DXVECTOR3 m_Position;
	D3DXVECTOR3 m_Forward;
	D3DXVECTOR3 m_Right;
	D3DXVECTOR3	m_Up;

	D3DXVECTOR3 m_CinematicPos;
	D3DXQUATERNION m_CinematicOrient;
	
	D3DXMATRIX m_ViewMat;
	D3DXMATRIX m_ProjMat;
	
	float slerp;
	float slerpValue;

	float m_Speed;
	float m_Sensitivity;

	float dx;
	float dy;

	bool cinematicMoving;
	
	Camera(void);
	void recalcVectors();


public:
	static Camera* Instance();

	void BuildViewMatrix();
	void update(float dt, D3DXVECTOR3 input);
	void menuUpdate(float dt);
	void cinematicUpdate(float dt);
	void setCinematicTarget(D3DXVECTOR3 pos, D3DXVECTOR3 tar, float slerpValue);
	void reset(D3DXVECTOR3 pos);
	void menuReset();
	void buildProjMat();
	
	void setPos(D3DXVECTOR3 position);
	void setTarget(D3DXVECTOR3 target);
	void setSpeed(float speed);
	void setSensitivity(float sen);
	void setState(camstate in);

	D3DXVECTOR3 getPos();
	D3DXVECTOR3 getForward();
	D3DXVECTOR3 getRight();
	D3DXVECTOR3 getUp();
	D3DXMATRIX getViewMat();
	camstate getState();

	bool isCinematicMoving();

	float getDX();
	float getDY();
	
	~Camera(void);

	//Particle Code Includes
	// Box coordinates should be relative to world space.
	//bool isVisible(const AABB& box)const;
	D3DXMATRIX getProjMat();
	D3DXMATRIX getViewProjMat();
};


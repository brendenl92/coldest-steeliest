#pragma once
#include "Renderer.h"
#include "AIManager.h"
#include "PhysicsInterface.h"
#include "MapLoader.h"
#include "LaserManager.h"

class GameWorld
{
	vector<MasterEntity*> entities;
	UINT numVerts;
	Renderer* renderer;
	int frames;
	int fps;
	float elapsed;
	char FPSBuffer[256];
	bool countDownStarted;
	bool countDownEnded;
	bool doCountDown;
	float countDown;
	int chosenObject;
	bool editor;
	float moveSize;
	float finishTimer;
	float finishTime;
	MapLoader ML;

	bool gameOver;
	UINT winner;
	MasterEntity* tv;
	POINT p;

public:
	GameWorld();
	~GameWorld(); 

	void init();
	void destroy();

	void handleInput(RAWINPUT *in);

	void update(double dt);
	void render();

	void onLostDevice();
	void onResetDevice();

};


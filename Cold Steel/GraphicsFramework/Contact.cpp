#include "Contact.h"
#include "collision function.h"
#include "MasterEntity.h"

//////////////////
//Contact
/////////////////
Contact::Contact(BoundingVolumeNode* lhs, BoundingVolumeNode* rhs, V3 POC, V3 Normal, float Depth, float Epsilon)
{
	mVolumes[0] = lhs;
	mVolumes[1] = rhs;
	mPoint = POC;
	mNormal = Normal;
	mDepth = Depth;
	mEpsilon = Epsilon;
}


/////////////////////
//Collision Data
////////////////////
CollisionData::CollisionData(int MC, int MI)
{
	mMaxContacts = MC;
	mMaxItterations = MC*4;
}

void CollisionData::AddContact(Contact* contact)
{
	int i = mContacts.size();

	if (i >= mMaxContacts)
		return;
	else
		mContacts.push_back(contact);

	//transform into priority queue logic
}


//Detector
bool CollisionData::DetectCollision(BoundingVolumeNode* lhs, BoundingVolumeNode* rhs)
{
	return mDetector.DetectCollisions(lhs, rhs);
}


//Generate Contact
Contact* CollisionData::GenerateContact(BoundingVolumeNode* lhs, BoundingVolumeNode* rhs)
{
	Contact* C = new Contact(*mGenerator.GenerateContact(lhs, rhs));
	AddContact(C);
	return C;
}

//Resolve Contacts
void CollisionData::RsolveContacts()
{
	int i = 0;

	if (mContacts.size() > 0)
	{
		while (i < mMaxItterations)
		{
 			mResolver.InterpenetrationFix(mContacts[i]);
			delete mContacts[i];
			mContacts[i] = NULL;
			mContacts.erase(mContacts.begin() + i);
			i++;

			if (((unsigned)i) >= mContacts.size())
				return;
		}
	}
}



/////////////////////
//Collision Detector
/////////////////////
bool CollisionDetector::DetectCollisions(BoundingVolumeNode* lhs, BoundingVolumeNode* rhs)
{
	//int priotity = Priority(lhs, rhs);	//Adjust priority of order of volumes to test
										//in order to reduce the number of tests required

	if (lhs->mVolume->mShape == VSPHERE &&
		rhs->mVolume->mShape == VSPHERE)
			return SphereToSphere(lhs, rhs);
	else if (lhs->mVolume->mShape == VCUBE &&
		rhs->mVolume->mShape == VCUBE)
	{
		if (SphereToSphere(lhs, rhs))
 			return OBBToOBB(lhs, rhs);
		else
			return NULL;
	}
	else
		return false;
}

bool CollisionDetector::SphereToSphere(BoundingVolumeNode* lhs, BoundingVolumeNode* rhs)
{
	//int priority;
	//V3 p1, p2;
	//if (lhs->mBody->Mass() == rhs->mBody->Mass())
	//{
	//	priority = LHS;
	//	p1 = lhs->mBody->GetPosition();
	//	p2 = rhs->mBody->GetPosition();
	//}
	//else if (lhs->mBody->Mass() > rhs->mBody->Mass())
	//{
	//	priority = LHS;
	//	p1 = lhs->mBody->GetPosition();
	//	p2 = rhs->mBody->GetPosition();
	//}
	//else
	//{
	//	priority = RHS;
	//	p1 = rhs->mBody->GetPosition();
	//	p2 = lhs->mBody->GetPosition();
	//}

	//test collision
	V3 p1, p2;
	float r1, r2;
	r1 = lhs->mVolume->mRadius;
	p1 = lhs->mBody->GetPosition();
	r2 = rhs->mVolume->mRadius;
	p2 = rhs->mBody->GetPosition();
	float distance = D3DXVec3LengthSq(&(p1 - p2));

	return (((r1*r1) + (r2*r2)) > distance);
}


//Sphere to OBB
bool CollisionDetector::SphereToOBB(BoundingVolumeNode* S, BoundingVolumeNode* C)
{
	D3DXVECTOR3 q;

	//find closest point on OBB to sphere
	ClosestPtPointOBB(S->mBody->GetPosition(), C, q);

	// Sphere and AABB intersect if the (squared) distance from sphere
	//center to
	// point p is less than the (squared) sphere radius
	D3DXVECTOR3 v = q - S->mBody->GetPosition();
	return  D3DXVec3Dot(&v, &v) <= (S->mVolume->mRadius * S->mVolume->mRadius);
}



//Seperating Axis Test Cube & Cube
bool CollisionDetector::OBBToOBB(BoundingVolumeNode* one, BoundingVolumeNode* two)
{
	//Geting axis from OBB 1
	V3 OneX_axis = one->mBody->GetAxis(0);
	V3 OneY_axis = one->mBody->GetAxis(1);
	V3 OneZ_axis = one->mBody->GetAxis(2);
	//Geting axis from OBB 2
	V3 TwoX_axis = two->mBody->GetAxis(0);
	V3 TwoY_axis = two->mBody->GetAxis(1);
	V3 TwoZ_axis = two->mBody->GetAxis(2);

	// Find the vector between the two centers
	V3 toCenter = two->mBody->GetPosition() - one->mBody->GetPosition();
	V3 temp;

	return (
			overlapOnAxis(one, two, OneX_axis, toCenter) && // Check on box one's axes
			overlapOnAxis(one, two, OneY_axis, toCenter) &&
			overlapOnAxis(one, two, OneZ_axis, toCenter) &&
			overlapOnAxis(one, two, TwoX_axis, toCenter) && // And on two's
			overlapOnAxis(one, two, TwoY_axis, toCenter) &&
			overlapOnAxis(one, two, TwoY_axis, toCenter) &&
			// Now on the cross products
			overlapOnAxis(one, two, Cross(OneX_axis, TwoX_axis), toCenter) &&
			overlapOnAxis(one, two, Cross(OneX_axis, TwoY_axis), toCenter) &&
			overlapOnAxis(one, two, Cross(OneX_axis, TwoY_axis), toCenter) &&
			overlapOnAxis(one, two, Cross(OneY_axis, TwoX_axis), toCenter) &&
			overlapOnAxis(one, two, Cross(OneY_axis, TwoY_axis), toCenter) &&
			overlapOnAxis(one, two, Cross(OneY_axis, TwoZ_axis), toCenter) &&
			overlapOnAxis(one, two, Cross(OneZ_axis, TwoX_axis), toCenter) &&
			overlapOnAxis(one, two, Cross(OneZ_axis, TwoY_axis), toCenter) &&
			overlapOnAxis(one, two, Cross(OneZ_axis, TwoZ_axis), toCenter)
		);
}


bool CollisionDetector::RayToSphere(V3& Rp, V3& Rn, BoundingVolumeNode* rhs)
{
	V3 m = Rp - rhs->mBody->GetPosition();
	float c = D3DXVec3Dot(&m, &m) - rhs->mVolume->mRadius * rhs->mVolume->mRadius;
	// If there is definitely at least one real root, there must be an intersection
	if (c <= 0.0f) return true;
	float b = D3DXVec3Dot(&m, &Rn);
	// Early exit if ray origin outside sphere and ray pointing away from sphere
	if (b > 0.0f) return 0;
	float disc = b*b - c;
	// A negative discriminant corresponds to ray missing sphere
	if (disc < 0.0f) return 0;
	// Now ray must hit sphere
	return 1;
}
























////////////////////////////////
//Contact Generator
//////////////////////////////
Contact* ContactGenerator::GenerateContact(BoundingVolumeNode* lhs, BoundingVolumeNode* rhs)
{
	if (lhs->mVolume->mShape == VSPHERE &&
		rhs->mVolume->mShape == VSPHERE)
		return SphereToSphere(lhs, rhs);
	if (lhs->mVolume->mShape == VCUBE &&
		rhs->mVolume->mShape == VCUBE)
		/*return OBBToOBB(lhs, rhs);*/
		return SphereToSphere(lhs, rhs);
	else
		return NULL;
}




//Sphere to Sphere
Contact* ContactGenerator::SphereToSphere(BoundingVolumeNode* lhs, BoundingVolumeNode* rhs)
{
	//int priority;
	V3 p1, p2, norm, point;
	float m1, m2;
	float depth;
	m1 = lhs->mBody->Mass();
	m2 = rhs->mBody->Mass();


	p1 = lhs->mBody->GetPosition();
	p2 = rhs->mBody->GetPosition();

	V3 midline = p1 - p2;
	float size = D3DXVec3Length(&midline);

	norm = midline * (((float)1.0) / size);
	point = p1 + midline * (float)0.5;
	depth = (lhs->mVolume->mRadius + rhs->mVolume->mRadius - size);

	return new Contact(lhs, rhs, point, norm, depth);


	////Bigger Mass takes priority
	//if (m1 < m2)
	//{
	//	p1 = rhs->mBody->GetPosition();
	//	p2 = lhs->mBody->GetPosition();
	//	priority = 1;
	//}
	//else
	//{
	//	p2 = rhs->mBody->GetPosition();
	//	p1 = lhs->mBody->GetPosition();
	//	priority = 0;
	//}


	////Contact Normal
	//norm = p2 - p1;
	//D3DXVec3Normalize(&norm, &norm);

	////Contact Point
	//if (priority)	//if rhs is hevyer
	//	point = (norm*rhs->mVolume->mRadius) + p1;	//equal to the normal of the two spheres multiplied by
	//												// the radius of the starting end of the norm vector
	//												//then adding the global position of the starting end
	//else
	//	point = (norm*lhs->mVolume->mRadius) + p1;


	////Penetration Depth
	//depth = D3DXVec3Length(&(p2 - point));


	//return new Contact(lhs, rhs, point, norm, depth);
}


//Sphere to OBB
Contact* ContactGenerator::SphereToOBB(BoundingVolumeNode* S, BoundingVolumeNode* B)
{
	// Transform the centre of the sphere into box coordinates
	D3DXVECTOR3 center = S->mBody->GetPosition();
	D3DXMATRIX ITM;
	D3DXMatrixInverse(&ITM, 0, &B->mBody->Master()->getTrans(0));
	D3DXVECTOR3 relCenter;
	D3DXVec3TransformCoord(&relCenter, &center, &ITM);

	D3DXVECTOR3 closestPt(0, 0, 0);
	float dist;

	// Clamp each coordinate to the box.
	dist = relCenter.x;
	if (dist > B->mVolume->mHalfsize.x) dist = B->mVolume->mHalfsize.x;
	if (dist < -B->mVolume->mHalfsize.x) dist = -B->mVolume->mHalfsize.x;
	closestPt.x = dist;

	dist = relCenter.y;
	if (dist > B->mVolume->mHalfsize.y) dist = B->mVolume->mHalfsize.y;
	if (dist < -B->mVolume->mHalfsize.y) dist = -B->mVolume->mHalfsize.y;
	closestPt.y = dist;

	dist = relCenter.z;
	if (dist > B->mVolume->mHalfsize.z) dist = B->mVolume->mHalfsize.z;
	if (dist < -B->mVolume->mHalfsize.z) dist = -B->mVolume->mHalfsize.z;
	closestPt.z = dist;

	// Check we're in contact
	dist = D3DXVec3LengthSq(&(closestPt - relCenter));
	if (dist > S->mVolume->mRadius * S->mVolume->mRadius) return 0;

	// Compile the contact
	D3DXVECTOR3 closestPtWorld;
	D3DXVec3TransformCoord(&relCenter, &closestPt, &B->mBody->Master()->getTrans(0));

	
	D3DXVECTOR3 contactNormal = (closestPtWorld - center);
	D3DXVec3Normalize(&contactNormal, &contactNormal);
	D3DXVECTOR3 contactPoint = closestPtWorld;
	float penetration = S->mVolume->mRadius - sqrt(dist);
	//contact->SetBodyData(sphere, box, .95);

	return new Contact(S, B, contactPoint, contactNormal, penetration);
}






//OBB OBB contact generation
Contact* ContactGenerator::OBBToOBB(BoundingVolumeNode *one, BoundingVolumeNode *two)
{
	// Find the vector between the two centres
	V3 toCentre = two->mBody->GetPosition() - one->mBody->GetPosition();

	// We start assuming there is no contact
	float pen = FLT_MAX;
	unsigned best = 0xffffff;

	// Now we check each axes, returning if it gives us
	// a separating axis, and keeping track of the axis with
	// the smallest penetration otherwise.
	CHECK_OVERLAP(one->mBody->GetAxis(0), 0);
	CHECK_OVERLAP(one->mBody->GetAxis(1), 1);
	CHECK_OVERLAP(one->mBody->GetAxis(2), 2);

	CHECK_OVERLAP(two->mBody->GetAxis(0), 3);
	CHECK_OVERLAP(two->mBody->GetAxis(1), 4);
	CHECK_OVERLAP(two->mBody->GetAxis(2), 5);

	// Store the best axis-major, in case we run into almost
	// parallel edge collisions later
	unsigned bestSingleAxis = best;

	CHECK_OVERLAP(Cross(one->mBody->GetAxis(0), two->mBody->GetAxis(0)), 6);
	CHECK_OVERLAP(Cross(one->mBody->GetAxis(0), two->mBody->GetAxis(1)), 7);
	CHECK_OVERLAP(Cross(one->mBody->GetAxis(0), two->mBody->GetAxis(2)), 8);
	CHECK_OVERLAP(Cross(one->mBody->GetAxis(1), two->mBody->GetAxis(0)), 9);
	CHECK_OVERLAP(Cross(one->mBody->GetAxis(1), two->mBody->GetAxis(1)), 10);
	CHECK_OVERLAP(Cross(one->mBody->GetAxis(1), two->mBody->GetAxis(2)), 11);
	CHECK_OVERLAP(Cross(one->mBody->GetAxis(2), two->mBody->GetAxis(0)), 12);
	CHECK_OVERLAP(Cross(one->mBody->GetAxis(2), two->mBody->GetAxis(1)), 13);
	CHECK_OVERLAP(Cross(one->mBody->GetAxis(2), two->mBody->GetAxis(2)), 14);

	// Make sure we've got a result.
	//assert(best != 0xffffff);

	// We now know there's a collision, and we know which
	// of the axes gave the smallest penetration. We now
	// can deal with it in different ways depending on
	// the case.
	if (best < 3)
	{
		// We've got a vertex of box two on a face of box one.
		return fillPointFaceBoxBox(one, two, toCentre, best, pen);
	}
	else if (best < 6)
	{
		// We've got a vertex of box one on a face of box two.
		// We use the same algorithm as above, but swap around
		// one and two (and therefore also the vector between their
		// centres).
		return fillPointFaceBoxBox(two, one, toCentre*-1.0f, best - 3, pen);
	}
	else
	{
		// We've got an edge-edge contact. Find out which axes
		best -= 6;
		unsigned oneAxisIndex = best / 3;
		unsigned twoAxisIndex = best % 3;
		V3 oneAxis = one->mBody->GetAxis(oneAxisIndex);
		V3 twoAxis = two->mBody->GetAxis(twoAxisIndex);
		V3 axis = Cross(oneAxis, twoAxis);
		D3DXVec3Normalize(&axis, &axis);

		// The axis should point from box one to box two.
		if (D3DXVec3Dot(&axis, &toCentre) > 0) axis = axis * -1.0f;

		// We have the axes, but not the edges: each axis has 4 edges parallel
		// to it, we need to find which of the 4 for each object. We do
		// that by finding the point in the centre of the edge. We know
		// its component in the direction of the box's collision axis is zero
		// (its a mid-point) and we determine which of the extremes in each
		// of the other axes is closest.
		V3 ptOnOneEdge = one->mVolume->mHalfsize;
		V3 ptOnTwoEdge = two->mVolume->mHalfsize;
		for (unsigned i = 0; i < 3; i++)
		{
			if (i == oneAxisIndex) SetM(ptOnOneEdge, i, 0);
			else if (D3DXVec3Dot(&one->mBody->GetAxis(i), &axis) > 0.0f) SetM(ptOnOneEdge, i, -1 * GetM(ptOnOneEdge, i));

			if (i == twoAxisIndex) SetM(ptOnTwoEdge, i, 0);
			else if (D3DXVec3Dot(&two->mBody->GetAxis(i), &axis) < 0.0f) SetM(ptOnTwoEdge, i, -1 * GetM(ptOnTwoEdge, i));
		}

		// Move them into world coordinates (they are already oriented
		// correctly, since they have been derived from the axes).
		D3DXVECTOR4 vec;

		D3DXVec3Transform(&vec, &ptOnOneEdge, &two->mBody->Transform());
		ptOnOneEdge = V3(vec.x, vec.y, vec.z);

		D3DXVec3Transform(&vec, &ptOnTwoEdge, &two->mBody->Transform());
		ptOnTwoEdge = V3(vec.x, vec.y, vec.z);

		// So we have a point and a direction for the colliding edges.
		// We need to find out point of closest approach of the two
		// line-segments.
		V3 vertex = contactPoint(
			ptOnOneEdge, oneAxis, GetM(one->mVolume->mHalfsize, oneAxisIndex),
			ptOnTwoEdge, twoAxis, GetM(two->mVolume->mHalfsize, twoAxisIndex),
			bestSingleAxis > 2
			);

		// We can fill the contact.
		Contact* contact = new Contact(one, two, vertex, axis, pen);

		return contact;
	}
	return 0;
}


//Ray to OBB contact Generation
Contact* ContactGenerator::RayToOBB(V3& Rp, V3& Rn, BoundingVolumeNode* rhs)
{
	V3 Rpoc, Bpoc, Bp;
	Bp = rhs->mBody->GetPosition();

	//Closest point on Ray to the center of OBB
	float t = D3DXVec3Dot(&Bp, &Rn);
	Rpoc = Rn*t;

	V3 norm;
	D3DXVec3Normalize(&norm, &(Bp - Rpoc));

	return new Contact(rhs, NULL, Rpoc, norm, 1);
}

//Ray to spheres
Contact* ContactGenerator::RayToSphere(V3& Rp, V3& Rn, BoundingVolumeNode* rhs)
{
		// Intersects ray r = p + td, |d| = 1, with sphere s and, if intersecting,
		// returns t value of intersection and intersection point q
		float t;
		V3 m = Rp - rhs->mBody->GetPosition();
		float b = D3DXVec3Dot(&m, &Rn);
		float c = D3DXVec3Dot(&m, &m) - rhs->mVolume->mRadius * rhs->mVolume->mRadius;
		// Exit if r�s origin outside s (c > 0)and r pointing away from s (b > 0)
		if (c > 0.0f && b > 0.0f) return 0;
		float discr = b*b - c;
		// A negative discriminant corresponds to ray missing sphere
		if (discr < 0.0f) return 0;
		// Ray now found to intersect sphere, compute smallest t value of intersection
		t = -b - sqrt(discr);
		// If t is negative, ray started inside sphere so clamp t to zero
		if (t < 0.0f) t = 0.0f;
		V3 q = Rp + t * Rn;
		V3 n;
		D3DXVec3Normalize(&n, &(q - Rp));
		return new Contact(NULL, NULL, q, n, 1);

	//--------------------------------------------------------------------------------

	//	// Test if ray r = p + td intersects sphere s
	//	int TestRaySphere(Point p, Vector d, Sphere s)
	//{
	//		Vector m = p � s.c;
	//		float c = Dot(m, m) � s.r * s.r;
	//		// If there is definitely at least one real root, there must be an intersection
	//		if (c <= 0.0f) return 1;
	//		float b = Dot(m, d);
	//		// Early exit if ray origin outside sphere and ray pointing away from sphere
	//		if (b > 0.0f) return 0;
	//		float disc = b*b � c;
	//		// A negative discriminant corresponds to ray missing sphere
	//		if (disc < 0.0f) return 0;
	//		// Now ray must hit sphere
	//		return 1;
	//	}
}


////////////////////////
//Contact Resolver
///////////////////////
void ContactResolver::InterpenetrationFix(Contact* C)
{
	float dist1, dist2;
	float m1, m2, total;
	float depth;
	V3 norm, point, p1, p2;
	V3 movePerMass;
	m1 = C->mVolumes[0]->mBody->Mass();
	m2 = C->mVolumes[1]->mBody->Mass();


	//Cache data
	depth = C->mDepth;
	norm = C->mNormal;
	point = C->mPoint;


	//Goofy Fix
	//!!my interpenetration would be too dramatic
	depth = depth * .02f;





	//mass tests
	total = m1 + m2;

	if (total <= 0)	//Return if masses are both zero
		return;


	dist1 = dist2 = 1;	//The amount of position will change of the total depth
						//Set to 1 in case one entity has infinite mass

	//As long as the entity does not have infinite mass
	//Set the amount the entity will move of the total depth
	if (m1 > 0)
		dist1 = m2 * 100 / total / 100 * depth;
	if (m2 > 0)
		dist2 = m1 * 100 / total / 100 * depth;

	
	//	movePerMass = norm * (depth / total);

	/*if (m1 > 0)
		dist1 = (m2 / total) * depth * norm;
	if (m2 > 0)
		dist2 = m1 * 100 / total / 100 * depth;*/

	/*if (m1 > 0)
		p1 = movePerMass * m1;
	if (m2 > 0)
		p2 = movePerMass * -m2;*/

	//if (D3DXVec3Length(&C->mVolumes[0]->mBody->Velocity()) <= 0)
	//{
	//	dist2 = 1;
	//	dist1 = 0;
	//}

	//if (D3DXVec3Length(&C->mVolumes[1]->mBody->Velocity()) <= 0)
	//{
	//	dist1 = 1;
	//	dist2 = 0;
	//}


	//Calculate movement along the normal
	//then set the positions
	//Zero out velocity to stop continus contact generation
	float dicks = C->mVolumes[0]->mBody->GetPosition().y;
	float dicks2 = C->mVolumes[1]->mBody->GetPosition().y;
	p1 = (C->mVolumes[0]->mBody->GetPosition() + norm * dist1);
	p2 = (C->mVolumes[1]->mBody->GetPosition() + norm * -1 * dist2);
	p1.y = dicks;
	p2.y = dicks2;
	C->mVolumes[0]->mBody->SetPosition(p1);
	//C->mVolumes[0]->mBody->SetVelocity(V3(0, 0, 0));
	C->mVolumes[1]->mBody->SetPosition(p2);
	//C->mVolumes[1]->mBody->SetVelocity(V3(0, 0, 0));
}
//ASM.cpp
#include "ASM.h"

BaseState::BaseState()
{
	BaseName = "Base";
}

BaseState::BaseState(int m_ID)
{
	m_ownerID = m_ID;
}

BaseState::~BaseState()
{
	m_ownerID = NULL;
}

ASM::ASM()
{
	currentState	= NULL;
	DelayState		= "None";
}

ASM::~ASM()
{
	statelist.clear();
	currentState	= NULL;
	DelayState		= "None";
}

void ASM::Update(double dt)
{
	//make sure our Entitiy has a state
	if(currentState == NULL) return;

	//check and see if we have a delay
	if(DelayState != "None")
	{
		Transition(DelayState);
		DelayState = "None";
	}

	//update the current state, may cause a transition
	currentState->Execute(dt);
}

//what do we do if we get a certian message
void ASM::HandleMsg(Mail message)
{
	if(currentState)
		currentState->HandleMsg(message);
}

//called to transition to another state
void ASM::Transition(string BaseName)
{
	//find the state
	BaseState* myState = NULL;
	for(auto i = statelist.begin(); i != statelist.end(); i++)
		if((*i)->BaseName == BaseName)
			myState = i->get();

	//Error, trying to transition to a non exsisting state
	if(myState == NULL)
	{
		//print error here....
		assert(myState != NULL);
	}

	currentState->Exit();
	myState->Enter();
	currentState = myState;
}

//Transition to another state(but theres a delay in time)
void ASM::Delay(string BaseName)
{
	DelayState = BaseName;
}

//add a state to the list, makin it the current state
void ASM::AddState(BaseState* newState, bool makeCurrent)
{
	//add this state to the ASM
	shared_ptr <BaseState> newStatePtr(newState);
	statelist.push_back(newStatePtr);
	//make this the new current state
	if(makeCurrent) currentState = newState;
}

//what is the name of the current state
string ASM::GetState()
{
	return currentState->BaseName;
}
#pragma once

#include "ASM.h"
#include <random>

#define AS AStar::Instance()

class AStar : public BaseState
{
private:
	static AStar* s_Instance;
	int numMsg1, numMsg2, numMsg3;

private:
	AStar();

public:
	static AStar* Instance();

public:
	AStar(ASM* fsm, int id);
	~AStar();

public:
	void Enter();					//enters into attack state
	void Execute(double dt);		//executes the code in attack state, steering forces will go here
	void Exit();					//will exit the attack state
	void HandleMsg(Mail message);	//handles msgs sent to the attack state
};
#pragma once
#include "DXUtil.h"
#include "GraphicsEntity.h"
class MainMenuBackground
{
	D3DXMATRIX m_ProjMat;
	vector<GraphicsEntity> Entities;
	LPD3DXEFFECT effect;
public:
	MainMenuBackground();
	~MainMenuBackground();
	void buildProjMat();
	void update(float dt);
	void render();
	void onLostDevice();
	void onResetDevice();
};


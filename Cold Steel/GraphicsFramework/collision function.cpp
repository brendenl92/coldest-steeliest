#include"collision function.h"
#include "RigidBody.h"
#include "Contact.h"
#include "MasterEntity.h"



V3 Cross(V3& Va, V3& Vb)
{
	V3 temp;
	D3DXVec3Cross(&temp, &Va, &Vb);
	return temp;
}


//projects half-sizes of an OBB to any axis
float transformToAxis(BoundingVolumeNode * box, V3 & projectionAxis)
{

	//Geting axis from OBB 
	V3 X_axis = box->mBody->GetAxis(0);
	V3 Y_axis = box->mBody->GetAxis(1);
	V3 Z_axis = box->mBody->GetAxis(2);

	//Getting HalfSize
	V3 halfsize = box->mVolume->mHalfsize;

	//return projection
	return	halfsize.x * abs(D3DXVec3Dot(&projectionAxis, &X_axis)) +
		halfsize.y * abs(D3DXVec3Dot(&projectionAxis, &Y_axis)) +
		halfsize.z * abs(D3DXVec3Dot(&projectionAxis, &Z_axis));
}


// projects half-sizes and separation vector of OBBs to any axis
bool overlapOnAxis(BoundingVolumeNode *one, BoundingVolumeNode *two, V3 &projectionAxis, V3 &toCenter)
{
	// Project the half-size of one onto axis
	double oneProject = transformToAxis(one, projectionAxis);
	double twoProject = transformToAxis(two, projectionAxis);
	// Project separating vector onto the projection axis
	double distance = abs(D3DXVec3Dot(&toCenter, &projectionAxis));
	// Check for overlap
	return (distance <= oneProject + twoProject);
}


/* This function checks if the two boxes overlap
* along the given axis, returning the ammount of overlap.
* The final parameter toCentre
* is used to pass in the vector between the boxes centre
* points, to avoid having to recalculate it each time.
*/
//static inline float penetrationOnAxis(BoundingVolumeNode *one, BoundingVolumeNode *two, V3 &axis, V3 &toCentre)
//{
//	// Project the half-size of one onto axis
//	float oneProject = transformToAxis(one, axis);
//	float twoProject = transformToAxis(two, axis);
//
//	// Project this onto the axis
//	float distance = abs(D3DXVec3Dot(&toCentre, &axis));
//
//	// Return the overlap (i.e. positive indicates
//	// overlap, negative indicates separation).
//	return oneProject + twoProject - distance;
//}


Contact* fillPointFaceBoxBox(BoundingVolumeNode* one, BoundingVolumeNode* two, V3 &toCentre, unsigned best, float pen)
{
	// This method is called when we know that a vertex from
	// box two is in contact with box one.

	// We know which axis the collision is on (i.e. best),
	// but we need to work out which of the two faces on
	// this axis.
	V3 normal = one->mBody->GetAxis(best);
	if (D3DXVec3Dot(&one->mBody->GetAxis(best), &toCentre) > 0)
	{
		normal = normal * -1.0f;
	}

	// Work out which vertex of box two we're colliding with.
	// Using toCentre doesn't work!
	V3 vertex = two->mVolume->mHalfsize;
	if (D3DXVec3Dot(&two->mBody->GetAxis(0), &normal) < 0) vertex.x = -vertex.x;
	if (D3DXVec3Dot(&two->mBody->GetAxis(1), &normal) < 0) vertex.y = -vertex.y;
	if (D3DXVec3Dot(&two->mBody->GetAxis(2), &normal) < 0) vertex.y = -vertex.y;

	// Create the contact m
	D3DXVECTOR4 poc;
	D3DXVec3Transform(&poc, &vertex, &two->mBody->Transform());
	V3 point = poc;
	Contact* contact = new Contact(one, two, point, normal, pen);

	return contact;
}


// This preprocessor definition is only used as a convenience
// in the boxAndBox contact generation method.
//#define CHECK_OVERLAP(axis, index) \
//if (!tryAxis(one, two, (axis), toCentre, (index), pen, best)) return 0;



//static inline bool tryAxis(BoundingVolumeNode *one, BoundingVolumeNode *two,
//	V3& axis, V3& toCentre, unsigned index, float& smallestPenetration, unsigned &smallestCase)
//{
//	// Make sure axis is normalized; and don't check almost parallel axes
//	if (D3DXVec3LengthSq(&axis) < 0.0001) return true;
//	D3DXVec3Normalize(&axis, &axis);
//	float penetration = penetrationOnAxis(one, two, axis, toCentre);
//	if (penetration < 0) return false;
//	if (penetration < smallestPenetration) {
//		smallestPenetration = penetration;
//		smallestCase = index;
//	}
//	return true;
//}




float GetM(V3 V, int dim)
{
	//Get vec x, or y, or z
	switch (dim)
	{
	case 0:
		return V.x;
	case 1:
		return V.y;
	case 2:
		return V.z;
	}

	return V.x;
}



void SetM(V3& V, int dim, float value)
{
	//Get vec x, or y, or z
	switch (dim)
	{
	case 0:
		V.x = value;
	case 1:
		V.y = value;
	case 2:
		V.z = value;
	}
}





//static inline V3 contactPoint(V3 &pOne, V3 &dOne, float oneSize, V3 &pTwo, V3 &dTwo, float twoSize,
//	// If this is true, and the contact point is outside
//	// the edge (in the case of an edge-face contact) then
//	// we use one's midpoint, otherwise we use two's.
//	bool useOne)
//{
//	V3 toSt, cOne, cTwo;
//	float dpStaOne, dpStaTwo, dpOneTwo, smOne, smTwo;
//	float denom, mua, mub;
//
//	smOne = D3DXVec3LengthSq(&dOne);
//	smTwo = D3DXVec3LengthSq(&dTwo);
//	dpOneTwo = D3DXVec3Dot(&dTwo, &dOne);
//
//	toSt = pOne - pTwo;
//	dpStaOne = D3DXVec3Dot(&dOne, &toSt);
//	dpStaTwo = D3DXVec3Dot(&dTwo, &toSt);
//
//	denom = smOne * smTwo - dpOneTwo * dpOneTwo;
//
//	// Zero denominator indicates parrallel lines
//	if (abs(denom) < 0.0001f) {
//		return useOne ? pOne : pTwo;
//	}
//
//	mua = (dpOneTwo * dpStaTwo - smTwo * dpStaOne) / denom;
//	mub = (smOne * dpStaTwo - dpOneTwo * dpStaOne) / denom;
//
//	// If either of the edges has the nearest point out
//	// of bounds, then the edges aren't crossed, we have
//	// an edge-face contact. Our point is on the edge, which
//	// we know from the useOne parameter.
//	if (mua > oneSize ||
//		mua < -oneSize ||
//		mub > twoSize ||
//		mub < -twoSize)
//	{
//		return useOne ? pOne : pTwo;
//	}
//	else
//	{
//		cOne = pOne + dOne * mua;
//		cTwo = pTwo + dTwo * mub;
//
//		return cOne * 0.5 + cTwo * 0.5;
//	}
//}


//Get Axis of rotation matrix
V3 GetAxisR(M4& RotM, int Col)
{
	V3 Axis;

	for (int i = 0; i < 3; i++)
		SetM(Axis, i, RotM.m[i][Col]);

	return Axis;
}



// Given point p, return point q on (or in) OBB b, closest to p
V3 ClosestPtPointOBB(V3 p, BoundingVolumeNode* b, V3 &q)
{
	V3 Bp = b->mBody->GetPosition();
	V3 Bh = b->mVolume->mHalfsize;
	V3 d = p - Bp;
	// Start result at center of box; make steps from there
	q = Bp;

	V3 axis[3];
	axis[0] = b->mBody->GetAxis(0);
	axis[1] = b->mBody->GetAxis(1);
	axis[2] = b->mBody->GetAxis(2);

	// For each OBB axis...
	for (int i = 0; i < 3; i++)
	{
		// project d onto that axis to get distance of d along that axis from the box center
		float dist = D3DXVec3Dot(&d, &axis[i]);
		// If distance farther than the box extents, clamp to the box
		if (dist > GetM(Bh, i))
			dist = GetM(Bh, i);
		if (dist < -GetM(Bh, i))
			dist = -GetM(Bh, i);
		// Step that distance along the axis to get world coordinate
		q += axis[i] * dist;
	}

	return q;
}

//Point of contact on Ray
bool POCRayToOBB(V3& Rp, V3& Rn, BoundingVolumeNode* rhs, V3& q)
{
	V3 Rpoc, Bpoc, Bp;
	Bp = rhs->mBody->GetPosition();

	//Closest point on Ray to the center of OBB
	float t = D3DXVec3Dot(&Bp, &Rn);
	if (t == 0)
		t = 1;
	Rpoc = (Rn*t)+Rp;

	//Closest point on OBB to closest point on OBB
	ClosestPtPointOBB(Rpoc, rhs, Bpoc);

	q = Rpoc;

	if (Rpoc == Bpoc)
		return true;
	else 
		return false;

}


//Clip to axis
void ClipToAxis(D3DXMATRIX& M)
{
	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			if (abs(M.m[i][j]) > .9f)
			{
				if (M.m[i][j] > 0)
					M.m[i][j] = 1;
				else
					M.m[i][j] = -1;
			}
			if (abs(M.m[i][j]) < .05f)
			{
				if (M.m[i][j] > 0)
					M.m[i][j] = 0;
			}
		}
	}
}


//Invert Z Axis
void InvertZAxis(D3DXMATRIX& RotM)
{
	for (unsigned int i = 0; i < 3; i++)
		RotM.m[i][2] *= -1;
}



//Create orthonormal basis based on a forward vector
void OrthnormBasFwdVec(M4& RotM, V3& Heading)
{
	static int CountL = 0;
	CountL++;
	
	V3 hed;
	float magSqr = D3DXVec3LengthSq(&Heading);
 	D3DXVec3Normalize(&hed, &Heading);

	if (abs(magSqr) > 0.001f)
	{
		V3 Ax, Ay, Az;

		Ax = hed;
		Ay = V3(0, 1, 0);

		Az = Cross(Ax, Ay);
		//Ay = Cross(Ax, Az);

		//seting rotation matrix
		for (int i = 0; i < 3; i++)
		{
			RotM.m[i][0] = GetM(Ax, i);
			RotM.m[i][1] = GetM(Ay, i);
			RotM.m[i][2] = GetM(Az, i);
		}

	/*	Az = hed;
		Ay = V3(0, 1, 0);

		Ax = Cross(Ay, Az);
		Ay = Cross(Ax, Az);

		for (int i = 0; i < 3; i++)
		{
			RotM.m[i][0] = GetM(Ax, i);
			RotM.m[i][1] = GetM(Ay, i);
			RotM.m[i][2] = GetM(Az, i);
		}*/

	}
	else
		Heading = V3(0,0,0);

	/*if (CountL >= 1000)
	{
 
		system("cls");

		cout << "\nVelocity" <<
			"\nX, Y, Z\n"
			<< Heading.x << ", " << Heading.y << ", " << Heading.z << endl;
			

		cout << "\nRotation" << 
			"\nX: ("    
			<< RotM.m[0][0] << ", " << RotM.m[1][0] << ", " << RotM.m[2][0] << ")\n"
			<< "Y: ("
			<< RotM.m[0][1] << ", " << RotM.m[1][1] << ", " << RotM.m[2][1] << ")\n"
			<< "Z: ("
			<< RotM.m[0][2] << ", " << RotM.m[2][1] << ", " << RotM.m[2][2] << ")\n";

		CountL = 0;
	}*/
}


//Sphere to OBB
bool SphereToOBBDetect(BoundingVolumeNode* S, BoundingVolumeNode* C, D3DXVECTOR3 lhsPosition)
{
	D3DXVECTOR3 q;

	//find closest point on OBB to sphere
	ClosestPtPointOBB(lhsPosition, C, q);

	// Sphere and AABB intersect if the (squared) distance from sphere
	//center to
	// point p is less than the (squared) sphere radius
	D3DXVECTOR3 v = q - lhsPosition;
	return  D3DXVec3Dot(&v, &v) <= (S->mVolume->mRadius * S->mVolume->mRadius);
}

//Generate Contact
Contact* SphereToOBBGenerate(BoundingVolumeNode* S, BoundingVolumeNode* B, D3DXVECTOR3 lhsPosition)
{
	// Transform the centre of the sphere into box coordinates
	D3DXVECTOR3 center = lhsPosition;
	D3DXMATRIX ITM;
	D3DXMatrixInverse(&ITM, 0, &B->mBody->Master()->getTrans(0));
	D3DXVECTOR3 relCenter;
	D3DXVec3TransformCoord(&relCenter, &center, &ITM);

	D3DXVECTOR3 closestPt(0, 0, 0);
	float dist;

	// Clamp each coordinate to the box.
	dist = relCenter.x;
	if (dist > B->mVolume->mHalfsize.x) dist = B->mVolume->mHalfsize.x;
	if (dist < -B->mVolume->mHalfsize.x) dist = -B->mVolume->mHalfsize.x;
	closestPt.x = dist;

	dist = relCenter.y;
	if (dist > B->mVolume->mHalfsize.y) dist = B->mVolume->mHalfsize.y;
	if (dist < -B->mVolume->mHalfsize.y) dist = -B->mVolume->mHalfsize.y;
	closestPt.y = dist;

	dist = relCenter.z;
	if (dist > B->mVolume->mHalfsize.z) dist = B->mVolume->mHalfsize.z;
	if (dist < -B->mVolume->mHalfsize.z) dist = -B->mVolume->mHalfsize.z;
	closestPt.z = dist;

	// Check we're in contact
	/*dist = D3DXVec3LengthSq(&(closestPt - relCenter));
	if (dist > S->mVolume->mRadius * S->mVolume->mRadius) return 0;*/

	// Compile the contact
	D3DXVECTOR3 closestPtWorld;
	D3DXVec3TransformCoord(&closestPtWorld, &closestPt, &B->mBody->Master()->getTrans(0));


	D3DXVECTOR3 contactNormal = (closestPtWorld - center);
	D3DXVec3Normalize(&contactNormal, &contactNormal);
	D3DXVECTOR3 contactPoint = closestPtWorld;
	float penetration = S->mVolume->mRadius - sqrt(dist);
	//contact->SetBodyData(sphere, box, .95);

	return new Contact(S, B, contactPoint, contactNormal, penetration);
}

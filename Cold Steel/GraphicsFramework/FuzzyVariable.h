#pragma once

#include "FuzzyTerm.h"
#include "FuzzySet.h"
#include <string>
#include <map> 

using namespace std;

///So when looking at the map in the book, on how Mat decided to build his
///Fuzzy Module, he shows that m_membersets is a list of sets
class FuzzyVariable
{
private:
	typedef map< string, FuzzySet* > MemberSets;

private:
	////no copies
	//FuzzyVariable(const FuzzyVariable&);
	//FuzzyVariable& operator= (const FuzzyVariable&);

private:
	//map of the fuzzy sets that make up this variable
	MemberSets	m_MemberSets;

	//the min and max value of the range of this variable
	double		m_Min;
	double		m_Max;

	//adjust the range, when a new set is added accordingly
	void		Adjust(double min, double max)
	{
		m_Min	= min;
		m_Max	= max;
	}

	/* In Programming Game AI by Example, By: Mat BuckLand. He suggest that when a 
	clien retrieves a reference to a fuzzy variable when an instance is created 
	via FuzzyModule::CreateFLV(), To prevent the client from deleting the instance the
	Fuzzy Variable destructor is made private and the FuzzyModule class made a friend.
	chapter 10, pg. 443*/
	~FuzzyVariable();

	friend class FuzzyModule;

public:
	FuzzyVariable()
	{
		m_Min	= 0.0;
		m_Max	= 0.0;
	}

	//creates sets and then adds them to the map
	FzSet addTriangularSet(string name, double peak, double min, double max);
	FzSet addLeftShoulderSet(string name, double peak, double min, double max);
	FzSet addRightShoulderSet(string name, double peak, double min, double max);

	//fuzzify a value by calculating the DOM
	void	Fuzzify(double val);

	//defuzzify variable
	double	DefuzzifyMaxAV() const;
};
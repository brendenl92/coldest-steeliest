#pragma once

#include "MovieState.h"
#include "MenuState.h"
#include "GameState.h"
#include "CreditsState.h"
#include "TankSelectBack.h"
#include "TankSelectionState.h"

class FSM
{
	State* m_CurrentState;
	State* m_LastState;

public:
	FSM(void);
	~FSM(void);

	void setCurrentState(State* state);
	void setLastState(State* state);

	void changeState(State* state);
	void revertStates();

	void update(float dt);

	void handleInput(RAWINPUT* raw);

	bool isInState(State* state);

	State* getState();

};


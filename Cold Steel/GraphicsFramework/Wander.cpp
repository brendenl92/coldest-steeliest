//Wander.cpp
#include "Wander.h"
#include "BaseAIEntity.h"
#include "AIManager.h"

Wander* Wander::s_Instance = 0;
Wander* Wander::Instance()
{
	if(!s_Instance)
		s_Instance = new Wander;
	return s_Instance;
}

Wander::Wander() 
	: BaseState()
{
	BaseName	= "WANDER";
	numMsg1		= 0;
	numMsg2		= 0;
	numMsg3		= 0;
}

Wander::Wander(ASM* fStatem, int id)
	: BaseState(fStatem)
{
	BaseName	= "WANDER";
	m_ownerID	= id;
	numMsg1		= 0;
	numMsg2		= 0;
	numMsg3		= 0;
}

Wander::~Wander()
{
}

void Wander::Enter()
{
}

void Wander::Execute(double dt)
{
	
}

void Wander::Exit()
{

}

void Wander::HandleMsg(Mail message)
{
	switch (message.m_Msg)
	{
	case _OnPatrol:
		if (AMI->GetBaseAIEntity()[message.m_Reciever]->Enemy == NULL)
		{
			m_fsm->Transition("WANDER");
			AMI->GetBaseAIEntity()[message.m_Reciever]->SetState(1);
		}
		if (AMI->GetBaseAIEntity()[message.m_Reciever]->GetCurrentHP() > 50)
		{
			m_fsm->Transition("ATTACK");
			//MD->SendToState(message.m_Reciever, message.m_Reciever, 0, _LockedOn, (void*)0);
			AMI->GetBaseAIEntity()[message.m_Reciever]->SetState(2);
		}
		else if (AMI->GetBaseAIEntity()[message.m_Reciever]->GetCurrentHP() < 50)
		{
			m_fsm->Transition("FLEE");
			MD->SendToState(message.m_Reciever, message.m_Reciever, 0, _FallingBack, (void*)0);
			AMI->GetBaseAIEntity()[message.m_Reciever]->SetState(3);
		}
	case _LockedOn:
	{
		if (AMI->GetBaseAIEntity()[message.m_Reciever]->GetCurrentHP() > 50)
		{
			m_fsm->Transition("ATTACK");
			//MD->SendToState(message.m_Reciever, message.m_Reciever, 0, _LockedOn, (void*)0);
			AMI->GetBaseAIEntity()[message.m_Reciever]->SetState(2);
		}
		break;
	}
	case _FallingBack:
		{
			m_fsm->Transition("FLEE");
			//MD->SendToState(message.m_Reciever, message.m_Reciever, 0, _FallingBack, (void*)0);
			AMI->GetBaseAIEntity()[message.m_Reciever]->SetState(3);
			break;
		}
	}
}

void Wander::ShutDown()
{
	s_Instance = NULL;
}
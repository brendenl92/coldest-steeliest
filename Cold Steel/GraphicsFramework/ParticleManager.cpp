#include "ParticleManager.h"

ParticleM *ParticleM::Instance()
{
	static ParticleM instance;
	return &instance;
}

ParticleM::ParticleM()
{
	mParticleSystems.clear();
	mMaxParts = 100;
	mChecked = false;

	//mExcludedDraw = 3500;

	mEffectID = 0;
	D3DXMatrixIdentity(&mRotation);
	//particlePos = D3DXVECTOR3(0, 0, 0);
	//bOnce = false;
}
//
//ParticleM::ParticleM(string EffectName)
//{
//	mParticleSystems.clear();
//	mMaxParts = 100;
//	mChecked = false;
//
//	//particlePos = D3DXVECTOR3(0, 0, 0);
//	ImportParticle(EffectName);
//}
ParticleM::~ParticleM()
{
	for (UINT i = 0; i < mParticleSystems.size(); i++)
	{
		delete mParticleSystems[i];
	}

	for (UINT i = 0; i < mFireEmitters.size(); i++)
	{
		delete mFireEmitters[i];
	}

	mParticleSystems.clear();
	mFireEmitters.clear();
}

void ParticleM::CreateFire(D3DXVECTOR3 pos, float scale)
{
	FireEmitter* peFire = new FireEmitter;

	// Initialize the particle system.
	D3DXMATRIX peWorld;
	D3DXMatrixIdentity(&peWorld);

	peFire->SetPosition(pos);

	peFire->setScale(scale);

	peFire->setWorldMtx(peWorld);
		
	mFireEmitters.push_back(peFire);
}

void ParticleM::CreateRain(int maxPart, float delay, bool restart)
{
	ParticleEmitter*  peRain;

	// Initialize the particle system.
	D3DXMATRIX peWorld;
	D3DXMatrixIdentity(&peWorld);

	// Rain always visible, so make infinitely huge bounding box
	// so that it is always seen.
	AABB psysBox;
	psysBox.maxPt = D3DXVECTOR3(FLT_MAX, FLT_MAX, FLT_MAX);
	psysBox.minPt = D3DXVECTOR3(-FLT_MAX, -FLT_MAX, -FLT_MAX);

	// Accelerate due to wind and gravity.
	peRain = new Rain("../shaders/rain.fx", "RainTech", "../media/images/raindrop.dds",
		D3DXVECTOR3(-1.0f, -9.8f, 0.0f), psysBox, maxPart, delay);
	peRain->setWorldMtx(peWorld);

	peRain->SetRestart(restart);

	peRain->setParticleID(mEffectID);
	mEffectID++;

	mParticleSystems.push_back(peRain);
}

void ParticleM::CreateSnow(int maxPart, float delay, bool restart)
{
	ParticleEmitter*  peSnow;

	// Initialize the particle system.
	D3DXMATRIX peWorld;
	D3DXMatrixIdentity(&peWorld);

	// Rain always visible, so make infinitely huge bounding box
	// so that it is always seen.
	AABB psysBox;
	psysBox.maxPt = D3DXVECTOR3(FLT_MAX, FLT_MAX, FLT_MAX);
	psysBox.minPt = D3DXVECTOR3(-FLT_MAX, -FLT_MAX, -FLT_MAX);

	// Accelerate due to wind and gravity.
	peSnow = new Snow("../shaders/snow.fx", "SnowTech", "../media/images/snowball.bmp",
		D3DXVECTOR3(2.0f, -9.8f, 0.0f), psysBox, maxPart, delay);
	peSnow->setWorldMtx(peWorld);

	peSnow->setParticleID(mEffectID);
	mEffectID++;
	peSnow->SetRestart(restart);

	mParticleSystems.push_back(peSnow);
}

void ParticleM::CreateSprinkler(int maxPart, float delay, bool restart, float startDelay)
{
	ParticleEmitter*  peSprinkler;

	// Initialize the particle system.
	D3DXMATRIX peWorld;
	D3DXMatrixIdentity(&peWorld);
	//D3DXMatrixTranslation(&psysWorld, 
	//55.0f, 40.f, 55.0f);

	// Don't cull, but in practice you'd want to figure out a
	// bounding box based on the settings of the system.
	AABB psysBox;
	psysBox.maxPt = D3DXVECTOR3(FLT_MAX, FLT_MAX, FLT_MAX);
	psysBox.minPt = D3DXVECTOR3(-FLT_MAX, -FLT_MAX, -FLT_MAX);

	// Accelerate due to wind and gravity.
	peSprinkler = new Sprinkler("../shaders/sprinkler.fx", "SprinklerTech", "../media/images/bolt.dds",
		D3DXVECTOR3(-3.0f, -9.8f, 0.0f), psysBox, maxPart, delay);

	peSprinkler->setWorldMtx(peWorld);
	peSprinkler->SetPosition(D3DXVECTOR3(55.0f, 40.0f, 55.0f));
	peSprinkler->SetStartDelay(startDelay);

	peSprinkler->setParticleID(mEffectID);
	peSprinkler->SetRestart(restart);
	peSprinkler->setRotationMtx(mRotation);

	mParticleSystems.push_back(peSprinkler);
}

void ParticleM::CreateSprinkler(int maxPart, float delay, bool restart, D3DXVECTOR3 pos, D3DXVECTOR3 life, D3DXVECTOR3 size, D3DXVECTOR3 VelocityX, D3DXVECTOR3 VelocityY, D3DXVECTOR3 VelocityZ, float startDelay, int EmitType, D3DXVECTOR3 offset)
{
	ParticleEmitter*  peSprinkler;

	// Initialize the particle system.
	D3DXMATRIX peWorld;
	D3DXMatrixIdentity(&peWorld);

	//D3DXMatrixTranslation(&psysWorld, 
	//55.0f, 40.f, 55.0f);

	// Don't cull, but in practice you'd want to figure out a
	// bounding box based on the settings of the system.
	AABB psysBox;
	psysBox.maxPt = D3DXVECTOR3(FLT_MAX, FLT_MAX, FLT_MAX);
	psysBox.minPt = D3DXVECTOR3(-FLT_MAX, -FLT_MAX, -FLT_MAX);

	// Accelerate due to wind and gravity.
	peSprinkler = new Sprinkler("../shaders/sprinkler.fx", "SprinklerTech", "../media/images/bolt.dds",
		D3DXVECTOR3(-3.0f, -9.8f, 0.0f), psysBox, maxPart, delay);

	peSprinkler->SetPosition(pos);
	peSprinkler->SetDelay(delay);
	peSprinkler->SetLifeMin(life.x);
	peSprinkler->SetLifeMax(life.y);
	peSprinkler->SetMinimumMaxSizes(size.x, size.y);
	peSprinkler->SetVelX(VelocityX);
	peSprinkler->SetVelY(VelocityY);
	peSprinkler->SetVelZ(VelocityZ);
	peSprinkler->SetStartDelay(startDelay);
	peSprinkler->SetEmitterType(EmitType);

	peSprinkler->setParticleID(mEffectID);
	peSprinkler->setWorldMtx(peWorld);

	peSprinkler->setOffset(offset);
	peSprinkler->SetRestart(restart);

	peSprinkler->setRotationMtx(mRotation);

	mParticleSystems.push_back(peSprinkler);
}

void ParticleM::CreateSmoke(int maxPart, float delay, bool restart, float startDelay)
{
	ParticleEmitter*  peSmoke;

	// Initialize the particle system.
	D3DXMATRIX peWorld;
	D3DXMatrixIdentity(&peWorld);

	// Don't cull, but in practice you'd want to figure out a
	// bounding box based on the settings of the system.
	AABB psysBox;
	psysBox.maxPt = D3DXVECTOR3(FLT_MAX, FLT_MAX, FLT_MAX);
	psysBox.minPt = D3DXVECTOR3(-FLT_MAX, -FLT_MAX, -FLT_MAX);

	// Accelerate due to wind and gravity.
	peSmoke = new Smoke("../shaders/smoke.fx", "SmokeTech", "../media/images/smoke.dds",
		D3DXVECTOR3(GetRandomFloat(0.0f, 0.0f), 0.f, 0.0f), psysBox, maxPart, delay);

	peSmoke->setWorldMtx(peWorld);
	peSmoke->SetPosition(D3DXVECTOR3(45.0f, 40.0f, 55.0f));
	peSmoke->SetStartDelay(startDelay);

	peSmoke->setParticleID(mEffectID);
	peSmoke->SetRestart(restart);
	peSmoke->setRotationMtx(mRotation);

	mParticleSystems.push_back(peSmoke);
}
void ParticleM::CreateSmoke(int maxPart, float delay, bool restart, D3DXVECTOR3 pos, D3DXVECTOR3 life, D3DXVECTOR3 size, D3DXVECTOR3 VelocityX, D3DXVECTOR3 VelocityY, D3DXVECTOR3 VelocityZ, float startDelay, int EmitType, D3DXVECTOR3 offset)
{
	ParticleEmitter*  peSmoke;

	// Initialize the particle system.
	D3DXMATRIX peWorld;
	D3DXMatrixIdentity(&peWorld);

	// Don't cull, but in practice you'd want to figure out a
	// bounding box based on the settings of the system.
	AABB psysBox;
	psysBox.maxPt = D3DXVECTOR3(FLT_MAX, FLT_MAX, FLT_MAX);
	psysBox.minPt = D3DXVECTOR3(-FLT_MAX, -FLT_MAX, -FLT_MAX);

	// Accelerate due to wind and gravity.
	peSmoke = new Smoke("../shaders/smoke.fx", "SmokeTech", "../media/images/smoke.dds",
		D3DXVECTOR3(GetRandomFloat(0.0f, 0.0f), 0.f, 0.0f), psysBox, maxPart, delay);

	peSmoke->SetPosition(pos);
	peSmoke->SetDelay(delay);
	peSmoke->SetLifeMin(life.x);
	peSmoke->SetLifeMax(life.y);
	peSmoke->SetMinimumMaxSizes(size.x, size.y);
	peSmoke->SetVelX(VelocityX);
	peSmoke->SetVelY(VelocityY);
	peSmoke->SetVelZ(VelocityZ);
	peSmoke->SetStartDelay(startDelay);
	peSmoke->SetEmitterType(EmitType);

	peSmoke->setParticleID(mEffectID);
	peSmoke->setWorldMtx(peWorld);

	peSmoke->setOffset(offset);
	peSmoke->SetRestart(restart);
	peSmoke->setRotationMtx(mRotation);

	mParticleSystems.push_back(peSmoke);
}

void ParticleM::CreateShockParticles(int maxPart, float delay, bool restart, float startDelay)
{
	ParticleEmitter*  peShock;

	// Initialize the particle system.
	D3DXMATRIX peWorld;
	D3DXMatrixIdentity(&peWorld);

	// Don't cull, but in practice you'd want to figure out a
	// bounding box based on the settings of the system.
	AABB psysBox;
	psysBox.maxPt = D3DXVECTOR3(FLT_MAX, FLT_MAX, FLT_MAX);
	psysBox.minPt = D3DXVECTOR3(-FLT_MAX, -FLT_MAX, -FLT_MAX);

	// Accelerate due to wind and gravity.
	peShock = new RockParticles("../shaders/sprinkler.fx", "SprinklerTech", "../media/images/ElectricShock.dds",
		D3DXVECTOR3(GetRandomFloat(0.0f, 0.0f), 0.f, 0.0f), psysBox, maxPart, delay);

	peShock->setWorldMtx(peWorld);
	peShock->SetPosition(D3DXVECTOR3(45.0f, 40.0f, 55.0f));
	peShock->SetStartDelay(startDelay);

	peShock->setType(8);

	peShock->setParticleID(mEffectID);
	peShock->SetRestart(restart);
	peShock->setRotationMtx(mRotation);

	mParticleSystems.push_back(peShock);
}
void ParticleM::CreateShockParticles(int maxPart, float delay, bool restart, D3DXVECTOR3 pos, D3DXVECTOR3 life, D3DXVECTOR3 size, D3DXVECTOR3 VelocityX, D3DXVECTOR3 VelocityY, D3DXVECTOR3 VelocityZ, float startDelay, int EmitType, D3DXVECTOR3 offset)
{
	ParticleEmitter*  peShock;

	// Initialize the particle system.
	D3DXMATRIX peWorld;
	D3DXMatrixIdentity(&peWorld);

	// Don't cull, but in practice you'd want to figure out a
	// bounding box based on the settings of the system.
	AABB psysBox;
	psysBox.maxPt = D3DXVECTOR3(FLT_MAX, FLT_MAX, FLT_MAX);
	psysBox.minPt = D3DXVECTOR3(-FLT_MAX, -FLT_MAX, -FLT_MAX);

	// Accelerate due to wind and gravity.
	peShock = new RockParticles("../shaders/sprinkler.fx", "SprinklerTech", "../media/images/ElectricShock.dds",
		D3DXVECTOR3(GetRandomFloat(0.0f, 2.0f), 0.f, 0.0f), psysBox, maxPart, delay);

	peShock->SetPosition(pos);
	peShock->SetDelay(delay);
	peShock->SetLifeMin(life.x);
	peShock->SetLifeMax(life.y);
	peShock->SetMinimumMaxSizes(size.x, size.y);
	peShock->SetVelX(VelocityX);
	peShock->SetVelY(VelocityY);
	peShock->SetVelZ(VelocityZ);
	peShock->SetStartDelay(startDelay);
	peShock->SetEmitterType(EmitType);

	peShock->setWorldMtx(peWorld);

	peShock->setParticleID(mEffectID);
	peShock->SetRestart(restart);
	peShock->setOffset(offset);
	peShock->setRotationMtx(mRotation);

	mParticleSystems.push_back(peShock);
}

void ParticleM::CreateRockParticles(int maxPart, float delay, bool restart, float startDelay)
{
	ParticleEmitter*  peSmoke;

	// Initialize the particle system.
	D3DXMATRIX peWorld;
	D3DXMatrixIdentity(&peWorld);

	// Don't cull, but in practice you'd want to figure out a
	// bounding box based on the settings of the system.
	AABB psysBox;
	psysBox.maxPt = D3DXVECTOR3(FLT_MAX, FLT_MAX, FLT_MAX);
	psysBox.minPt = D3DXVECTOR3(-FLT_MAX, -FLT_MAX, -FLT_MAX);

	// Accelerate due to wind and gravity.
	peSmoke = new RockParticles("../shaders/sprinkler.fx", "SprinklerTech", "../media/images/RockParticles.dds",
		D3DXVECTOR3(GetRandomFloat(0.0f, 0.0f), 0.f, 0.0f), psysBox, maxPart, delay);

	peSmoke->setWorldMtx(peWorld);
	peSmoke->SetPosition(D3DXVECTOR3(45.0f, 40.0f, 55.0f));
	peSmoke->SetStartDelay(startDelay);

	peSmoke->setParticleID(mEffectID);
	peSmoke->SetRestart(restart);
	peSmoke->setRotationMtx(mRotation);

	mParticleSystems.push_back(peSmoke);
}
void ParticleM::CreateRockParticles(int maxPart, float delay, bool restart, D3DXVECTOR3 pos, D3DXVECTOR3 life, D3DXVECTOR3 size, D3DXVECTOR3 VelocityX, D3DXVECTOR3 VelocityY, D3DXVECTOR3 VelocityZ, float startDelay, int EmitType, D3DXVECTOR3 offset)
{
	ParticleEmitter*  peSmoke;

	// Initialize the particle system.
	D3DXMATRIX peWorld;
	D3DXMatrixIdentity(&peWorld);

	// Don't cull, but in practice you'd want to figure out a
	// bounding box based on the settings of the system.
	AABB psysBox;
	psysBox.maxPt = D3DXVECTOR3(FLT_MAX, FLT_MAX, FLT_MAX);
	psysBox.minPt = D3DXVECTOR3(-FLT_MAX, -FLT_MAX, -FLT_MAX);

	// Accelerate due to wind and gravity.
	peSmoke = new RockParticles("../shaders/sprinkler.fx", "SprinklerTech", "../media/images/RockParticles.dds",
		D3DXVECTOR3(GetRandomFloat(0.0f, 2.0f), 0.f, 0.0f), psysBox, maxPart, delay);

	peSmoke->SetPosition(pos);
	peSmoke->SetDelay(delay);
	peSmoke->SetLifeMin(life.x);
	peSmoke->SetLifeMax(life.y);
	peSmoke->SetMinimumMaxSizes(size.x, size.y);
	peSmoke->SetVelX(VelocityX);
	peSmoke->SetVelY(VelocityY);
	peSmoke->SetVelZ(VelocityZ);
	peSmoke->SetStartDelay(startDelay);
	peSmoke->SetEmitterType(EmitType);

	peSmoke->setWorldMtx(peWorld);

	peSmoke->setParticleID(mEffectID);
	peSmoke->SetRestart(restart);
	peSmoke->setOffset(offset);
	peSmoke->setRotationMtx(mRotation);

	mParticleSystems.push_back(peSmoke);
}

void ParticleM::CreateDirtCloud(int maxPart, float delay, bool restart, float startDelay)
{
	ParticleEmitter*  peDirtCloud;

	// Initialize the particle system.
	D3DXMATRIX peWorld;
	D3DXMatrixIdentity(&peWorld);

	// Don't cull, but in practice you'd want to figure out a
	// bounding box based on the settings of the system.
	AABB psysBox;
	psysBox.maxPt = D3DXVECTOR3(FLT_MAX, FLT_MAX, FLT_MAX);
	psysBox.minPt = D3DXVECTOR3(-FLT_MAX, -FLT_MAX, -FLT_MAX);

	// Accelerate due to wind and gravity.
	peDirtCloud = new Smoke("../shaders/DirtCloud.fx", "DirtCloudTech", "../media/images/dirtCloud.dds",//"RockParticles.png", 
		D3DXVECTOR3(0.0f, 0.0f, 0.0f), psysBox, maxPart, delay);

	peDirtCloud->setWorldMtx(peWorld);
	peDirtCloud->SetPosition(D3DXVECTOR3(45.0f, 40.0f, 55.0f));

	peDirtCloud->setParticleID(mEffectID);
	peDirtCloud->SetRestart(restart);
	peDirtCloud->setType(6);
	peDirtCloud->SetVelZMax(4);
	peDirtCloud->SetVelZMin(-4);
	peDirtCloud->SetStartDelay(startDelay);
	peDirtCloud->setRotationMtx(mRotation);

	mParticleSystems.push_back(peDirtCloud);
}

void ParticleM::CreateDirtCloud(int maxPart, float delay, bool restart, D3DXVECTOR3 pos, D3DXVECTOR3 life, D3DXVECTOR3 size, D3DXVECTOR3 VelocityX, D3DXVECTOR3 VelocityY, D3DXVECTOR3 VelocityZ, float startDelay, int EmitType, D3DXVECTOR3 offset)
{
	ParticleEmitter*  peDirtCloud;

	// Initialize the particle system.
	D3DXMATRIX peWorld;
	D3DXMatrixIdentity(&peWorld);

	// Don't cull, but in practice you'd want to figure out a
	// bounding box based on the settings of the system.
	AABB psysBox;
	psysBox.maxPt = D3DXVECTOR3(FLT_MAX, FLT_MAX, FLT_MAX);
	psysBox.minPt = D3DXVECTOR3(-FLT_MAX, -FLT_MAX, -FLT_MAX);

	// Accelerate due to wind and gravity.
	peDirtCloud = new Smoke("../shaders/DirtCloud.fx", "DirtCloudTech", "../media/images/dirtCloud.dds",
		D3DXVECTOR3(GetRandomFloat(0.0f, 0.0f), 0.f, 0.0f), psysBox, maxPart, delay);

	peDirtCloud->SetPosition(pos);
	peDirtCloud->SetDelay(delay);
	peDirtCloud->SetLifeMin(life.x);
	peDirtCloud->SetLifeMax(life.y);
	peDirtCloud->SetMinimumMaxSizes(size.x, size.y);
	peDirtCloud->SetVelX(VelocityX);
	peDirtCloud->SetVelY(VelocityY);
	peDirtCloud->SetVelZ(VelocityZ);
	peDirtCloud->SetVelZMax(4);
	peDirtCloud->SetVelZMin(-4);
	peDirtCloud->SetStartDelay(startDelay);

	peDirtCloud->setType(6);
	
	peDirtCloud->SetEmitterType(EmitType);

	peDirtCloud->setWorldMtx(peWorld);
	peDirtCloud->setOffset(offset);

	peDirtCloud->setParticleID(mEffectID);
	peDirtCloud->SetRestart(restart);
	peDirtCloud->setRotationMtx(mRotation);

	mParticleSystems.push_back(peDirtCloud);
}

void ParticleM::CreateAirCloud(int maxPart, float delay, bool restart, float startDelay)
{
	ParticleEmitter*  peDirtCloud;

	// Initialize the particle system.
	D3DXMATRIX peWorld;
	D3DXMatrixIdentity(&peWorld);

	// Don't cull, but in practice you'd want to figure out a
	// bounding box based on the settings of the system.
	AABB psysBox;
	psysBox.maxPt = D3DXVECTOR3(FLT_MAX, FLT_MAX, FLT_MAX);
	psysBox.minPt = D3DXVECTOR3(-FLT_MAX, -FLT_MAX, -FLT_MAX);

	// Accelerate due to wind and gravity.
	peDirtCloud = new Smoke("../shaders/DirtCloud.fx", "DirtCloudTech", "../media/images/ElectricPush.dds",
		D3DXVECTOR3(0.0f, 0.0f, 0.0f), psysBox, maxPart, delay);

	peDirtCloud->setWorldMtx(peWorld);

	peDirtCloud->SetPosition(D3DXVECTOR3(45.0f, 40.0f, 55.0f));
	peDirtCloud->SetRestart(restart);
	peDirtCloud->setType(6);
	peDirtCloud->SetVelZMax(4);
	peDirtCloud->SetVelZMin(-4);
	peDirtCloud->SetStartDelay(startDelay);
	peDirtCloud->SetEmitterType(2);
	peDirtCloud->setRotationMtx(mRotation);
	peDirtCloud->setParticleID(mEffectID);

	mParticleSystems.push_back(peDirtCloud);
}

void ParticleM::CreateAirCloud(int maxPart, float delay, bool restart, D3DXVECTOR3 pos, D3DXVECTOR3 life, D3DXVECTOR3 size, D3DXVECTOR3 VelocityX, D3DXVECTOR3 VelocityY, D3DXVECTOR3 VelocityZ, float startDelay, int EmitType, D3DXVECTOR3 offset)
{
	ParticleEmitter*  peDirtCloud;

	// Initialize the particle system.
	D3DXMATRIX peWorld;
	D3DXMatrixIdentity(&peWorld);

	// Don't cull, but in practice you'd want to figure out a
	// bounding box based on the settings of the system.
	AABB psysBox;
	psysBox.maxPt = D3DXVECTOR3(FLT_MAX, FLT_MAX, FLT_MAX);
	psysBox.minPt = D3DXVECTOR3(-FLT_MAX, -FLT_MAX, -FLT_MAX);

	// Accelerate due to wind and gravity.
	peDirtCloud = new Smoke("../shaders/AirEffect.fx", "DirtCloudTech", "../media/images/ElectricPush.dds",
		D3DXVECTOR3(0.0f, 0.f, 0.0f), psysBox, maxPart, delay);

	peDirtCloud->SetPosition(pos);
	peDirtCloud->SetDelay(delay);
	peDirtCloud->SetLifeMin(life.x);
	peDirtCloud->SetLifeMax(life.y);
	peDirtCloud->SetMinimumMaxSizes(size.x, size.y);
	peDirtCloud->SetVelX(VelocityX);
	peDirtCloud->SetVelY(VelocityY);
	peDirtCloud->SetVelZMax(4);
	peDirtCloud->SetVelZMin(-4);
	peDirtCloud->SetStartDelay(startDelay);

	peDirtCloud->setType(6);

	peDirtCloud->setWorldMtx(peWorld);

	peDirtCloud->setParticleID(mEffectID);
	peDirtCloud->SetRestart(restart);
	peDirtCloud->SetEmitterType(EmitType);
	peDirtCloud->setRotationMtx(mRotation);
	peDirtCloud->setOffset(offset);

	mParticleSystems.push_back(peDirtCloud);
}

void ParticleM::CreateExplosion(int maxPart, float delay, bool restart, float startDelay)
{
	ParticleEmitter*  peExplosion;

	// Initialize the particle system.
	D3DXMATRIX peWorld;
	D3DXMatrixIdentity(&peWorld);

	// Don't cull, but in practice you'd want to figure out a
	// bounding box based on the settings of the system.
	AABB psysBox;
	psysBox.maxPt = D3DXVECTOR3(FLT_MAX, FLT_MAX, FLT_MAX);
	psysBox.minPt = D3DXVECTOR3(-FLT_MAX, -FLT_MAX, -FLT_MAX);

	// Accelerate due to wind and gravity.
	peExplosion = new Explosion("../shaders/explosion.fx", "ExplosionTech", "../media/images/explosion.png",
		D3DXVECTOR3(0.0f, 0.0f, 0.0f), psysBox, maxPart, delay);

	peExplosion->setWorldMtx(peWorld);
	peExplosion->SetPosition(D3DXVECTOR3(55.0f, 50.0f, 55.0f));

	peExplosion->SetRestart(restart);
	peExplosion->SetStartDelay(startDelay);
	peExplosion->setParticleID(mEffectID);
	peExplosion->setRotationMtx(mRotation);

	mParticleSystems.push_back(peExplosion);
}

void ParticleM::CreateExplosion(int maxPart, float delay, bool restart, D3DXVECTOR3 pos, D3DXVECTOR3 life, D3DXVECTOR3 size, D3DXVECTOR3 VelocityX, D3DXVECTOR3 VelocityY, D3DXVECTOR3 VelocityZ, float startDelay, int EmitType, D3DXVECTOR3 offset)
{
	ParticleEmitter*  peExplosion;

	// Initialize the particle system.
	D3DXMATRIX peWorld;
	D3DXMatrixIdentity(&peWorld);

	// Don't cull, but in practice you'd want to figure out a
	// bounding box based on the settings of the system.
	AABB psysBox;
	psysBox.maxPt = D3DXVECTOR3(FLT_MAX, FLT_MAX, FLT_MAX);
	psysBox.minPt = D3DXVECTOR3(-FLT_MAX, -FLT_MAX, -FLT_MAX);

	// Accelerate due to wind and gravity.
	peExplosion = new Explosion("../shaders/explosion.fx", "ExplosionTech", "../media/images/explosion.png",
		D3DXVECTOR3(0.0f, 0.0f, 0.0f), psysBox, maxPart, delay);

	peExplosion->SetPosition(pos);
	peExplosion->SetDelay(delay);
	peExplosion->SetLifeMin(life.x);
	peExplosion->SetLifeMax(life.y);
	peExplosion->SetMinimumMaxSizes(size.x, size.y);
	peExplosion->SetVelX(VelocityX);
	peExplosion->SetVelY(VelocityY);
	peExplosion->SetVelZ(VelocityZ);
	peExplosion->SetStartDelay(startDelay);
	peExplosion->SetEmitterType(EmitType);

	peExplosion->setWorldMtx(peWorld);
	peExplosion->setOffset(offset);

	peExplosion->SetRestart(restart);
	peExplosion->setParticleID(mEffectID);
	peExplosion->setRotationMtx(mRotation);

	mParticleSystems.push_back(peExplosion);
}

void ParticleM::draw()
{
	bool bDraw = true;
	for (UINT i = 0; i < mParticleSystems.size(); i++)
	{
		bDraw = true;
		for (UINT j = 0; j < mExcludedDraw.size(); j++)
		{
			if (mExcludedDraw[j] == mParticleSystems[i]->getParticleID())
			{
				bDraw = false;
			}
		}
		if ( bDraw == true)
			mParticleSystems[i]->draw();
	}

	for (UINT i = 0; i < mFireEmitters.size(); i++)
	{
		mFireEmitters[i]->draw();
	}
}

void ParticleM::update(float dt)
{
	//Update all Particle Emitters
	for (UINT i = 0; i < mParticleSystems.size(); i++)
	{
		mParticleSystems[i]->update(dt);
	}
	//Update all Fire Emitters
	for (UINT i = 0; i < mFireEmitters.size(); i++)
	{
		mFireEmitters[i]->update(dt);
	}

	//Delete all Dead particle Emitters
	for (UINT i = 0; i < mParticleSystems.size(); i++)
	{
		if (mParticleSystems[i]->isFinished() == true)
		{
			mParticleSystems[i]->~ParticleEmitter();
			mParticleSystems.erase(mParticleSystems.begin() + (i));
			i--;
		}
	}

	////Output number of Particles in scene
	//int ParticleCount = 0;
	//for (UINT i = 0; i < mParticleSystems.size(); i++)
	//{
	//	ParticleCount += mParticleSystems[i]->getAliveParticleCount();
	//}

}

void ParticleM::onLostDevice()
{
	for (UINT i = 0; i < mParticleSystems.size(); i++)
	{
		mParticleSystems[i]->onLostDevice();
	}
}

void ParticleM::onResetDevice()
{
	for (UINT i = 0; i < mParticleSystems.size(); i++)
	{
		mParticleSystems[i]->onResetDevice();
	}
}

void ParticleM::ImportParticle(std::string FilePath)
{
	std::ifstream myFile(FilePath);
	std::string line;
	int iType, iMaxParticles;
	float fPosX, fPosY, fPosZ, fLifeMin, fLifeMax, fMinSize, fMaxSize, fDelay, fStartDelay;
	float fVelXMin, fVelXMax, fVelYMin, fVelYMax, fVelZMin, fVelZMax;
	bool bRestart;
	int iRestart, iEmitType;

	if (myFile.is_open())
	{
		while (getline(myFile, line))
		{
			if (line.compare("<Particle>") == 0)// || line.compare("</Particle>") )
			{
				getline(myFile, line);
				//Type
				line.erase(line.begin(), line.begin() + 7);
				line.erase(line.end() - 7, line.end());
				iType = ::atoi(line.c_str());
				getline(myFile, line);
				//Position X
				line.erase(line.begin(), line.begin() + 12);
				line.erase(line.end() - 12, line.end());
				fPosX = (float)::atof(line.c_str());
				getline(myFile, line);
				//Position Y
				line.erase(line.begin(), line.begin() + 12);
				line.erase(line.end() - 12, line.end());
				fPosY = (float)::atof(line.c_str());
				getline(myFile, line);
				//Position Z
				line.erase(line.begin(), line.begin() + 12);
				line.erase(line.end() - 12, line.end());
				fPosZ = (float)::atof(line.c_str());
				getline(myFile, line);
				//LifeMin
				line.erase(line.begin(), line.begin() + 10);
				line.erase(line.end() - 9, line.end());
				fLifeMin = (float)::atof(line.c_str());
				getline(myFile, line);
				//LifeMax
				line.erase(line.begin(), line.begin() + 10);
				line.erase(line.end() - 9, line.end());
				fLifeMax = (float)::atof(line.c_str());
				getline(myFile, line);
				//MinSize
				line.erase(line.begin(), line.begin() + 10);
				line.erase(line.end() - 9, line.end());
				fMinSize = (float)::atof(line.c_str());
				getline(myFile, line);
				//MaxSize
				line.erase(line.begin(), line.begin() + 10);
				line.erase(line.end() - 9, line.end());
				fMaxSize = (float)::atof(line.c_str());
				getline(myFile, line);
				//Delay
				line.erase(line.begin(), line.begin() + 8);
				line.erase(line.end() - 8, line.end());
				fDelay = (float)::atof(line.c_str());
				getline(myFile, line);
				//MaxParticles
				line.erase(line.begin(), line.begin() + 15);
				line.erase(line.end() - 15, line.end());
				iMaxParticles = ::atoi(line.c_str());
				getline(myFile, line);
				//VelocityXMin
				line.erase(line.begin(), line.begin() + 15);
				line.erase(line.end() - 15, line.end());
				fVelXMin = (float)::atof(line.c_str());
				getline(myFile, line);
				//VelocityXMax
				line.erase(line.begin(), line.begin() + 15);
				line.erase(line.end() - 15, line.end());
				fVelXMax = (float)::atof(line.c_str());
				getline(myFile, line);
				//VelocityYMin
				line.erase(line.begin(), line.begin() + 15);
				line.erase(line.end() - 15, line.end());
				fVelYMin = (float)::atof(line.c_str());
				getline(myFile, line);
				//VelocityYMax
				line.erase(line.begin(), line.begin() + 15);
				line.erase(line.end() - 15, line.end());
				fVelYMax = (float)::atof(line.c_str());
				getline(myFile, line);
				//VelocityZMin
				line.erase(line.begin(), line.begin() + 15);
				line.erase(line.end() - 15, line.end());
				fVelZMin = (float)::atof(line.c_str());
				getline(myFile, line);
				//VelocityZMax
				line.erase(line.begin(), line.begin() + 15);
				line.erase(line.end() - 15, line.end());
				fVelZMax = (float)::atof(line.c_str());
				getline(myFile, line);
				//Start Delay
				line.erase(line.begin(), line.begin() + 13);
				line.erase(line.end() - 13, line.end());
				fStartDelay = (float)::atof(line.c_str());
				getline(myFile, line);
				//Restart ???
				line.erase(line.begin(), line.begin() + 10);
				line.erase(line.end() - 10, line.end());
				iRestart = (int)::atoi(line.c_str());
				getline(myFile, line);
				//EmitType
				line.erase(line.begin(), line.begin() + 11);
				line.erase(line.end() - 11, line.end());
				iEmitType = ::atoi(line.c_str());
				getline(myFile, line);//Skip </Particle>

				if (iRestart == 1)
				{
					bRestart = true;
				}
				else
				{
					bRestart = false;
				}
			}
			/*
			int iType, iMaxParticles;
			float fPosX, fPosY, fPosZ, fLifeMin, fLifeMax, fMinSize, fMaxSize, fDelay;
			float fVelXMin, fVelXMax, fVelYMin, fVelYMax, fVelZMin, fVelZMax;
			*/
			D3DXVECTOR3 offSet = D3DXVECTOR3(fPosX, fPosY, fPosZ);

			if (iType == 3) //Create Sprinkler
			{
				CreateSprinkler(iMaxParticles, fDelay, bRestart, D3DXVECTOR3(fPosX, fPosY, fPosZ), D3DXVECTOR3(fLifeMin, fLifeMax, 0.0f), D3DXVECTOR3(fMinSize, fMaxSize, 0.0f), D3DXVECTOR3(fVelXMin, fVelXMax, 0.0f), D3DXVECTOR3(fVelYMin, fVelYMax, 0.0f), D3DXVECTOR3(fVelZMin, fVelZMax, 0.0f), fStartDelay, iEmitType, offSet);
			}
			else if (iType == 4) //Create Smoke
			{
				CreateSmoke(iMaxParticles, fDelay, bRestart, D3DXVECTOR3(fPosX, fPosY, fPosZ), D3DXVECTOR3(fLifeMin, fLifeMax, 0.0f), D3DXVECTOR3(fMinSize, fMaxSize, 0.0f), D3DXVECTOR3(fVelXMin, fVelXMax, 0.0f), D3DXVECTOR3(fVelYMin, fVelYMax, 0.0f), D3DXVECTOR3(fVelZMin, fVelZMax, 0.0f), fStartDelay, iEmitType, offSet);
			}
			else if (iType == 5) //Create Explosion
			{
				CreateExplosion(iMaxParticles, fDelay, bRestart, D3DXVECTOR3(fPosX, fPosY, fPosZ), D3DXVECTOR3(fLifeMin, fLifeMax, 0.0f), D3DXVECTOR3(fMinSize, fMaxSize, 0.0f), D3DXVECTOR3(fVelXMin, fVelXMax, 0.0f), D3DXVECTOR3(fVelYMin, fVelYMax, 0.0f), D3DXVECTOR3(fVelZMin, fVelZMax, 0.0f), fStartDelay, iEmitType, offSet);
			}
			else if (iType == 6) //Create Dirt Cloud
			{
				CreateDirtCloud(iMaxParticles, fDelay, bRestart, D3DXVECTOR3(fPosX, fPosY, fPosZ), D3DXVECTOR3(fLifeMin, fLifeMax, 0.0f), D3DXVECTOR3(fMinSize, fMaxSize, 0.0f), D3DXVECTOR3(fVelXMin, fVelXMax, 0.0f), D3DXVECTOR3(fVelYMin, fVelYMax, 0.0f), D3DXVECTOR3(fVelZMin, fVelZMax, 0.0f), fStartDelay, iEmitType, offSet);
			}
			else if (iType == 7) //Create Rock Particle
			{
				CreateRockParticles(iMaxParticles, fDelay, bRestart, D3DXVECTOR3(fPosX, fPosY, fPosZ), D3DXVECTOR3(fLifeMin, fLifeMax, 0.0f), D3DXVECTOR3(fMinSize, fMaxSize, 0.0f), D3DXVECTOR3(fVelXMin, fVelXMax, 0.0f), D3DXVECTOR3(fVelYMin, fVelYMax, 0.0f), D3DXVECTOR3(fVelZMin, fVelZMax, 0.0f), fStartDelay, iEmitType, offSet);
			}
			else if (iType == 8) //Create Shock Particle
			{
				CreateShockParticles(iMaxParticles, fDelay, bRestart, D3DXVECTOR3(fPosX, fPosY, fPosZ), D3DXVECTOR3(fLifeMin, fLifeMax, 0.0f), D3DXVECTOR3(fMinSize, fMaxSize, 0.0f), D3DXVECTOR3(fVelXMin, fVelXMax, 0.0f), D3DXVECTOR3(fVelYMin, fVelYMax, 0.0f), D3DXVECTOR3(fVelZMin, fVelZMax, 0.0f), fStartDelay, iEmitType, offSet);
			}
			else if (iType == 9) //Create Air Cloud Particle
			{
				CreateAirCloud(iMaxParticles, fDelay, bRestart, D3DXVECTOR3(fPosX, fPosY, fPosZ), D3DXVECTOR3(fLifeMin, fLifeMax, 0.0f), D3DXVECTOR3(fMinSize, fMaxSize, 0.0f), D3DXVECTOR3(fVelXMin, fVelXMax, 0.0f), D3DXVECTOR3(fVelYMin, fVelYMax, 0.0f), D3DXVECTOR3(fVelZMin, fVelZMax, 0.0f), fStartDelay, iEmitType, offSet);
			}
		}
		myFile.close();

	}
}

void ParticleM::LoadParticleEffect(std::string FilePath, D3DXVECTOR3 position)
{
	std::string path = "../particleEffectFiles/";
	std::ifstream myFile(path + FilePath);
	std::string line;
	int iType, iMaxParticles;
	float fPosX, fPosY, fPosZ, fLifeMin, fLifeMax, fMinSize, fMaxSize, fDelay, fStartDelay;
	float fVelXMin, fVelXMax, fVelYMin, fVelYMax, fVelZMin, fVelZMax;
	bool bRestart;
	int iRestart, iEmitType;
	float scale;

	if (myFile.is_open())
	{
		while (getline(myFile, line))
		{
			if (line.compare("<Particle>") == 0)// || line.compare("</Particle>") )
			{
				getline(myFile, line);
				//Type
				line.erase(line.begin(), line.begin() + 7);
				line.erase(line.end() - 7, line.end());
				iType = ::atoi(line.c_str());
				getline(myFile, line);
				//Position X
				line.erase(line.begin(), line.begin() + 12);
				line.erase(line.end() - 12, line.end());
				fPosX = (float)::atof(line.c_str());
				getline(myFile, line);
				//Position Y
				line.erase(line.begin(), line.begin() + 12);
				line.erase(line.end() - 12, line.end());
				fPosY = (float)::atof(line.c_str());
				getline(myFile, line);
				//Position Z
				line.erase(line.begin(), line.begin() + 12);
				line.erase(line.end() - 12, line.end());
				fPosZ = (float)::atof(line.c_str());
				getline(myFile, line);
				//LifeMin
				line.erase(line.begin(), line.begin() + 10);
				line.erase(line.end() - 9, line.end());
				fLifeMin = (float)::atof(line.c_str());
				getline(myFile, line);
				//LifeMax
				line.erase(line.begin(), line.begin() + 10);
				line.erase(line.end() - 9, line.end());
				fLifeMax = (float)::atof(line.c_str());
				getline(myFile, line);
				//MinSize
				line.erase(line.begin(), line.begin() + 10);
				line.erase(line.end() - 9, line.end());
				fMinSize = (float)::atof(line.c_str());
				getline(myFile, line);
				//MaxSize
				line.erase(line.begin(), line.begin() + 10);
				line.erase(line.end() - 9, line.end());
				fMaxSize = (float)::atof(line.c_str());
				getline(myFile, line);
				//Delay
				line.erase(line.begin(), line.begin() + 8);
				line.erase(line.end() - 8, line.end());
				fDelay = (float)::atof(line.c_str());
				getline(myFile, line);
				//MaxParticles
				line.erase(line.begin(), line.begin() + 15);
				line.erase(line.end() - 15, line.end());
				iMaxParticles = ::atoi(line.c_str());
				getline(myFile, line);
				//VelocityXMin
				line.erase(line.begin(), line.begin() + 15);
				line.erase(line.end() - 15, line.end());
				fVelXMin = (float)::atof(line.c_str());
				getline(myFile, line);
				//VelocityXMax
				line.erase(line.begin(), line.begin() + 15);
				line.erase(line.end() - 15, line.end());
				fVelXMax = (float)::atof(line.c_str());
				getline(myFile, line);
				//VelocityYMin
				line.erase(line.begin(), line.begin() + 15);
				line.erase(line.end() - 15, line.end());
				fVelYMin = (float)::atof(line.c_str());
				getline(myFile, line);
				//VelocityYMax
				line.erase(line.begin(), line.begin() + 15);
				line.erase(line.end() - 15, line.end());
				fVelYMax = (float)::atof(line.c_str());
				getline(myFile, line);
				//VelocityZMin
				line.erase(line.begin(), line.begin() + 15);
				line.erase(line.end() - 15, line.end());
				fVelZMin = (float)::atof(line.c_str());
				getline(myFile, line);
				//VelocityZMax
				line.erase(line.begin(), line.begin() + 15);
				line.erase(line.end() - 15, line.end());
				fVelZMax = (float)::atof(line.c_str());
				getline(myFile, line);
				//Start Delay
				line.erase(line.begin(), line.begin() + 13);
				line.erase(line.end() - 13, line.end());
				fStartDelay = (float)::atof(line.c_str());
				getline(myFile, line);
				//Restart ???
				line.erase(line.begin(), line.begin() + 10);
				line.erase(line.end() - 10, line.end());
				iRestart = (int)::atoi(line.c_str());
				getline(myFile, line);
				//EmitType
				line.erase(line.begin(), line.begin() + 11);
				line.erase(line.end() - 11, line.end());
				iEmitType = ::atoi(line.c_str());
				getline(myFile, line);
				//Scale size for Fire
				line.erase(line.begin(), line.begin() + 8);
				line.erase(line.end() - 8, line.end());
				scale = (float)::atof(line.c_str());
				getline(myFile, line);//Skip </Particle>

				if (iRestart == 1)
				{
					bRestart = true;
				}
				else
				{
					bRestart = false;
				}
			}
			/*
			int iType, iMaxParticles;
			float fPosX, fPosY, fPosZ, fLifeMin, fLifeMax, fMinSize, fMaxSize, fDelay;
			float fVelXMin, fVelXMax, fVelYMin, fVelYMax, fVelZMin, fVelZMax;
			*/
			D3DXVECTOR3 offSet = D3DXVECTOR3(fPosX, fPosY, fPosZ);

			if (iType == 3) //Create Sprinkler
			{
				CreateSprinkler(iMaxParticles, fDelay, bRestart, D3DXVECTOR3(fPosX + position.x, fPosY + position.y, fPosZ + position.z), D3DXVECTOR3(fLifeMin, fLifeMax, 0.0f), D3DXVECTOR3(fMinSize, fMaxSize, 0.0f), D3DXVECTOR3(fVelXMin, fVelXMax, 0.0f), D3DXVECTOR3(fVelYMin, fVelYMax, 0.0f), D3DXVECTOR3(fVelZMin, fVelZMax, 0.0f), fStartDelay, iEmitType, offSet);
			}
			else if (iType == 4) //Create Smoke
			{
				CreateSmoke(iMaxParticles, fDelay, bRestart, D3DXVECTOR3(fPosX + position.x, fPosY + position.y, fPosZ + position.z), D3DXVECTOR3(fLifeMin, fLifeMax, 0.0f), D3DXVECTOR3(fMinSize, fMaxSize, 0.0f), D3DXVECTOR3(fVelXMin, fVelXMax, 0.0f), D3DXVECTOR3(fVelYMin, fVelYMax, 0.0f), D3DXVECTOR3(fVelZMin, fVelZMax, 0.0f), fStartDelay, iEmitType, offSet);
			}
			else if (iType == 5) //Create Explosion
			{
				CreateExplosion(iMaxParticles, fDelay, bRestart, D3DXVECTOR3(fPosX + position.x, fPosY + position.y, fPosZ + position.z), D3DXVECTOR3(fLifeMin, fLifeMax, 0.0f), D3DXVECTOR3(fMinSize, fMaxSize, 0.0f), D3DXVECTOR3(fVelXMin, fVelXMax, 0.0f), D3DXVECTOR3(fVelYMin, fVelYMax, 0.0f), D3DXVECTOR3(fVelZMin, fVelZMax, 0.0f), fStartDelay, iEmitType, offSet);
			}
			else if (iType == 6) //Create Dirt Cloud
			{
				CreateDirtCloud(iMaxParticles, fDelay, bRestart, D3DXVECTOR3(fPosX + position.x, fPosY + position.y, fPosZ + position.z), D3DXVECTOR3(fLifeMin, fLifeMax, 0.0f), D3DXVECTOR3(fMinSize, fMaxSize, 0.0f), D3DXVECTOR3(fVelXMin, fVelXMax, 0.0f), D3DXVECTOR3(fVelYMin, fVelYMax, 0.0f), D3DXVECTOR3(fVelZMin, fVelZMax, 0.0f), fStartDelay, iEmitType, offSet);
			}
			else if (iType == 7) //Create Rock Particle
			{
				CreateRockParticles(iMaxParticles, fDelay, bRestart, D3DXVECTOR3(fPosX + position.x, fPosY + position.y, fPosZ + position.z), D3DXVECTOR3(fLifeMin, fLifeMax, 0.0f), D3DXVECTOR3(fMinSize, fMaxSize, 0.0f), D3DXVECTOR3(fVelXMin, fVelXMax, 0.0f), D3DXVECTOR3(fVelYMin, fVelYMax, 0.0f), D3DXVECTOR3(fVelZMin, fVelZMax, 0.0f), fStartDelay, iEmitType, offSet);
			}
			else if (iType == 8) //Create Shock Particle
			{
				CreateShockParticles(iMaxParticles, fDelay, bRestart, D3DXVECTOR3(fPosX + position.x, fPosY + position.y, fPosZ + position.z), D3DXVECTOR3(fLifeMin, fLifeMax, 0.0f), D3DXVECTOR3(fMinSize, fMaxSize, 0.0f), D3DXVECTOR3(fVelXMin, fVelXMax, 0.0f), D3DXVECTOR3(fVelYMin, fVelYMax, 0.0f), D3DXVECTOR3(fVelZMin, fVelZMax, 0.0f), fStartDelay, iEmitType, offSet);
			}
			else if (iType == 9) //Create Air Cloud Particle
			{
				CreateAirCloud(iMaxParticles, fDelay, bRestart, D3DXVECTOR3(fPosX + position.x, fPosY + position.y, fPosZ + position.z), D3DXVECTOR3(fLifeMin, fLifeMax, 0.0f), D3DXVECTOR3(fMinSize, fMaxSize, 0.0f), D3DXVECTOR3(fVelXMin, fVelXMax, 0.0f), D3DXVECTOR3(fVelYMin, fVelYMax, 0.0f), D3DXVECTOR3(fVelZMin, fVelZMax, 0.0f), fStartDelay, iEmitType, offSet);
			}
			else if (iType == 10) //Create Fire
			{
				D3DXVECTOR3 pos(fPosX, fPosY, fPosZ);
				CreateFire(pos + position, scale);
			}
		}
		myFile.close();
		mEffectID++; //Loaded one effect, increment for next load
	}
}

void ParticleM::LoadSpecialPE(std::string FilePath, D3DXVECTOR3 position, D3DXMATRIX rotation)
{
	std::string path = "../particleEffectFiles/";
	std::ifstream myFile(path + FilePath);

	mRotation = rotation;

	std::string line;
	int iType, iMaxParticles;
	float fPosX, fPosY, fPosZ, fLifeMin, fLifeMax, fMinSize, fMaxSize, fDelay, fStartDelay;
	float fVelXMin, fVelXMax, fVelYMin, fVelYMax, fVelZMin, fVelZMax;
	bool bRestart;
	int iRestart, iEmitType;
	float scale;

	if (myFile.is_open())
	{
		while (getline(myFile, line))
		{
			if (line.compare("<Particle>") == 0)// || line.compare("</Particle>") )
			{
				getline(myFile, line);
				//Type
				line.erase(line.begin(), line.begin() + 7);
				line.erase(line.end() - 7, line.end());
				iType = ::atoi(line.c_str());
				getline(myFile, line);
				//Position X
				line.erase(line.begin(), line.begin() + 12);
				line.erase(line.end() - 12, line.end());
				fPosX = (float)::atof(line.c_str());
				getline(myFile, line);
				//Position Y
				line.erase(line.begin(), line.begin() + 12);
				line.erase(line.end() - 12, line.end());
				fPosY = (float)::atof(line.c_str());
				getline(myFile, line);
				//Position Z
				line.erase(line.begin(), line.begin() + 12);
				line.erase(line.end() - 12, line.end());
				fPosZ = (float)::atof(line.c_str());
				getline(myFile, line);
				//LifeMin
				line.erase(line.begin(), line.begin() + 10);
				line.erase(line.end() - 9, line.end());
				fLifeMin = (float)::atof(line.c_str());
				getline(myFile, line);
				//LifeMax
				line.erase(line.begin(), line.begin() + 10);
				line.erase(line.end() - 9, line.end());
				fLifeMax = (float)::atof(line.c_str());
				getline(myFile, line);
				//MinSize
				line.erase(line.begin(), line.begin() + 10);
				line.erase(line.end() - 9, line.end());
				fMinSize = (float)::atof(line.c_str());
				getline(myFile, line);
				//MaxSize
				line.erase(line.begin(), line.begin() + 10);
				line.erase(line.end() - 9, line.end());
				fMaxSize = (float)::atof(line.c_str());
				getline(myFile, line);
				//Delay
				line.erase(line.begin(), line.begin() + 8);
				line.erase(line.end() - 8, line.end());
				fDelay = (float)::atof(line.c_str());
				getline(myFile, line);
				//MaxParticles
				line.erase(line.begin(), line.begin() + 15);
				line.erase(line.end() - 15, line.end());
				iMaxParticles = ::atoi(line.c_str());
				getline(myFile, line);
				//VelocityXMin
				line.erase(line.begin(), line.begin() + 15);
				line.erase(line.end() - 15, line.end());
				fVelXMin = (float)::atof(line.c_str());
				getline(myFile, line);
				//VelocityXMax
				line.erase(line.begin(), line.begin() + 15);
				line.erase(line.end() - 15, line.end());
				fVelXMax = (float)::atof(line.c_str());
				getline(myFile, line);
				//VelocityYMin
				line.erase(line.begin(), line.begin() + 15);
				line.erase(line.end() - 15, line.end());
				fVelYMin = (float)::atof(line.c_str());
				getline(myFile, line);
				//VelocityYMax
				line.erase(line.begin(), line.begin() + 15);
				line.erase(line.end() - 15, line.end());
				fVelYMax = (float)::atof(line.c_str());
				getline(myFile, line);
				//VelocityZMin
				line.erase(line.begin(), line.begin() + 15);
				line.erase(line.end() - 15, line.end());
				fVelZMin = (float)::atof(line.c_str());
				getline(myFile, line);
				//VelocityZMax
				line.erase(line.begin(), line.begin() + 15);
				line.erase(line.end() - 15, line.end());
				fVelZMax = (float)::atof(line.c_str());
				getline(myFile, line);
				//Start Delay
				line.erase(line.begin(), line.begin() + 13);
				line.erase(line.end() - 13, line.end());
				fStartDelay = (float)::atof(line.c_str());
				getline(myFile, line);
				//Restart ???
				line.erase(line.begin(), line.begin() + 10);
				line.erase(line.end() - 10, line.end());
				iRestart = (int)::atoi(line.c_str());
				getline(myFile, line);
				//EmitType
				line.erase(line.begin(), line.begin() + 11);
				line.erase(line.end() - 11, line.end());
				iEmitType = ::atoi(line.c_str());
				getline(myFile, line);
				//Scale size for Fire
				line.erase(line.begin(), line.begin() + 8);
				line.erase(line.end() - 8, line.end());
				scale = (float)::atof(line.c_str());
				getline(myFile, line);//Skip </Particle>

				if (iRestart == 1)
				{
					bRestart = true;
				}
				else
				{
					bRestart = false;
				}
			}
			/*
			int iType, iMaxParticles;
			float fPosX, fPosY, fPosZ, fLifeMin, fLifeMax, fMinSize, fMaxSize, fDelay;
			float fVelXMin, fVelXMax, fVelYMin, fVelYMax, fVelZMin, fVelZMax;
			*/
			D3DXVECTOR3 offSet = D3DXVECTOR3(fPosX, fPosY, fPosZ);

			if (iType == 3) //Create Sprinkler
			{
				CreateSprinkler(iMaxParticles, fDelay, bRestart, D3DXVECTOR3(fPosX + position.x, fPosY + position.y, fPosZ + position.z), D3DXVECTOR3(fLifeMin, fLifeMax, 0.0f), D3DXVECTOR3(fMinSize, fMaxSize, 0.0f), D3DXVECTOR3(fVelXMin, fVelXMax, 0.0f), D3DXVECTOR3(fVelYMin, fVelYMax, 0.0f), D3DXVECTOR3(fVelZMin, fVelZMax, 0.0f), fStartDelay, iEmitType, offSet);
			}
			else if (iType == 4) //Create Smoke
			{
				CreateSmoke(iMaxParticles, fDelay, bRestart, D3DXVECTOR3(fPosX + position.x, fPosY + position.y, fPosZ + position.z), D3DXVECTOR3(fLifeMin, fLifeMax, 0.0f), D3DXVECTOR3(fMinSize, fMaxSize, 0.0f), D3DXVECTOR3(fVelXMin, fVelXMax, 0.0f), D3DXVECTOR3(fVelYMin, fVelYMax, 0.0f), D3DXVECTOR3(fVelZMin, fVelZMax, 0.0f), fStartDelay, iEmitType, offSet);
			}
			else if (iType == 5) //Create Explosion
			{
				CreateExplosion(iMaxParticles, fDelay, bRestart, D3DXVECTOR3(fPosX + position.x, fPosY + position.y, fPosZ + position.z), D3DXVECTOR3(fLifeMin, fLifeMax, 0.0f), D3DXVECTOR3(fMinSize, fMaxSize, 0.0f), D3DXVECTOR3(fVelXMin, fVelXMax, 0.0f), D3DXVECTOR3(fVelYMin, fVelYMax, 0.0f), D3DXVECTOR3(fVelZMin, fVelZMax, 0.0f), fStartDelay, iEmitType, offSet);
			}
			else if (iType == 6) //Create Dirt Cloud
			{
				CreateDirtCloud(iMaxParticles, fDelay, bRestart, D3DXVECTOR3(fPosX + position.x, fPosY + position.y, fPosZ + position.z), D3DXVECTOR3(fLifeMin, fLifeMax, 0.0f), D3DXVECTOR3(fMinSize, fMaxSize, 0.0f), D3DXVECTOR3(fVelXMin, fVelXMax, 0.0f), D3DXVECTOR3(fVelYMin, fVelYMax, 0.0f), D3DXVECTOR3(fVelZMin, fVelZMax, 0.0f), fStartDelay, iEmitType, offSet);
			}
			else if (iType == 7) //Create Rock Particle
			{
				CreateRockParticles(iMaxParticles, fDelay, bRestart, D3DXVECTOR3(fPosX + position.x, fPosY + position.y, fPosZ + position.z), D3DXVECTOR3(fLifeMin, fLifeMax, 0.0f), D3DXVECTOR3(fMinSize, fMaxSize, 0.0f), D3DXVECTOR3(fVelXMin, fVelXMax, 0.0f), D3DXVECTOR3(fVelYMin, fVelYMax, 0.0f), D3DXVECTOR3(fVelZMin, fVelZMax, 0.0f), fStartDelay, iEmitType, offSet);
			}
			else if (iType == 8) //Create Shock Particle
			{
				CreateShockParticles(iMaxParticles, fDelay, bRestart, D3DXVECTOR3(fPosX + position.x, fPosY + position.y, fPosZ + position.z), D3DXVECTOR3(fLifeMin, fLifeMax, 0.0f), D3DXVECTOR3(fMinSize, fMaxSize, 0.0f), D3DXVECTOR3(fVelXMin, fVelXMax, 0.0f), D3DXVECTOR3(fVelYMin, fVelYMax, 0.0f), D3DXVECTOR3(fVelZMin, fVelZMax, 0.0f), fStartDelay, iEmitType, offSet);
			}
			else if (iType == 9) //Create Air Cloud Particle
			{
				CreateAirCloud(iMaxParticles, fDelay, bRestart, D3DXVECTOR3(fPosX + position.x, fPosY + position.y, fPosZ + position.z), D3DXVECTOR3(fLifeMin, fLifeMax, 0.0f), D3DXVECTOR3(fMinSize, fMaxSize, 0.0f), D3DXVECTOR3(fVelXMin, fVelXMax, 0.0f), D3DXVECTOR3(fVelYMin, fVelYMax, 0.0f), D3DXVECTOR3(fVelZMin, fVelZMax, 0.0f), fStartDelay, iEmitType, offSet);
			}
			else if (iType == 10) //Create Fire
			{
				D3DXVECTOR3 pos(fPosX, fPosY, fPosZ);
				CreateFire(pos + position, scale);
			}
		}
		myFile.close();
		mEffectID++; //Loaded one effect, increment for next load
	}

	D3DXMatrixIdentity(&mRotation);
}

int ParticleM::LastEffectID()
{
	int ID = mEffectID - 1;
	return ID;
}

void ParticleM::setRotationMatrix(D3DXMATRIX rot, int ID)
{
	//mParticleSystems[ID]->setRotationMtx(rot);

	for (UINT i = 0; i < mParticleSystems.size(); i++)
	{
		if (mParticleSystems[i]->getParticleID() == ID)
		{
			mParticleSystems[i]->setRotationMtx(rot);
		}
	}
}

void ParticleM::DontDrawExactEffects(int firstEffect)
{
	bool bNotIn = true;
	for (UINT i = 0; i < mExcludedDraw.size(); i++)
	{
		if (firstEffect == mExcludedDraw[i])
			bNotIn = false;
	}
	if ( bNotIn == true)
		mExcludedDraw.push_back(firstEffect);
}
void ParticleM::DrawEffect(int effect)
{
	for (UINT i = 0; i < mExcludedDraw.size(); i++)
	{
		if (effect == mExcludedDraw[i])
		{
			mExcludedDraw.erase(mExcludedDraw.begin() + (i));
		}
	}
}

void ParticleM::SetExactPosition(D3DXVECTOR3 position, int ID)
{
	//D3DXVECTOR3 newPosition = mParticleSystems[ID]->GetPosition();

	//mParticleSystems[ID]->SetPosition(position);

	for (UINT i = 0; i < mParticleSystems.size(); i++)
	{
		if (mParticleSystems[i]->getParticleID() == ID)
		{
			mParticleSystems[i]->SetPosition(position);
		}
	}
}

void ParticleM::SetExactVelocity(D3DXVECTOR3 velocity, int ID)
{
	D3DXVECTOR3 velx, velz;
	velx = D3DXVECTOR3(velocity.x - 0.01f, velocity.x + 0.01f, 0.0f);
	velz = D3DXVECTOR3(velocity.z - 0.01f, velocity.z + 0.01f, 0.0f);

	if (velx.x > 18.0f || velx.x < 18.0f)
		velx *= 0.5f;

	if (velx.x < 5 && velx.x > -5)
		velx *= 2.0f;

	if (velz.x > 18.0f || velz.x < 18.0f)
		velz *= 0.5f;

	if (velz.x < 5 && velz.x > -5)
		velz *= 2.0f;

	//velz *= 0.5f;


	for (UINT i = 0; i < mParticleSystems.size(); i++)
	{
		if (mParticleSystems[i]->getParticleID() == ID)
		{
			mParticleSystems[i]->SetVelX(velx);
			mParticleSystems[i]->SetVelZ(velz);
		}
	}

	//mParticleSystems[ID]->SetVelX(velx);
	//mParticleSystems[ID]->SetVelZ(velz);
}

void ParticleM::SetPaused(bool paused)
{
	for (UINT i = 0; i < mParticleSystems.size(); i++)
	{
		mParticleSystems[i]->setPaused(paused);
	}
	for (UINT i = 0; i < mFireEmitters.size(); i++)
	{
		mFireEmitters[i]->setPaused(paused);
	}
}
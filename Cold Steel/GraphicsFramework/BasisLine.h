#pragma once
#include "DXUtil.h"
class BasisLine
{
	D3DXVECTOR3 pos;
	D3DXVECTOR3 x;
	D3DXVECTOR3 y;
	D3DXVECTOR3 z;

public:
	BasisLine();
	BasisLine(D3DXVECTOR3 pos, D3DXVECTOR3 x, D3DXVECTOR3 y, D3DXVECTOR3 z);
	~BasisLine();
	void update(D3DXVECTOR3 pos, D3DXVECTOR3 x, D3DXVECTOR3 y, D3DXVECTOR3 z);
	void render();
};


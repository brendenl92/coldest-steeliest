#pragma once
#include "d3dapp.h"
#include "FSM.h"
class GameApp :
	public D3DAPP
{
	FSM* Machine;
public:
	GameApp(HINSTANCE instance, string caption, D3DDEVTYPE type, DWORD requestedVP);
	~GameApp();
	void onLostDevice();
	void changeState(string name);
	void onResetDevice();
	void updateScene(float dt);
	void HandleInput(RAWINPUT* raw);
	void initializeState();
	void drawScene();
	int run();
};


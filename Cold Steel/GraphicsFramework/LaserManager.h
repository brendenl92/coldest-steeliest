#pragma once
#include "Laser.h"
#define LM LaserManager::thenameofthefunctiondoesntreallymattericannameitanythingeventhoughweusuallycallitinstance()

class LaserManager
{
	vector<Laser> lasers;
	LaserManager();
public:
	static LaserManager* thenameofthefunctiondoesntreallymattericannameitanythingeventhoughweusuallycallitinstance();
	~LaserManager();
	void leave();
	void addLaser(D3DXVECTOR3 pos, D3DXVECTOR3 vel, UINT mID, MasterEntity* me);
	void delLaser(UINT index);
	MasterEntity* getME(UINT index);
	void update(double dt);

	vector<Laser> getLasers();
};

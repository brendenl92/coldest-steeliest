#include "ResourceManager.h"
#include "GameApp.h"
ResourceManager* ResourceManager::instance()
{
	static ResourceManager* ptr = new ResourceManager();
	return ptr;
}

ResourceManager::ResourceManager()
{
	if (!SUCCEEDED(D3DXCreateSprite(g_D3dDevice, &m_Sprite)))
	{
		MessageBox(NULL, "Sprite failed Creation", "Sprite Failure", MB_ICONWARNING);
		PostQuitMessage(0);
	}
}

ResourceManager::~ResourceManager()
{
	for (UINT i = 0; i < TexturePool.size(); i++)
		delete TexturePool[i];
	for (UINT i = 0; i < MeshPool.size(); i++)
		delete MeshPool[i];
}

void ResourceManager::ImportMeshes(string* name, int numEntries, UINT tech)
{
	for(int i = 0; i < numEntries; i++)
		MeshPool.push_back(new Mesh(name[i], tech));
}

void ResourceManager::DeleteMesh(int mesh)
{
	MeshPool.erase(MeshPool.begin() + mesh);
}

Mesh* ResourceManager::getMesh(int id)
{
	return MeshPool[id];
}

void ResourceManager::ImportTextures(string* names, int num)
{

	for(int i = 0; i < num; i++)
	{
		TexturePool.push_back(new LPDIRECT3DTEXTURE9());
		if (D3DXCreateTextureFromFile(g_D3dDevice, names[i].c_str(), TexturePool[TexturePool.size() - 1]) == D3D_OK)
		{
			TextureInfo.push_back(D3DXIMAGE_INFO());
			if (D3DXGetImageInfoFromFile(names[i].c_str(), &TextureInfo[TextureInfo.size() - 1]) != D3D_OK)
			{
				char message[100];
				ZeroMemory(message, sizeof(char[100]));
				strcat_s(message, "Texture: ");
				strcat_s(message, names[i].c_str());
				strcat_s(message, " failed to load.");
				MessageBox(NULL, message, "Sprite Failure", MB_ICONWARNING);
				PostQuitMessage(0);
			}
			if (TexturePool.size() != TextureInfo.size())
				cout << names[i].c_str();
		}
	}
}

void ResourceManager::DeleteTexture(int num)
{
	TexturePool.erase(TexturePool.begin() + num);
	TextureInfo.erase(TextureInfo.begin() + num);
}
LPDIRECT3DTEXTURE9* ResourceManager::getTexture(int i)
{
	return TexturePool[i];
}
int ResourceManager::getTextureX(int num)
{
	return TextureInfo[num].Width;
}

int ResourceManager::getTextureY(int num)
{
	return TextureInfo[num].Height;
}

LPD3DXSPRITE ResourceManager::getSprite()
{
	return m_Sprite;
}

LPD3DXFONT ResourceManager::getFont(int i)
{
	return FontPool[i];
}

int ResourceManager::createFont(string name, UINT height, UINT width, UINT weight, bool italic)
{
	FontPool.push_back(LPD3DXFONT());
	
	D3DXFONT_DESC font;
	font.Height = height;
	font.Width = width;
	font.Weight = weight;
	font.MipLevels = 0;
	font.Italic = italic;
	font.CharSet = DEFAULT_CHARSET;
	font.OutputPrecision = OUT_DEFAULT_PRECIS;
	font.Quality = DEFAULT_QUALITY;
	font.PitchAndFamily = DEFAULT_PITCH | FF_DONTCARE;
	strcpy_s(font.FaceName, name.c_str());
	FontSize.push_back(D3DXVECTOR2((float)width, (float)height));
	HR(D3DXCreateFontIndirect(g_D3dDevice, &font, &FontPool[FontPool.size()-1]));
	return FontPool.size() - 1;

}

void ResourceManager::deleteFont(int i)
{
	FontPool.erase(FontPool.begin() + i);
}

D3DXVECTOR2 ResourceManager::getFontSize(int i)
{
	return FontSize[i];
}

void ResourceManager::clearAll()
{
	for (UINT i = 0; i < FontPool.size(); i++)
		FontPool[i]->Release();
	FontPool.clear();
	FontSize.clear();
	for (UINT i = 0; i < MeshPool.size(); i++)
		delete MeshPool[i];
	MeshPool.clear();
	for (UINT i = 0; i < TexturePool.size(); i++)
		delete TexturePool[i];
	TexturePool.clear();
	TextureInfo.clear();
}

void ResourceManager::onLostDevice()
{
	if (m_Sprite)
		m_Sprite->OnLostDevice();
	for (UINT i = 0; i < FontPool.size(); i++)
		FontPool[i]->OnLostDevice();
}

void ResourceManager::onResetDevice()
{
	if (g_D3DAPP->getOldHeight() != 0 && g_D3DAPP->getOldWidth() != 0)
	{
		float x = ((float)g_D3DAPP->getWidth() / (float)g_D3DAPP->getOldWidth());
		float y = ((float)g_D3DAPP->getHeight() / (float)g_D3DAPP->getOldHeight());
		updateTextureScale(D3DXVECTOR2(x, y));
	}
	if (m_Sprite)
		m_Sprite->OnResetDevice();
	for (UINT i = 0; i < FontPool.size(); i++)
		FontPool[i]->OnResetDevice();

}

void ResourceManager::updateTextureScale(D3DXVECTOR2 newScale)
{
	for (UINT i = 0; i < TextureInfo.size(); i++)
	{
		double h = (double)TextureInfo[i].Height;
		double w = (double)TextureInfo[i].Width;
		h *= newScale.y;
		w *= newScale.x;
		TextureInfo[i].Height = (UINT)h;
		TextureInfo[i].Width = (UINT)w;
	}
}
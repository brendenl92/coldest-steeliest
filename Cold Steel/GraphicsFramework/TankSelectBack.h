#pragma once
#include "DXUtil.h"
#include "GraphicsEntity.h"
class TankSelectBack
{
	D3DXMATRIX m_ProjMat;
	vector<GraphicsEntity> Entities;
	LPD3DXEFFECT effect;
	int tankIndex; 
	float rotation;
public:
	TankSelectBack();
	~TankSelectBack();
	void buildProjMat();
	void nextTank();
	void prevTank();
	void update(float dt);
	void render();
	void onLostDevice();
	void onResetDevice();

	int getTankIndex();
};


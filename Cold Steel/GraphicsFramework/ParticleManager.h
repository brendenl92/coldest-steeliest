#ifndef PARTICLEMANAGER_H
#define PARTICLEMANAGER_H

#include "d3dApp.h"
#include "ParticleEmitter.h"
#include "Camera.h"
#include "XInput.h"
#include "FireEmitter.h"

#include <fstream>
#include <string>

#define ParticleManager ParticleM::Instance()// Particle Manager instance, so there is only one, no copies.

//Particle Manager Time
class ParticleM
{
private:
	//D3DXVECTOR3 particlePos;
	ParticleM();
	//Stores all particle Emitters, going to be changed later on, might use different ones for instancing help
	std::vector<ParticleEmitter*> mParticleSystems;
	std::vector<FireEmitter*> mFireEmitters;

	int mMaxParts;
	bool mChecked;//Min or Max , if checked Max values are set instead; (this is used to save space with sliders)

	std::vector<int> mExcludedDraw; //Set at 3500, doubt we will have that many effects at one time

	int mEffectID;//Gives all particle effects that are loaded in a batch a unique ID, ie: Explosion has 3 effects (all three will have the same ID) so when Mutating it we can use one ID only

	D3DXMATRIX mRotation; //Rotation for Shooting Effect that require the rotation matrix early.
public:
	~ParticleM();
	//ParticleM(string EffectName);

	static ParticleM* Instance();//Decleration instance of Particle Manager
	void CreateFire(D3DXVECTOR3 pos, float scale);

	void CreateSmoke(int maxPart, float delay, bool restart, float startDelay);
	void CreateSmoke(int maxPart, float delay, bool restart, D3DXVECTOR3 pos, D3DXVECTOR3 life, D3DXVECTOR3 size, D3DXVECTOR3 VelocityX, D3DXVECTOR3 VelocityY, D3DXVECTOR3 VelocityZ, float startDelay, int EmitType, D3DXVECTOR3 offSet);
	
	void CreateDirtCloud(int maxPart, float delay, bool restart, float startDelay);
	void CreateDirtCloud(int maxPart, float delay, bool restart, D3DXVECTOR3 pos, D3DXVECTOR3 life, D3DXVECTOR3 size, D3DXVECTOR3 VelocityX, D3DXVECTOR3 VelocityY, D3DXVECTOR3 VelocityZ, float startDelay, int EmitType, D3DXVECTOR3 offSet);
	
	void CreateExplosion(int maxPart, float delay, bool restart, float startDelay);
	void CreateExplosion(int maxPart, float delay, bool restart, D3DXVECTOR3 pos, D3DXVECTOR3 life, D3DXVECTOR3 size, D3DXVECTOR3 VelocityX, D3DXVECTOR3 VelocityY, D3DXVECTOR3 VelocityZ, float startDelay, int EmitType, D3DXVECTOR3 offSet);
	
	void CreateSprinkler(int maxPart, float delay, bool restart, float startDelay);
	void CreateSprinkler(int maxPart, float delay, bool restart, D3DXVECTOR3 pos, D3DXVECTOR3 life, D3DXVECTOR3 size, D3DXVECTOR3 VelocityX, D3DXVECTOR3 VelocityY, D3DXVECTOR3 VelocityZ, float startDelay, int EmitType, D3DXVECTOR3 offSet);

	void CreateRockParticles(int maxPart, float delay, bool restart, float startDelay);
	void CreateRockParticles(int maxPart, float delay, bool restart, D3DXVECTOR3 pos, D3DXVECTOR3 life, D3DXVECTOR3 size, D3DXVECTOR3 VelocityX, D3DXVECTOR3 VelocityY, D3DXVECTOR3 VelocityZ, float startDelay, int EmitType, D3DXVECTOR3 offSet);
	
	void CreateShockParticles(int maxPart, float delay, bool restart, float startDelay);
	void CreateShockParticles(int maxPart, float delay, bool restart, D3DXVECTOR3 pos, D3DXVECTOR3 life, D3DXVECTOR3 size, D3DXVECTOR3 VelocityX, D3DXVECTOR3 VelocityY, D3DXVECTOR3 VelocityZ, float startDelay, int iEmitType, D3DXVECTOR3 offSet);

	void CreateAirCloud(int maxPart, float delay, bool restart, float startDelay);
	void CreateAirCloud(int maxPart, float delay, bool restart, D3DXVECTOR3 pos, D3DXVECTOR3 life, D3DXVECTOR3 size, D3DXVECTOR3 VelocityX, D3DXVECTOR3 VelocityY, D3DXVECTOR3 VelocityZ, float startDelay, int EmitType, D3DXVECTOR3 offSet);

	void CreateRain(int maxPart, float delay, bool restart);//maxPart = Maximum Particles for this Emmitter
	void CreateSnow(int maxPart, float delay, bool restart);//Same as rain, just different texture and instead of going left, it drops right

	//All around Functions
	void onLostDevice();
	void onResetDevice();
	void update(float dt);
	void draw();

	//void ExportParticle(std::string FilePath); //Exports all particles in scene into an a text file in XML format, only requires the path of the file
	void ImportParticle(std::string FileName); //Imports a/all particles from a text file in XML format, Pushes all the particles with parameters into the scene
	void LoadParticleEffect(std::string FileName, D3DXVECTOR3 position);

	int LastEffectID();
	void setRotationMatrix(D3DXMATRIX rot, int ID);	//Set Rotation for specific effect
	void DontDrawExactEffects(int firstEffect); //Don't draw these two effects
	void DrawEffect(int firstEffect);
	void SetExactPosition(D3DXVECTOR3 position, int ID);	//Update Position of Particles with tanks new postion for specific effect
	void SetExactVelocity(D3DXVECTOR3 velocity, int ID);	//Update the Velocity to be going the opposite way for the moving effect
	void SetPaused(bool paused);
	void LoadSpecialPE(std::string FileName, D3DXVECTOR3 position, D3DXMATRIX rotation); //Special Effect with Rotation
};

class Rain : public ParticleEmitter
{
public:
	Rain(const std::string& fxName,
		const std::string& techName,
		const std::string& texName,
		const D3DXVECTOR3& accel,
		const AABB& box,
		int maxNumParticles,
		float timePerParticle)
		: ParticleEmitter(fxName, techName, texName, accel, box, maxNumParticles,
		timePerParticle)
	{
		mType = 1;
	}

	void initParticle(Particle& out)
	{
		// Generate about the camera.
		out.initialPos = CAM->getPos();

		// Spread the particles out on xz-plane.
		out.initialPos.x += GetRandomFloat(-100.0f, 100.0f);
		out.initialPos.z += GetRandomFloat(-100.0f, 100.0f);

		// Generate above the camera.
		out.initialPos.y += GetRandomFloat(50.0f, 55.0f);

		out.initialTime = mTime;
		out.lifeTime = GetRandomFloat(2.0f, 2.5f);
		out.initialColor = WHITE;
		out.initialSize = GetRandomFloat(6.0f, 7.0f);

		// Give them an initial falling down velocity.
		out.initialVelocity.x = GetRandomFloat(-1.5f, 0.0f);
		out.initialVelocity.y = GetRandomFloat(-50.0f, -45.0f);
		out.initialVelocity.z = GetRandomFloat(-0.5f, 0.5f);
	}
};

class Snow : public ParticleEmitter
{
public:
	Snow(const std::string& fxName,
		const std::string& techName,
		const std::string& texName,
		const D3DXVECTOR3& accel,
		const AABB& box,
		int maxNumParticles,
		float timePerParticle)
		: ParticleEmitter(fxName, techName, texName, accel, box, maxNumParticles,
		timePerParticle)
	{
		mType = 2;
	}

	void initParticle(Particle& out)
	{
		// Generate about the camera.
		out.initialPos = CAM->getPos();

		// Spread the particles out on xz-plane.
		out.initialPos.x += GetRandomFloat(-100.0f, 100.0f);
		out.initialPos.z += GetRandomFloat(-100.0f, 100.0f);

		// Generate above the camera.
		out.initialPos.y += GetRandomFloat(50.0f, 55.0f);

		out.initialTime = mTime;
		out.lifeTime = GetRandomFloat(mLifeMin, mLifeMax);
		out.initialColor = WHITE;
		out.initialSize = GetRandomFloat(6.0f, 7.0f);

		// Give them an initial falling down velocity.
		out.initialVelocity.x = GetRandomFloat(-1.5f, 0.0f);
		out.initialVelocity.y = GetRandomFloat(-50.0f, -45.0f);
		out.initialVelocity.z = GetRandomFloat(-0.5f, 0.5f);
	}
};

class Sprinkler : public ParticleEmitter
{
public:
	Sprinkler(const std::string& fxName,
		const std::string& techName,
		const std::string& texName,
		const D3DXVECTOR3& accel,
		const AABB& box,
		int maxNumParticles,
		float timePerSecond)
		: ParticleEmitter(fxName, techName, texName, accel, box, maxNumParticles,
		timePerSecond)
	{
		mPosition = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
		mMinimumSize = 8.0f;
		mMaximumSize = 12.0f;
		mType = 3;
		mVelocityX.x = -2.5f;
		mVelocityX.y = 2.5f;
		mVelocityY.x = 15.0f;
		mVelocityY.y = 25.0f;
		mVelocityZ.x = -2.5f;
		mVelocityZ.y = 2.5f;
	}

	void initParticle(Particle& out)
	{
		// Generate about the origin.
		out.initialPos = mPosition;

		out.initialTime = mTime;
		out.lifeTime = GetRandomFloat(mLifeMin, mLifeMax);
		out.initialColor = WHITE;
		out.initialSize = GetRandomFloat(mMinimumSize, mMaximumSize); //8.0f, 12.0f //GetRandomFloat(mMinimumSize, mMaximumSize)
		out.mass = GetRandomFloat(0.8f, 1.2f);

		out.initialVelocity.x = GetRandomFloat(mVelocityX.x, mVelocityX.y);
		out.initialVelocity.y = GetRandomFloat(mVelocityY.x, mVelocityY.y);
		out.initialVelocity.z = GetRandomFloat(mVelocityZ.x, mVelocityZ.y);
	}
};

class Smoke : public ParticleEmitter
{
public:
	Smoke(const std::string& fxName,
		const std::string& techName,
		const std::string& texName,
		const D3DXVECTOR3& accel,
		const AABB& box,
		int maxNumParticles,
		float timePerSecond)
		: ParticleEmitter(fxName, techName, texName, accel, box, maxNumParticles,
		timePerSecond)
	{
		mPosition = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
		mType = 4;
		mVelocityX.x = 0.0f;
		mVelocityX.y = 2.0f;
		mVelocityY.x = 5.0f;
		mVelocityY.y = 10.0f;
		mVelocityZ.x = -1.5f;
		mVelocityZ.y = 1.5f;
	}

	void initParticle(Particle& out)
	{
		// Generate about the origin.
		out.initialPos = mPosition;

		out.initialTime = mTime;
		out.lifeTime = GetRandomFloat(mLifeMin, mLifeMax);
		out.initialColor = GREY;
		out.initialSize = GetRandomFloat(mMinimumSize, mMaximumSize);//GetRandomFloat(8.0f, 12.0f);
		out.mass = GetRandomFloat(0.8f, 1.2f);

		out.initialVelocity.x = GetRandomFloat(mVelocityX.x, mVelocityX.y);
		out.initialVelocity.y = GetRandomFloat(mVelocityY.x, mVelocityY.y);
		out.initialVelocity.z = GetRandomFloat(mVelocityZ.x, mVelocityZ.y);
	}
};

class Explosion : public ParticleEmitter
{
public:
	Explosion(const std::string& fxName,
		const std::string& techName,
		const std::string& texName,
		const D3DXVECTOR3& accel,
		const AABB& box,
		int maxNumParticles,
		float timePerSecond)
		: ParticleEmitter(fxName, techName, texName, accel, box, maxNumParticles,
		timePerSecond)
	{
		mPosition = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
		mMinimumSize = 1.0f;
		mMaximumSize = 6.0f;
		mType = 5;
		mRestart = false;

		mVelocityX.x = 0.0f;
		mVelocityX.y = 4.0f;
		mVelocityY.x = 0.0f;
		mVelocityY.y = 4.0f;
		mVelocityZ.x = 0.0f;
		mVelocityZ.y = 0.0f;
	}

	void initParticle(Particle& out)
	{
		// Generate about the origin.
		out.initialPos = mPosition;

		out.initialTime = mTime;
		out.lifeTime = GetRandomFloat(mLifeMin, mLifeMax);
		out.initialColor = GREY;
		out.initialSize = GetRandomFloat(mMinimumSize, mMaximumSize);//GetRandomFloat(8.0f, 12.0f);
		out.mass = GetRandomFloat(0.8f, 1.2f);

		out.initialVelocity.x = -(mVelocityX.y / 2.0f) + GetRandomFloat(0.0f, mVelocityX.y);
		out.initialVelocity.y = -(mVelocityY.y / 2.0f) + GetRandomFloat(0.0f, mVelocityY.y);//GetRandomFloat(15.0f, 25.0f);
		out.initialVelocity.z = 0.0f;
	}
};

class RockParticles : public ParticleEmitter
{
public:
	RockParticles(const std::string& fxName,
		const std::string& techName,
		const std::string& texName,
		const D3DXVECTOR3& accel,
		const AABB& box,
		int maxNumParticles,
		float timePerSecond)
		: ParticleEmitter(fxName, techName, texName, accel, box, maxNumParticles,
		timePerSecond)
	{
		mPosition = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
		mType = 7;
		mVelocityX.x = 0.0f;
		mVelocityX.y = 2.0f;
		mVelocityY.x = 5.0f;
		mVelocityY.y = 10.0f;
		mVelocityZ.x = -1.5f;
		mVelocityZ.y = 1.5f;
	}

	void initParticle(Particle& out)
	{
		// Generate about the origin.
		out.initialPos = mPosition;

		out.initialTime = mTime;
		out.lifeTime = GetRandomFloat(mLifeMin, mLifeMax);
		out.initialColor = GREY;
		out.initialSize = GetRandomFloat(mMinimumSize, mMaximumSize);//GetRandomFloat(8.0f, 12.0f);
		out.mass = GetRandomFloat(0.8f, 1.2f);


		out.initialVelocity.x = GetRandomFloat(mVelocityX.x, mVelocityX.y);
		out.initialVelocity.y = GetRandomFloat(mVelocityY.x, mVelocityY.y);
		out.initialVelocity.z = GetRandomFloat(mVelocityZ.x, mVelocityZ.y);
	}
};

#endif
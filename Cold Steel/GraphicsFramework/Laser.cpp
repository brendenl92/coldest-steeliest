#include "Laser.h"
#include "MasterEntity.h"

Laser::Laser(D3DXVECTOR3 pos, D3DXVECTOR3 vel, UINT mID, MasterEntity* me)
{
	this->mID = mID;
	this->pos = pos;
	this->vel = vel;
	
	D3DXVECTOR3 up(0, 1, 0);
	D3DXVECTOR3 right;
	D3DXVec3Cross(&right, &up, &vel);
	D3DXVec3Cross(&up, &right, &vel);	
	
	D3DXVec3Normalize(&up, &up);
	D3DXVec3Normalize(&right, &right);

	transform = D3DXMATRIX(right.x, right.y, right.z, 0,
						   up.x, up.y, up.z, 0,
						   vel.x, vel.y, vel.z , 0,
						   pos.x, pos.y, pos.z, 1);

	this->endTime = CLK->getTime() + 5;
	this->me = me;
}

Laser::~Laser()
{
}
MasterEntity* Laser::getMasterEntity()
{
	return me;
}

void Laser::update(double dt)
{
	pos += (vel * 500) * (float)dt;
	transform._41 = pos.x;
	transform._42 = pos.y;
	transform._43 = pos.z;
}

UINT Laser::getID()
{
	return mID;
}

D3DXMATRIX Laser::getMat()
{
	return transform;
}

double Laser::getEnd()
{
	return endTime;
}
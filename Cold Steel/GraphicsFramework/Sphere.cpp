#include "Sphere.h"


Sphere::Sphere(D3DXVECTOR3 pos, float rad, UINT meshID)
{
	D3DXCreateSphere(g_D3dDevice, rad, 4, 4, &mesh, NULL);
	D3DXMatrixTransformation(&mat, NULL, NULL, &D3DXVECTOR3(rad, rad, rad), NULL, NULL, &pos);
	this->meshID = meshID;
	color = RED;
}

Sphere::~Sphere()
{

}

void Sphere::setPos(D3DXVECTOR3 in)
{
	mat._41 = in.x;
	mat._42 = in.y;
	mat._43 = in.z;
}

void Sphere::setColor(UINT color)
{
	switch (color)
	{
	case 0:
		meshID = 126;
		break;
	case 1:
		meshID = 127;
		break;
	case 2:
		meshID = 128;
		break;
	case 3:
		meshID = 129;
		break;
	case 4:
		meshID = 130;
		break;
	case 5:
		meshID = 131;
		break;
	}
}

D3DXMATRIX Sphere::getTrans()
{
	return mat;
}

UINT Sphere::getColor()
{
	return color;
}

UINT Sphere::getID()
{
	return meshID;
}
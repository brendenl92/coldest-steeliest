#include "MasterEntity.h"
#include "HUD.h"

MasterEntity::MasterEntity()
{

}

MasterEntity::MasterEntity(char options, UINT mid, UINT id, D3DXVECTOR3 pos, D3DXVECTOR3 sca, D3DXMATRIX rot, D3DXMATRIX rot2)
{
	this->GraphicsMeshID = mid;
	this->EntityID = id;
	//if (EntityID == 1)
	//{
	//	tank = JUGGERNAUT;
	//	switch (tank->TankID())
	//	{
	//	case 0:
	//		GraphicsMeshID = 4;
	//		break;
	//	case 1:
	//		GraphicsMeshID = 10;
	//		break;
	//	case 2:
	//		GraphicsMeshID = 0;
	//		break;
	//	case 3:
	//		GraphicsMeshID = 12;
	//		break;

	//	case 4:
	//		GraphicsMeshID = 6;
	//		break;
	//	case 5:
	//		GraphicsMeshID = 14;
	//		break;		
	//	case 6:
	//		GraphicsMeshID = 2;
	//		break;
	//	case 7:
	//		GraphicsMeshID = 8;
	//		break;

	//	}
	//}
	//else if (EntityID == 2)
	//{
	//	int index = rand() % 8;
	//	switch (index)
	//	{
	//	case 0:
	//		tank = JUGGERNAUT;
	//		GraphicsMeshID = 4;
	//		break;
	//	case 1:
	//		tank = PREDATOR;
	//		GraphicsMeshID = 10;
	//		break;
	//	case 2:
	//		tank = SQUIRREL;
	//		GraphicsMeshID = 0;
	//		break;
	//	case 3:
	//		tank = ROMAN;
	//		GraphicsMeshID = 12;
	//		break;
	//	case 4:
	//		tank = RAPTOR;
	//		GraphicsMeshID = 6;
	//		break;
	//	case 5:
	//		tank = ARMADILLO;
	//		GraphicsMeshID = 14;
	//		break;
	//	case 6:
	//		tank = TRINITY;
	//		GraphicsMeshID = 2;
	//		break;
	//	case 7:
	//		tank = CLARK;
	//		GraphicsMeshID = 8;
	//		break;
	//	}
	//}

	tank = JUGGERNAUT;

	if (pos)
		this->position = pos;
	else
		this->position = D3DXVECTOR3(0, 0, 0);

	if (sca)
		scale = sca;
	else
		this->scale = D3DXVECTOR3(1, 1, 1);
	if (rot)
		baseRot = rot;
	else
		baseRot = D3DXMATRIX(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);
	if (rot2)
		turrRot = rot2;
	else
		turrRot = D3DXMATRIX(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);

	mHealth = 100;
	mRecharge = 100;

	if (EntityID == 2)
		mHealth = 20;

	if (options & PHYENABLED)
		rb = PhyI->CreateRigidBody(this, pos, 0, D3DXVECTOR3(abs(RM->getMesh(GraphicsMeshID)->scaledBounds(scale).x), abs(RM->getMesh(GraphicsMeshID)->scaledBounds(scale).y), abs(RM->getMesh(GraphicsMeshID)->scaledBounds(scale).z)), EntityID > 2 ? 100000.0f : 10.0f);//Added
	if (options & AIENABLED)
		bai = AMI->createAIObject();

	this->options = options;

	if (this->EntityID == 1 || this->EntityID == 2)
	{
		//Particle Code
		ParticleManager->LoadParticleEffect("hoverEffect.pef", position);
		this->myHoverEffect = ParticleManager->LastEffectID();
		ParticleManager->LoadParticleEffect("movingEffect.pef", position);
		this->myMoveEffect = ParticleManager->LastEffectID();
		ParticleManager->DontDrawExactEffects(myMoveEffect);
	}
}

MasterEntity::~MasterEntity()
{
	
}

char MasterEntity::getOption()
{
	return options;
}

UINT MasterEntity::getScore()
{
	return score;
}

D3DXVECTOR3 MasterEntity::getScale()
{
	return scale;
}

void MasterEntity::update(double dt, vector<MasterEntity*> entities)
{
	static double deBounce = 0.0f;

	deBounce += dt;

	if (bai)
		bai->Update(dt, entities, this);
	if (rb)
	{
		rb->Update((float)dt, &position, &velocity, &baseRot);
		if (velocity.x <= 0.1f && velocity.x >= -0.1f)
			velocity.x = 0.0f;
		if (velocity.y <= 0.1f && velocity.y >= -0.1f)
			velocity.y = 0.0f;
		if (velocity.z <= 0.1f && velocity.z >= -0.1f)
			velocity.z = 0.0f;
		if (this->EntityID == 1 || this->EntityID == 2)
		{
			//Always have the following three effects on the tank
			ParticleManager->SetExactPosition(position, this->myMoveEffect);
			ParticleManager->SetExactPosition(position, this->myHoverEffect);

			if (velocity.x != 0.0f || velocity.y != 0.0f || velocity.z != 0.0f)
			{
				//Draw Moving State of Particle Effect
				//Tell Manager to not draw the hover effect
				ParticleManager->DontDrawExactEffects(this->myHoverEffect);
				ParticleManager->DrawEffect(this->myMoveEffect);

				D3DXVECTOR3 vel = velocity;
				vel.x *= -1;
				vel.z *= -1;
				//Change velocity of the particle
				ParticleManager->SetExactVelocity(vel, myMoveEffect);

				//Rotate Particles according to the base rotation
				ParticleManager->setRotationMatrix(baseRot, myMoveEffect);
			}
			else//not moving hovering state
			{
				//Draw Hovering State instead
				ParticleManager->DontDrawExactEffects(myMoveEffect);
				ParticleManager->DrawEffect(this->myHoverEffect);
			}
		}
	}

	if (EntityID == 1 && CAM->getState() == FIRST || CAM->getState() == THIRD)
	{
		D3DXMATRIX temp;
		D3DXMatrixInverse(&temp, NULL, &CAM->getViewMat());
		turrRot = temp;
		/*D3DXMatrixIdentity(&temp);
		D3DXMatrixRotationY(&temp, 110);
		turrRot *= temp;*/
	}

	/*if (RAW->keyPressed(H) && deBounce > 0.5f && EntityID == 1)
	{
		ParticleManager->LoadParticleEffect("hitEffect.pef", this->position);
		deBounce = 0.0f;

		int explosion = ParticleManager->LastEffectID();

		ParticleManager->SetExactPosition(position + D3DXVECTOR3(1.0f, 2.0f, 2.0f), explosion);
	}*/

	//if (RAW->keyPressed(B) && deBounce > 1.0f && EntityID == 1)
	//{
	//	D3DXMATRIX rotOffSet, rotInverse;// , turretTrans;
	//	//D3DXMatrixTranslation(&turretTrans, 0, 8, 0);
	//	D3DXMatrixRotationY(&rotOffSet, 90 * 0.0174532925f);
	//	D3DXMatrixInverse(&rotInverse, 0, &turrRot);
	//	rotOffSet = rotOffSet*rotInverse;
	//	//rotOffSet = turretTrans * rotOffSet;
	//	ParticleManager->LoadSpecialPE("shotEffect.pef", this->position, rotOffSet);
	//}

	if (mRecharge < 100)
		mRecharge += (float)dt * 8;
	else
		mRecharge = 100;

	/*if (mHealth < 100)
		mHealth += tank->HealthRegen() * (float)dt;
	else
		mHealth = 100;*/
}

void MasterEntity::shoot()
{
	if (this->EntityID == 1)
	{
		D3DXMATRIX rotOffSet, rotInverse;
		D3DXMatrixRotationY(&rotOffSet, 90 * 0.0174532925f);
		D3DXMatrixInverse(&rotInverse, 0, &turrRot);
		rotOffSet = rotOffSet*rotInverse;
		ParticleManager->LoadSpecialPE("shotEffect.pef", this->position, rotOffSet);
	}
	else
	{
		ParticleManager->LoadSpecialPE("shotEffect.pef", this->position, turrRot);
	}
	this->mRecharge -= 20;
}

void MasterEntity::setBaseRot(D3DXMATRIX base)
{
	this->baseRot = base;
}

void MasterEntity::setTurrRot(D3DXMATRIX turr)
{
	this->turrRot = turr;
}

void MasterEntity::setPosition(D3DXVECTOR3 pos)
{
	this->position = pos;
}

void MasterEntity::setVelocity(D3DXVECTOR3 vel)
{
	this->velocity = vel;
}

D3DXMATRIX MasterEntity::getBaseRot()
{
	return baseRot;
}

D3DXMATRIX MasterEntity::getTurrRot()
{
	return turrRot;
}

D3DXMATRIX MasterEntity::getTrans(int index)
{
	D3DXMATRIX out;
	switch (index)
	{
	case 0:
	{
		D3DXQUATERNION rot;
		D3DXQuaternionRotationMatrix(&rot, &baseRot);
		rot.y *= -1;
		D3DXMatrixTransformation(&out, NULL, NULL, &scale, NULL, &rot, &position);
		break;
	}
	case 1:
	{
		if (EntityID == 1)
		{
			D3DXQUATERNION rot;
			D3DXQUATERNION exrot;

			D3DXQuaternionRotationMatrix(&rot, &turrRot);

			D3DXVECTOR3 temp = position + D3DXVECTOR3(0, 0, 0);

			D3DXQuaternionRotationAxis(&exrot, &D3DXVECTOR3(turrRot._21, turrRot._22, turrRot._23), D3DXToRadian(-90));
			rot *= exrot;

			D3DXQuaternionRotationAxis(&exrot, &D3DXVECTOR3(turrRot._11, turrRot._12, turrRot._13), D3DXToRadian(-10));
			rot *= exrot;

			D3DXMatrixTransformation(&out, NULL, NULL, &scale, NULL, &rot, &temp);
			break;
		}
		else
		{
			D3DXQUATERNION rot;
			D3DXQuaternionRotationMatrix(&rot, &turrRot);
			rot.y *= -1;
			D3DXVECTOR3 temp = position + D3DXVECTOR3(0, 0, 0);
			D3DXMatrixTransformation(&out, NULL, NULL, &scale, NULL, &rot, &temp);

		}
	}
	}
	return out;
}
D3DXVECTOR3 MasterEntity::getPosition()
{
	return position;
}

void MasterEntity::addScore(UINT score)
{
	this->score += score;
}

void MasterEntity::setDead(bool dead)
{
	this->mAlive = dead;
}

D3DXVECTOR3 MasterEntity::getVelocity()
{
	return velocity;
}
Tank* MasterEntity::getTank()
{
	return tank;
}
UINT MasterEntity::getMeshID()
{
	return GraphicsMeshID;
}

float MasterEntity::getHealth()
{
	return mHealth;
}

float MasterEntity::getRecharge()
{
	return mRecharge;
}

RigidBody* MasterEntity::getRigidBody()
{
	return rb;
}

BaseAIEntity* MasterEntity::getBaseAIEntity()
{
	return bai;
}

bool MasterEntity::getDead()
{
	return mAlive;
}

bool MasterEntity::AIEnt()
{
	return (options & AIENABLED);
}

void MasterEntity::DamageHealth(double damage)
{
	if (mHealth != 0)
		mHealth -= (float)damage * tank->Defense();
}

void MasterEntity::ReplenishHealth()
{
	if (mHealth != 100)
		mHealth += 5;
}

UINT MasterEntity::getEntityID()
{
	return EntityID;
}
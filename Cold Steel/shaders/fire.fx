////////////////////////////////////////////////////////////////////////////////
// Filename: fire.fx
////////////////////////////////////////////////////////////////////////////////


/////////////
// GLOBALS //
/////////////
uniform extern texture fireTexture;
uniform extern texture noiseTexture;
uniform extern texture alphaTexture;

uniform extern float frameTime;
uniform extern float3 scrollSpeeds;
uniform extern float3 scales;
uniform extern float2 distortion1;
uniform extern float2 distortion2;
uniform extern float2 distortion3;
uniform extern float distortionScale;
uniform extern float distortionBias;

uniform extern float4x4 gWVP;
uniform extern float3   gPosition; //Instanced Data


///////////////////
// SAMPLE STATES //
///////////////////
SamplerState SampleType
{
    Filter = MIN_MAG_MIP_LINEAR;
    AddressU = Wrap;
    AddressV = Wrap;
};

SamplerState SampleType2
{
    Filter = MIN_MAG_MIP_LINEAR;
    AddressU = Clamp;
    AddressV = Clamp;
};
sampler TexFire = sampler_state
{
	Texture = <fireTexture>;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	AddressU  = Clamp;
    AddressV  = Clamp;
};
sampler TexAlpha = sampler_state
{
	Texture = <alphaTexture>;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	AddressU  = Clamp;
    AddressV  = Clamp;
};
sampler TexNoise = sampler_state
{
	Texture = <noiseTexture>;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	AddressU  = Wrap;
    AddressV  = Wrap;
};

//////////////
// TYPEDEFS //
//////////////
//struct VertexInputType
//{
//   float4 position : POSITION;
//    float2 tex : TEXCOORD0;
//};

struct VertexInputType
{
	float3 posL    : POSITION0; 
    float2 tex : TEXCOORD0;
};

struct PixelInputType
{
    float4 position : SV_POSITION;
    float2 tex : TEXCOORD0;
	float2 texCoords1 : TEXCOORD1;
	float2 texCoords2 : TEXCOORD2;
	float2 texCoords3 : TEXCOORD3;
	float time : TEXCOORD4;
	float lifeTime : TEXCOORD5;
};


////////////////////////////////////////////////////////////////////////////////
// Vertex Shader
////////////////////////////////////////////////////////////////////////////////
PixelInputType FireVertexShader(VertexInputType input)
{
    //PixelInputType output;

	// Zero out our output.
	PixelInputType outVS = (PixelInputType)0;

	outVS.position = mul(float4(input.posL, 1.0f), gWVP);
        
	// Store the texture coordinates for the pixel shader.
    outVS.tex = input.tex;
    
    // Compute texture coordinates for first noise texture using the first scale and upward scrolling speed values.
	outVS.texCoords1 = (input.tex * scales.x);
	outVS.texCoords1.y = outVS.texCoords1.y + (frameTime * scrollSpeeds.x);

    // Compute texture coordinates for second noise texture using the second scale and upward scrolling speed values.
	outVS.texCoords2 = (input.tex * scales.y);
	outVS.texCoords2.y = outVS.texCoords2.y + (frameTime * scrollSpeeds.y);

    // Compute texture coordinates for third noise texture using the third scale and upward scrolling speed values.
	outVS.texCoords3 = (input.tex * scales.z);
	outVS.texCoords3.y = outVS.texCoords3.y + (frameTime * scrollSpeeds.z);

	return outVS;
}


////////////////////////////////////////////////////////////////////////////////
// Pixel Shader
////////////////////////////////////////////////////////////////////////////////
float4 FirePixelShader(PixelInputType input) : SV_Target
{
	float4 noise1;
	float4 noise2;
	float4 noise3;
	float4 finalNoise;
	float perturb;
	float2 noiseCoords;
	float4 fireColor;
	float4 alphaColor;


	// Sample the same noise texture using the three different texture coordinates to get three different noise scales.
    noise1 = tex2D(TexNoise , input.texCoords1);
    noise2 = tex2D(TexNoise , input.texCoords2);
	noise3 = tex2D(TexNoise , input.texCoords3);

	// Move the noise from the (0, 1) range to the (-1, +1) range.
    noise1 = (noise1 - 0.5f) * 2.0f;
    noise2 = (noise2 - 0.5f) * 2.0f;
    noise3 = (noise3 - 0.5f) * 2.0f;

	// Distort the three noise x and y coordinates by the three different distortion x and y values.
	noise1.xy = noise1.xy * distortion1.xy;
	noise2.xy = noise2.xy * distortion2.xy;
	noise3.xy = noise3.xy * distortion3.xy;

	// Combine all three distorted noise results into a single noise result.
	finalNoise = noise1 + noise2 + noise3;

	// Perturb the input texture Y coordinates by the distortion scale and bias values.  
	// The perturbation gets stronger as you move up the texture which creates the flame flickering at the top effect.
	perturb = ((1.0f - input.tex.y) * distortionScale) + distortionBias;

	// Now create the perturbed and distorted texture sampling coordinates that will be used to sample the fire color texture.
	noiseCoords.xy = (finalNoise.xy * perturb) + input.tex.xy;

	// Sample the color from the fire texture using the perturbed and distorted texture sampling coordinates.
	// Use the clamping sample state instead of the wrap sample state to prevent flames wrapping around.
    fireColor = tex2D(TexFire, noiseCoords.xy);

	// Sample the alpha value from the alpha texture using the perturbed and distorted texture sampling coordinates.
	// This will be used for transparency of the fire.
	// Use the clamping sample state instead of the wrap sample state to prevent flames wrapping around.
    alphaColor = tex2D(TexAlpha, noiseCoords.xy);

	// Set the alpha blending of the fire to the perturbed and distored alpha texture value.
	fireColor.a = alphaColor;
		
    return fireColor;
}


////////////////////////////////////////////////////////////////////////////////
// Technique
////////////////////////////////////////////////////////////////////////////////
technique FireTechnique
{
    pass pass0
    {
	    vertexShader = compile vs_3_0 FireVertexShader();
        pixelShader  = compile ps_3_0 FirePixelShader();

		PointSpriteEnable = true;
        AlphaBlendEnable = true;
	    SrcBlend     = SrcAlpha;
	    DestBlend    = InvSrcAlpha;
		AlphaRef = 100;
		AlphaFunc = GreaterEqual;
		ZFunc = lessequal;
		// Don't write to depth buffer; that is, particles don't obscure
		// the objects behind them.
		//ZWriteEnable = false;
    }
}
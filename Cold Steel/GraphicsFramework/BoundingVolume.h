#pragma once

#include "RigidBody.h"

struct BoundingVolume;



enum Shape_Type{VSPHERE, VCUBE};

////////////////////////////////
//Bounding Volume Node
///////////////////////////////
class BoundingVolumeNode
{
public:

	BoundingVolumeNode(RigidBody* Body, BoundingVolume* Volume);

	~BoundingVolumeNode();

	RigidBody* mBody;

	BoundingVolume* mVolume;
};



////////////////////////////
//Bounding Volume
//////////////////////////
struct BoundingVolume
{
	int mShape;
	float mRadius;
	V3 mHalfsize;
	UINT environment;

	BoundingVolume(float Radius, UINT environment, V3 Halfsize = V3(0, 0, 0));
};
#pragma once

#include "Attack.h"
#include "Flee.h"
#include "Wander.h"
#include "AStar.h"
#include <stdlib.h>
#include <time.h>
#include <vector>
#include <queue>
#include "LaserManager.h"
#include "DXUtil.h"

//Forward Delcaration
class SteeringManager;

class MasterEntity;

class BaseAIEntity
{
public:
	MasterEntity*			Enemy;						//Closest Enemy 
	SteeringManager*		p_mSteering;
	vector<D3DXVECTOR3>		m_WD;						//detects walls
	queue<int>			    m_BPSF;
	static FLOAT			s_IDCounter;				//static id counter
	D3DXVECTOR3				m_CN;
	D3DXVECTOR3				m_SForce;					//steering force
	D3DXVECTOR3				m_DB;						//detects obstacles
	D3DXVECTOR3				m_Target;					//position of the vector the object will seek
	FLOAT					m_wTime;					//keeps track of time as we increment through animation
	bool					IsAlive;					//determines if the AI obj is alive or not
	ASM*					m_fsm;						//objs own fsm
	FLOAT					m_ID;						//obj id
	FLOAT					m_tolerance;				//whats my safe zone.
	FLOAT					maxSpeed;					//control speed variable
	FLOAT					stateID;					//helps me control what state/steering i will recieve
	FLOAT					m_start;					//tells me which room node Im in.
	FLOAT					m_finish;					//tells me my goal
	FLOAT					m_currentNode;				//current node 
	FLOAT					m_cost;
	FLOAT					currentWeaponID;			//current weapon in hand
	FLOAT					m_maxHealth;				//max Hp
	FLOAT					m_currentHP;				//current Hp
	FLOAT					out_msg;					//shows the last decision the AI Entity made
	FLOAT					out_wpn_msg;				//shows the last weapon decision the AI Entity made
	FLOAT					enemyID;
	FLOAT					maxdegreespersecond;		//determines how many degrees persecond I want the object || turret to rotate...
	FLOAT					m_ShotTimer;
public:
	BaseAIEntity();
	~BaseAIEntity();

public:
	void SetTolerance(FLOAT t)
	{
		m_tolerance = t;
	}

	void SetLOM(FLOAT lom)
	{
		out_msg = lom;
	}

	void SetCN(D3DXVECTOR3 pos)
	{
		m_CN		= pos;
	}

	void SetTarget(D3DXVECTOR3 pos)
	{
		m_Target	= pos;
	}

	void SetState(FLOAT sid)
	{
		stateID = sid;
	}

	void SetMaxDegreesPerSecond(FLOAT mdps)
	{
		maxdegreespersecond = mdps;
	}

	void SetCurrentHP(FLOAT hp)
	{
		m_currentHP = hp;
	}

	FLOAT GetTolerance() const
	{
		return m_tolerance;
	}

	FLOAT GetAIID() const
	{
		return m_ID;
	}

	FLOAT GetMSpeed() const
	{
		return maxSpeed;
	}

	FLOAT GetLOM() const
	{
		return out_msg;
	}

	FLOAT GetEnemyID() const
	{
		return enemyID;
	}

	FLOAT GetCurrentHP() const
	{
		return m_currentHP;
	}

	D3DXVECTOR3 GetCN() const
	{
		return m_CN;
	}

	D3DXVECTOR3 GetTarget() const
	{
		return m_Target;
	}
	
	FLOAT GetState() const
	{
		return stateID;
	}

	FLOAT GetMaxDegreesPerSecond() const
	{
		return maxdegreespersecond;
	}

public:
	void		Zone(vector<MasterEntity* > m_vMEntities, MasterEntity* Base);		//determines if something is in my safe zone
	void		Update(double dt, vector<MasterEntity*> m_vMEntities, MasterEntity* Base);
	void		CheckState(vector<MasterEntity* > m_vMEntities, MasterEntity* Base);
	void		CheckTROT(double dt, MasterEntity* Base);
	void		FireCommand(MasterEntity* Base);
	void		HandleMsg(Mail message);
	void		ShutDown();
	D3DXVECTOR3	Truncate(D3DXVECTOR3 s, FLOAT ms);
};
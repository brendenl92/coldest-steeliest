#include "HUD.h"
#include "D3DAPP.h"
#include "Camera.h"
#include "ResourceManager.h"
#include "MasterEntity.h"

HUD* HUD::Instance()
{
	static HUD* temp = new HUD();
	return temp;
}

HUD::HUD()
{

}


HUD::~HUD()
{

}

void HUD::Initialize()
{
	PauseClearToDraw = false;
	ScoreClearToDraw = false;
	SettingsClearToDraw = false;

	radar = new Radar();

	mHealthBarRect.left = 0;
	mHealthBarRect.top = 0;
	mHealthBarRect.right = 512;
	mHealthBarRect.bottom = 64;

	mRechargeMeterRect.left = 0;
	mRechargeMeterRect.top = 0;
	mRechargeMeterRect.right = 512;
	mRechargeMeterRect.bottom = 64;

	HR(D3DXCreateLine(g_D3dDevice, &mLine));

	RM->createFont("Arial", 20, 9, 300, false);
	RM->createFont("Avenir", 50, 35, 700, false);
	DisplayHud = true;
}

void HUD::ClearHud()
{
	buttons.clear();
	delete radar;
}

void HUD::HandleInput(RAWINPUT* raw)
{
	static bool tabPressed = false;
	static double tabWait = 0;
	if (!buttons.empty())
	{
		if (PauseClearToDraw && !SettingsClearToDraw)
		{
			if (raw->data.mouse.ulButtons & RI_MOUSE_LEFT_BUTTON_UP)
			{
				for (UINT i = 0; i < buttons.size(); i++)
				{
					if (RAW->getMousePos().x <= buttons[i].getPos().x + (buttons[i].getHalfExtents().x * 2) && RAW->getMousePos().x >= buttons[i].getPos().x &&
						RAW->getMousePos().y <= buttons[i].getPos().y + (buttons[i].getHalfExtents().y * 2) && RAW->getMousePos().y >= buttons[i].getPos().y)
					{
						SOUND->playSoundEffect("../media/soundeffect/Button_Click.wav");

						if (buttons[i].getName() == "ResumeButton")
						{
							PauseClearToDraw = false;
							g_Paused = false;
							RAW->setDX(0.0f);
							RAW->setDY(0.0f);
							int cursor = 100;
							while (cursor > 0)
								cursor = ShowCursor(FALSE);
							DisplayHud = true;
							radar->SetPaused(PauseClearToDraw);
						}

						else if (buttons[i].getName() == "OptionsButton")
						{
							SettingsClearToDraw = true;
							PauseClearToDraw = false;
							DestructPause();
							ConstructSettings();
						}

						else if (buttons[i].getName() == "QuitButton")
						{
							PauseClearToDraw = false;
							g_D3DAPP->changeState("MENU");
						}
					}
				}
			}
		}

		else
		{
			if (raw->data.mouse.ulButtons & RI_MOUSE_LEFT_BUTTON_UP)
			{
				for (UINT i = 0; i < buttons.size(); i++)
				{
					if (RAW->getMousePos().x <= buttons[i].getPos().x + (buttons[i].getHalfExtents().x * 2) && RAW->getMousePos().x >= buttons[i].getPos().x &&
						RAW->getMousePos().y <= buttons[i].getPos().y + (buttons[i].getHalfExtents().y * 2) && RAW->getMousePos().y >= buttons[i].getPos().y)
					{
						SOUND->playSoundEffect("../media/soundeffect/Button_Click.wav");

						if (buttons[i].getName() == "SettingstoMain")
						{
							SettingsClearToDraw = false;
							PauseClearToDraw = true;
							DestructSettings();
							ConstructPause();
						}

						if (buttons[i].getName() == "SettingstoSave")
						{
							SET->setSettings(cboxes[0].getChecked(), cboxes[1].getChecked(), cboxes[2].getChecked(), sliders[0].getPosition(), sliders[1].getPosition(), sliders[2].getPosition() * 15);
							CAM->setSensitivity(sliders[2].getPosition() * 15);
							Sleep(200);
							SettingsClearToDraw = false;
							PauseClearToDraw = true;
							DestructSettings();
							ConstructPause();
						}
					}
				}
			}
		}
	}

	/*if (raw->data.keyboard.VKey == VK_TAB && tabWait < CLK->getTime())
	{
		if (tabPressed == true)
		{
			tabWait = CLK->getTime() + .3;
			ScoreClearToDraw = false;
			tabPressed = false;
			DestructScoreBoard();
		}
		else if (tabPressed == false)
		{
			tabWait = CLK->getTime() + .3;
			ScoreClearToDraw = true;
			tabPressed = true;
			ConstructScoreBoard();
		}
	}*/
}

void HUD::UpdateHud(float dt, vector <MasterEntity*> EntityVec)
{
	static bool pressed = false;

	if (ScoreClearToDraw == false && RAW->keyPressed(ESC) == true && pressed == false)
	{
		pressed = true;

		if (PauseClearToDraw && !ScoreClearToDraw)
		{

			PauseClearToDraw = false;
			RAW->setDX(0.0f);
			RAW->setDY(0.0f);
			g_Paused = false;
			int cursor = 100;
			while (cursor > 0)
				cursor = ShowCursor(FALSE);
			DestructPause();
			DisplayHud = true;

		}
		else if (SettingsClearToDraw)
		{

			SettingsClearToDraw = false;
			PauseClearToDraw = true;
			DestructSettings();
			ConstructPause();
			DisplayHud = true;

		}
		else
		{

			PauseClearToDraw = true;
			g_Paused = true;
			ShowCursor(TRUE);
			ConstructPause();
			DisplayHud = false;

		}

		radar->SetPaused(PauseClearToDraw);
	}

	else if (!RAW->keyPressed(ESC))
		pressed = false;



	if (PauseClearToDraw && !SettingsClearToDraw)
	{
		for (UINT i = 0; i < buttons.size(); i++)
			buttons[i].update(dt);
	}

	if (SettingsClearToDraw)
	{
		for (UINT i = 0; i < buttons.size(); i++)
			buttons[i].update(dt);

		for (UINT i = 0; i < sliders.size(); i++)
			sliders[i].update(dt);

		for (UINT i = 0; i < cboxes.size(); i++)
			cboxes[i].update(dt);
	}

	radar->Update(EntityVec);
	Entities = EntityVec;
}

void HUD::Draw()
{
	HR(RM->getSprite()->Begin(D3DXSPRITE_ALPHABLEND));

	//Everything Else
	if (DisplayHud)
	{

		D3DXMatrixTransformation2D(&mMatrix, &D3DXVECTOR2(0, 0), 0, &D3DXVECTOR2(1.16f, 1.1f), NULL, 0, NULL);
		HR(RM->getSprite()->SetTransform(&mMatrix));
		HR(RM->getSprite()->Draw(*RM->getTexture(0), &mHealthBarRect, &D3DXVECTOR3(0, 0, 0), &D3DXVECTOR3(128.5f, ((float)g_D3DAPP->getHeight()) - 434.5f , 0), D3DXCOLOR(1, 1, 1, 1)));

		D3DXMatrixTransformation2D(&mMatrix, &D3DXVECTOR2(0, 0), 0, &D3DXVECTOR2(1.17f, 1.1f), NULL, 0, NULL);
		HR(RM->getSprite()->SetTransform(&mMatrix));
		HR(RM->getSprite()->Draw(*RM->getTexture(1), &mRechargeMeterRect, &D3DXVECTOR3(0, 0, 0), &D3DXVECTOR3(125.5f, ((float)g_D3DAPP->getHeight()) - 325, 0), D3DXCOLOR(1, 1, 1, 1)));

		D3DXMatrixTransformation2D(&mMatrix, &D3DXVECTOR2(0, 0), 0, &D3DXVECTOR2(1.5f, 1.5f), NULL, 0, NULL);
		HR(RM->getSprite()->SetTransform(&mMatrix));
		HR(RM->getSprite()->Draw(*RM->getTexture(21), NULL, &D3DXVECTOR3(0, 0, 0), &D3DXVECTOR3(5, 940, 0), D3DXCOLOR(1, 1, 1, 1)));

	}
	if (DisplayHud)
		radar->Draw();

	if (!PauseClearToDraw && !ScoreClearToDraw)
		DrawCrossHair();

	//ScoreBoard
	if (ScoreClearToDraw)
	{
		//Text On Screen

		//Render the ScoreBoard
		DrawScoreBoard();

		for (UINT i = 0; i < texts.size(); i++)
			RM->getFont(texts[i].getFont())->DrawTextA(0, texts[i].getText().c_str(), -1, &texts[i].getRect(), DT_CENTER | DT_VCENTER, D3DCOLOR_XRGB(255, 255, 255));
	}

	//Pause Menu
	if (PauseClearToDraw && !SettingsClearToDraw)
	{
		for (UINT i = 0; i < buttons.size(); i++)
		{
			D3DXMatrixTransformation2D(&mMatrix, NULL, 0.0, &buttons[i].getScale(), &(buttons[i].getPos() + buttons[i].getHalfExtents()), 0, &buttons[i].getPos());
			HR(RM->getSprite()->SetTransform(&mMatrix));
			HR(RM->getSprite()->Draw(*RM->getTexture(buttons[i].getID()), &buttons[i].getRect(), NULL, NULL, D3DXCOLOR(1, 1, 1, 1)));
		}
	}

	//Settings Menu
	if (SettingsClearToDraw)
	{
		//Text On Screen
		for (UINT i = 0; i < texts.size(); i++)
			RM->getFont(texts[i].getFont())->DrawTextA(0, texts[i].getText().c_str(), -1, &texts[i].getRect(), DT_CENTER | DT_VCENTER, D3DCOLOR_XRGB(255, 255, 255));

		//Buttons
		for (UINT i = 0; i < buttons.size(); i++)
		{
			D3DXMatrixTransformation2D(&mMatrix, NULL, 0.0, &buttons[i].getScale(), NULL, 0, NULL);
			RM->getSprite()->SetTransform(&mMatrix);
			RM->getSprite()->Draw(*RM->getTexture(buttons[i].getID()), &buttons[i].getRect(), NULL, &D3DXVECTOR3(buttons[i].getPos().x, buttons[i].getPos().y, 0), 0xFFFFFFFF);
		}

		//Sliders
		for (UINT i = 0; i < sliders.size(); i++)
		{
			D3DXMatrixTransformation2D(&mMatrix, NULL, (float)0.0, &D3DXVECTOR2((float)1, (float)1), &(sliders[i].getPos() + D3DXVECTOR2((float)(RM->getTextureX(sliders[i].getID()) / 2), (float)(RM->getTextureY(sliders[i].getID()) / 2))), (float)0, &sliders[i].getPos());
			RM->getSprite()->SetTransform(&mMatrix);
			RM->getSprite()->Draw(*RM->getTexture(sliders[i].getID()), NULL, NULL, NULL, 0xFFFFFFFF);

			D3DXMatrixTransformation2D(&mMatrix, NULL, 0.0, &D3DXVECTOR2(1, 1), &(sliders[i].getButton().getPos() + sliders[i].getButton().getHalfExtents()), 0, &sliders[i].getButton().getPos());
			RM->getSprite()->SetTransform(&mMatrix);
			RM->getSprite()->Draw(*RM->getTexture(sliders[i].getButton().getID()), &sliders[i].getButton().getRect(), NULL, NULL, 0xFFFFFFFF);
		}

		//Check Boxes
		for (UINT i = 0; i < cboxes.size(); i++)
		{
			D3DXMatrixTransformation2D(&mMatrix, NULL, (float)0.0, &D3DXVECTOR2((float).6, (float).6), &(cboxes[i].getPos() + cboxes[i].getHalfExtents()), 0, &cboxes[i].getPos());
			RM->getSprite()->SetTransform(&mMatrix);
			RM->getSprite()->Draw(*RM->getTexture(cboxes[i].getID()), &cboxes[i].getRect(), NULL, NULL, 0xFFFFFFFF);
		}
	}

	HR(RM->getSprite()->End());

}

void HUD::DrawCrossHair()
{
	//Draw the Crosshair
	D3DXVECTOR2 temp[2] = { D3DXVECTOR2(float(g_D3DAPP->getWidth() / 2), float((g_D3DAPP->getHeight() / 2) - 15)), D3DXVECTOR2(float(g_D3DAPP->getWidth() / 2), float((g_D3DAPP->getHeight() / 2) + 15)) };

	mLine->SetWidth(2);
	mLine->SetAntialias(false);
	mLine->SetGLLines(true);

	HR(mLine->Begin());
	HR(mLine->Draw(temp, 2, D3DCOLOR_XRGB(150, 0, 0)));
	HR(mLine->End());

	D3DXVECTOR2 temp2[2] = { D3DXVECTOR2(float((g_D3DAPP->getWidth() / 2) - 15), float(g_D3DAPP->getHeight() / 2)), D3DXVECTOR2(float((g_D3DAPP->getWidth() / 2) + 15), float(g_D3DAPP->getHeight() / 2)) };

	mLine->SetWidth(2);
	mLine->SetAntialias(false);
	mLine->SetGLLines(true);

	HR(mLine->Begin());
	HR(mLine->Draw(temp2, 2, D3DCOLOR_XRGB(150, 0, 0)));
	HR(mLine->End());
}

void HUD::UpdateHealth(float health)
{
	mHealthBarRect.right = (LONG)(512 * (health / 100));
}

void HUD::UpdateRecharge(float recharge)
{

	mRechargeMeterRect.right = (LONG)(512 * (recharge / 100));

}

void HUD::ReplenishRecharge(double amount)
{
	mRechargeMeterRect.right = (LONG)(512 * (amount / 100));
}

void HUD::ConstructPause()
{
	ShowCursor(TRUE);
	buttons.push_back(Button(4, D3DXVECTOR2((float)g_D3DAPP->getWidth() / 2 - 256, 350), "ResumeButton"));
	buttons.push_back(Button(5, D3DXVECTOR2((float)g_D3DAPP->getWidth() / 2 - 256, 600), "OptionsButton"));
	buttons.push_back(Button(6, D3DXVECTOR2((float)g_D3DAPP->getWidth() / 2 - 256, 850 ), "QuitButton"));
}

void HUD::ConstructSettings()
{
	ShowCursor(TRUE);
	//Sliders
	sliders.push_back(Slider(Button(13, D3DXVECTOR2((float)(g_D3DAPP->getWidth() / 3 - (RM->getTextureX(7) + 20)), 600), "Slider"), 12,
		D3DXVECTOR2((float)(g_D3DAPP->getWidth() / 4) + 230, 600)));
	sliders[0].setSlidePosition(SET->getFXVolume());

	sliders.push_back(Slider(Button(13, D3DXVECTOR2((float)(g_D3DAPP->getWidth() / 3 - (RM->getTextureX(7) + 20)), 800), "Slider"), 12,
		D3DXVECTOR2((float)(g_D3DAPP->getWidth() / 4) + 230, 800)));
	sliders[1].setSlidePosition(SET->getMusicVolume());

	sliders.push_back(Slider(Button(13, D3DXVECTOR2((float)(g_D3DAPP->getWidth() / 3 - (RM->getTextureX(7) + 20)), 1100), "Slider"), 12,
		D3DXVECTOR2((float)(g_D3DAPP->getWidth() / 4) + 230, 1000)));
	sliders[2].setSlidePosition(SET->getSensitivity() / 20);
	//*******************************************************

	//Buttons
	buttons.push_back(Button(10, D3DXVECTOR2((float)((g_D3DAPP->getWidth() / 2) - (RM->getTextureX(5) - 20)), (float)1200), "SettingstoMain"));
	buttons.push_back(Button(11, D3DXVECTOR2((float)((g_D3DAPP->getWidth() / 2) + 20), (float)1200), "SettingstoSave"));
	//*******************************************************

	//CheckBoxes
	cboxes.push_back(CheckBox(14, D3DXVECTOR2((float)(g_D3DAPP->getWidth() / 2 + 300), (float)600)));
	cboxes.push_back(CheckBox(14, D3DXVECTOR2((float)(g_D3DAPP->getWidth() / 2 + 300), (float)800)));
	cboxes.push_back(CheckBox(14, D3DXVECTOR2((float)(g_D3DAPP->getWidth() / 2 + 300), (float)1000)));
	cboxes[0].setChecked(SET->getController());
	cboxes[1].setChecked(SET->getInverted());
	cboxes[2].setChecked(SET->getThird());
	//******************************************************

	//Texts
	texts.push_back(Text(0, D3DXVECTOR2((float)(g_D3DAPP->getWidth() / 2 + 200), (float)545), "Controller"));
	texts.push_back(Text(0, D3DXVECTOR2((float)(g_D3DAPP->getWidth() / 2 + 230), (float)745), "Inverted"));
	texts.push_back(Text(0, D3DXVECTOR2((float)(g_D3DAPP->getWidth() / 3) + 140, 545), "FX Volume"));
	texts.push_back(Text(0, D3DXVECTOR2((float)(g_D3DAPP->getWidth() / 3) + 120, 745), "Music Volume"));
	texts.push_back(Text(0, D3DXVECTOR2((float)(g_D3DAPP->getWidth() / 3) + 130, 945), "Sensitivity"));
	texts.push_back(Text(0, D3DXVECTOR2((float)(g_D3DAPP->getWidth() / 2 + 180), (float)945), "Third Person"));
	texts.push_back(Text(1, D3DXVECTOR2((float)(g_D3DAPP->getWidth() / 2 - 300), (float)200), "Settings"));
	//******************************************************
}

void HUD::ConstructScoreBoard()
{
	texts.push_back(Text(1, D3DXVECTOR2((float)250, (float)50), "Score Board"));
	texts.push_back(Text(0, D3DXVECTOR2((float)g_D3DAPP->getWidth() / 4, (float)g_D3DAPP->getHeight() / 5), " Dead"));
	texts.push_back(Text(0, D3DXVECTOR2(float((g_D3DAPP->getWidth() / 4) + (g_D3DAPP->getWidth() / 8)), float(g_D3DAPP->getHeight() / 5)), "Player"));
	texts.push_back(Text(0, D3DXVECTOR2(float(((g_D3DAPP->getWidth() / 4) * 3) - (g_D3DAPP->getWidth() / 8)), float(g_D3DAPP->getHeight() / 5)), " Kills"));
	texts.push_back(Text(0, D3DXVECTOR2(float((g_D3DAPP->getWidth() / 4) + (g_D3DAPP->getWidth() / 8)), float(g_D3DAPP->getHeight() / 4)), "Player 1"));
}

void HUD::DestructPause()
{
	buttons.clear();
	ShowCursor(FALSE);
}

void HUD::DestructSettings()
{
	sliders.clear();
	buttons.clear();
	cboxes.clear();
	texts.clear();
}

void HUD::DestructScoreBoard()
{
	texts.clear();
}

void HUD::DrawScoreBoard()
{
	//Line 1
	D3DXVECTOR2 temp[2] = { D3DXVECTOR2(float(g_D3DAPP->getWidth() / 4), float(g_D3DAPP->getHeight() / 4)), D3DXVECTOR2(float(g_D3DAPP->getWidth() / 4), float((g_D3DAPP->getHeight() / 4) * 3)) };

	mLine->SetWidth(2);
	mLine->SetAntialias(false);
	mLine->SetGLLines(true);

	HR(mLine->Begin());
	HR(mLine->Draw(temp, 2, D3DCOLOR_XRGB(255, 255, 255)));
	HR(mLine->End());

	//Line 2
	D3DXVECTOR2 temp2[2] = { D3DXVECTOR2(float((g_D3DAPP->getWidth() / 4)) * 3, float(g_D3DAPP->getHeight() / 4)), D3DXVECTOR2(float((g_D3DAPP->getWidth() / 4) * 3), float((g_D3DAPP->getHeight() / 4) * 3)) };

	mLine->SetWidth(2);
	mLine->SetAntialias(false);
	mLine->SetGLLines(true);

	HR(mLine->Begin());
	HR(mLine->Draw(temp2, 2, D3DCOLOR_XRGB(255, 255, 255)));
	HR(mLine->End());

	//Line 3
	D3DXVECTOR2 temp3[2] = { D3DXVECTOR2(float(g_D3DAPP->getWidth() / 4), float(g_D3DAPP->getHeight() / 4)), D3DXVECTOR2(float((g_D3DAPP->getWidth() / 4) * 3), float(g_D3DAPP->getHeight() / 4)) };

	mLine->SetWidth(2);
	mLine->SetAntialias(false);
	mLine->SetGLLines(true);

	HR(mLine->Begin());
	HR(mLine->Draw(temp3, 2, D3DCOLOR_XRGB(255, 255, 255)));
	HR(mLine->End());

	//Line 4
	D3DXVECTOR2 temp4[2] = { D3DXVECTOR2(float(g_D3DAPP->getWidth() / 4), float((g_D3DAPP->getHeight() / 4) * 3)), D3DXVECTOR2(float((g_D3DAPP->getWidth() / 4) * 3), float((g_D3DAPP->getHeight() / 4) * 3)) };

	mLine->SetWidth(2);
	mLine->SetAntialias(false);
	mLine->SetGLLines(true);

	HR(mLine->Begin());
	HR(mLine->Draw(temp4, 2, D3DCOLOR_XRGB(255, 255, 255)));
	HR(mLine->End());

	//Line 5
	D3DXVECTOR2 temp5[2] = { D3DXVECTOR2(float((g_D3DAPP->getWidth() / 4) + (g_D3DAPP->getWidth() / 8)), float(g_D3DAPP->getHeight() / 4)), D3DXVECTOR2(float((g_D3DAPP->getWidth() / 4) + (g_D3DAPP->getWidth() / 8)), float((g_D3DAPP->getHeight() / 4) * 3)) };

	mLine->SetWidth(2);
	mLine->SetAntialias(false);
	mLine->SetGLLines(true);

	HR(mLine->Begin());
	HR(mLine->Draw(temp5, 2, D3DCOLOR_XRGB(255, 255, 255)));
	HR(mLine->End());

	//Line 6
	D3DXVECTOR2 temp6[2] = { D3DXVECTOR2(float(((g_D3DAPP->getWidth() / 4) * 3) - (g_D3DAPP->getWidth() / 8)), float(g_D3DAPP->getHeight() / 4)), D3DXVECTOR2(float(((g_D3DAPP->getWidth() / 4) * 3) - (g_D3DAPP->getWidth() / 8)), float((g_D3DAPP->getHeight() / 4) * 3)) };

	mLine->SetWidth(2);
	mLine->SetAntialias(false);
	mLine->SetGLLines(true);

	HR(mLine->Begin());
	HR(mLine->Draw(temp6, 2, D3DCOLOR_XRGB(255, 255, 255)));
	HR(mLine->End());

	//Line 7
	D3DXVECTOR2 temp7[2] = { D3DXVECTOR2(float(g_D3DAPP->getWidth() / 4), float((g_D3DAPP->getHeight() / 4) + (g_D3DAPP->getHeight() / 16))), D3DXVECTOR2(float((g_D3DAPP->getWidth() / 4) * 3), float((g_D3DAPP->getHeight() / 4) + (g_D3DAPP->getHeight() / 16))) };

	mLine->SetWidth(2);
	mLine->SetAntialias(false);
	mLine->SetGLLines(true);

	HR(mLine->Begin());
	HR(mLine->Draw(temp7, 2, D3DCOLOR_XRGB(255, 255, 255)));
	HR(mLine->End());

	//Line 8
	D3DXVECTOR2 temp8[2] = { D3DXVECTOR2(float(g_D3DAPP->getWidth() / 4), float((g_D3DAPP->getHeight() / 4) + ((g_D3DAPP->getHeight() / 16)) * 2)), D3DXVECTOR2(float((g_D3DAPP->getWidth() / 4) * 3), float((g_D3DAPP->getHeight() / 4) + ((g_D3DAPP->getHeight() / 16)) * 2)) };

	mLine->SetWidth(2);
	mLine->SetAntialias(false);
	mLine->SetGLLines(true);

	HR(mLine->Begin());
	HR(mLine->Draw(temp8, 2, D3DCOLOR_XRGB(255, 255, 255)));
	HR(mLine->End());

	//Line 9
	D3DXVECTOR2 temp9[2] = { D3DXVECTOR2(float(g_D3DAPP->getWidth() / 4), float((g_D3DAPP->getHeight() / 4) + ((g_D3DAPP->getHeight() / 16)) * 3)), D3DXVECTOR2(float((g_D3DAPP->getWidth() / 4) * 3), float((g_D3DAPP->getHeight() / 4) + ((g_D3DAPP->getHeight() / 16)) * 3)) };

	mLine->SetWidth(2);
	mLine->SetAntialias(false);
	mLine->SetGLLines(true);

	HR(mLine->Begin());
	HR(mLine->Draw(temp9, 2, D3DCOLOR_XRGB(255, 255, 255)));
	HR(mLine->End());

	//Line 10
	D3DXVECTOR2 temp10[2] = { D3DXVECTOR2(float(g_D3DAPP->getWidth() / 4), float((g_D3DAPP->getHeight() / 4) + ((g_D3DAPP->getHeight() / 16)) * 4)), D3DXVECTOR2(float((g_D3DAPP->getWidth() / 4) * 3), float((g_D3DAPP->getHeight() / 4) + ((g_D3DAPP->getHeight() / 16)) * 4)) };

	mLine->SetWidth(2);
	mLine->SetAntialias(false);
	mLine->SetGLLines(true);

	HR(mLine->Begin());
	HR(mLine->Draw(temp10, 2, D3DCOLOR_XRGB(255, 255, 255)));
	HR(mLine->End());

	//Line 11
	D3DXVECTOR2 temp11[2] = { D3DXVECTOR2(float(g_D3DAPP->getWidth() / 4), float((g_D3DAPP->getHeight() / 4) + ((g_D3DAPP->getHeight() / 16)) * 5)), D3DXVECTOR2(float((g_D3DAPP->getWidth() / 4) * 3), float((g_D3DAPP->getHeight() / 4) + ((g_D3DAPP->getHeight() / 16)) * 5)) };

	mLine->SetWidth(2);
	mLine->SetAntialias(false);
	mLine->SetGLLines(true);

	HR(mLine->Begin());
	HR(mLine->Draw(temp11, 2, D3DCOLOR_XRGB(255, 255, 255)));
	HR(mLine->End());

	//Line 12
	D3DXVECTOR2 temp12[2] = { D3DXVECTOR2(float(g_D3DAPP->getWidth() / 4), float((g_D3DAPP->getHeight() / 4) + ((g_D3DAPP->getHeight() / 16)) * 6)), D3DXVECTOR2(float((g_D3DAPP->getWidth() / 4) * 3), float((g_D3DAPP->getHeight() / 4) + ((g_D3DAPP->getHeight() / 16)) * 6)) };

	mLine->SetWidth(2);
	mLine->SetAntialias(false);
	mLine->SetGLLines(true);

	HR(mLine->Begin());
	HR(mLine->Draw(temp12, 2, D3DCOLOR_XRGB(255, 255, 255)));
	HR(mLine->End());

	//Line 13
	D3DXVECTOR2 temp13[2] = { D3DXVECTOR2(float(g_D3DAPP->getWidth() / 4), float((g_D3DAPP->getHeight() / 4) + ((g_D3DAPP->getHeight() / 16)) * 7)), D3DXVECTOR2(float((g_D3DAPP->getWidth() / 4) * 3), float((g_D3DAPP->getHeight() / 4) + ((g_D3DAPP->getHeight() / 16)) * 7)) };

	mLine->SetWidth(2);
	mLine->SetAntialias(false);
	mLine->SetGLLines(true);

	HR(mLine->Begin());
	HR(mLine->Draw(temp13, 2, D3DCOLOR_XRGB(255, 255, 255)));
	HR(mLine->End());
}
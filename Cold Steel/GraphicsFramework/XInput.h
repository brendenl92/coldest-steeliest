#pragma once

#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <XInput.h>
#include "DXUtil.h"

#pragma comment(lib, "XINPUT9_1_0.LIB")

#define XBOX XBOXController::Instance()

class XBOXController
{
private:
	XINPUT_STATE controllerState;
	XINPUT_VIBRATION Vibration;
	XBOXController();
	int controllerNum;
	float vibEndTime;
public:
	static XBOXController* Instance();
	XINPUT_STATE GetState();
	bool IsConnected();
	void update();
	void Vibrate(int leftVal, int rightVal, float durration);
};
//weapon manager.cpp
#include "WeaponManager.h"

///I will try to have this setup to where it only needs to be called once
void WeaponManager::SetTermsRules()
{
	///Here I'll add my variables...
	///Weapon Distances
	FuzzyVariable& _RifleDistance		= fm->CreateFLV("_RifleDistance");
	FuzzyVariable& _RocketDistance		= fm->CreateFLV("_RocketDistance");
	FuzzyVariable& _PistolDistance		= fm->CreateFLV("_PistolDistance");

	///Weapon Ammo
	FuzzyVariable& _RifleAmmo			= fm->CreateFLV("_RifleAmmo");
	FuzzyVariable& _RocketAmmo			= fm->CreateFLV("_RocketAmmo");
	FuzzyVariable& _PistolAmmo			= fm->CreateFLV("_PistolAmmo");

	///Desirability
	FuzzyVariable& _Desirability		= fm->CreateFLV("_Desirability");

	///Here I will add my weapon terms...
	///Close range
	FzSet& _RifleClose					= _RifleDistance.addLeftShoulderSet("Close", 15, 0, 30);
	FzSet& _RocketClose					= _RocketDistance.addLeftShoulderSet("Close", 10, 0, 20);
	FzSet& _PistolClose					= _PistolDistance.addLeftShoulderSet("Close", 7, 0, 14);

	///Mid range
	FzSet& _RifleMid					= _RifleDistance.addTriangularSet("Mid", 30, 15, 45);
	FzSet& _RocketMid					= _RocketDistance.addTriangularSet("Mid", 20, 10, 30);
	FzSet& _PistolMid					= _PistolDistance.addTriangularSet("Mid", 14, 7, 21);

	///Long Range
	FzSet& _RifleFar					= _RifleDistance.addRightShoulderSet("Far", 45, 30, 60);
	FzSet& _RocketFar					= _RocketDistance.addRightShoulderSet("Far", 30, 20, 40);
	FzSet& _PistolFar					= _PistolDistance.addRightShoulderSet("Far", 21, 14, 28);

	///Low Ammo
	FzSet& _RifleLow					= _RifleAmmo.addLeftShoulderSet("Low", 5, 0, 10);
	FzSet& _RocketLow					= _RocketAmmo.addLeftShoulderSet("Low", 2, 0, 5);
	FzSet& _PistolLow					= _PistolAmmo.addLeftShoulderSet("Low", 7, 0, 15);

	///Ammo
	FzSet& _RifleSome					= _RifleAmmo.addTriangularSet("Mid", 10, 5, 15);
	FzSet& _RocketSome					= _RocketAmmo.addTriangularSet("Mid", 5, 2, 8);
	FzSet& _PistolSome					= _PistolAmmo.addTriangularSet("Mid", 15, 7, 23);

	///High Ammo
	FzSet& _RifleHigh					= _RifleAmmo.addRightShoulderSet("High", 15, 10, 20);
	FzSet& _RocketHigh					= _RocketAmmo.addRightShoulderSet("High", 8, 5, 10);
	FzSet& _PistolHigh					= _PistolAmmo.addRightShoulderSet("High", 23, 15, 30);

	///Undesirable
	FzSet& _Undesirable					= _Desirability.addLeftShoulderSet("Undesirable", 25, 0, 50);

	///Desirable
	FzSet& _Desirable					= _Desirability.addTriangularSet("Desirable", 50, 25, 75);

	///Very Desirable
	FzSet& _VeryDesirable				= _Desirability.addRightShoulderSet("VeryDesirable", 75, 50, 100);

	///Here I will set my Rules...
	///Rifle Close Range
	//fm->AddRule(_RifleClose, _Undesirable);
	//fm->AddRule(_RifleLow, _Undesirable);
	fm->AddRule(FzAND(_RifleClose, _RifleLow), _Undesirable);			
	fm->AddRule(FzAND(_RifleClose, _RifleSome), _Undesirable);				
	fm->AddRule(FzAND(_RifleClose, _RifleHigh), _Undesirable);			

	///Rocket Close Range
	//fm->AddRule(_RocketClose, _Undesirable);
	//fm->AddRule(_RocketLow, _Undesirable);
	fm->AddRule(FzAND(_RocketClose, _RocketLow), _Undesirable);
	fm->AddRule(FzAND(_RocketClose, _RocketSome), _Undesirable);
	fm->AddRule(FzAND(_RocketClose, _RocketHigh), _Undesirable);
	
	///Pistol Close Range
	//fm->AddRule(_PistolClose, _Desirable);
	//fm->AddRule(_PistolLow, _Undesirable);
	fm->AddRule(FzAND(_PistolClose, _PistolLow), _VeryDesirable);
	fm->AddRule(FzAND(_PistolClose, _PistolSome), _VeryDesirable);
	fm->AddRule(FzAND(_PistolClose, _PistolHigh), _VeryDesirable);

	///Rifle Mid Range
	//fm->AddRule(_RifleMid, _Desirable);
	//fm->AddRule(_RifleSome, _Desirable);
	fm->AddRule(FzAND(_RifleMid, _RifleLow), _Undesirable);
	fm->AddRule(FzAND(_RifleMid, _RifleSome), _Undesirable);
	fm->AddRule(FzAND(_RifleMid, _RifleHigh), _Desirable);

	///Rocket Mid Range
	//fm->AddRule(_RocketMid, _VeryDesirable);
	//fm->AddRule(_RocketSome, _Desirable);
	fm->AddRule(FzAND(_RocketMid, _RocketLow), _Desirable);
	fm->AddRule(FzAND(_RocketMid, _RocketSome), _VeryDesirable);
	fm->AddRule(FzAND(_RocketMid, _RocketHigh), _VeryDesirable);

	///Pistol Mid Range
	//fm->AddRule(_PistolMid, _Desirable);
	//fm->AddRule(_PistolSome, _Desirable);
	fm->AddRule(FzAND(_PistolMid, _PistolLow), _Undesirable);
	fm->AddRule(FzAND(_PistolMid, _PistolSome), _Undesirable);
	fm->AddRule(FzAND(_PistolMid, _PistolHigh), _Desirable);

	///Rifle Long Range 
	//fm->AddRule(_RifleFar, _VeryDesirable);
	//fm->AddRule(_RifleHigh, _VeryDesirable);
	fm->AddRule(FzAND(_RifleFar, _RifleLow), _Desirable);
	fm->AddRule(FzAND(_RifleFar, _RifleSome), _VeryDesirable);
	fm->AddRule(FzAND(_RifleFar, _RifleHigh), _VeryDesirable);

	///Rocket Long Range
	//fm->AddRule(_RocketFar, _Desirable);
	//fm->AddRule(_RocketHigh, _VeryDesirable);
	fm->AddRule(FzAND(_RocketFar, _RocketLow), _Undesirable);
	fm->AddRule(FzAND(_RocketFar, _RocketSome), _Undesirable);
	fm->AddRule(FzAND(_RocketFar, _RocketHigh), _Desirable);

	///Pistol Long Range
	//fm->AddRule(_PistolFar, _Undesirable);
	//fm->AddRule(_PistolHigh, _VeryDesirable);
	fm->AddRule(FzAND(_PistolFar, _PistolLow), _Undesirable);
	fm->AddRule(FzAND(_PistolFar, _PistolSome), _Undesirable);
	fm->AddRule(FzAND(_PistolFar, _PistolHigh), _Desirable);
}

///Calculate the Rifles DOM and returns the value;
double WeaponManager::CalcRifle(double distance, double ammo)
{
	double riftemp;

	///Fuzzify
	fm->Fuzzify("_RifleDistance", distance);
	fm->Fuzzify("_RifleAmmo", ammo);

	///Defuzzify
	riftemp = fm->Defuzzify("_Desirability");
	
	return riftemp;
}

///Calculate the Rockests DOM and returns the value;
double WeaponManager::CalcRocket(double distance, double ammo)
{
	double roctemp;

	///Fuzzify
	fm->Fuzzify("_RocketDistance", distance);
	fm->Fuzzify("_RocketAmmo", ammo);

	///Defuzzify
	roctemp = fm->Defuzzify("_Desirability");

	return roctemp;
}

///Calculate the Pistols DOM and returns the value;
double WeaponManager::CalcPistol(double distance, double ammo)
{
	double pistemp;

	///Fuzzify
	fm->Fuzzify("_PistolDistance", distance);
	fm->Fuzzify("_PistolAmmo", ammo);

	///Defuzzify
	pistemp = fm->Defuzzify("_Desirability");

	return pistemp;
}

int WeaponManager::Calculate(double distance, double rifleammo, double rocketammo, double pistolammo)
{
	///Setup terms and rules
	SetTermsRules();

	///Fuzzy Logic Module Prcess
	m_wPistolDOM	= CalcPistol(distance, pistolammo);
	m_wRocketDOM	= CalcRocket(distance, rocketammo);
	m_wRifleDOM		= CalcRifle(distance, rifleammo);

	///based on which DOM is the greatest out of the three
	///that will be the weapon selected
	if(m_wPistolDOM > m_wRifleDOM && m_wPistolDOM >= m_wRocketDOM)
		currentWeaponID = m_wSet->GetPistolID();
	if(m_wRocketDOM >= m_wRifleDOM && m_wRocketDOM > m_wPistolDOM)
		currentWeaponID = m_wSet->GetRocketID();
	if(m_wRifleDOM > m_wRocketDOM && m_wRifleDOM > m_wPistolDOM)
		currentWeaponID = m_wSet->GetRifleID();
	

	///return the weapon with the best DOM
	return currentWeaponID;
}
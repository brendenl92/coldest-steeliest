#pragma once

#include "ASM.h"

#define Atk Attack::Instance()

class Attack : public BaseState
{
private:
	static Attack* s_Instance;
	int numMsg1, numMsg2, numMsg3;

private:
	Attack();

public:
	static Attack* Instance();

public:
	Attack(ASM* fsm, int id);
	~Attack();

public:
	void Enter();					//enters into attack state
	void Execute(double dt);		//executes the code in attack state, steering forces will go here
	void Exit();					//will exit the attack state
	void HandleMsg(Mail message);	//handles msgs sent to the attack state
	void ShutDown();
};
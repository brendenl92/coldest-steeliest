#include "MainMenuBackground.h"
#include "D3DAPP.h"


MainMenuBackground::MainMenuBackground()
{
	string meshes[3];
	meshes[0] = "../media/models/skysphere2.X";
	meshes[1] = "../media/models/tank.X";
	meshes[2] = "../media/models/mpmap2.X";

	RM->ImportMeshes(meshes, 3, 0);

	D3DXQUATERNION temp;
	D3DXQuaternionIdentity(&temp);
	
	//Entities.push_back(new Entity(0, D3DXVECTOR3(0, 0, 0), D3DXVECTOR3(6, 6, 6), temp));	
	Entities.push_back(GraphicsEntity(1, D3DXVECTOR3(0, 0, 0), D3DXVECTOR3(8, 8, 8), temp));
	Entities.push_back(GraphicsEntity(2, D3DXVECTOR3(0, -39, -50), D3DXVECTOR3(1, 1, 1), temp));
	
	ID3DXBuffer* errors = 0;

	D3DXCreateEffectFromFileA(g_D3dDevice, "../shaders/PhongShader.fx", 0, 0, D3DXFX_NOT_CLONEABLE | D3DXSHADER_SKIPOPTIMIZATION | D3DXSHADER_DEBUG, 0, &effect, &errors);
	if (errors)
		cout << (char*)errors->GetBufferPointer();
	if (errors)
		errors->Release();

}

void MainMenuBackground::onLostDevice()
{
	effect->OnLostDevice();
}
void MainMenuBackground::onResetDevice()
{
	effect->OnResetDevice();
}

MainMenuBackground::~MainMenuBackground()
{
	int hi = 0; 
}

void MainMenuBackground::buildProjMat()
{
	D3DXMatrixPerspectiveFovLH(&m_ProjMat, (float)(D3DX_PI * 0.25f), (float)(g_D3DAPP->getWidth() / g_D3DAPP->getHeight()), 1.0f, 5000.0f);
}

void MainMenuBackground::update(float dt)
{
	CAM->menuUpdate(dt);
	buildProjMat();
}

void MainMenuBackground::render()
{
	
	HR(effect->SetVector("LightDiffuse", (D3DXVECTOR4*)&D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f)));
	HR(effect->SetVector("LightSpecular", (D3DXVECTOR4*)&D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f)));
	HR(effect->SetValue("LightDirection", D3DXVECTOR3(0, -1, 0), sizeof(D3DXVECTOR3)));

	D3DXCOLOR spec(1.0f, 1.0f, 1.0f, 1.0f);
	D3DXVECTOR3 dirW(0, -1, 0);
	D3DXVECTOR3 pos(0, 100, 0);

	UINT numPasses;

	D3DXMATRIX VP = CAM->getViewMat() * m_ProjMat;
	D3DXMATRIX invtran;
	
	for(UINT k = 0; k < Entities.size(); k++)
	{
		{
			HR(effect->SetMatrix("WVP", &(Entities[k].getTransform() *VP)));
			HR(effect->SetMatrix("World", &Entities[k].getTransform()));
			D3DXMatrixInverse(&invtran, 0, &Entities[k].getTransform());
			D3DXMatrixTranspose(&invtran, &invtran);
			HR(effect->SetMatrix("WorldInvTrans", &invtran));
			HR(effect->SetValue("EyePosW", &CAM->getPos(), sizeof(D3DXVECTOR3)));
			HR(effect->SetVector("MatAmbient", (D3DXVECTOR4*)&RM->getMesh(Entities[k].getMeshID())->getMaterial()->Ambient));
			HR(effect->SetVector("MatDiffuse", (D3DXVECTOR4*)&RM->getMesh(Entities[k].getMeshID())->getMaterial()->Diffuse));
			HR(effect->SetVector("MatSpecular", (D3DXVECTOR4*)&RM->getMesh(Entities[k].getMeshID())->getMaterial()->Specular));
			HR(effect->SetFloat("MatPower", RM->getMesh(Entities[k].getMeshID())->getMaterial()->Power));
		}

		for(int j = 0; j < RM->getMesh(Entities[k].getMeshID())->getNumMaterials(); j++)
		{

			if (RM->getMesh(Entities[k].getMeshID())->getTexture(j) != NULL)
				effect->SetTexture("Tex", RM->getMesh(Entities[k].getMeshID())->getTexture(j));

			HR(effect->Begin(&numPasses, 0));

			HR(effect->BeginPass(0));
			HR(RM->getMesh(Entities[k].getMeshID())->getMesh()->DrawSubset(j));
			HR(effect->EndPass());

			HR(effect->End());
		}
	}
}
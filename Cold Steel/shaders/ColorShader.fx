uniform extern float4x4 WVP;
uniform extern texture tex;

struct VSIn
{
	float4 position : POSITION0;
	float3 tex0 : TEXCOORD0;
};

struct VSOut
{

	float4 pos : POSITION0;
	float2 tex0 : TEXCOORD0;
};

sampler LaserSampler = sampler_state
{
	Texture = <tex>;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	MipFilter = LINEAR;
	AddressU = CLAMP;
	AddressV = CLAMP;
};

VSOut LaserVS(VSIn i)
{
	VSOut o = (VSOut)0;

	o.pos = mul(i.position, WVP);
	o.tex0 = i.tex0;

	return o;
}

float4 LaserPS(VSOut input) : COLOR
{
	return float4(tex2D(LaserSampler, input.tex0).rgb, 1);
}

technique main
{
	pass p0
	{
		VertexShader = compile vs_2_0 LaserVS();
		PixelShader = compile ps_2_0 LaserPS();
		CullMode = none;
		ZWriteEnable = true;
		ZFunc = lessequal;

	}

};
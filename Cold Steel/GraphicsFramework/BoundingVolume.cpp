#include "BoundingVolume.h"


//////////////////////////////////
//Bounding Volume Node
/////////////////////////////////
//Ctor
BoundingVolumeNode::BoundingVolumeNode(RigidBody* Body, BoundingVolume* Volume)
{ 
	mBody = Body;
	mVolume = Volume;
}

//DCTOR
BoundingVolumeNode::~BoundingVolumeNode()
{
	delete mVolume;
}



/////////////////////////////
//Bounding Volume
///////////////////////////
BoundingVolume::BoundingVolume(float Radius, UINT environment, V3 Halfsize)
{
	mHalfsize = Halfsize;
	this->environment = environment;
	if (D3DXVec3LengthSq(&Halfsize) > 0)
	{
		mRadius = sqrt((Halfsize.x*Halfsize.x) + (Halfsize.y*Halfsize.y) + (Halfsize.z*Halfsize.z));
		mShape = VCUBE;
	}
	else
	{
		mRadius = Radius;
		mShape = VSPHERE;
		Halfsize.x = Radius;
		Halfsize.y = Radius;
		Halfsize.z = Radius;
	}
}
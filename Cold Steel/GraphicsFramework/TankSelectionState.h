#pragma once

#include "State.h"
#include "D3DAPP.h"
#include "TankSelectBack.h"
#include "UIElements.h"
#include "Tank.h"

class TankSelectionState : public State
{
public:
	TankSelectionState();
	~TankSelectionState();
	static TankSelectionState* Instance();

	virtual void Initialize();
	virtual void UpdateScene(float dt);
	virtual void RenderScene();
	virtual void HandleInput(RAWINPUT* raw);

	virtual void OnResetDevice();
	virtual void OnLostDevice();
	virtual void Leave();

private:
	vector<Button> buttons;
	vector<Button2> HoverButtons;
	vector<Text> texts;
	vector<Image> images;

	TankSelectBack* back;

	int chosen;
	bool options;
	float selectPause;

	void ConstructSelection();
};


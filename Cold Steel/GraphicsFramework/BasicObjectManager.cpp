#include "BasicObjectManager.h"

BasicObjectManager::BasicObjectManager()
{

}

BasicObjectManager::~BasicObjectManager()
{
	for (UINT i = 0; i < spheres.size(); i++)
	{
		delete spheres[i];
	}
	spheres.clear();
}
BasicObjectManager* BasicObjectManager::instance()
{
	static BasicObjectManager* ptr = new BasicObjectManager();
	return ptr;
}

void BasicObjectManager::addSphere(D3DXVECTOR3 pos, float rad, UINT mesh)
{
	spheres.push_back(new Sphere(pos, rad, mesh));
}

Sphere* BasicObjectManager::getSpheres(UINT it)
{
	return spheres[it];
}
UINT BasicObjectManager::sphereVecSize()
{
	return spheres.size();
}
#pragma once

class FuzzySet
{
protected:
	//this will hold the DOM 
	double	m_DOM;

	//maximum value of set.
	double m_MaxRep;

public:
	FuzzySet() {}

	FuzzySet(double RepVal)
	{
		m_DOM		= 0.0;
		m_MaxRep	= RepVal;
	}

	virtual double	CalculateDOM(double val) const = 0;					//returns the DOM
	
	void			ORwithDOM(double val)								//fired by a rule, either the DOM has reached the limit or is the same as the sets exsisting m_DOM value
	{
		if(val > m_DOM)
			m_DOM = val;
	}

public:
	void			ClearDOM()
	{
		m_DOM = 0.0;
	}

	void			SetDOM(double val)
	{
		m_DOM = val;
	}

	void			SetRep(double val)
	{
		m_MaxRep = val;
	}

	double			GetDOM() const
	{
		return m_DOM;
	}

	double			GetRep() const
	{
		return m_MaxRep;
	}
};

class FuzzySet_Triangle : public FuzzySet
{
private:
	//values that define this sets shape
	double m_Peak;
	double m_Left;
	double m_Right;

public:
	FuzzySet_Triangle(double mid, double left, double right) : FuzzySet(mid)
	{
		m_Peak	= mid;
		m_Left	= left;
		m_Right = right;
	}

	//this method calculates the DOM 
	double CalculateDOM(double val) const;
};

class FuzzySet_LeftShoulder : public FuzzySet
{
private:
	double m_Peak;
	double m_Left;
	double m_Right;

public:
	FuzzySet_LeftShoulder(double mid, double left, double right) : FuzzySet(mid)
	{
		m_Peak	= mid;
		m_Left	= left;
		m_Right = right;
	}

	//this method calculates the DOM
	double CalculateDOM(double val) const;
};

class FuzzySet_RightShoulder : public FuzzySet
{
private:
	double m_Peak;
	double m_Left;
	double m_Right;

public:
	FuzzySet_RightShoulder(double mid, double left, double right) : FuzzySet(mid)
	{
		m_Peak	= mid;
		m_Left	= left;
		m_Right = right;
	}

	//this method calculates the DOM
	double CalculateDOM(double val) const;
};
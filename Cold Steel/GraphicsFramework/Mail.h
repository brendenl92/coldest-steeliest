#pragma once

#include <math.h>

enum Message{
	_None,
	_OnPatrol,
	_LockedOn,
	_FallingBack,
	_Calculating};

class Mail
{
public:
	int		m_Reciever, m_Sender;
	double	m_Delay;
	Message m_Msg;
	void*	m_ExtraInfo;

public:
	Mail(int r, int s, double d, int msg, void* info);
	~Mail()
	{
		m_Reciever = 0, m_Sender = 0, m_Delay = 0, m_Msg = (Message)0, m_ExtraInfo;
	}
};
#include "MapLoader.h"


MapLoader::MapLoader()
{
}


MapLoader::~MapLoader()
{
}

vector<MasterEntity*> MapLoader::loadMap(string map, vector<MasterEntity*> output)
{
	ifstream stream;
	stream.open(map);
	if (!stream.good())
	{
		MessageBox(NULL, "Cannot load world", "World load fail", MB_ICONWARNING);
		PostQuitMessage(0);
	}
	string input;
	string line;

	int inputNum = 0;

	D3DXVECTOR3 pos;
	D3DXVECTOR3 scale;
	D3DXVECTOR3 size;
	D3DXMATRIX rot;
	UINT MeshID;
	//	char option;

	rot._14 = 0;
	rot._24 = 0;
	rot._34 = 0;
	rot._44 = 1;
	rot._41 = 0;
	rot._42 = 0;
	rot._43 = 0;
	input.clear();
	while (!stream.eof())
	{
		getline(stream, input);
		if (input[0] != ' ' && input[0] != '\0')
			switch (inputNum)
		{
			case 0:
				pos.x = (float)atof(input.c_str());
				inputNum += 1;
				break;
			case 1:
				pos.y = (float)atof(input.c_str());
				inputNum += 1;
				break;
			case 2:
				pos.z = (float)atof(input.c_str());
				inputNum += 1;
				break;
			case 3:
				rot._11 = (float)atof(input.c_str());
				inputNum += 1;
				break;
			case 4:
				rot._12 = (float)atof(input.c_str());
				inputNum += 1;
				break;
			case 5:
				rot._13 = (float)atof(input.c_str());
				inputNum += 1;
				break;
			case 6:
				rot._21 = (float)atof(input.c_str());
				inputNum += 1;
				break;
			case 7:
				rot._22 = (float)atof(input.c_str());
				inputNum += 1;
				break;
			case 8:
				rot._23 = (float)atof(input.c_str());
				inputNum += 1;
				break;
			case 9:
				rot._31 = (float)atof(input.c_str());
				inputNum += 1;
				break;
			case 10:
				rot._32 = (float)atof(input.c_str());
				inputNum += 1;
				break;
			case 11:
				rot._33 = (float)atof(input.c_str());
				inputNum += 1;
				break;
			case 12:
				MeshID = (UINT)atoi(input.c_str());
				inputNum += 1;
				break;
			case 13:
				scale.x = (float)atof(input.c_str());
				inputNum += 1;
				break;
			case 14:
				scale.y = (float)atof(input.c_str());
				inputNum += 1;
				break;
			case 15:
				scale.z = (float)atof(input.c_str());
				inputNum += 1;
				break;
			case 16:
				size.x = (float)atof(input.c_str());
				inputNum += 1;
				break;
			case 17:
				size.y = (float)atof(input.c_str());
				inputNum += 1;
				break;
			case 18:
				size.z = (float)atof(input.c_str());
				inputNum = 0;
				output.push_back(new MasterEntity(PHYENABLED, MeshID, output.size(), pos + D3DXVECTOR3(0, 0, 0) , scale, rot, rot));
				cout << pos.x << " " << pos.y << " " << pos.z << " " << rot._11 << " " << rot._12 << " " << rot._13 << " " << rot._21 << " " << rot._22 << " " << rot._23 << " " <<
					rot._31 << " " << rot._32 << " " << rot._33 << " " << MeshID << " " << scale.x << " " << scale.y << " " << scale.z << " " << size.x << " " << size.y << " " <<
 					size.z << " " << " " << endl;
				break;

		}
		//cout << "Position: " << pos.x << ", " << pos.y << ", " << pos.z << endl;
		//cout << "Scale: " << scale.x << ", " << scale.y << ", " << scale.z << endl;
		//cout << "Rot: " << rot._11 << ", " << rot._12 << ", " << rot._13 << ", " << rot._14 << endl;
		//cout << "     " << rot._21 << ", " << rot._22 << ", " << rot._23 << ", " << rot._24 << endl;
		//cout << "     " << rot._31 << ", " << rot._32 << ", " << rot._33 << ", " << rot._34 << endl;
		//cout << "     " << rot._41 << ", " << rot._42 << ", " << rot._43 << ", " << rot._44 << endl;


	}
	stream.close();
	return output;
}
void MapLoader::saveMap(string map, vector<MasterEntity*> entities)
{
	ofstream stream;
	stream.open(map);
	if (!stream.good())
	{
		MessageBox(NULL, "Cannot load world", "World load fail", MB_ICONWARNING);
		PostQuitMessage(0);
	}
	D3DXVECTOR3 pos;
	D3DXVECTOR3 scale;
	D3DXVECTOR3 size;
	D3DXMATRIX rot;
	UINT MeshID;
	char option;

	for (UINT i = 0; i < entities.size(); i++)
	{
		pos = entities[i]->getPosition();
		scale = entities[i]->getScale();
		size = entities[i]->getScale();
		rot = entities[i]->getBaseRot();
		MeshID = entities[i]->getMeshID();
		option = entities[i]->getOption();
		stream << pos.x << " " << pos.y << " " << pos.z << " " << rot._11 << " " << rot._12 << " " << rot._13 << " " << rot._21 << " " << rot._22 << " " << rot._23 << " " <<
				  rot._31 << " " << rot._32 << " " << rot._33 << " " << MeshID << " " << scale.x << " " << scale.y << " " << scale.z << " " << size.x << " " << size.y << " " <<
				  size.z << " " << option << " " << endl;
	}
	cout << "Saved " << map << " successfully\n";
	stream.close();
}
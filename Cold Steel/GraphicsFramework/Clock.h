#pragma once

#include <Windows.h>

using namespace std;

#define CLK Clock::Instance()

class Clock
{
	_int64 cntsPerSec;
	_int64 prevTimeStamp;
	_int64 currTimeStamp;
	float secsPerCnt;
	float dt;
	float timeElapsed;

	Clock();
public:
	static Clock* Instance();
	void start();
	float end();
	void finish();
	void add(float in);
	float getTime();
	float getDT();
	~Clock();
};


#pragma once

#include "Radar.h"
#include "RawInput.h"
#include "UIElements.h"
#include <vector>

using namespace std;

#define Hud HUD::Instance()

class Button;
class MasterEntity;
class HUD
{
public:
	static HUD* Instance();
	HUD();
	~HUD();
	void UpdateHud(float dt, vector <MasterEntity*> EntityVec),
		Draw(),
		Initialize(),
		HandleInput(RAWINPUT* raw),
		ClearHud(),
		UpdateHealth(float health),
		UpdateRecharge(float recharge),
		ReplenishRecharge(double amount);
	inline bool isPauseEnabled(){ return PauseClearToDraw; }
	inline int GetRechargeRectRight(){ return mRechargeMeterRect.right; }
	bool isScoreEnabled(){ return ScoreClearToDraw; }

private:
	RECT mHealthBarRect,
		mRechargeMeterRect;

	LPDIRECT3DTEXTURE9 mHealthBar,
		mRechargeMeter,
		mRadar,
		mPauseMenu,
		mResumeButton,
		mOptionsButton,
		mQuitButton;

	D3DXMATRIX mMatrix;

	bool PauseClearToDraw,
		ScoreClearToDraw,
		SettingsClearToDraw,
		DisplayHud;

	vector <Button> buttons;
	vector <Slider> sliders;
	vector <CheckBox> cboxes;
	vector <Text> texts;
	vector <MasterEntity*> Entities;

	ID3DXLine* mLine;

	void DrawCrossHair(),
		ConstructPause(),
		ConstructSettings(),
		ConstructScoreBoard(),
		DestructPause(),
		DestructSettings(),
		DestructScoreBoard(),
		DrawScoreBoard();

	Radar* radar;
};


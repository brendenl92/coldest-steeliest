#pragma once

#include "DXUtil.h"

class Contact;
class BoundingVolumeNode;



D3DXVECTOR3 Cross(D3DXVECTOR3& Va, D3DXVECTOR3& Vb);


//projects half-sizes of an OBB to any axis
float transformToAxis(BoundingVolumeNode * box, D3DXVECTOR3 & projectionAxis);



// projects half-sizes and separation vector of OBBs to any axis
bool overlapOnAxis(BoundingVolumeNode *one, BoundingVolumeNode *two, D3DXVECTOR3 &projectionAxis, D3DXVECTOR3 &toCenter);





/* This function checks if the two boxes overlap
* along the given axis, returning the ammount of overlap.
* The final parameter toCentre
* is used to pass in the vector between the boxes centre
* points, to avoid having to recalculate it each time.
*/
static inline float penetrationOnAxis(BoundingVolumeNode *one, BoundingVolumeNode *two, D3DXVECTOR3 &axis, D3DXVECTOR3 &toCentre)
{
	// Project the half-size of one onto axis
	float oneProject = transformToAxis(one, axis);
	float twoProject = transformToAxis(two, axis);

	// Project this onto the axis
	float distance = abs(D3DXVec3Dot(&toCentre, &axis));

	// Return the overlap (i.e. positive indicates
	// overlap, negative indicates separation).
	return oneProject + twoProject - distance;
}



Contact* fillPointFaceBoxBox(BoundingVolumeNode* one, BoundingVolumeNode* two, D3DXVECTOR3 &toCentre, unsigned best, float pen);



// This preprocessor definition is only used as a convenience
// in the boxAndBox contact generation method.
#define CHECK_OVERLAP(axis, index) \
if (!tryAxis(one, two, (axis), toCentre, (index), pen, best)) return 0;



static inline bool tryAxis(BoundingVolumeNode *one, BoundingVolumeNode *two,
	D3DXVECTOR3& axis, D3DXVECTOR3& toCentre, unsigned index, float& smallestPenetration, unsigned &smallestCase)
{
	// Make sure axis is normalized; and don't check almost parallel axes
	if (D3DXVec3LengthSq(&axis) < 0.0001) return true;
	D3DXVec3Normalize(&axis, &axis);
	float penetration = penetrationOnAxis(one, two, axis, toCentre);
	if (penetration < 0) return false;
	if (penetration < smallestPenetration) {
		smallestPenetration = penetration;
		smallestCase = index;
	}
	return true;
}




float GetM(D3DXVECTOR3 V, int dim);



void SetM(D3DXVECTOR3& V, int dim, float value);


//Invert Z Axis
void InvertZAxis(D3DXMATRIX& RotM);




static inline D3DXVECTOR3 contactPoint(D3DXVECTOR3 &pOne, D3DXVECTOR3 &dOne, float oneSize, D3DXVECTOR3 &pTwo, D3DXVECTOR3 &dTwo, float twoSize,
	// If this is true, and the contact point is outside
	// the edge (in the case of an edge-face contact) then
	// we use one's midpoint, otherwise we use two's.
	bool useOne)
{
	D3DXVECTOR3 toSt, cOne, cTwo;
	float dpStaOne, dpStaTwo, dpOneTwo, smOne, smTwo;
	float denom, mua, mub;

	smOne = D3DXVec3LengthSq(&dOne);
	smTwo = D3DXVec3LengthSq(&dTwo);
	dpOneTwo = D3DXVec3Dot(&dTwo, &dOne);

	toSt = pOne - pTwo;
	dpStaOne = D3DXVec3Dot(&dOne, &toSt);
	dpStaTwo = D3DXVec3Dot(&dTwo, &toSt);

	denom = smOne * smTwo - dpOneTwo * dpOneTwo;

	// Zero denominator indicates parrallel lines
	if (abs(denom) < 0.0001f) {
		return useOne ? pOne : pTwo;
	}

	mua = (dpOneTwo * dpStaTwo - smTwo * dpStaOne) / denom;
	mub = (smOne * dpStaTwo - dpOneTwo * dpStaOne) / denom;

	// If either of the edges has the nearest point out
	// of bounds, then the edges aren't crossed, we have
	// an edge-face contact. Our point is on the edge, which
	// we know from the useOne parameter.
	if (mua > oneSize ||
		mua < -oneSize ||
		mub > twoSize ||
		mub < -twoSize)
	{
		return useOne ? pOne : pTwo;
	}
	else
	{
		cOne = pOne + dOne * mua;
		cTwo = pTwo + dTwo * mub;

		return cOne * 0.5 + cTwo * 0.5;
	}
}


//Get Axis of rotation matrix
D3DXVECTOR3 GetAxisR(D3DXMATRIX& RotM, int Col);

// Given point p, return point q on (or in) OBB b, closest to p
D3DXVECTOR3 ClosestPtPointOBB(D3DXVECTOR3 p, BoundingVolumeNode* box, D3DXVECTOR3 &q);

//Return closest point on Ray
bool POCRayToOBB(D3DXVECTOR3& Rp, D3DXVECTOR3& Rn, BoundingVolumeNode* rhs, D3DXVECTOR3& q);


//Clip to axis
void ClipToAxis(D3DXMATRIX& RotM);


//Create orthonormal basis based on a forward vector
void OrthnormBasFwdVec(D3DXMATRIX& RotM, D3DXVECTOR3& Heading);


bool SphereToOBBDetect(BoundingVolumeNode* Sphere, BoundingVolumeNode* Box, D3DXVECTOR3 lhsPosition);
Contact* SphereToOBBGenerate(BoundingVolumeNode* lhs, BoundingVolumeNode* rhs, D3DXVECTOR3 lhsPosition);
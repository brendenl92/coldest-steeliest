#pragma once 



struct Item
{
	int ammo;						//the ammo that we have "currently available" for the weapon Item
	int maxAmmo;
	int ItemID;						//the weapons ID
};

class WeaponSet
{
public:
	Item* Rifle;
	Item* Rocket;
	Item* Pistol;

public:
	WeaponSet()
	{
		///set up weapon set
		Rifle			= new Item;
		Rocket			= new Item;
		Pistol			= new Item;

		///Set Ammo
		Rifle->ammo		= 20;
		Rocket->ammo	= 10;
		Pistol->ammo	= 30;

		///Set Max Ammo
		Rifle->maxAmmo	= 20;
		Rocket->maxAmmo	= 10;
		Pistol->maxAmmo	= 30;

		///Set ID's
		Rifle->ItemID	= 1;
		Rocket->ItemID	= 2;
		Pistol->ItemID	= 3;
	}

	~WeaponSet()
	{
		delete Rifle;
		delete Rocket;
		delete Pistol;
	}

	int GetRifleID() const
	{
		return Rifle->ItemID;
	}

	int GetRocketID() const
	{
		return Rocket->ItemID;
	}

	int GetPistolID() const
	{
		return Pistol->ItemID;
	}

	double GetRifleAmmo() const
	{
		return Rifle->ammo;
	}

	double GetRocketAmmo() const
	{
		return Rocket->ammo;
	}

	double GetPistolAmmo() const
	{
		return Pistol->ammo;
	}

	void DecRifleAmmo()
	{
		Rifle->ammo--;
	}

	void DecRocketAmmo()
	{
		Rocket->ammo--;
	}

	void DecPistolAmmo()
	{
		Pistol->ammo--;
	}

};
#pragma once

#include "BaseAIEntity.h"
#include "Random.h"
#include <vector>

#define AMI AIManager::Instance()

//typedef D3DXVECTOR3 vector3;

///forward Declare
class MasterEntity;

class AIManager
{
private:
	vector<BaseAIEntity*>	m_vAIEntities;
	static AIManager*		s_Instance;

public:
	static AIManager* Instance();

public:
	~AIManager();

public:
	void ShutDown();

public:
	BaseAIEntity* createAIObject();								//creates a AI object

public:
	vector<BaseAIEntity*>& GetBaseAIEntity();					//get entity from list
};
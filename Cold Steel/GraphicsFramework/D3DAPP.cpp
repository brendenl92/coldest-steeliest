#include "D3DAPP.h"
#include "RawInput.h"
#include <stdlib.h>

D3DAPP* g_D3DAPP = NULL;
IDirect3DDevice9* g_D3dDevice = NULL;
bool g_Paused = false;

LRESULT CALLBACK MainWndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{

	// Don't start processing messages until the application has been created.
	if(g_D3DAPP)
		return g_D3DAPP->msgProc(msg, wParam, lParam);
	else
		return DefWindowProc(hwnd, msg, wParam, lParam);
	
}
void D3DAPP::changeState(string name)
{
	
}

void D3DAPP::setNetworking(bool in)
{
	this->networking = in;
}

bool D3DAPP::getNetworking()
{
	return this->networking;
}
void D3DAPP::HandleInput(RAWINPUT* raw)
{

}

D3DCAPS9 D3DAPP::getCaps()
{
	return this->caps;
}

D3DADAPTER_IDENTIFIER9 D3DAPP::getID()
{
	return ID;
}

D3DAPP::D3DAPP(HINSTANCE instance, string caption, D3DDEVTYPE type, DWORD requestedVP)
{

	m_MainWndCaption = caption;
	m_DevType = type;
	m_RequestedVP = requestedVP;
	m_AppInst = instance;
	m_MainWndInst = NULL;
	m_d3Object = NULL;
	m_AppPaused = false;
	ZeroMemory(&m_D3dPP, sizeof(m_D3dPP));



	//RECT desktop;
	//HWND hDesktop = GetDesktopWindow();
	//GetWindowRect(hDesktop, &desktop);
	m_ResHeight = GetSystemMetrics(SM_CYSCREEN);
	m_ResLength = GetSystemMetrics(SM_CXSCREEN);

	m_AppHeight = m_ResHeight;
	m_AppLength = m_ResLength;
	
	//delete RI;

	initMainWindow();
	initDirect3d();
	SetCursor(LoadCursorFromFile("../media/cursor/Cursor.ani"));

	g_D3dDevice->GetDeviceCaps(&caps);
	m_d3Object->GetAdapterIdentifier(D3DADAPTER_DEFAULT, NULL, &ID);

}

D3DAPP::~D3DAPP(void)
{

	ReleaseCOM(m_d3Object);
	ReleaseCOM(g_D3dDevice);

}

HINSTANCE D3DAPP::getInstance()
{

	return m_AppInst;

}

HWND D3DAPP::getMainWnd()
{

	return m_MainWndInst;

}

bool D3DAPP::checkDeviceCaps()
{
	return true;
}
void D3DAPP::onLostDevice()
{

}
void D3DAPP::onResetDevice()
{

}
void D3DAPP::updateScene(float dt)
{

}

D3DXVECTOR2 D3DAPP::getRes()
{

	return D3DXVECTOR2((float)m_ResLength, (float)m_ResHeight);

}
void D3DAPP::drawScene()
{

}

void D3DAPP::initMainWindow()
{

	WNDCLASS wc;
	wc.lpszClassName	= "datwindowdoe";
	wc.hInstance		= m_AppInst;
	wc.lpfnWndProc		= MainWndProc;
	wc.style            = CS_HREDRAW | CS_VREDRAW;  //redraw if window is resized
	wc.cbClsExtra		= 0;						//extra bytes after classstructure
	wc.cbWndExtra		= 0;						//extra bytes after windowinstance
	wc.hIcon			= 0;
	wc.hCursor			= LoadCursorFromFile("Cursor.ani");
	wc.hbrBackground	= (HBRUSH)GetStockObject(WHITE_BRUSH);
	wc.lpszMenuName		= NULL;

	if(!RegisterClass(&wc))
	{
		MessageBox(NULL, "RegisterClass Failed", "Register Failure", MB_ICONWARNING);
		PostQuitMessage(0);
	}

	RECT rect = { 0, 0, m_ResLength, m_ResHeight};
	AdjustWindowRect(&rect, WS_POPUP, false);
	m_MainWndInst = CreateWindowA("datwindowdoe", m_MainWndCaption.c_str(), WS_POPUP, 0, 0, m_ResLength, m_ResHeight, NULL, NULL, m_AppInst, NULL);
	
	if(!m_MainWndInst)
	{
		MessageBox(0, "Create Window Failed", "Window Failure", MB_ICONWARNING);
		PostQuitMessage(0);
	}
	SetWindowLongA(m_MainWndInst, GWL_STYLE, WS_POPUP); //sets window to popup windowstyle
	SetWindowPos(m_MainWndInst, HWND_TOP, 0, 0, m_ResLength, m_ResHeight, SWP_NOZORDER | SWP_SHOWWINDOW);
	ShowWindow(m_MainWndInst, SW_SHOW);
	UpdateWindow(m_MainWndInst);

}

void D3DAPP::initDirect3d()
{

	m_d3Object = Direct3DCreate9(D3D_SDK_VERSION); //Creates Direct3D Interface
	if(!m_d3Object)
	{
		MessageBox(NULL, "Direct Object Failed!", "Failure!", MB_ICONWARNING);
		PostQuitMessage(0);
	}

	D3DDISPLAYMODE mode;
	m_d3Object->GetAdapterDisplayMode(D3DADAPTER_DEFAULT, &mode);
	HR(m_d3Object->CheckDeviceType(D3DADAPTER_DEFAULT, m_DevType, mode.Format, mode.Format, true)); //checks windowed format
	HR(m_d3Object->CheckDeviceType(D3DADAPTER_DEFAULT, m_DevType, D3DFMT_X8R8G8B8, D3DFMT_X8R8G8B8, false)); //checks fullscreen format

	D3DCAPS9 caps;
	HR(m_d3Object->GetDeviceCaps(D3DADAPTER_DEFAULT, m_DevType, &caps));

	DWORD behaviorFlags = NULL;
	if(caps.DevCaps & D3DDEVCAPS_HWTRANSFORMANDLIGHT) //tests for hardware vertex processing
	{
		behaviorFlags |= m_RequestedVP;			//if so it assigns hardware processing flag(passed in from winmain)
		if(caps.DevCaps & D3DDEVCAPS_PUREDEVICE)
			behaviorFlags |= D3DCREATE_PUREDEVICE;
	}
	else
		behaviorFlags |= D3DCREATE_SOFTWARE_VERTEXPROCESSING; //if hardware isn't supported (just stop here and upgrade your video card)

	m_D3dPP.Windowed = true;
	m_D3dPP.SwapEffect = D3DSWAPEFFECT_DISCARD;
	m_D3dPP.hDeviceWindow = m_MainWndInst;
	m_D3dPP.BackBufferFormat = D3DFMT_X8R8G8B8;
	m_D3dPP.BackBufferCount = 1;
	m_D3dPP.MultiSampleType = D3DMULTISAMPLE_NONE;
	m_D3dPP.MultiSampleQuality = 0;
	m_D3dPP.EnableAutoDepthStencil = true;
	m_D3dPP.AutoDepthStencilFormat = D3DFMT_D24S8;
	m_D3dPP.Flags = D3DPRESENTFLAG_DISCARD_DEPTHSTENCIL;
	m_D3dPP.FullScreen_RefreshRateInHz = D3DPRESENT_RATE_DEFAULT;
	m_D3dPP.PresentationInterval = D3DPRESENT_INTERVAL_IMMEDIATE;
	m_D3dPP.BackBufferHeight = m_ResHeight;
	m_D3dPP.BackBufferWidth = m_ResLength;

	HRESULT temp = m_d3Object->CreateDevice(D3DADAPTER_DEFAULT, m_DevType, m_MainWndInst, behaviorFlags, &m_D3dPP, &g_D3dDevice);

}

int D3DAPP::run()
{

	MSG message;
	message.message = WM_NULL;
	CLK->start();
	while(message.message != WM_QUIT)
	{
		while(PeekMessage(&message, m_MainWndInst, NULL, NULL, PM_REMOVE))
		{
			TranslateMessage(&message);
			DispatchMessage(&message);
		}
		
		{
			if(m_AppPaused)
			{
				Sleep(20);
				continue;
			}

			if(!isDeviceLost())
			{
				
				float dt = CLK->end();
				updateScene(dt);
				drawScene();
				CLK->finish();
			}
		}
	}

	return 0;

}

LRESULT D3DAPP::msgProc(UINT msg, WPARAM wParam, LPARAM lParam)
{
	//true = maximized, false = minimized
	static bool isMaximizedorMaximized = false;
	RECT rectangle = {0, 0, 0, 0};

	switch(msg)
	{
	case WM_INPUT:
		RAW->handleMessage(msg, wParam, lParam);
		break;
	case WM_ACTIVATE:
		if( LOWORD(wParam) == WA_INACTIVE)//if message says the window is now inactive
			m_AppPaused = true; //pause program
		else
			m_AppPaused = false;//unpause program
		return 0;
		
	case WM_SIZE: //resize window
		if(g_D3dDevice)
		{
			m_OldHeight = m_AppHeight;
			m_OldWidth = m_AppLength;
			m_D3dPP.BackBufferWidth = LOWORD(lParam);
			m_D3dPP.BackBufferHeight = HIWORD(lParam); //resets backbuffer so stretching doesn't occur
			if(wParam == SIZE_MINIMIZED) //minimizing application
			{ 
				m_AppPaused = true;
				isMaximizedorMaximized = true;
			}
			else if(wParam == SIZE_MAXIMIZED) //maximizing application
			{
				m_AppPaused = false;
				isMaximizedorMaximized = true;
				onLostDevice();
				m_D3dPP.BackBufferWidth = LOWORD(lParam);
				m_D3dPP.BackBufferHeight = HIWORD(lParam);
				HR(g_D3dDevice->Reset(&m_D3dPP));
				exit(-400000);
				onResetDevice();
			}
			else if(wParam == SIZE_RESTORED) //resizing that's not maximizing or minimizing
			{
				m_AppPaused = false;

				if(isMaximizedorMaximized && m_D3dPP.Windowed ) //only resize if restoring from minimized or maximized and windowed, elsewise wait for EXITSIZEMOVE
				{
					onLostDevice();
					HR(g_D3dDevice->Reset(&m_D3dPP));
					exit(-400000);
					onResetDevice();
				}
				isMaximizedorMaximized = false; 
			}
		}
		return 0;
	case WM_EXITSIZEMOVE://released resize bar
		GetClientRect(m_MainWndInst, &rectangle);
		m_D3dPP.BackBufferWidth = rectangle.right;
		m_D3dPP.BackBufferHeight = rectangle.bottom;
		m_AppHeight = rectangle.bottom;
		m_AppLength = rectangle.right;
		onLostDevice();
		HR(g_D3dDevice->Reset(&m_D3dPP));
		exit(-400000);
		onResetDevice();
		return 0;
	case WM_QUIT:
		DestroyWindow(m_MainWndInst);
		delete g_D3DAPP;
		return 0;
	case WM_CLOSE: //x pressed
		DestroyWindow(m_MainWndInst);
		delete g_D3DAPP;
		return 0;
	case WM_DESTROY:
		PostQuitMessage(0);
		delete g_D3DAPP;
		return 0;
	}
	return DefWindowProc(m_MainWndInst, msg, wParam, lParam);
}

int D3DAPP::getHeight()
{

	return m_AppHeight;

}

int D3DAPP::getWidth()
{

	return m_AppLength;

}

int D3DAPP::getOldWidth()
{
	return m_OldWidth;
}

int D3DAPP::getOldHeight()
{
	return m_OldHeight;
}

void D3DAPP::enableFullScreen(bool input)
{

	if(input)
	{
		m_D3dPP.BackBufferFormat = D3DFMT_X8R8G8B8;
		m_D3dPP.BackBufferHeight = m_ResHeight;
		m_D3dPP.BackBufferWidth  = m_ResLength;
		m_D3dPP.Windowed		 = false;

		SetWindowLongPtr(m_MainWndInst, GWL_STYLE, WS_POPUP); //sets window to popup windowstyle
		SetWindowPos(m_MainWndInst, HWND_TOP, 0, 0, m_ResLength, m_ResHeight, SWP_NOZORDER | SWP_SHOWWINDOW);
	}
	else
	{
		RECT rectangle = {0, 0, 900, 600};
		AdjustWindowRect(&rectangle, WS_OVERLAPPEDWINDOW, false);
		m_D3dPP.BackBufferFormat = D3DFMT_UNKNOWN;
		m_D3dPP.BackBufferWidth  = rectangle.right;
		m_D3dPP.BackBufferHeight = rectangle.bottom;
		m_D3dPP.Windowed		 = true;

		SetWindowLongPtr(m_MainWndInst, GWL_STYLE, WS_OVERLAPPEDWINDOW);
		SetWindowPos(m_MainWndInst, HWND_TOP, (m_ResLength / 2) - (m_AppLength / 2), (m_ResHeight / 2) - (m_AppHeight / 2), rectangle.bottom, rectangle.right, SWP_NOZORDER | SWP_SHOWWINDOW);
	}

		onLostDevice();
		HR(g_D3dDevice->Reset(&m_D3dPP));
		onResetDevice();


}

bool D3DAPP::isDeviceLost()
{

	HRESULT result = g_D3dDevice->TestCooperativeLevel();

	if(result == D3DERR_DEVICELOST) //device lost and can't reset yet
	{
		Sleep(20);
		return true;
	}
	else if( result == D3DERR_DRIVERINTERNALERROR)
	{
		MessageBox(m_MainWndInst, "Internal Driver Error" , "Driver Error", MB_ICONWARNING);
		PostQuitMessage(0);
		return true;
	}
	else if(result == D3DERR_DEVICENOTRESET) //device lost and can reset
	{
		onLostDevice();
		HRESULT temp = g_D3dDevice->Reset(&m_D3dPP);
		onResetDevice();
		return false;
	}
	else if(result == D3D_OK)
		return false;
	return false;
}

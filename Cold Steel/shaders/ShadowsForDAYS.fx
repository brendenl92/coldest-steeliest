float4x4 g_mWorldView;
float4x4 g_mProj;

void VertShadow(float4 Pos : POSITION, float3 Normal : NORMAL, out float4 oPos : POSITION, out float2 Depth : TEXCOORD0)
{
	oPos = mul(Pos, g_mWorldView);
	oPos = mul(oPos, g_mProj);

	Depth.xy = oPos.zw;
}

void PixShadow(float2 Depth : TEXCOORD0, out float4 Color : COLOR)
{
	Color = Depth.x / Depth.y;
}

technique RenderShadow
{
	pass p0
	{
		VertexShader = compile vs_2_0 VertShadow();
		PixelShader = compile ps_2_0 PixShadow();
	}
}

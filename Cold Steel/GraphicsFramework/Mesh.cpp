#include "Mesh.h"
#include "D3DAPP.h"


Mesh::Mesh(string path, UINT tech)
{
	this->path = path;
	this->tech = tech;

	HR(D3DXLoadMeshFromXA(path.c_str(), D3DXMESH_MANAGED, g_D3dDevice, NULL, &buffer, NULL, &numMaterials, &mesh));

	inMat = (D3DXMATERIAL*)buffer->GetBufferPointer();

	material = new D3DMATERIAL9[numMaterials];
	texture = new LPDIRECT3DTEXTURE9[numMaterials];

	for (UINT i = 0; i < numMaterials; i++)
	{
		material[i] = inMat[i].MatD3D;
		material[i].Ambient = material[i].Diffuse;
		char file[100] = "../media/textures/";
		if (inMat[i].pTextureFilename)
		{
			strcat_s(file, inMat[i].pTextureFilename);
		}
		if (FAILED(D3DXCreateTextureFromFileA(g_D3dDevice, file, &texture[i])))
			texture[i] = NULL;
	}
	vector<D3DXVECTOR3> vecVertices;

	UINT numVerts = mesh->GetNumVertices();

	BYTE* pVerts;

	DWORD numBytesPerVertex = mesh->GetNumBytesPerVertex();

	unsigned int uiSize = D3DXGetFVFVertexSize(mesh->GetFVF());

	mesh->LockVertexBuffer(0, (void**)&pVerts);

	int iOffset = 0;
	for (WORD i = 0; i<numVerts; i++)
	{
		D3DXVECTOR3 vec3Vertice = *((D3DXVECTOR3*)(pVerts + i*uiSize + iOffset));
		vecVertices.push_back(vec3Vertice);
	}

	// unlock the vertex buffer
	mesh->UnlockVertexBuffer();
	D3DXComputeBoundingBox(&vecVertices[0], numVerts, 0, &min, &max);
}

D3DXVECTOR3 Mesh::scaledBounds(D3DXVECTOR3 scale)
{
	D3DXVECTOR3 out;
	out.x = min.x * scale.x;
	out.y = min.y * scale.y;
	out.z = min.z * scale.z;
	out *= 2.3f;
	return out;

}

UINT Mesh::getNumVerts()
{
	return (UINT)mesh->GetNumVertices();
}

D3DXVECTOR3 Mesh::getBounds(int i)
{
	if (i == 0)
		return min;
	if (1 == 1)
		return max;
	return D3DXVECTOR3(0, 0, 0);
}

Mesh::~Mesh()
{
	for (UINT i = 0; i < numMaterials; i++)
		if (texture[i])
			texture[i]->Release();
	delete texture;
	mesh->Release();
	if (material)
		delete material;
	buffer->Release();
}

LPDIRECT3DTEXTURE9 Mesh::getTextures()
{

	return texture[0];

}


D3DMATERIAL9* Mesh::getMaterial()
{

	return material;

}

LPDIRECT3DTEXTURE9 Mesh::getTexture(int i)
{

	return texture[i];

}

int Mesh::getNumMaterials()
{

	return (int)numMaterials;

}

LPD3DXMESH Mesh::getMesh()
{

	return mesh;

}

UINT Mesh::getTech()
{
	return tech;
}
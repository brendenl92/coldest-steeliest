#include "FSM.h"
#include "HUD.h"
FSM::FSM(void)
{
	
}


FSM::~FSM(void)
{
	RM->clearAll();
	
	delete CAM;
	delete LM;
	delete SET;
	delete CLK;
	delete XBOX;
	delete SOUND;
	//delete Hud;
	//delete GameState::Instance();
	//delete MenuState::Instance();
	if (MovieState::Instance())
		delete MovieState::Instance();
	delete RAW;

}

void FSM::handleInput(RAWINPUT* raw)
{
	m_CurrentState->HandleInput(raw);
}

void FSM::setCurrentState(State* state)
{

	m_CurrentState = state;
	m_CurrentState->Initialize();

}

void FSM::setLastState(State* state)
{

	m_LastState = state;

}

void FSM::changeState(State* state)
{

	if(state)
	{

		m_LastState = m_CurrentState;
		m_CurrentState->Leave();
		m_CurrentState = state;
		m_CurrentState->Initialize();

	}

}

void FSM::revertStates()
{

	changeState(m_LastState);

}

void FSM::update(float dt)
{

	if(m_CurrentState)
		m_CurrentState->UpdateScene(dt);
	if (m_CurrentState == MovieState::Instance() && RAW->keyPressed(ESC) == true)
		changeState(MenuState::Instance());

}

bool FSM::isInState(State* state)
{

	return m_CurrentState == state;

}

State* FSM::getState()
{

	return m_CurrentState;

}
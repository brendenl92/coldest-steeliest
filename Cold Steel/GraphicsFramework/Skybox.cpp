#include "Skybox.h"

Skybox::Skybox()
{

	D3DXMatrixTransformation(&world, NULL, NULL, &D3DXVECTOR3(1, 1, 1), NULL, NULL, &D3DXVECTOR3(0, 0, 0));
	onResetDevice();
}

Skybox::~Skybox()
{
}

LPD3DXMESH Skybox::getMesh()
{
	return mesh;
}

IDirect3DCubeTexture9* Skybox::getTex(int index)
{
	switch (index)
	{
	case 0:
		return mountains;
		break;
	case 1:
		return sunrise;
		break;
	case 2:
		return sunset;
		break;
	case 3:
		return dusk;
		break;
	}
	return 0;
}

D3DXMATRIX Skybox::getWorld()
{
	return world;
}

void Skybox::onLostDevice()
{
	mesh->Release();
	mountains->Release();
	sunrise->Release();
	sunset->Release();
	dusk->Release();
}

void Skybox::onResetDevice()
{
	D3DXCreateBox(g_D3dDevice, 100000, 100000, 100000, &mesh, NULL);
	D3DXCreateCubeTextureFromFileA(g_D3dDevice, "../media/textures/BSkyBox.dds", &mountains);
	D3DXCreateCubeTextureFromFileA(g_D3dDevice, "../media/textures/sunrise.dds", &sunrise);
	D3DXCreateCubeTextureFromFileA(g_D3dDevice, "../media/textures/sunset.dds", &sunset);
	D3DXCreateCubeTextureFromFileA(g_D3dDevice, "../media/textures/dusk.dds", &dusk);
}

#pragma once

#include "State.h"
#include "D3DAPP.h"
#include "UIElements.h"

class TankSelectBack;
class CreditsState : public State
{
public:
	CreditsState();
	~CreditsState();
	static CreditsState* Instance();

	virtual void Initialize();
	virtual void UpdateScene(float dt);
	virtual void RenderScene();
	virtual void HandleInput(RAWINPUT* raw);

	virtual void OnResetDevice();
	virtual void OnLostDevice();
	virtual void Leave();

private:
	TankSelectBack* back;

	D3DXVECTOR2 CreditsPosition;

	void CreateCredits();

	vector<Text> texts;

	RECT rect;
};

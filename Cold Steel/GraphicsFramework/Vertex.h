#ifndef VERTEX_H
#define VERTEX_H

#include"D3dx9math.h"
#include "DXUtil.h"

// Call in constructor and destructor, respectively, of derived application class.
void InitAllVertexDeclarations();
void DestroyAllVertexDeclarations();

struct VertexP
{
	VertexP() :mPosition(D3DXVECTOR3(0, 0, 0)){};
	VertexP(D3DXVECTOR3 position) :mPosition(position){};

	D3DXVECTOR3 mPosition;

	static IDirect3DVertexDeclaration9* Decl;
};

struct VertexPT
{
	VertexPT() :mPosition(D3DXVECTOR3(0, 0, 0)), mTexture(D3DXVECTOR2(0, 0)){};
	VertexPT(D3DXVECTOR3 position, D3DXVECTOR2 texture) :mPosition(position), mTexture(texture){};

	D3DXVECTOR3 mPosition;
	D3DXVECTOR2 mTexture;

	static IDirect3DVertexDeclaration9* Decl;
};

struct VertexPNT
{
	VertexPNT() :mPosition(D3DXVECTOR3(0, 0, 0)), mTexture(D3DXVECTOR2(0, 0)), mNormal(D3DXVECTOR3(0, 0, 0)){};
	VertexPNT(D3DXVECTOR3 position, D3DXVECTOR2 texture, D3DXVECTOR3 normal) :mPosition(position), mTexture(texture), mNormal(normal){};

	D3DXVECTOR3 mPosition;
	D3DXVECTOR2 mTexture;
	D3DXVECTOR3 mNormal;

	static IDirect3DVertexDeclaration9* Decl;
};

//===============================================================
struct Particle
{
	D3DXVECTOR3 initialPos;
	D3DXVECTOR3 initialVelocity;
	float       initialSize; // In pixels.
	float       initialTime;
	float       lifeTime;
	float       mass;
	D3DCOLOR    initialColor;

	static IDirect3DVertexDeclaration9* Decl;
};

struct FireVertex
{
	D3DXVECTOR3 initialPos;
	D3DXVECTOR2 mTexture;

	static IDirect3DVertexDeclaration9* Decl;
};
#endif
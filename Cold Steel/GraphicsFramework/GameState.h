#pragma once
#include "state.h"
#include "GameWorld.h"
#include "XInput.h"
class GameState :
	public State
{
	D3DXVECTOR3 movement;
	GameWorld* gw;
	GameState(void);
public:
	static GameState* Instance();
	~GameState(void);
	D3DXVECTOR3 getMovement();
	virtual void Initialize();
	virtual void UpdateScene(float dt);
	virtual void RenderScene();
	virtual void HandleInput(RAWINPUT* raw);
	virtual void OnResetDevice();
	virtual void OnLostDevice();
	virtual void Leave();
};


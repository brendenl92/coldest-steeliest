#pragma once
#include "DXUtil.h"

class Mesh
{
	LPD3DXMESH mesh;
	DWORD numMaterials;
	LPD3DXBUFFER buffer;
	LPDIRECT3DTEXTURE9* texture;
	D3DMATERIAL9* material;
	D3DXMATERIAL* inMat;
	UINT tech;
	string path;
	D3DXVECTOR3 min;
	D3DXVECTOR3 max;
public:
	Mesh(string path, UINT tech);
	~Mesh(void);
	int getNumMaterials();
	LPDIRECT3DTEXTURE9 getTexture(int i);
	LPDIRECT3DTEXTURE9 getTextures();
	D3DMATERIAL9* getMaterial();
	LPD3DXMESH getMesh();
	D3DXVECTOR3 getBounds(int i);
	D3DXVECTOR3 scaledBounds(D3DXVECTOR3 scale);
	UINT getTech();
	UINT getNumVerts();
};


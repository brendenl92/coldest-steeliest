#include "BasisLine.h"
#include "ResourceManager.h"

BasisLine::BasisLine()
{
}

BasisLine::BasisLine(D3DXVECTOR3 pos, D3DXVECTOR3 x, D3DXVECTOR3 y, D3DXVECTOR3 z)
{
	this->pos = pos;
	this->x = x;
	this->y = y;
	this->z = z;
}

BasisLine::~BasisLine()
{
}

void BasisLine::update(D3DXVECTOR3 pos, D3DXVECTOR3 x, D3DXVECTOR3 y, D3DXVECTOR3 z)
{
	this->pos = pos;
	this->x = x;
	this->y = y;
	this->z = z;
}
void BasisLine::render()
{
	struct line_vertex
	{
		float x, y, z;
		DWORD color;
	};

	line_vertex line_vertices[6];

	line_vertices[0].x = x.x * 50;
	line_vertices[0].y = x.y * 50;
	line_vertices[0].z = x.z * 50;
	line_vertices[0].color = D3DCOLOR_XRGB(0, 255, 0);

	line_vertices[1].x = pos.x;
	line_vertices[1].y = pos.y;
	line_vertices[1].z = pos.z;
	line_vertices[1].color = D3DCOLOR_XRGB(0, 255, 0);

	line_vertices[2].x = pos.x;
	line_vertices[2].y = pos.y;
	line_vertices[2].z = pos.z;
	line_vertices[2].color = D3DCOLOR_XRGB(0, 255, 0);

	line_vertices[3].x = y.x * 50;
	line_vertices[3].y = y.y * 50;
	line_vertices[3].z = y.z * 50;
	line_vertices[3].color = D3DCOLOR_XRGB(0, 255, 0);

	line_vertices[4].x = pos.x;
	line_vertices[4].y = pos.y;
	line_vertices[4].z = pos.z;
	line_vertices[4].color = D3DCOLOR_XRGB(0, 255, 0);

	line_vertices[5].x = z.x * 50;
	line_vertices[5].y = z.y * 50;
	line_vertices[5].z = z.z * 50;
	line_vertices[5].color = D3DCOLOR_XRGB(0, 255, 0);

	g_D3dDevice->SetRenderState(D3DRS_LIGHTING, false);
	g_D3dDevice->SetFVF(D3DFVF_XYZ | D3DFVF_DIFFUSE);
	g_D3dDevice->DrawPrimitiveUP(D3DPT_LINESTRIP, 3, line_vertices, sizeof(line_vertex));

}

#pragma once
#include "Sphere.h"

#define BOM BasicObjectManager::instance()

class BasicObjectManager
{
	vector<Sphere*> spheres;
	BasicObjectManager();
public:
	static BasicObjectManager* instance();
	~BasicObjectManager();

	void addSphere(D3DXVECTOR3 pos, float rad, UINT meshID);
	UINT sphereVecSize();
	Sphere* getSpheres(UINT it);
};


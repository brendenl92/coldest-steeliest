#pragma once

#include "ASM.h"

#define WD Wander::Instance()

class Wander: public BaseState
{
private:
	static Wander* s_Instance;
	int numMsg1, numMsg2, numMsg3;

private:
	Wander();

public:
	static Wander* Instance();

public:
	Wander(ASM* fsm, int id);
	~Wander();

public:
	void Enter();					//enters into attack state
	void Execute(double dt);		//executes the code in attack state, steering forces will go here
	void Exit();					//will exit the attack state
	void HandleMsg(Mail message);	//handles msgs sent to the attack state
	void ShutDown();
};
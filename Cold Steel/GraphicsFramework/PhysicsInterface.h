# pragma once

#include "RigidBody.h"
#include "BoundingVolume.h"
#include "Contact.h"
#include <map>
#include <vector>

class MadterEntity;

#define PhyI PhysicsInterface::Instance() 

class PhysicsInterface
{
	PhysicsInterface();

	static PhysicsInterface* s_Instance;

public:

	static PhysicsInterface* Instance();

	//Variables
	
	typedef std::vector<BoundingVolumeNode*> BoundingVolumes;
	BoundingVolumes mCollisionVolumes;

	CollisionData mCollisions;


	/*typedef std::map<RigidBody, BoundingVolumeNode*> BoundingVolumes;
	BoundingVolumes mCollisionVolumes;*/

	//Methods

	RigidBody* CreateTank(V3 Position);
	RigidBody* CreateRigidBody(V3 Position);
	RigidBody* CreateRigidBody(MasterEntity* Master, V3 Position, float Radius = 10.0f, V3 Halfsize = V3(0,0,0), float mass = 10.0f);
	void UpdatePhysics(float DeltaTime);
	void RemoveCollisionVolume(RigidBody* Body);
	void GenertateContacts();
	void ResolveContacts();

	//Tank control
	void AddLinearMotion();
	void AddRotationLeft();
	void AddRotationRight();

	void RotateAroundTheYAxis90DegreesCounterClockwise(RigidBody* rb);
	bool ShootRay(V3& Rp, V3& Rn, V3& q/*, MasterEntity* Me*/);
	bool BulletHit(V3& SPosition, float Radius, V3& poc, MasterEntity& Me);



	void Check();
};
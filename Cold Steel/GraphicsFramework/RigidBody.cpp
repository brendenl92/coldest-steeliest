#pragma once
#include "MasterEntity.h"
#include "RigidBody.h"
//#include "BoundingVolume.h"
#include "PhysicsInterface.h"
#include "collision function.h"


//Ctor
RigidBody::RigidBody(V3 center, V3 head, float maxspeed, float maxrot)
{
	mInverted = false;
	mMaxSpeed = maxspeed;
	mMaxTurn = maxrot;
	mDampening = 0.15f;
	mVelocity = V3(0, 0, 0);
	mOmega = V3(0, 0, 0);
	mInvMass = 1;
	ResetTransform();
	SetPosition(center);
	D3DXQuaternionIdentity(&mHeading);

	mVolume = NULL;


	mMaster = NULL;
}


//Ctor
RigidBody::RigidBody(MasterEntity* Master, V3 center, V3 head, float maxspeed, float maxrot, float Mass)
{
	mInverted = false;
	mMaxSpeed = maxspeed;
	mMaxTurn = maxrot;
	mDampening = 0.15f;
	mVelocity = V3(0, 0, 0);
	mOmega = V3(0, 0, 0);
	ResetTransform();
	D3DXQuaternionRotationAxis(&mOffsetHeading, &D3DXVECTOR3(0,1,0) , 1.57f);
	//SetPosition(center);
	mInvMass = Mass / 1; 
	D3DXQuaternionIdentity(&mHeading);
	D3DXMatrixRotationY(&mOffsetM, 1.57f);

	mVolume = NULL;


	mMaster = Master;
}


//DCTOR
RigidBody::~RigidBody()
{
	if (mVolume != NULL)
		PhyI->RemoveCollisionVolume(this);
}


//Update
void RigidBody::Update(float dt, V3* PositionOut, V3* VelocityOut, M4* rotOut)
{
	float agiganticbagofdicks = PositionOut->y;
	//if (RAW->keyPressed(M) == true)
		//int hi = 0; 
	//if (mInverted)
	//{
	//	InvertZAxis(*rotOut);
	//	mInverted = false;
	//}
	

	*VelocityOut *= pow(mDampening, dt);
	mOmega *= pow(mDampening, dt);

	//First create orthonormal basis
	if (mMaster->AIEnt())
		OrthnormBasFwdVec(*rotOut, *VelocityOut);

	D3DXMATRIX H(*rotOut);
	D3DXMATRIX Hp;
	//D3DXMATRIX Rx;
	//D3DXMATRIX Ry;
	//D3DXMATRIX Rz;
	//D3DXMatrixRotationX(&Rx, mOmega.x);
	//D3DXMatrixRotationY(&Ry, mOmega.y);
	//D3DXMatrixRotationZ(&Rz, mOmega.z);
	//Hp = Rx*Ry*Rz;
	D3DXMatrixRotationYawPitchRoll(&Hp, mOmega.y , mOmega.x, mOmega.z);

	//D3DXMatrixRotationY(&Ry, -2 * mOmega.y);

	*rotOut = H * Hp;



	//rotOut->m[0][2] *= -1;
	//rotOut->m[1][2] *= -1;
	//rotOut->m[2][2] *= -1;

	//*rotOut *= Ry;

//	D3DXMatrixTranspose(rotOut, rotOut);

	//Multiply by an offset matrix
//	*rotOut = *rotOut * mOffsetM;

	//Position Update
	float speed;
	
//	*VelocityOut *= pow(mDampening, dt);
	speed = D3DXVec3Length(VelocityOut);

	VelocityOut->x = rotOut->m[0][0];
	VelocityOut->y = rotOut->m[1][0];
	VelocityOut->z = rotOut->m[2][0];

	*VelocityOut = *VelocityOut * speed;


	V3 Center(*PositionOut);
	Center += (*VelocityOut*dt);
 	*PositionOut = Center;

	/*if (!mInverted)
	{
		InvertZAxis(*rotOut);
		mInverted = true;
	}*/


	//static int CountL = 0;
	//CountL++;

	//if (CountL > 200)
	//{


	//	system("cls");

	//	cout << "\nVelocity" <<
	//		"\nX, Y, Z\n"
	//		<< VelocityOut->x << ", " << VelocityOut->y << ", " << VelocityOut->z << endl;


	//	cout << "\nRotation" <<
	//		"\nX: ("
	//		<< rotOut->m[0][0] << ", " << rotOut->m[1][0] << ", " << rotOut->m[2][0] << ")\n"
	//		<< "Y: ("
	//		<< rotOut->m[0][1] << ", " << rotOut->m[1][1] << ", " << rotOut->m[2][1] << ")\n"
	//		<< "Z: ("
	//		<< rotOut->m[0][2] << ", " << rotOut->m[2][1] << ", " << rotOut->m[2][2] << ")\n";

	//	cout << "\nOmega(" << mOmega.x << ", " << mOmega.y << ", " << mOmega.z << ")\n";

	//	cout << "\nPosition(" << PositionOut->x << ", " << PositionOut->y << ", " << PositionOut->z << ")\n";

	//	CountL = 0;
	//}

}


//Reset Transform
void RigidBody::ResetTransform()
{
	D3DXMatrixIdentity(&mTransform);
}

//Add Velocity
void RigidBody::AddVelocity(V3 V)
{
	mMaster->setVelocity(mMaster->getVelocity() + V);
}

void RigidBody::AddVelocity(V3 Heading, float Magnitude)
{

	mMaster->setVelocity(mMaster->getVelocity() += Heading * Magnitude);
	//cout << "Speed: " << Magnitude << endl;

}
void RigidBody::AddRotation(int Axis, float Theta)
{
	float rad = Theta * 3.14159f / 180.0f;
	switch (Axis)
	{
	case 0:
		mOmega.x += rad;
		break;
	case 1:
		mOmega.y += rad;
		break;
	case 2:
		mOmega.z += rad;
		break;
	}

}
//Rotate Around an Axis
void RigidBody::RotateBoutanAxis(int A, float theta)
{
	Qu temp = D3DXQUATERNION();
	//temp//GetAxis(A), theta
	Qu current(GetQuaternion());

	//current *= temp;

	Transform(current, GetPosition());
}

//Heading
V3 RigidBody::Heading()
{
	return GetAxis(0);
}

//Mass
float RigidBody::Mass()
{
	if (mInvMass == 0)
		return 0;
	else
		return mInvMass;
}

//Get Halfsize
V3 RigidBody::GetHalfSize()
{
	return mVolume->mVolume->mHalfsize;
}


//Get Axis
V3 RigidBody::GetAxis(int A)
{
	D3DXMATRIX Tr = mMaster->getTrans(0);
	return V3(Tr.m[0][A], Tr.m[1][A], Tr.m[2][A]);
}

//Get Position
V3 RigidBody::GetPosition()
{
	return mMaster->getPosition();
}

//Get Quaternion from Transformation Matrix
Qu RigidBody::GetQuaternion()
{
	Qu q;
	D3DXMATRIX Tr = mMaster->getTrans(0);
	D3DXQuaternionRotationMatrix(&q, &Tr);

	/*q.w = sqrt((1 + mTransform.m[0][0] + mTransform.m[1][1] + mTransform.m[2][2]) / 2);
	q.x = (mTransform.m[2][1] - mTransform.m[1][2]) / (4 * q.w);
	q.y = (mTransform.m[0][2] - mTransform.m[2][0]) / (4 * q.w);
	q.x = (mTransform.m[1][0] - mTransform.m[0][1]) / (4 * q.w);*/

	return q;


	//D3DXQUATERNION* WINAPI D3DXQuaternionRotationMatrix(D3DXQUATERNION *pout, CONST D3DXMATRIX *pm)


	/*M4 pm;
	Qu pout;
		int i, maxi;
		float maxdiag, S, trace;
		
			trace = pm.m[0][0] + pm.m[1][1] + pm.m[2][2] + 1.0f;
		if (trace > 0.0f)
			 {
			pout.x = (pm.m[1][2] - pm.m[2][1]) / (2.0f * sqrt(trace));
			pout.y = (pm.m[2][0] - pm.m[0][2]) / (2.0f * sqrt(trace));
			pout.z = (pm.m[0][1] - pm.m[1][0]) / (2.0f * sqrt(trace));
			pout.w = sqrt(trace) / 2.0f;
			return pout;
			}
		maxi = 0;
		maxdiag = pm.m[0][0];
		for (i = 1; i<3; i++)
			 {
			if (pm.m[i][i] > maxdiag)
				 {
				maxi = i;
				maxdiag = pm.m[i][i];
				}
			}
		switch (maxi)
			{
			case 0:
				S = 2.0f * sqrt(1.0f + pm.m[0][0] - pm.m[1][1] - pm.m[2][2]);
				pout.x = 0.25f * S;
				pout.y = (pm.m[0][1] + pm.m[1][0]) / S;
				pout.z = (pm.m[0][2] + pm.m[2][0]) / S;
				pout.w = (pm.m[1][2] - pm.m[2][1]) / S;
				break;
				case 1:
					S = 2.0f * sqrt(1.0f + pm.m[1][1] - pm.m[0][0] - pm.m[2][2]);
					pout.x = (pm.m[0][1] + pm.m[1][0]) / S;
					pout.y = 0.25f * S;
					pout.z = (pm.m[1][2] + pm.m[2][1]) / S;
					pout.w = (pm.m[2][0] - pm.m[0][2]) / S;
					break;
					case 2:
						S = 2.0f * sqrt(1.0f + pm.m[2][2] - pm.m[0][0] - pm.m[1][1]);
						pout.x = (pm.m[0][2] + pm.m[2][0]) / S;
						pout.y = (pm.m[1][2] + pm.m[2][1]) / S;
						pout.z = 0.25f * S;
						pout.w = (pm.m[0][1] - pm.m[1][0]) / S;
						break;
						}
		return pout;*/
}

//Set Position
void RigidBody::SetPosition(V3 p)
{
	//for (int i = 0; i < 3; i++)
	mMaster->setPosition(p);
	
}

//Create Transform
void RigidBody::Transform(Qu q, V3 p)
{
		/*mTransform.m[0][0] = 1 - ((2 * q.y * q.y) + (2 * q.z * q.z));
		mTransform.m[0][1] = ((2 * q.x*q.y) + (2 * q.z*q.w));
		mTransform.m[0][2] = ((2 * q.x*q.z) - (2 * q.y*q.w));
		
		mTransform.m[1][0] = ((2 * q.x*q.y) - (2 * q.z*q.w));
		mTransform.m[1][1] = 1 - ((2 * q.x * q.x) + (2 * q.z * q.z));
		mTransform.m[1][2] = ((2 * q.y*q.z) + (2 * q.x*q.w));
		
		mTransform.m[2][0] = ((2 * q.x*q.z) + (2 * q.y*q.w));
		mTransform.m[2][1] = ((2 * q.y*q.z) - (2 * q.x*q.w));
		mTransform.m[2][2] = 1 - ((2 * q.x * q.x) + (2 * q.y * q.y));
*/


	mTransform.m[0][0] = 1-2*(q.y*q.y+q.z*q.z);
	mTransform.m[0][1] = 2*(q.x*q.y+q.z*q.w);
	mTransform.m[0][2] = 2*(q.x*q.z-q.y*q.w);

	mTransform.m[1][0] = 2*(q.x*q.y-q.z*q.w);
	mTransform.m[1][1] = 1-2*(q.x*q.x+q.z*q.z);
	mTransform.m[1][2] = 2*(q.y*q.z+q.x*q.w);

	mTransform.m[2][0] = 2*(q.x*q.z+q.y*q.w);
	mTransform.m[2][1] = 2*(q.y*q.z-q.x*q.w);
	mTransform.m[2][2] = 1-2*(q.x*q.x+q.y*q.y);

//	D3DXMatrixTransformation(&mTransform, &p, NULL, &V3(1, 1, 1), &p, &q, &p);

		
		//SetPosition(p);

		//mTransform.m[0][3] = mTransform.m[1][3] = mTransform.m[2][3] = 0;
		//mTransform.m[3][3] = 1;
}

//Set Velocity
//void RigidBody::SetVelocity(V3 v)
//{
//	mMaster->setVelocity(v);
//}


#pragma once

#include <iostream>
#include <string>
#include <list>
#include <memory>
#include <cassert>
#include "MailDispatch.h"

using namespace std;

///forward declaration
class ASM;

///this class will represent my base state
class BaseState
{
public:
	BaseState();
	BaseState(int m_ID);						//ctor
	BaseState(ASM* f) { m_fsm = f; }
	virtual ~BaseState();						//dctor

public:
	virtual void Enter() = 0;					//enter
	virtual void Execute(double dt) = 0;		//execute
	virtual void Exit() = 0;					//exit
	virtual void HandleMsg(Mail message) = 0;	//handles the message passed in

public:
	string BaseName;							//used to switch between states
	ASM* m_fsm;

protected:
	int m_ownerID;								//stores obj's ID
};

//will create a list of shared pointers housing all the states in the machine
typedef list< shared_ptr<BaseState> > StateList;

//Finite State Machine
class ASM
{
public:
	ASM();										//ctor
	~ASM();
	
public:
	void Update(double dt);
	void HandleMsg(Mail message);
	void Transition(string BaseName);
	void Delay(string BaseName);
	void AddState(BaseState *newState, bool makeCurrent);
	string GetState();

public:
	BaseState*	currentState;
	StateList	statelist;
	string		DelayState;
};
#include "RawInput.h"
#include "DXUtil.h"
#include "D3DAPP.h"
#include <iostream>
#include "MenuState.h"

RawInput* RAW = 0;

RawInput::RawInput()
{
	devs[0].usUsagePage = 1;
	devs[0].usUsage = 6;
	devs[0].dwFlags = 0;
	devs[0].hwndTarget = g_D3DAPP->getMainWnd();

	devs[1].usUsagePage = 1;
	devs[1].usUsage = 2;
	devs[1].dwFlags = 0;
	devs[1].hwndTarget = g_D3DAPP->getMainWnd();

	for( int i = 0; i < 6; i++ )
		keys[i] = false;

	mousePos = new POINT;
	GetCursorPos(mousePos);
	ScreenToClient(g_D3DAPP->getMainWnd(), mousePos);

	if(!RegisterRawInputDevices(devs,2,sizeof(RAWINPUTDEVICE)))
	{
		MessageBox(NULL, "Raw Input Failed", "KHAAAAAAAN", MB_ICONWARNING);
		PostQuitMessage(0);
	}

	buffer = new BYTE[40];
	
	raw = RAWINPUT();

	for(UINT i = 0; i < 8; i++)
		mouseButton[i] = false;

	dx = 0; 
	dx = 0;

	regMouse = false;

}

RawInput::~RawInput(void)
{
	if (buffer)
		delete buffer;
	if (mousePos)
		delete mousePos;
	if (lastPos)
		delete lastPos;

}

void RawInput::setMousePos(POINT *point)
{
	this->mousePos = point;
}
void RawInput::setDX(float i)
{
	dx = i;
}
void RawInput::setDY(float i)
{
	dy = i;
}
bool RawInput::keyPressed(int button)
{
	return keys[button];
}

POINT RawInput::getMousePos()
{
	return *mousePos;
}
void RawInput::setRegMouse(bool in)
{
	regMouse = in;
}
bool RawInput::getregMouse()
{
	return regMouse;
}
float RawInput::getLX()
{
	float temp = dx;
	dx = 0;
	return temp;
}

float RawInput::getLY()
{
	float temp = dy;
	dy = 0;
	return temp;
}

bool RawInput::mousePressed(int button)
{

	return mouseButton[button];

}

LRESULT RawInput::handleMessage(UINT msg, WPARAM wParam, LPARAM lParam)
{

	UINT bufferSize;
	GetRawInputData((HRAWINPUT)lParam, RID_INPUT, NULL, &bufferSize, sizeof (RAWINPUTHEADER));
	if (bufferSize <= 40)
		GetRawInputData((HRAWINPUT)lParam, RID_INPUT, (LPVOID)buffer, &bufferSize, sizeof (RAWINPUTHEADER));

	raw = *(RAWINPUT*)buffer;
	
	if (raw.header.dwType == RIM_TYPEMOUSE)
	{
		//cout << "Mouse Data found\n";
		if (regMouse == true)
		{
			this->dx = (float)raw.data.mouse.lLastX;
			this->dy = (float)raw.data.mouse.lLastY;
		}

		GetCursorPos(mousePos);
		ScreenToClient(g_D3DAPP->getMainWnd(), mousePos);

		if (raw.data.mouse.ulButtons & RI_MOUSE_LEFT_BUTTON_DOWN)
			this->mouseButton[LEFT] = true;
		else if (raw.data.mouse.ulButtons & RI_MOUSE_LEFT_BUTTON_UP) 
			this->mouseButton[LEFT] = false;

		if (raw.data.mouse.ulButtons & RI_MOUSE_RIGHT_BUTTON_DOWN) 
			this->mouseButton[RIGHT] = true;
		else if (raw.data.mouse.ulButtons & RI_MOUSE_RIGHT_BUTTON_UP) 
			this->mouseButton[RIGHT] = false;

		if (raw.data.mouse.ulButtons & RI_MOUSE_BUTTON_1_DOWN)
			this->mouseButton[BUTTON1] = true;
		else if (raw.data.mouse.ulButtons & RI_MOUSE_BUTTON_1_UP)
			this->mouseButton[BUTTON1] = false;

		if (raw.data.mouse.ulButtons & RI_MOUSE_BUTTON_2_DOWN)
			this->mouseButton[BUTTON2] = true;
		else if (raw.data.mouse.ulButtons & RI_MOUSE_BUTTON_2_UP)
			this->mouseButton[BUTTON2] = false;

		if (raw.data.mouse.ulButtons & RI_MOUSE_BUTTON_3_DOWN)
			this->mouseButton[BUTTON3] = true;
		else if (raw.data.mouse.ulButtons & RI_MOUSE_BUTTON_3_UP)
			this->mouseButton[BUTTON3] = false;
		
		if (raw.data.mouse.ulButtons & RI_MOUSE_BUTTON_4_DOWN)
			this->mouseButton[BUTTON4] = true;
		else if (raw.data.mouse.ulButtons & RI_MOUSE_BUTTON_4_UP)
			this->mouseButton[BUTTON4] = false;

		if (raw.data.mouse.ulButtons & RI_MOUSE_BUTTON_5_DOWN)
			this->mouseButton[BUTTON5] = true;
		else if (raw.data.mouse.ulButtons & RI_MOUSE_BUTTON_5_UP)
			this->mouseButton[BUTTON5] = false;

	}

	if (raw.header.dwType == RIM_TYPEKEYBOARD)
	{
		switch (raw.data.keyboard.VKey)
		{
		case 0x08:
			if (raw.data.keyboard.Flags == 0)
				keys[BSPC] = true;
			else
				keys[BSPC] = false;
			break;
		case 0x09:
			if (raw.data.keyboard.Flags == 0)
				keys[TAB] = true;
			else
				keys[TAB] = false;
			break;
		case 0x0D:
			if (raw.data.keyboard.Flags == 0)
				keys[ENTER] = true;
			else
				keys[ENTER] = false;
			break;
		case 0xA4:
			if (raw.data.keyboard.Flags == 0)
				keys[LALT] = true;
			else
				keys[LALT] = false;
			break;
		case 0xA5:
			if (raw.data.keyboard.Flags == 0)
				keys[RALT] = true;
			else
				keys[RALT] = false;
			break;
		case 0x1B:
			if (raw.data.keyboard.Flags == 0)
				keys[ESC] = true;
			else
				keys[ESC] = false;
			break;
		case 0x20:
			if (raw.data.keyboard.Flags == 0)
				keys[SPACE] = true;
			else
				keys[SPACE] = false;
			break;
		case 0x21:
			if (raw.data.keyboard.Flags == 0)
				keys[PGUP] = true;
			else
				keys[PGUP] = false;
			break;
		case 0x22:
			if (raw.data.keyboard.Flags == 0)
				keys[PGDN] = true;
			else
				keys[PGDN] = false;
			break;
		case 0x23:
			if (raw.data.keyboard.Flags == 0)
				keys[END] = true;
			else
				keys[END] = false;
			break;
		case 0x24:
			if (raw.data.keyboard.Flags == 0)
				keys[HOME] = true;
			else
				keys[HOME] = false;
			break;
		case 0x25:
			if (raw.data.keyboard.Flags == 0 || raw.data.keyboard.Flags == 2)
				keys[LEFT] = true;
			else
				keys[LEFT] = false;
			break;
		case VK_UP:
			if (raw.data.keyboard.Flags == 0 || raw.data.keyboard.Flags == 2)
				keys[UP] = true;
			else
				keys[UP] = false;
			break;
		case 0x27:
			if (raw.data.keyboard.Flags == 0 || raw.data.keyboard.Flags == 2)
				keys[RIGHT] = true;
			else
				keys[RIGHT] = false;
			break;
		case VK_DOWN:
			if (raw.data.keyboard.Flags == 0 || raw.data.keyboard.Flags == 2)
				keys[DWN] = true;
			else
				keys[DWN] = false;
			break;
		case 0x2D:
			if (raw.data.keyboard.Flags == 0)
				keys[INS] = true;
			else
				keys[INS] = false;
			break;
		case 0x2E:
			if (raw.data.keyboard.Flags == 0)
				keys[DEL] = true;
			else
				keys[DEL] = false;
			break;
		case 0x30:
			if (raw.data.keyboard.Flags == 0)
				keys[ZERO] = true;
			else
				keys[ZERO] = false;
			break;
		case 0x31:
			if (raw.data.keyboard.Flags == 0)
				keys[ONE] = true;
			else
				keys[ONE] = false;
			break;
		case 0x32:
			if (raw.data.keyboard.Flags == 0)
				keys[TWO] = true;
			else
				keys[TWO] = false;
			break;
		case 0x33:
			if (raw.data.keyboard.Flags == 0)
				keys[THREE] = true;
			else
				keys[THREE] = false;
			break;
		case 0x34:
			if (raw.data.keyboard.Flags == 0)
				keys[FOUR] = true;
			else
				keys[FOUR] = false;
			break;
		case 0x35:
			if (raw.data.keyboard.Flags == 0)
				keys[FIVE] = true;
			else
				keys[FIVE] = false;
			break;
		case 0x36:
			if (raw.data.keyboard.Flags == 0)
				keys[SIX] = true;
			else
				keys[SIX] = false;
			break;
		case 0x37:
			if (raw.data.keyboard.Flags == 0)
				keys[SEVEN] = true;
			else
				keys[SEVEN] = false;
			break;
		case 0x38:
			if (raw.data.keyboard.Flags == 0)
				keys[EIGHT] = true;
			else
				keys[EIGHT] = false;
			break;
		case 0x39:
			if (raw.data.keyboard.Flags == 0)
				keys[NINE] = true;
			else
				keys[NINE] = false;
			break;
		case 0x41:
			if (raw.data.keyboard.Flags == 0)
				keys[A] = true;
			else
				keys[A] = false;
			break;
		case 0x42:
			if (raw.data.keyboard.Flags == 0)
				keys[B] = true;
			else
				keys[B] = false;
			break;
		case 0x43:
			if (raw.data.keyboard.Flags == 0)
				keys[C] = true;
			else
				keys[C] = false;
			break;
		case 0x44:
			if (raw.data.keyboard.Flags == 0)
				keys[D] = true;
			else
				keys[D] = false;
			break;
		case 0x45:
			if (raw.data.keyboard.Flags == 0)
				keys[E] = true;
			else
				keys[E] = false;
			break;
		case 0x46:
			if (raw.data.keyboard.Flags == 0)
				keys[F] = true;
			else
				keys[F] = false;
			break;
		case 0x47:
			if (raw.data.keyboard.Flags == 0)
				keys[G] = true;
			else
				keys[G] = false;
			break;
		case 0x48:
			if (raw.data.keyboard.Flags == 0)
				keys[H] = true;
			else
				keys[H] = false;
			break;
		case 0x49:
			if (raw.data.keyboard.Flags == 0)
				keys[I] = true;
			else
				keys[I] = false;
			break;
		case 0x4A:
			if (raw.data.keyboard.Flags == 0)
				keys[J] = true;
			else
				keys[J] = false;
			break;
		case 0x4B:
			if (raw.data.keyboard.Flags == 0)
				keys[K] = true;
			else
				keys[K] = false;
			break;
		case 0x4C:
			if (raw.data.keyboard.Flags == 0)
				keys[L] = true;
			else
				keys[L] = false;
			break;
		case 0x4D:
			if (raw.data.keyboard.Flags == 0)
				keys[M] = true;
			else
				keys[M] = false;
			break;
		case 0x4E:
			if (raw.data.keyboard.Flags == 0)
				keys[N] = true;
			else
				keys[N] = false;
			break;
		case 0x4F:
			if (raw.data.keyboard.Flags == 0)
				keys[O] = true;
			else
				keys[O] = false;
			break;
		case 0x50:
			if (raw.data.keyboard.Flags == 0)
				keys[P] = true;
			else
				keys[P] = false;
			break;
		case 0x51:
			if (raw.data.keyboard.Flags == 0)
				keys[Q] = true;
			else
				keys[Q] = false;
			break;
		case 0x52:
			if (raw.data.keyboard.Flags == 0)
				keys[R] = true;
			else
				keys[R] = false;
			break;
		case 0x53:
			if (raw.data.keyboard.Flags == 0)
				keys[S] = true;
			else
				keys[S] = false;
			break;
		case 0x54:
			if (raw.data.keyboard.Flags == 0)
				keys[T] = true;
			else
				keys[T] = false;
			break;
		case 0x55:
			if (raw.data.keyboard.Flags == 0)
				keys[U] = true;
			else
				keys[U] = false;
			break;
		case 0x56:
			if (raw.data.keyboard.Flags == 0)
				keys[V] = true;
			else
				keys[V] = false;
			break;
		case 0x57:
			if (raw.data.keyboard.Flags == 0)
				keys[W] = true;
			else
				keys[W] = false;
			break;
		case 0x58:
			if (raw.data.keyboard.Flags == 0)
				keys[X] = true;
			else
				keys[X] = false;
			break;
		case 0x59:
			if (raw.data.keyboard.Flags == 0)
				keys[Y] = true;
			else
				keys[Y] = false;
			break;
		case 0x5A:
			if (raw.data.keyboard.Flags == 0)
				keys[Z] = true;
			else
				keys[Z] = false;
			break;
		case 0x70:
			if (raw.data.keyboard.Flags == 0)
				keys[F1] = true;
			else
				keys[F1] = false;
			break;
		case 0x71:
			if (raw.data.keyboard.Flags == 0)
				keys[F2] = true;
			else
				keys[F2] = false;
			break;
		case 0x72:
			if (raw.data.keyboard.Flags == 0)
				keys[F3] = true;
			else
				keys[F3] = false;
			break;
		case 0x73:
			if (raw.data.keyboard.Flags == 0)
				keys[F4] = true;
			else
				keys[F4] = false;
			break;
		case 0x74:
			if (raw.data.keyboard.Flags == 0)
				keys[F5] = true;
			else
				keys[F5] = false;
			break;
		case 0x75:
			if (raw.data.keyboard.Flags == 0)
				keys[F6] = true;
			else
				keys[F6] = false;
			break;
		case 0x76:
			if (raw.data.keyboard.Flags == 0)
				keys[F7] = true;
			else
				keys[F7] = false;
			break;
		case 0x77:
			if (raw.data.keyboard.Flags == 0)
				keys[F8] = true;
			else
				keys[F8] = false;
			break;
		case 0x78:
			if (raw.data.keyboard.Flags == 0)
				keys[F9] = true;
			else
				keys[F9] = false;
			break;
		case 0x79:
			if (raw.data.keyboard.Flags == 0)
				keys[F10] = true;
			else
				keys[F10] = false;
			break;
		case 0x7A:
			if (raw.data.keyboard.Flags == 0)
				keys[F11] = true;
			else
				keys[F11] = false;
			break;
		case 0x7B:
			if (raw.data.keyboard.Flags == 0)
				keys[F12] = true;
			else
				keys[F12] = false;
			break;
		case 0x10:
			if (raw.data.keyboard.Flags == 0)
				keys[LSHIFT] = true;
			else
				keys[LSHIFT] = false;
			break;
		case 0xA1:
			if (raw.data.keyboard.Flags == 0)
				keys[RSHIFT] = true;
			else
				keys[RSHIFT] = false;
			break;
		case 0xA2:
			if (raw.data.keyboard.Flags == 0)
				keys[LCTRL] = true;
			else
				keys[LCTRL] = false;
			break;
		case 0xA3:
			if (raw.data.keyboard.Flags == 0)
				keys[RCTRL] = true;
			else
				keys[RCTRL] = false;
			break;
		case 0xBA:
			if (raw.data.keyboard.Flags == 0)
				keys[COLON] = true;
			else
				keys[COLON] = false;
			break;
		case 0xBB:
			if (raw.data.keyboard.Flags == 0)
				keys[PLUS] = true;
			else
				keys[PLUS] = false;
			break;
		case 0xBC:
			if (raw.data.keyboard.Flags == 0)
				keys[LTHAN] = true;
			else
				keys[LTHAN] = false;
			break;
		case 0xBD:
			if (raw.data.keyboard.Flags == 0)
				keys[MNUS] = true;
			else
				keys[MNUS] = false;
			break;
		case 0xBE:
			if (raw.data.keyboard.Flags == 0)
				keys[GTHAN] = true;
			else
				keys[GTHAN] = false;
			break;
		case 0xBF:
			if (raw.data.keyboard.Flags == 0)
				keys[QMARK] = true;
			else
				keys[QMARK] = false;
			break;
		case 0xC0:
			if (raw.data.keyboard.Flags == 0)
				keys[TILDE] = true;
			else
				keys[TILDE] = false;
			break;
		case 0xDB:
			if (raw.data.keyboard.Flags == 0)
				keys[LBRKT] = true;
			else
				keys[LBRKT] = false;
			break;
		case 0xDC:
			if (raw.data.keyboard.Flags == 0)
				keys[FWDSLSH] = true;
			else
				keys[FWDSLSH] = false;
			break;
		case 0xDD:
			if (raw.data.keyboard.Flags == 0)
				keys[RBRKT] = true;
			else
				keys[RBRKT] = false;
			break;
		case 0xDE:
			if (raw.data.keyboard.Flags == 0)
				keys[QUOTE] = true;
			else
				keys[QUOTE] = false;
			break;
		}

	}
	g_D3DAPP->HandleInput(&raw);
	return 0;
}
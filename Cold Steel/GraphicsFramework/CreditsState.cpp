#include "CreditsState.h"
#include "Camera.h"
#include "SoundManager.h"
#include "TankSelectBack.h"

CreditsState::CreditsState()
{

}


CreditsState::~CreditsState()
{
	if (back)
		delete back;
}

CreditsState* CreditsState::Instance()
{
	static CreditsState* ptr = new CreditsState();
	return ptr;
}

void CreditsState::Initialize()
{
	back = new TankSelectBack();

	ShowCursor(FALSE);
	
	CAM->menuReset();
	CAM->setTarget(D3DXVECTOR3(0, 0, 0));

	RM->createFont("Avenir", 50, 35, 700, false);
	RM->createFont("Arial", 35, 15, 300, false);

	SOUND->startSong("../media/music/CreditsMusic.mp3");

	CreditsPosition = D3DXVECTOR2((float)g_D3DAPP->getWidth() / 2, (float)g_D3DAPP->getHeight() + 5);

	CreateCredits();
}
void CreditsState::HandleInput(RAWINPUT* raw)
{

}

void CreditsState::UpdateScene(float dt)
{
	SOUND->update();
	back->update(dt);

	if (RAW->keyPressed(ENTER) || RAW->keyPressed(ESC))
	{
		g_D3DAPP->changeState("MENU");
		return;
	}

	if (texts.back().getPos().y < -15)
		g_D3DAPP->changeState("MENU");
}

void CreditsState::RenderScene()
{
	HR(g_D3dDevice->Clear(0, 0, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, D3DCOLOR_XRGB(84, 84, 84), 1.0F, 0));
	HR(g_D3dDevice->BeginScene());
	
	back->render();

	for (UINT i = 0; i < texts.size(); i++)
	{
		texts[i].setPos(texts[i].getPos() - D3DXVECTOR2(0, 2));

		rect.left = (LONG)texts[i].getPos().x - ((LONG)RM->getFontSize(texts[i].getFont()).x * 15);
		rect.top = (LONG)texts[i].getPos().y - (LONG)RM->getFontSize(texts[i].getFont()).y;
		rect.right = (LONG)texts[i].getPos().x + ((LONG)RM->getFontSize(texts[i].getFont()).x * 15);
		rect.bottom = (LONG)texts[i].getPos().y + (LONG)RM->getFontSize(texts[i].getFont()).y;

		RM->getFont(texts[i].getFont())->DrawText(NULL, texts[i].getText().c_str(), -1, &rect, DT_CENTER | DT_VCENTER, D3DCOLOR_XRGB(0, 255, 100));
	}

	Sleep(25);

	g_D3dDevice->EndScene();
	g_D3dDevice->Present(0, 0, 0, 0);
}

void CreditsState::OnResetDevice()
{
	back->onResetDevice();
	RM->onResetDevice();
	CreditsPosition = D3DXVECTOR2((float)g_D3DAPP->getWidth() / 2, (float)g_D3DAPP->getHeight() + 5);
}

void CreditsState::OnLostDevice()
{
	back->onLostDevice();
	RM->onLostDevice();
}

void CreditsState::Leave()
{
	RM->clearAll();
	SOUND->endSong();
	delete back;
	texts.clear();
}

void CreditsState::CreateCredits()
{
	texts.push_back(Text(0, CreditsPosition, "Crew"));
	texts.push_back(Text(1, D3DXVECTOR2(CreditsPosition.x, CreditsPosition.y + 100), "Codi Davis : Physics"));
	texts.push_back(Text(1, D3DXVECTOR2(CreditsPosition.x, CreditsPosition.y + 150), "Brenden Lyons : Graphics"));
	texts.push_back(Text(1, D3DXVECTOR2(CreditsPosition.x, CreditsPosition.y + 200), "Evan Meadows : GUI"));
	texts.push_back(Text(1, D3DXVECTOR2(CreditsPosition.x, CreditsPosition.y + 250), "Bryan O'Dell : AI"));
	texts.push_back(Text(1, D3DXVECTOR2(CreditsPosition.x, CreditsPosition.y + 300), "Jose Sanroman : Particles"));
	texts.push_back(Text(0, D3DXVECTOR2(CreditsPosition.x, CreditsPosition.y + 420), "Special Thanks"));
	texts.push_back(Text(1, D3DXVECTOR2(CreditsPosition.x, CreditsPosition.y + 520), "Professor Corey Clark"));
	texts.push_back(Text(1, D3DXVECTOR2(CreditsPosition.x, CreditsPosition.y + 570), "Our Families"));
}
//=============================================================================
// ParticleEmitter.cpp by Frank Luna (C) 2004 All Rights Reserved.
//=============================================================================

#include "ParticleEmitter.h"
#include "Camera.h"
#include "d3dApp.h"
#include <cassert>

ParticleEmitter::ParticleEmitter(const std::string& fxName,
	const std::string& techName,
	const std::string& texName,
	const D3DXVECTOR3& accel,
	const AABB& box,
	int maxNumParticles,
	float timePerParticle)
	: mAccel(accel), mBox(box), mTime(0.0f),
	mMaxNumParticles(maxNumParticles), mTimePerParticle(timePerParticle)
{

	//ID 
	mID = 3500;

	//For all Particles
	mMinimumSize = 1.0f;
	mMaximumSize = 100.0f;
	mLifeMax = 1.0f;
	mLifeMin = 0.0f;
	mStartDelay = 0.0f;
	angle = 0.0f;

	mAcceleration = D3DXVECTOR3(0, 0, 0);
	mPosition = D3DXVECTOR3(0, 0, 0);

	D3DXMatrixIdentity(&mPos);
	D3DXMatrixIdentity(&mPos2);
	D3DXMatrixIdentity(&mRotation);

	mType = 0;
	timeAccum = 0.0f;
	mRestart = true;
	mSpawned = 0;
	mFinished = false;

	mEmitType = 0;

	mPaused = false;

	// Allocate memory for maximum number of particles.
	mParticles.resize(mMaxNumParticles);
	mAliveParticles.reserve(mMaxNumParticles);
	mDeadParticles.reserve(mMaxNumParticles);

	// They start off all dead.
	for (int i = 0; i < mMaxNumParticles; ++i)
	{
		mParticles[i].lifeTime = -1.0f;
		mParticles[i].initialTime = 0.0f;
	}

	D3DXMatrixIdentity(&mWorld);
	D3DXMatrixIdentity(&mInvWorld);

	// Create the texture.
	HR(D3DXCreateTextureFromFile(g_D3dDevice, texName.c_str(), &mTex));

	// Create the FX.
	ID3DXBuffer* errors = 0;
	HR(D3DXCreateEffectFromFile(g_D3dDevice, fxName.c_str(),
		0, 0, D3DXSHADER_DEBUG, 0, &mFX, &errors));
	if (errors)
		MessageBox(0, (char*)errors->GetBufferPointer(), 0, 0);

	mhTech = mFX->GetTechniqueByName(techName.c_str());
	mhWVP = mFX->GetParameterByName(0, "gWVP");
	mhEyePosL = mFX->GetParameterByName(0, "gEyePosL");
	mhTex = mFX->GetParameterByName(0, "gTex");
	mhTime = mFX->GetParameterByName(0, "gTime");
	mhAccel = mFX->GetParameterByName(0, "gAccel");
	mhViewportHeight = mFX->GetParameterByName(0, "gViewportHeight");
	//Uniform Instance
	mhInsPos = mFX->GetParameterByName(0, "gPosition");
	mhInsVel = mFX->GetParameterByName(0, "gVelocity");
	mhInsSTL = mFX->GetParameterByName(0, "gSTL");
	mhPosH = mFX->GetParameterByName(0, "gPosH");

	// We don't need to set these every frame since they do not change.
	HR(mFX->SetTechnique(mhTech));
	HR(mFX->SetValue(mhAccel, mAccel, sizeof(D3DXVECTOR3)));
	HR(mFX->SetTexture(mhTex, mTex));

	HR(g_D3dDevice->CreateVertexBuffer(mMaxNumParticles*sizeof(Particle), D3DUSAGE_WRITEONLY | D3DUSAGE_POINTS, 0, D3DPOOL_MANAGED, &mVB, 0));

	BufferCode();
}

ParticleEmitter::~ParticleEmitter()
{
	ReleaseCOM(mFX);
	ReleaseCOM(mTex);
	ReleaseCOM(mVB);
}

float ParticleEmitter::getTime()
{
	return mTime;
}

void ParticleEmitter::setTime(float t)
{
	mTime = t;
}

const AABB& ParticleEmitter::getAABB()const
{
	return mBox;
}

void ParticleEmitter::setWorldMtx(const D3DXMATRIX& world)
{
	mWorld = world;

	// Compute the change of coordinates matrix that changes coordinates 
	// relative to world space so that they are relative to the particle
	// system's local space.
	D3DXMatrixInverse(&mInvWorld, 0, &mWorld);
}

void ParticleEmitter::addParticle()
{
	if (mStartDelay <= mTime)
	{
		if (mRestart == false)
		{
			if (mDeadParticles.size() > 0 && mSpawned < mMaxNumParticles)
			{
				// Reinitialize a particle.
				Particle* p = mDeadParticles.back();
				initParticle(*p);

				p->initialPos -= mOffSet;

				D3DXVECTOR3 newOffset;
				D3DXVec3TransformCoord(&newOffset, &mOffSet, &mRotation);
				p->initialPos += newOffset;

				D3DXVECTOR3 newVelocity = p->initialVelocity;
				D3DXVec3TransformCoord(&newVelocity, &newVelocity, &mRotation);
				p->initialVelocity = newVelocity;

				// No longer dead.
				mDeadParticles.pop_back();
				mAliveParticles.push_back(p);
				mSpawned++;
			}
		}
		else
		{
			if (mDeadParticles.size() > 0)
			{
				// Reinitialize a particle.
				Particle* p = mDeadParticles.back();
				initParticle(*p);
			
				D3DXVECTOR3 oldOffSet = mOffSet;

				p->initialPos -= mOffSet;

				D3DXVECTOR3 newOffset;
				D3DXVec3TransformCoord(&newOffset, &mOffSet, &mRotation);
				p->initialPos += newOffset;

				// No longer dead.
				mDeadParticles.pop_back();
				mAliveParticles.push_back(p);
				mSpawned++;
			}
		}
	}
}

void ParticleEmitter::CircleParticleOut()
{
	//if(mEmitType == 1)
	for (int i = 0; i < mMaxNumParticles; i++)
	{
		// Reinitialize a particle.
		Particle* p = mDeadParticles.back();
		initParticle(*p);

		static float angle;

		D3DXVECTOR4 newPos;

		D3DXMATRIX matrixX;

		angle += 360.0f / mMaxNumParticles;

		if (angle >= 360)
			angle = 0.0f;

		D3DXVECTOR3 pos = p->initialPos;
		if (pos.x == 0.0f)
			pos.x += 0.001f;
		if (pos.y == 0.0f)
			pos.y += 0.001f;
		if (pos.z == 0.0f)
			pos.z += 0.001f;

		D3DXVECTOR3 oldPos = mPosition;

		D3DXMatrixIdentity(&matrixX);

		D3DXMatrixRotationY(&matrixX, angle);

		D3DXVec3Transform(&newPos, &pos, &matrixX);

		pos.x = newPos.x;
		pos.y = newPos.y;
		pos.z = newPos.z;

		//p->initialPos = oldPOs;
		static int flip = 0;
		D3DXVECTOR3 newVel;
		if (flip == 0)
		{
			flip++;
			newVel = pos - oldPos;
		}
		else
		{
			newVel = oldPos - pos;
			flip = 0;
		}

		//p->lifeTime = 0.5f;

		D3DXVec3Normalize(&newVel, &newVel);

		if (newVel.x == 0.0f && newVel.y == 0.0f && newVel.z == 0.0f)
			newVel = D3DXVECTOR3(1.0f, 0.0f, 0.0f);

		p->initialPos += newVel;//*10

		newVel = newVel * 12;// *20

		p->initialVelocity = newVel;

		// No longer dead.
		mDeadParticles.pop_back();
		mAliveParticles.push_back(p);
		if (mDeadParticles.empty())
			break;
	}
}

void ParticleEmitter::CircleParticleOutZ()
{
	//D3DXMatrixInverse(&mRotation, 0 , &mRotation);
	static float angle;

	D3DXVECTOR4 newPos;

	D3DXMATRIX matrixX;

	static int flip = 0;

	D3DXVECTOR3 newVel;

	//if(mEmitType == 3)
	for (int i = 0; i < mMaxNumParticles; i++)
	{
		// Reinitialize a particle.
		Particle* p = mDeadParticles.back();
		initParticle(*p);

		angle += 360.0f / mMaxNumParticles;

		if (angle >= 360)
			angle = 0.0f;

		D3DXVECTOR3 pos = p->initialPos;

		if (pos.x == 0.0f)
			pos.x += 0.001f;
		if (pos.y == 0.0f)
			pos.y += 0.001f;
		if (pos.z == 0.0f)
			pos.z += 0.001f;

		D3DXVECTOR3 oldPos = mPosition;

		D3DXMatrixIdentity(&matrixX);

		D3DXMatrixRotationX(&matrixX, angle);

		D3DXVec3Transform(&newPos, &pos, &matrixX);

		pos.x = newPos.x;
		pos.y = newPos.y;
		pos.z = newPos.z;

		if (flip == 0)
		{
			flip++;
			newVel = pos - oldPos;
		}
		else
		{
			newVel = oldPos - pos;
			flip = 0;
		}

		//p->lifeTime = 1.8f;

		D3DXVec3Normalize(&newVel, &newVel);

		if (newVel.x == 0.0f && newVel.y == 0.0f && newVel.z == 0.0f)
			newVel = D3DXVECTOR3(0.0f, 0.0f, 1.0f);

		//p->initialPos += newVel * 10;

		newVel = newVel * 12; // *20

		p->initialPos -= mOffSet;

		D3DXVECTOR3 newOffset = mOffSet;

		//mRotation._41 = 0.0f;
		//mRotation._42 = 0.0f;
		//mRotation._43 = 0.0f;
		//mRotation._44 = 1.0f;

		D3DXVec3TransformCoord(&newOffset, &mOffSet, &mRotation);

		p->initialPos += newOffset;

		D3DXVECTOR3 newVelocity;
		D3DXVec3TransformCoord(&newVelocity, &newVel, &mRotation);
		newVel = newVelocity;

		p->initialVelocity = newVel;

		// No longer dead.
		mDeadParticles.pop_back();
		mAliveParticles.push_back(p);
		if (mDeadParticles.empty())
			break;
	}
}
void ParticleEmitter::CircleParticleIn()
{
	//if(mEmitType == 2)
	for (int i = 0; i < mMaxNumParticles; i++)
	{
		// Reinitialize a particle.
		Particle* p = mDeadParticles.back();
		initParticle(*p);

		static float angle;

		D3DXVECTOR4 newPos;

		D3DXMATRIX matrixX;

		angle += 360 / mMaxNumParticles;

		if (angle >= 360)
			angle = 0.0f;

		D3DXVECTOR3 pos = p->initialPos;

		if (pos.x == 0.0f)
			pos.x += 0.001f;
		if (pos.y == 0.0f)
			pos.y += 0.001f;
		if (pos.z == 0.0f)
			pos.z += 0.001f;

		D3DXVECTOR3 oldPos = mPosition;

		D3DXMatrixIdentity(&matrixX);

		D3DXMatrixRotationY(&matrixX, angle);

		D3DXVec3Transform(&newPos, &pos, &matrixX);

		pos.x = newPos.x;
		pos.y = newPos.y;
		pos.z = newPos.z;

		//p->initialPos = oldPOs;
		static int flip = 0;
		D3DXVECTOR3 newVel;
		if (flip == 0)
		{
			flip++;
			newVel = pos - oldPos;
		}
		else
		{
			newVel = oldPos - pos;
			flip = 0;
		}

		//p->lifeTime = 2.0f;

		D3DXVec3Normalize(&newVel, &newVel);

		if (newVel.x == 0.0f && newVel.y == 0.0f && newVel.z == 0.0f)
			newVel = D3DXVECTOR3(1.0f, 0.0f, 0.0f);

		p->initialPos += newVel * 25;

		newVel = newVel * -10;

		p->initialVelocity = newVel;

		// No longer dead.
		mDeadParticles.pop_back();
		mAliveParticles.push_back(p);
		if (mDeadParticles.empty())
			break;
	}
}
void ParticleEmitter::CircleParticleInCenter()
{
	//if(mEmitType == 4)
	for (int i = 0; i < mMaxNumParticles; i++)
	{
		// Reinitialize a particle.
		Particle* p = mDeadParticles.back();
		initParticle(*p);

		static float angle;

		D3DXVECTOR4 newPos;

		D3DXMATRIX matrixX;

		angle += 360 / mMaxNumParticles;

		if (angle >= 360)
			angle = 0.0f;

		D3DXVECTOR3 pos = p->initialPos;

		D3DXVECTOR3 oldPos = mPosition;

		D3DXMatrixIdentity(&matrixX);

		D3DXMatrixRotationY(&matrixX, angle);

		D3DXVec3Transform(&newPos, &pos, &matrixX);

		pos.x = newPos.x;
		pos.y = newPos.y;
		pos.z = newPos.z;

		//p->initialPos = oldPOs;
		static int flip = 0;
		D3DXVECTOR3 newVel;
		if (flip == 0)
		{
			flip++;
			newVel = pos - oldPos;
		}
		else
		{
			newVel = oldPos - pos;
			flip = 0;
		}

		//p->lifeTime = 2.0f;

		D3DXVec3Normalize(&newVel, &newVel);

		if (newVel.x == 0.0f && newVel.y == 0.0f && newVel.z == 0.0f)
			newVel = D3DXVECTOR3(1.0f, 0.0f, 0.0f);

		p->initialPos += newVel * 18;

		newVel = newVel * -10;

		newVel.y = -13.0f;

		p->initialVelocity = newVel;

		// No longer dead.
		mDeadParticles.pop_back();
		mAliveParticles.push_back(p);
		if (mDeadParticles.empty())
			break;
	}
}

void ParticleEmitter::FireWorkStyle()
{
	//if(mEmitType == 5)
	for (int i = 0; i < mMaxNumParticles; i++)
	{
		// Reinitialize a particle.
		Particle* p = mDeadParticles.back();
		initParticle(*p);

		D3DXVECTOR3 vel;

		vel = D3DXVECTOR3(GetRandomFloat(-1.0f, 1.0f), GetRandomFloat(-1.0f, 1.0f), GetRandomFloat(-1.0f, 1.0f));

		D3DXVec3Normalize(&vel, &vel);

		vel *= 40.0f; //50

		p->initialVelocity = vel;

		// No longer dead.
		mDeadParticles.pop_back();
		mAliveParticles.push_back(p);
		if (mDeadParticles.empty())
			break;
	}
}

void ParticleEmitter::onLostDevice()
{
	HR(mFX->OnLostDevice());

	// Default pool resources need to be freed before reset.
	ReleaseCOM(mVB);
}

void ParticleEmitter::onResetDevice()
{
	HR(mFX->OnResetDevice());

	// Default pool resources need to be recreated after reset.
	if (mVB == 0)
	{
		HR(g_D3dDevice->CreateVertexBuffer(mMaxNumParticles*sizeof(Particle), D3DUSAGE_DYNAMIC | D3DUSAGE_WRITEONLY | D3DUSAGE_POINTS, 0, D3DPOOL_DEFAULT, &mVB, 0));
	}
}

void ParticleEmitter::update(float dt)
{
	if (mPaused == false)
		mTime += dt;

	// Rebuild the dead and alive list.  Note that resize(0) does
	// not deallocate memory (i.e., the capacity of the vector does
	// not change).
	mDeadParticles.resize(0);
	mAliveParticles.resize(0);

	// For each particle.
	for (int i = 0; i < mMaxNumParticles; ++i)
	{
		// Is the particle dead?
		if ((mTime - mParticles[i].initialTime) > mParticles[i].lifeTime)
		{
			mParticles[i].initialSize = 0;
			mDeadParticles.push_back(&mParticles[i]);
		}
		else
		{
			mAliveParticles.push_back(&mParticles[i]);
		}
	}

	if (mRestart == false && mDeadParticles.size() == mMaxNumParticles && mFinished == false && mSpawned > 0)
	{
		mFinished = true;
	}

	// A negative or zero mTimePerParticle value denotes
	// not to emit any particles.
	if (mTimePerParticle > 0.0f)
	{
		if (mEmitType == 0)
		{
			// Emit particles.
			//static float timeAccum = 0.0f;
			if (mPaused == false)
				timeAccum += dt;
			while (timeAccum >= mTimePerParticle)
			{
				addParticle();
				timeAccum -= mTimePerParticle;
			}
		}
		if (mEmitType == 1 && mDeadParticles.size() == mMaxNumParticles && mFinished == false && mStartDelay <= mTime)
		{
			if (mOldAcc != mAcceleration)
			{
				HR(mFX->SetValue(mhAccel, mAccel, sizeof(D3DXVECTOR3)));
			}
			CircleParticleOut();

			mSpawned++;
		}
		if (mEmitType == 2 && mDeadParticles.size() == mMaxNumParticles && mFinished == false && mStartDelay <= mTime)
		{
			if (mOldAcc != mAcceleration)
			{
				HR(mFX->SetValue(mhAccel, mAccel, sizeof(D3DXVECTOR3)));
			}
			CircleParticleIn();

			mSpawned++;
		}
		if (mEmitType == 3 && mDeadParticles.size() == mMaxNumParticles && mFinished == false && mStartDelay <= mTime)
		{
			if (mOldAcc != mAcceleration)
			{
				HR(mFX->SetValue(mhAccel, mAccel, sizeof(D3DXVECTOR3)));
			}
			CircleParticleOutZ();

			mSpawned++;
		}
		if (mEmitType == 4 && mDeadParticles.size() == mMaxNumParticles && mFinished == false && mStartDelay <= mTime)
		{
			if (mOldAcc != mAcceleration)
			{
				HR(mFX->SetValue(mhAccel, mAccel, sizeof(D3DXVECTOR3)));
			}
			CircleParticleInCenter();

			mSpawned++;
		}
		if (mEmitType == 5 && mDeadParticles.size() == mMaxNumParticles && mFinished == false && mStartDelay <= mTime)
		{
			if (mOldAcc != mAcceleration)
			{
				HR(mFX->SetValue(mhAccel, mAccel, sizeof(D3DXVECTOR3)));
			}
			FireWorkStyle();
			
			mSpawned++;
		}
	}
}

void ParticleEmitter::draw()
{

	// Get camera position relative to world space system and make it 
	// relative to the particle system's local system.
	D3DXVECTOR3 eyePosW = CAM->getPos();
	D3DXVECTOR3 eyePosL;
	D3DXVec3TransformCoord(&eyePosL, &eyePosW, &mInvWorld);
	
	// Set FX parameters.
	HR(mFX->SetValue(mhEyePosL, &eyePosL, sizeof(D3DXVECTOR3)));
	HR(mFX->SetFloat(mhTime, mTime));
	HR(mFX->SetMatrix(mhWVP, &(mWorld * CAM->getViewProjMat())));

	// Point sprite sizes are given in pixels.  So if the viewport size 
	// is changed, then more or less pixels become available, which alters
	// the perceived size of the particles.  For example, if the viewport
	// is 32x32, then a 32x32 sprite covers the entire viewport!  But if
	// the viewport is 1024x1024, then a 32x32 sprite only covers a small
	// portion of the viewport.  Thus, we scale the particle's
	// size by the viewport height to keep them in proportion to the 
	// viewport dimensions.
	HWND hwnd = g_D3DAPP->getMainWnd();
	RECT clientRect;
	GetClientRect(hwnd, &clientRect);
	HR(mFX->SetInt(mhViewportHeight, clientRect.bottom));

	UINT numPasses = 0;
	HR(mFX->Begin(&numPasses, 0));
	HR(mFX->BeginPass(0));

	HR(g_D3dDevice->SetStreamSource(0, mVB, 0, sizeof(Particle)));
	HR(g_D3dDevice->SetVertexDeclaration(Particle::Decl));

	D3DXMATRIX WorldPos;
	

	D3DXVECTOR3 instanceSTL;
	for (int i = 0; i < mMaxNumParticles; i++)
	{
		if (mParticles[i].initialSize > 0)
		{
			instanceSTL.x = mParticles[i].initialSize;
			instanceSTL.y = mParticles[i].initialTime;
			instanceSTL.z = mParticles[i].lifeTime;

			HR(mFX->SetValue(mhInsPos, mParticles[i].initialPos, sizeof(D3DXVECTOR3)));
			HR(mFX->SetValue(mhInsVel, mParticles[i].initialVelocity, sizeof(D3DXVECTOR3)));
			HR(mFX->SetValue(mhInsSTL, instanceSTL, sizeof(D3DXVECTOR3)));
		
			HR(mFX->CommitChanges());
			HR(g_D3dDevice->DrawPrimitive(D3DPT_POINTLIST, i, 1));

		}
	}

	HR(mFX->EndPass());
	HR(mFX->End());
}

void ParticleEmitter::BufferCode()
{
	// Initial lock of VB for writing.
	Particle* p = 0;

	HR(mVB->Lock(0, 0, (void**)&p, D3DLOCK_DISCARD));

	// For each living particle.
	for (int i = 0; i < mMaxNumParticles; ++i)
	{
		// Copy particle to VB
		p[i] = mParticles[i];
	}

	HR(mVB->Unlock());

}

int ParticleEmitter::getAliveParticleCount()
{
	return mAliveParticles.size();
}

void ParticleEmitter::SetPosition(D3DXVECTOR3 pos)
{
	mPosition = pos + mOffSet;
}

D3DXVECTOR3 ParticleEmitter::GetPosition()
{
	return mPosition;
}

void ParticleEmitter::SetMaximumSize(float max)
{
	if (mMaximumSize > mMinimumSize)
		mMaximumSize = max;
}

void ParticleEmitter::SetMinimumMaxSizes(float min, float max)
{
	mMinimumSize = min;
	mMaximumSize = max;
}

void ParticleEmitter::SetMinimumSize(float min)
{
	if (mMinimumSize - 1 < mMaximumSize)
		mMinimumSize = min;

}

float ParticleEmitter::GetMinimumSize()
{
	return mMinimumSize;
}

float ParticleEmitter::GetMaximumSize()
{
	return mMaximumSize;
}

void ParticleEmitter::SetRestart(bool restart)
{
	mRestart = restart;
}

bool ParticleEmitter::getRestart()
{
	return mRestart;
}

int ParticleEmitter::getType()
{
	return mType;
}

void ParticleEmitter::setType(int type)
{
	mType = type;
}

D3DXVECTOR3 ParticleEmitter::GetSize()
{
	return D3DXVECTOR3(mMinimumSize, mMaximumSize, 0.0f);
}

void ParticleEmitter::SetDelay(float delay)
{
	mTimePerParticle = delay;
}

float ParticleEmitter::GetDelay()
{
	return mTimePerParticle;
}

void ParticleEmitter::SetLifeMax(float max)
{
	mLifeMax = max;
}

void ParticleEmitter::SetLifeMin(float min)
{
	mLifeMin = min;
}

D3DXVECTOR3 ParticleEmitter::GetLife()
{
	return D3DXVECTOR3(mLifeMin, mLifeMax, 0.0f);
}

void ParticleEmitter::SetMaxparticles(int max)
{
	mMaxNumParticles = max;

	mParticles.resize(mMaxNumParticles);
	mAliveParticles.reserve(mMaxNumParticles);
	mDeadParticles.reserve(mMaxNumParticles);

	// They start off all dead.
	for (int i = 0; i < mMaxNumParticles; ++i)
	{
		mParticles[i].lifeTime = -1.0f;
		mParticles[i].initialTime = 0.0f;
	}

}

int ParticleEmitter::GetMaxParticles()
{
	return mMaxNumParticles;
}

void ParticleEmitter::SetVelXMin(float min)
{
	mVelocityX.x = min;
}
void ParticleEmitter::SetVelXMax(float max)
{
	mVelocityX.y = max;
}
void ParticleEmitter::SetVelX(D3DXVECTOR3 velx)
{
	mVelocityX.x = velx.x;
	mVelocityX.y = velx.y;
}
void ParticleEmitter::SetVelYMin(float min)
{
	mVelocityY.x = min;
}
void ParticleEmitter::SetVelYMax(float max)
{
	mVelocityY.y = max;
}
void ParticleEmitter::SetVelY(D3DXVECTOR3 vely)
{
	mVelocityY.x = vely.x;
	mVelocityY.y = vely.y;
}
void ParticleEmitter::SetVelZMin(float min)
{
	mVelocityZ.x = min;
}
void ParticleEmitter::SetVelZMax(float max)
{
	mVelocityZ.y = max;
}
void ParticleEmitter::SetVelZ(D3DXVECTOR3 velz)
{
	mVelocityZ.x = velz.x;
	mVelocityZ.y = velz.y;
}

D3DXVECTOR3 ParticleEmitter::GetVelocityX()
{
	return mVelocityX;
}
D3DXVECTOR3 ParticleEmitter::GetVelocityY()
{
	return mVelocityY;
}
D3DXVECTOR3 ParticleEmitter::GetVelocityZ()
{
	return mVelocityZ;
}

void ParticleEmitter::SetStartDelay(float time)
{
	mStartDelay = time;
}

float ParticleEmitter::GetStartDelay()
{
	return mStartDelay;
}

void ParticleEmitter::SetEmitterType(int type)
{
	mEmitType = type;
}

void ParticleEmitter::SetAcceleration(D3DXVECTOR3 acc)
{
	mOldAcc = mAccel;
	mAccel = acc;
	if (acc != D3DXVECTOR3(0, 0, 0))
		HR(mFX->SetValue(mhAccel, mAccel, sizeof(D3DXVECTOR3)));
	//mAcceleration = acc;
}

int ParticleEmitter::getEmitterType()
{
	return mEmitType;
}

void ParticleEmitter::Reset()
{
	// They start off all dead.
	for (int i = 0; i < mMaxNumParticles; ++i)
	{
		mParticles[i].lifeTime = -1.0f;
		mParticles[i].initialTime = 0.0f;
	}

}

bool ParticleEmitter::isFinished()
{
	return mFinished;
}

void ParticleEmitter::setRotationMtx(D3DXMATRIX rot)
{
	mRotation = rot;
	D3DXMatrixInverse(&mRotation, 0, &mRotation);
	mRotation._41 = 0.0f;
	mRotation._42 = 0.0f;
	mRotation._43 = 0.0f;
	mRotation._44 = 1.0f;

}

void ParticleEmitter::setParticleID(int id)
{
	mID = id;
}

int ParticleEmitter::getParticleID()
{
	return mID;
}

void ParticleEmitter::setOffset(D3DXVECTOR3 offset)
{
	mOffSet = offset;
}

D3DXVECTOR3 ParticleEmitter::getOffset()
{
	return mOffSet;
}

void ParticleEmitter::setPaused(bool paused)
{
	mPaused = paused;
}
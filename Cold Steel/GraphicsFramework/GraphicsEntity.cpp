#include "GraphicsEntity.h"


GraphicsEntity::GraphicsEntity()
{
	m_MeshID = 0;
	m_Pos = D3DXVECTOR3(0, 0, 0);
	m_Scale = D3DXVECTOR3(0, 0, 0);
	D3DXQuaternionIdentity(&orientation);
}

GraphicsEntity::GraphicsEntity(UINT id, D3DXVECTOR3 pos, D3DXVECTOR3 sca, D3DXQUATERNION orient)
{
	this->m_MeshID = id;
	this->m_Pos = pos;
	this->m_Scale = sca;
	this->orientation = orient;
}


GraphicsEntity::~GraphicsEntity()
{

}

void GraphicsEntity::setPos(D3DXVECTOR3 pos)
{
	this->m_Pos = pos;
}
void GraphicsEntity::setScale(D3DXVECTOR3 scale)
{
	this->m_Scale = scale;
}
void GraphicsEntity::setOrientation(D3DXQUATERNION orient)
{
	this->orientation = orient;
}

D3DXQUATERNION GraphicsEntity::getOrientation()
{
	return orientation;
}

D3DXMATRIX GraphicsEntity::getTransform()
{
	D3DXMATRIX mat;
	D3DXMatrixTransformation(&mat, NULL, NULL, &m_Scale, NULL, &orientation, &m_Pos);
	return mat;
}

UINT GraphicsEntity::getMeshID()
{
	return m_MeshID;
}

D3DXVECTOR3 GraphicsEntity::getPos()
{
	return m_Pos;
}

float GraphicsEntity::getBaseRot()
{
	return baseRot;
}

void GraphicsEntity::setBaseRot(float rot)
{
	baseRot = rot;
}
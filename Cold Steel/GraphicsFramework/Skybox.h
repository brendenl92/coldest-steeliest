#pragma once
#include "dxutil.h"

class Skybox
{

	LPD3DXMESH mesh;
	IDirect3DCubeTexture9* mountains;
	IDirect3DCubeTexture9* sunset;
	IDirect3DCubeTexture9* sunrise;
	IDirect3DCubeTexture9* dusk;


	D3DXMATRIX world;
public:
	Skybox();
	~Skybox();

	LPD3DXMESH getMesh();
	IDirect3DCubeTexture9* getTex(int index);
	
	D3DXMATRIX getWorld();

	void onLostDevice();
	void onResetDevice();
};


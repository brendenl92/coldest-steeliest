//Attack.cpp
#include "Attack.h"
#include "BaseAIEntity.h"
#include "AIManager.h"

Attack* Attack::s_Instance = 0;
Attack* Attack::Instance()
{
	if(!s_Instance)
		s_Instance = new Attack;
	return s_Instance;
}

Attack::Attack() 
	: BaseState()
{
	BaseName	= "ATTACK";
	numMsg1		= 0;
	numMsg2		= 0;
	numMsg3		= 0;
}

Attack::Attack(ASM* fStatem, int id)
	: BaseState(fStatem)
{
	BaseName	= "ATTACK";
	m_ownerID	= id;
	numMsg1		= 0;
	numMsg2		= 0;
	numMsg3		= 0;
}

Attack::~Attack()
{
}

void Attack::Enter()
{
}

void Attack::Execute(double dt)
{
	
}

void Attack::Exit()
{

}

void Attack::HandleMsg(Mail message)
{
	switch (message.m_Msg)
	{
	case _OnPatrol :
		{
			m_fsm->Transition("WANDER");
			//MD->SendToState(message.m_Reciever, message.m_Reciever, 0, _OnPatrol, (void*)0);
			AMI->GetBaseAIEntity()[message.m_Reciever]->SetState(1);
			break;
		}
	case _LockedOn : 
		{
			if (AMI->GetBaseAIEntity()[message.m_Reciever]->Enemy == NULL)
			{
				m_fsm->Transition("WANDER");
				AMI->GetBaseAIEntity()[message.m_Reciever]->SetState(1);
			}
			else if (AMI->GetBaseAIEntity()[message.m_Reciever]->GetCurrentHP() > 50)
			{
				m_fsm->Transition("ATTACK");
				//MD->SendToState(message.m_Reciever, message.m_Reciever, 0, _LockedOn, (void*)0);
				AMI->GetBaseAIEntity()[message.m_Reciever]->SetState(2);
			}
			else if (AMI->GetBaseAIEntity()[message.m_Reciever]->GetCurrentHP() < 50)
			{
				m_fsm->Transition("FLEE");
				//MD->SendToState(message.m_Reciever, message.m_Reciever, 0, _FallingBack, (void*)0);
				AMI->GetBaseAIEntity()[message.m_Reciever]->SetState(3);
			}
			break;
		}
	case _FallingBack :
		{
			m_fsm->Transition("FLEE");
			//MD->SendToState(message.m_Reciever, message.m_Reciever, 0, _FallingBack, (void*)0);
			AMI->GetBaseAIEntity()[message.m_Reciever]->SetState(3);
			break;
		}
	}
}

void Attack::ShutDown()
{
	s_Instance = NULL;
}
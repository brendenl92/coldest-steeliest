#pragma once

#include "FuzzyModule.h"
#include "Weapon.h"
#include "AIManager.h"

class WeaponManager
{
private:
	FuzzyModule*	fm;
	double			m_wRifleDOM;
	double			m_wRocketDOM;
	double			m_wPistolDOM;
	int				currentWeaponID;

public:
	WeaponSet*		m_wSet;

	///time checks
	double			checkTime;
	double			timeTolerance;

public:
	WeaponManager()
	{
		fm				= new FuzzyModule();
		m_wSet			= new WeaponSet();
		currentWeaponID = 0;
		checkTime		= 0;
		timeTolerance	= .90;
	}

	~WeaponManager()
	{
		delete fm;
		delete m_wSet;
	}

	double	CalcRifle(double distance, double ammo);
	double	CalcRocket(double distance, double ammo);
	double	CalcPistol(double distance, double ammo);
	void	SetTermsRules();
	int		Calculate(double distance, double rifleammo, double rocketammo, double pistolammo);
};
//AStar.cpp
#include "AStar.h"
#include "AIManager.h"
#include "BaseAIEntity.h"
#include "MailDispatch.h"

AStar* AStar::s_Instance = 0;
AStar* AStar::Instance()
{
	if(!s_Instance)
		s_Instance = new AStar;
	return s_Instance;
}

AStar::AStar() 
	: BaseState()
{
	BaseName	= "ASTAR";
	numMsg1		= 0;
	numMsg2		= 0;
	numMsg3		= 0;
}

AStar::AStar(ASM* fStatem, int id)
	: BaseState(fStatem)
{
	BaseName	= "ASTAR";
	m_ownerID	= id;
	numMsg1		= 0;
	numMsg2		= 0;
	numMsg3		= 0;
}

AStar::~AStar()
{
	delete Instance();
}

void AStar::Enter()
{
	///change objects color here
}

void AStar::Execute(double dt)
{
	
}

void AStar::Exit()
{

}

void AStar::HandleMsg(Mail message)
{
	/*switch (message.m_Msg)
	{
	case _OnPatrol :
		{
			m_fsm->Transition("WANDER");
			AMI->GetBaseAIEntity()[message.m_Reciever]->SetState(1);
			break;
		}
	case _LockedOn : 
		{
			if(!AMI->GetBaseAIEntity()[message.m_Sender]->m_BPSF.empty())
			{
				m_fsm->Transition("ATTACK");
				AMI->GetBaseAIEntity()[message.m_Reciever]->SetState(2);
			}
			break;
		}
	case _FallingBack :
		{
			if(AMI->GetBaseAIEntity()[message.m_Reciever]->GetWeight() < AMI->GetBaseAIEntity()[message.m_Sender]->GetWeight())
			{
				m_fsm->Transition("FLEE");
				AMI->GetBaseAIEntity()[message.m_Reciever]->SetState(3);
			}
			break;
		}
	case _Calculating :
		{
			if(AMI->GetBaseAIEntity()[message.m_Sender]->m_BPSF.empty())
			{
				int temp = GD->GetNodeList().size();

				for(int i = 0; i < temp; i++)
				{
					if(!GD->GetNodeList().empty())
					{
						GD->GetNodeList().pop();
					}
				}

				///here instead of setting my start to the "current node in my list I want to set it to the closest
				AMI->GetBaseAIEntity()[message.m_Reciever]->m_start = GD->calcClosestRoom(AMI->GetBaseAIEntity()[message.m_Reciever]->Base->getPosition());
				AMI->GetBaseAIEntity()[message.m_Reciever]->m_finish = rand() % 25;
				
				///if start "==" finish, reroll finish
				if(AMI->GetBaseAIEntity()[message.m_Reciever]->m_start == AMI->GetBaseAIEntity()[message.m_Reciever]->m_finish)
					AMI->GetBaseAIEntity()[message.m_Reciever]->m_finish = rand() % 25;

				AMI->GetBaseAIEntity()[message.m_Reciever]->m_BPSF = GD->calcShortestPath(AMI->GetBaseAIEntity()[message.m_Reciever]);

				m_fsm->Transition("ATTACK");
				AMI->GetBaseAIEntity()[message.m_Reciever]->stateID = 2;
			}
			break;
		}
	}*/
}
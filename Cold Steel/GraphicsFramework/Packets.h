#pragma once

struct ClientPosition
{
	char clientID;
	int x;
	int y;
	int z;
};

struct Server_ClientJoin //PACKET 0
{
	char packetID;
	char clientID;
	ClientPosition data;
};

struct Server_UpdatePosition //PACKET 1
{
	char packetID;
	ClientPosition data[8];
};

struct Client_UpdatePosition //PACKET 2
{
	char packetID;
	char clientID;
	int x;
	int y;
	int z;
};

struct Client_Shoot //PACKET3
{
	char packetID;
	char clientID;
};
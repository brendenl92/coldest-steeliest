#include "FuzzyModule.h"

void FuzzyModule::SetToZero()
{
	///set the rules DOM to zero
	for(auto i = m_Rules.begin(); i != m_Rules.end(); ++i)
	{
		(*i)->SetToZero();
	}
}

FuzzyVariable& FuzzyModule::CreateFLV(const string& VarName)
{
	m_Variables[VarName] = new FuzzyVariable();

	return *m_Variables[VarName];
}

void FuzzyModule::AddRule(FuzzyTerm& precede, FuzzyTerm& follow) 
{
	FuzzyRule* tempR = new FuzzyRule(precede, follow);
	m_Rules.push_back(tempR);
}

void FuzzyModule::Fuzzify(const string& FLV, double val)
{
	m_Variables[FLV]->Fuzzify(val);
}

double FuzzyModule::Defuzzify(const string& key)
{
	///clear all the DOMs 
	SetToZero();

	///process the rules
	for(auto i = m_Rules.begin(); i != m_Rules.end(); ++i)
	{
		(*i)->Calculate();
	}

	///now defuzzify the result
	return m_Variables[key]->DefuzzifyMaxAV();
}
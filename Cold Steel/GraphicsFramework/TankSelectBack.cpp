#include "TankSelectBack.h"
#include "D3DAPP.h"


TankSelectBack::TankSelectBack()
{
	string meshes[17];
	meshes[0] = "../media/models/HT_1_0.X";
	meshes[1] = "../media/models/HT_1_1.X";
	meshes[2] = "../media/models/HT_2_0.X";
	meshes[3] = "../media/models/HT_2_1.X";
	meshes[4] = "../media/models/HT_3_0.X";
	meshes[5] = "../media/models/HT_3_1.X";
	meshes[6] = "../media/models/HT_4_0.X";
	meshes[7] = "../media/models/HT_4_0.X";
	meshes[8] = "../media/models/HT_5_0.X";
	meshes[9] = "../media/models/HT_5_1.X";
	meshes[10] = "../media/models/HT_6_0.x";
	meshes[11] = "../media/models/HT_6_1.x";
	meshes[12] = "../media/models/HT_7_0.x";
	meshes[13] = "../media/models/HT_7_1.x";
	meshes[14] = "../media/models/HT_8_0.x";
	meshes[15] = "../media/models/HT_8_1.x";
	meshes[16] = "../media/models/tankSelectBack.x";

	RM->ImportMeshes(meshes, 17, 0);

	D3DXQUATERNION temp;
	D3DXQuaternionIdentity(&temp);
	Entities.push_back(GraphicsEntity(0, D3DXVECTOR3(0, 0, 37), D3DXVECTOR3(.4f, .4f, .4f), temp));
	Entities.push_back(GraphicsEntity(1, D3DXVECTOR3(0, 0, 37), D3DXVECTOR3(.4f, .4f, .4f), temp));
	D3DXQuaternionRotationAxis(&temp, &D3DXVECTOR3(0, 1, 0), D3DXToRadian(180));
	Entities[0].setBaseRot(D3DXToRadian(180));
	Entities[1].setBaseRot(D3DXToRadian(180));
	Entities.push_back(GraphicsEntity(2, D3DXVECTOR3(0, 0, -37), D3DXVECTOR3(.4f, .4f, .4f), temp));
	Entities.push_back(GraphicsEntity(3, D3DXVECTOR3(0, 0, -37), D3DXVECTOR3(.4f, .4f, .4f), temp));
	D3DXQuaternionRotationAxis(&temp, &D3DXVECTOR3(0, 1, 0), D3DXToRadian(90));
	Entities[0].setBaseRot(D3DXToRadian(90));
	Entities[1].setBaseRot(D3DXToRadian(90));
	Entities.push_back(GraphicsEntity(4, D3DXVECTOR3(37, 0, 0), D3DXVECTOR3(.2f, .2f, .2f), temp));
	Entities.push_back(GraphicsEntity(5, D3DXVECTOR3(37, 0, 0), D3DXVECTOR3(.2f, .2f, .2f), temp));
	D3DXQuaternionRotationAxis(&temp, &D3DXVECTOR3(0, 1, 0), D3DXToRadian(-90));
	Entities[0].setBaseRot(D3DXToRadian(-90));
	Entities[1].setBaseRot(D3DXToRadian(-90));
	Entities.push_back(GraphicsEntity(6, D3DXVECTOR3(-37, 0, 0), D3DXVECTOR3(.2f, .2f, .2f), temp));
	Entities.push_back(GraphicsEntity(7, D3DXVECTOR3(-37, 0, 0), D3DXVECTOR3(.2f, .2f, .2f), temp));
	D3DXQuaternionRotationAxis(&temp, &D3DXVECTOR3(0, 1, 0), D3DXToRadian(135));
	Entities[0].setBaseRot(D3DXToRadian(135));
	Entities[1].setBaseRot(D3DXToRadian(135));
	Entities.push_back(GraphicsEntity(8, D3DXVECTOR3(27, 0, -27), D3DXVECTOR3(.2f, .2f, .2f), temp));
	Entities.push_back(GraphicsEntity(9, D3DXVECTOR3(27, 0, -27), D3DXVECTOR3(.2f, .2f, .2f), temp));
	D3DXQuaternionRotationAxis(&temp, &D3DXVECTOR3(0, 1, 0), D3DXToRadian(45));
	Entities[0].setBaseRot(D3DXToRadian(45));
	Entities[1].setBaseRot(D3DXToRadian(45));
	Entities.push_back(GraphicsEntity(10, D3DXVECTOR3(27, 0, 27), D3DXVECTOR3(.2f, .2f, .2f), temp));
	Entities.push_back(GraphicsEntity(11, D3DXVECTOR3(27, 0, 27), D3DXVECTOR3(.2f, .2f, .2f), temp));
	D3DXQuaternionRotationAxis(&temp, &D3DXVECTOR3(0, 1, 0), D3DXToRadian(-45));
	Entities[0].setBaseRot(D3DXToRadian(-45));
	Entities[1].setBaseRot(D3DXToRadian(-45));
	Entities.push_back(GraphicsEntity(12, D3DXVECTOR3(-27, 0, 27), D3DXVECTOR3(.2f, .2f, .2f), temp));
	Entities.push_back(GraphicsEntity(13, D3DXVECTOR3(-27, 0, 27), D3DXVECTOR3(.2f, .2f, .2f), temp));
	D3DXQuaternionRotationAxis(&temp, &D3DXVECTOR3(0, 1, 0), D3DXToRadian(-135));
	Entities[0].setBaseRot(D3DXToRadian(-135));
	Entities[1].setBaseRot(D3DXToRadian(-135));
	Entities.push_back(GraphicsEntity(14, D3DXVECTOR3(-27, 0, -27), D3DXVECTOR3(.2f, .2f, .2f), temp));
	Entities.push_back(GraphicsEntity(15, D3DXVECTOR3(-27, 0, -27), D3DXVECTOR3(.2f, .2f, .2f), temp));

	Entities.push_back(GraphicsEntity(16, D3DXVECTOR3(0, 0, 0), D3DXVECTOR3(.6f, .6f, .6f), temp));

	tankIndex = 0;
	ID3DXBuffer* errors = 0;

	buildProjMat();

	D3DXCreateEffectFromFileA(g_D3dDevice, "../shaders/PhongShader.fx", 0, 0, D3DXFX_NOT_CLONEABLE | D3DXSHADER_SKIPOPTIMIZATION | D3DXSHADER_DEBUG, 0, &effect, &errors);
	if (errors)
		cout << (char*)errors->GetBufferPointer();
	if (errors)
		errors->Release();
	CAM->setCinematicTarget(D3DXVECTOR3(60, 10, 0), D3DXVECTOR3(0, 0, 0), 1);
}

void TankSelectBack::onLostDevice()
{
	effect->OnLostDevice();
}
void TankSelectBack::onResetDevice()
{
	effect->OnResetDevice();
}

TankSelectBack::~TankSelectBack()
{
}

void TankSelectBack::buildProjMat()
{
	D3DXMatrixPerspectiveFovLH(&m_ProjMat, D3DX_PI * 0.20f, (float)g_D3DAPP->getWidth() / (float)g_D3DAPP->getHeight(), 1, 5000.0f);
}

void TankSelectBack::update(float dt)
{
	D3DXQUATERNION rot;

	rotation += .5f * dt;

	D3DXQuaternionRotationAxis(&rot, &D3DXVECTOR3(0, 1, 0), rotation);
	cout << tankIndex << endl;
	switch (tankIndex)
	{
	case 0:
		Entities[4].setOrientation(rot);
		Entities[5].setOrientation(rot);
		break;
	case 1:
		Entities[10].setOrientation(rot);
		Entities[11].setOrientation(rot);
		break;	
	case 2:
		Entities[0].setOrientation(rot);
		Entities[1].setOrientation(rot);
		break;
	case 3:
		Entities[12].setOrientation(rot);
		Entities[13].setOrientation(rot);
		break;
	case 4:
		Entities[6].setOrientation(rot);
		Entities[7].setOrientation(rot);
		break;
	case 5:
		Entities[14].setOrientation(rot);
		Entities[15].setOrientation(rot);
		break;
	case 6:
		Entities[2].setOrientation(rot);
		Entities[3].setOrientation(rot);
		break;
	case 7:
		Entities[8].setOrientation(rot);
		Entities[9].setOrientation(rot);
		break;
	}

	CAM->cinematicUpdate(dt);

}

void TankSelectBack::nextTank()
{

	if (!CAM->isCinematicMoving())
	{
		if (tankIndex == 7)
			tankIndex = 0;
		else
			tankIndex++;

		cout << tankIndex - 1 << " UP TO " << tankIndex << endl;
		switch (tankIndex)
		{
		case 1:
			CAM->setCinematicTarget(D3DXVECTOR3(45, 10, 45), D3DXVECTOR3(0, 0, 0), .01f);
			cout << "going to 1\n";
			break;
		case 2:
			CAM->setCinematicTarget(D3DXVECTOR3(0, 10, 60), D3DXVECTOR3(0, 0, 0), .01f);
			cout << "going to 2\n";
			break;
		case 3:
			CAM->setCinematicTarget(D3DXVECTOR3(-45, 10, 45), D3DXVECTOR3(0, 0, 0), .01f);
			cout << "going to 3\n";
			break;
		case 4:
			CAM->setCinematicTarget(D3DXVECTOR3(-60, 10, 0), D3DXVECTOR3(0, 0, 0), .01f);
			cout << "going to 4\n";
			break;
		case 5:
			CAM->setCinematicTarget(D3DXVECTOR3(-45, 10, -45), D3DXVECTOR3(0, 0, 0), .01f);
			cout << "going to 5\n";
			break;
		case 6:
			CAM->setCinematicTarget(D3DXVECTOR3(0, 10, -60), D3DXVECTOR3(0, 0, 0), .01f);
			cout << "going to 6\n";
			break;
		case 7:
			CAM->setCinematicTarget(D3DXVECTOR3(45, 10, -45), D3DXVECTOR3(0, 0, 0), .01f);
			cout << "going to 7\n";
			break;
		case 0:
			CAM->setCinematicTarget(D3DXVECTOR3(60, 10, 0), D3DXVECTOR3(0, 0, 0), .01f);
			cout << "going to 0\n";
			break;
		}
	}
}

void TankSelectBack::prevTank()
{
	if (tankIndex == 0)
		tankIndex = 7;
	else
		tankIndex--;

	if (!CAM->isCinematicMoving())
	{
		cout << tankIndex + 1 << " DOWN TO " << tankIndex << endl;
		switch (tankIndex)
		{
		case 1:
			CAM->setCinematicTarget(D3DXVECTOR3(45, 10, 45), D3DXVECTOR3(0, 0, 0), .01f);
			break;
		case 2:
			CAM->setCinematicTarget(D3DXVECTOR3(0, 10, 60), D3DXVECTOR3(0, 0, 0), .01f);
			break;
		case 3:
			CAM->setCinematicTarget(D3DXVECTOR3(-45, 10, 45), D3DXVECTOR3(0, 0, 0), .01f);
			break;
		case 4:
			CAM->setCinematicTarget(D3DXVECTOR3(-60, 10, 0), D3DXVECTOR3(0, 0, 0), .01f);
			break;
		case 5:
			CAM->setCinematicTarget(D3DXVECTOR3(-45, 10, -45), D3DXVECTOR3(0, 0, 0), .01f);
			break;
		case 6:
			CAM->setCinematicTarget(D3DXVECTOR3(0, 10, -60), D3DXVECTOR3(0, 0, 0), .01f);
			break;
		case 7:
			CAM->setCinematicTarget(D3DXVECTOR3(45, 10, -45), D3DXVECTOR3(0, 0, 0), .01f);
			break;
		case 0:
			CAM->setCinematicTarget(D3DXVECTOR3(60, 10, 0), D3DXVECTOR3(0, 0, 0), .01f);
			break;
		}
	}
}

void TankSelectBack::render()
{

	HR(effect->SetVector("LightDiffuse", (D3DXVECTOR4*)&D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f)));
	HR(effect->SetVector("LightSpecular", (D3DXVECTOR4*)&D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f)));
	HR(effect->SetValue("LightDirection", D3DXVECTOR3(0, -1, 0), sizeof(D3DXVECTOR3)));

	D3DXCOLOR spec(1.0f, 1.0f, 1.0f, 1.0f);
	D3DXVECTOR3 dirW(0, -1, 0);
	D3DXVECTOR3 pos(0, 100, 0);

	UINT numPasses;

	D3DXMATRIX VP = CAM->getViewMat() * m_ProjMat;
	D3DXMATRIX invtran;

	for (UINT k = 0; k < Entities.size(); k++)
	{
		if (k == 4 || k == 5)
		{
			effect->SetBool("lit", true);
			effect->SetBool("black", false);
		}
		else if (k == 16)
		{
			effect->SetBool("lit", false);
			effect->SetBool("black", false);
		}
		else
		{
			effect->SetBool("black", true);
		}

		{
			HR(effect->SetMatrix("WVP", &(Entities[k].getTransform() *VP)));
			HR(effect->SetMatrix("World", &Entities[k].getTransform()));
			D3DXMatrixInverse(&invtran, 0, &Entities[k].getTransform());
			D3DXMatrixTranspose(&invtran, &invtran);
			HR(effect->SetMatrix("WorldInvTrans", &invtran));
			HR(effect->SetValue("EyePosW", &CAM->getPos(), sizeof(D3DXVECTOR3)));
			HR(effect->SetVector("MatAmbient", (D3DXVECTOR4*)&RM->getMesh(Entities[k].getMeshID())->getMaterial()->Ambient));
			HR(effect->SetVector("MatDiffuse", (D3DXVECTOR4*)&RM->getMesh(Entities[k].getMeshID())->getMaterial()->Diffuse));
			HR(effect->SetVector("MatSpecular", (D3DXVECTOR4*)&RM->getMesh(Entities[k].getMeshID())->getMaterial()->Specular));
			HR(effect->SetFloat("MatPower", RM->getMesh(Entities[k].getMeshID())->getMaterial()->Power));
		}

		for (int j = 0; j < RM->getMesh(Entities[k].getMeshID())->getNumMaterials(); j++)
		{

			if (RM->getMesh(Entities[k].getMeshID())->getTexture(j) != NULL)
				effect->SetTexture("Tex", RM->getMesh(Entities[k].getMeshID())->getTexture(j));

			HR(effect->Begin(&numPasses, 0));

			HR(effect->BeginPass(0));
			HR(RM->getMesh(Entities[k].getMeshID())->getMesh()->DrawSubset(j));
			HR(effect->EndPass());

			HR(effect->End());
		}
	}
}

int TankSelectBack::getTankIndex()
{
	return tankIndex;
}
//BaseAIEntity
#include "collision function.h"
#include "SteeringManager.h"
#include "MasterEntity.h"
#include "BaseAIEntity.h"

FLOAT BaseAIEntity::s_IDCounter = -1;
BaseAIEntity::BaseAIEntity()
{
	m_ID					= ++s_IDCounter;
	m_fsm					= new ASM();
	IsAlive					= true;
	stateID					= 1;
	maxSpeed				= 5;
	m_DB					= D3DXVECTOR3(0, 0, 0);
	m_SForce				= D3DXVECTOR3(0, 0, 0);
	m_Target				= D3DXVECTOR3(0, 0, 0);
	m_tolerance				= (FLOAT)100;
	m_start					= (FLOAT)0;
	m_finish				= (FLOAT)0;
	m_wTime					= (FLOAT)0;
	m_maxHealth				= (FLOAT)100;
	enemyID					= (FLOAT)-1;
	m_currentHP				= (FLOAT)m_maxHealth;
	maxdegreespersecond		= (FLOAT)30;
	m_ShotTimer				= (FLOAT)0;
	//m_Weapon				= new WeaponManager();
	p_mSteering				= new SteeringManager();

	///FSM setup
	m_fsm->AddState(new Attack(m_fsm, (int)m_ID), false);
	m_fsm->AddState(new Flee(m_fsm, (int)m_ID), false);
	m_fsm->AddState(new Wander(m_fsm, (int)m_ID), true);
	//m_fsm->AddState(new AStar(m_fsm, m_ID), false);

	out_msg			= (FLOAT)-1;
	out_wpn_msg		= (FLOAT)-1;
}

BaseAIEntity::~BaseAIEntity()
{
	--s_IDCounter;
}

void BaseAIEntity::ShutDown()
{
	Atk->ShutDown();
	FL->ShutDown();
	WD->ShutDown();

	delete Atk;
	delete FL;
	delete WD;
}

void BaseAIEntity::HandleMsg(Mail message)
{
	m_fsm->HandleMsg(message);
}

void BaseAIEntity::Zone(vector<MasterEntity* > m_vMEntities, MasterEntity* Base)
{
	float	best_mag = 0;

	//If something has penetrated into our safe zone
	//we will identify what it is, how big/small it it
	//and handle it accordingly.
	for (auto i = m_vMEntities.begin();
		i != m_vMEntities.end(); i++){
		if ((*i)->getMeshID() != GROUND && (*i)->getMeshID() < 16)
		{
			///Checking to see if the enemy tanks will look for the players tank...
			if (!(*i)->getBaseAIEntity())
			{
				if ((D3DXVec3Length(&((*i)->getPosition() - Base->getPosition())) <
					(GetTolerance() + Base->getRigidBody()->GetHalfSize().x + GetTolerance() + (*i)->getRigidBody()->GetHalfSize().x) ||
					(GetTolerance() + Base->getRigidBody()->GetHalfSize().y + GetTolerance() + (*i)->getRigidBody()->GetHalfSize().y) ||
					(GetTolerance() + Base->getRigidBody()->GetHalfSize().z + GetTolerance() + (*i)->getRigidBody()->GetHalfSize().z)))
				{
					if (D3DXVec3Length(&((*i)->getPosition() - Base->getPosition())) > best_mag)
					{
						enemyID = (FLOAT)(*i)->getEntityID();
						best_mag = (float)D3DXVec3Length(&((*i)->getPosition() - Base->getPosition()));
						Enemy = (*i);
					}
				}
			}
			else if ((*i)->getBaseAIEntity())
			{
				if ((*i)->getBaseAIEntity()->GetAIID() == m_ID)
					continue;
				if ((D3DXVec3Length(&((*i)->getPosition() - Base->getPosition())) <
					(GetTolerance() + Base->getRigidBody()->GetHalfSize().x + (*i)->getBaseAIEntity()->GetTolerance() + (*i)->getRigidBody()->GetHalfSize().x) ||
					(GetTolerance() + Base->getRigidBody()->GetHalfSize().y + (*i)->getBaseAIEntity()->GetTolerance() + (*i)->getRigidBody()->GetHalfSize().y) ||
					(GetTolerance() + Base->getRigidBody()->GetHalfSize().z + (*i)->getBaseAIEntity()->GetTolerance() + (*i)->getRigidBody()->GetHalfSize().z)))
				{
					if (D3DXVec3Length(&((*i)->getPosition() - Base->getPosition())) > best_mag)
					{
						enemyID = (FLOAT)(*i)->getEntityID();
						best_mag = (float)D3DXVec3Length(&((*i)->getPosition() - Base->getPosition()));
						Enemy = (*i);
					}
				}
			}
		}
	}

	///Now if I have spotted and enemy I want to send a message saying that I need to seek them out
	if (Enemy != NULL)
		MD->SendToState((int)Base->getBaseAIEntity()->GetAIID(), (int)Base->getBaseAIEntity()->GetAIID(), 0, _LockedOn, (void*)0);

}

void BaseAIEntity::Update(double dt, vector<MasterEntity* > m_vMEntities, MasterEntity* Base)
{
	//keep track of time
	m_wTime += (FLOAT)dt;

	//clear our steering force every update
	m_SForce = D3DXVECTOR3(0, 0, 0);

	//Base->DamageHealth((double).5);

	//SetCurrentHP(Base->getHealth());

	//checks to see if there are any enemies around
	//Zone(m_vMEntities, Base);

	if (m_wTime > (double)3)
	{
		//Base->DamageHealth((double).3);

	SetCurrentHP(Base->getHealth());

		//checks to see if there are any enemies around
	Zone(m_vMEntities, Base);
	}

	//Calculate the Steering Force
	CheckState(m_vMEntities, Base);

	OrthnormBasFwdVec(Base->getBaseRot(), m_SForce);

	//Update the turrets position
	CheckTROT(dt, Base);

	//update fsm
	m_fsm->Update(dt);

	Base->setVelocity(m_SForce * GetMSpeed());
	Base->setVelocity(Truncate(m_SForce, GetMSpeed()));

	//update position
	Base->setPosition(Base->getPosition() + (Base->getVelocity() * (FLOAT)dt));

	//Base->DamageHealth((double)-0.3);
}

void BaseAIEntity::CheckState(vector<MasterEntity* > m_vMEntities, MasterEntity* Base)
{
	if (stateID == 1)
	{
		m_SForce += p_mSteering->GetHide(m_vMEntities, Base);
	}
	else if (stateID == 2)
	{
		m_SForce += p_mSteering->GetArrive(m_vMEntities, Base) * 6;

		//Now shoot the Laser
		FireCommand(Base);

	}
	else if (stateID == 3)
	{
		m_SForce += p_mSteering->GetFlee(m_vMEntities, Base);
	}
}

void BaseAIEntity::CheckTROT(double dt, MasterEntity* Base)
{
	D3DXMATRIX LookAt = Base->getTurrRot(); // Base->getTrans(1); //*D3DXMatrixIdentity(&LookAt);
	//D3DXVECTOR3 Shrekisloveshrekislife = D3DXVECTOR3(0, 0, 0);
	D3DXVECTOR3 LookAtV;
	FLOAT CODISNUMBERS = D3DXVec3LengthSq(&LookAtV);
	if (stateID == 3)
		*D3DXVec3Normalize(&LookAtV, &(Base->getVelocity()));
	else if (Enemy != NULL)
		*D3DXVec3Normalize(&LookAtV, &(Enemy->getPosition() - Base->getPosition()));
	else if (Enemy == NULL)
		*D3DXVec3Normalize(&LookAtV, &(Base->getVelocity()));
	
	if (LookAt != D3DXVECTOR3(0, 0, 0))//Enemy != NULL)
	{
		if (CODISNUMBERS > 0)
		{
			D3DXVECTOR3 THING1, THING2, THING3;

			THING1 = LookAtV;
			THING2 = D3DXVECTOR3(0, 1, 0);

			THING3 = *D3DXVec3Cross(&THING3, &THING1, &THING2);

			///ROTATION BITCHES!!!
			for (int i = 0; i < 3; i++)
			{
				LookAt._11 = THING1.x;
				LookAt._21 = THING1.y;
				LookAt._31 = THING1.z;
				LookAt._12 = THING2.x;
				LookAt._22 = THING2.y;
				LookAt._32 = THING2.z;
				LookAt._13 = THING3.x;
				LookAt._23 = THING3.y;
				LookAt._33 = THING3.z;
			}

			Base->setTurrRot(LookAt);
		}
	}
}

void BaseAIEntity::FireCommand(MasterEntity* Base)
{
	srand((UINT)time(NULL));

	int jitter = rand() % 3 + 1;

	D3DXVECTOR3 temp = *D3DXVec3Normalize(&temp, &D3DXVECTOR3(Base->getTurrRot()._11 * jitter, Base->getTurrRot()._21 * jitter, Base->getTurrRot()._31 * jitter));

	if (m_ShotTimer < CLK->getTime() && Base->getRecharge() > 20)
	{
		LM->addLaser(D3DXVECTOR3(Base->getPosition().x + (temp.x /*Base->getTurrRot()._11*/ * (FLOAT)14), 2.5f, Base->getPosition().z + (temp.z /*Base->getTurrRot()._31*/ * (FLOAT)14)), D3DXVECTOR3( temp.x/*Base->getTurrRot()._11*/, temp.y /*Base->getTurrRot()._21*/, temp.z /*Base->getTurrRot()._31*/), 19, Base);
		Base->shoot();
		m_ShotTimer = CLK->getTime() + Base->getTank()->RateOfFire();
	}
}

D3DXVECTOR3 BaseAIEntity::Truncate(D3DXVECTOR3 s, FLOAT ms)
{
	if (D3DXVec3Length(&(s)) > 0.5f)
	{
		D3DXVec3Normalize(&s, &s);
		s = s * ms;
	}

	return s;
}
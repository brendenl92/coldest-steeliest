#pragma once

#include <d3d9.h>
#include "../inc/d3dx9.h"
#include "../inc/DxErr.h"
#include <string>
#include <vector>
#include "Camera.h"
#include "Settings.h"
#include "RawInput.h"
#include "SoundManager.h"
#include "Clock.h"
//#include "ParticleManager.h"
#pragma comment(lib, "d3d9.lib")

using namespace std;

typedef D3DXVECTOR3 V3;
typedef D3DXMATRIX MAT4;
class Tank;
extern Tank* g_PlayerTank;
class D3DAPP;
extern D3DAPP* g_D3DAPP;
extern IDirect3DDevice9* g_D3dDevice;
extern bool g_Paused;

#define ReleaseCOM(input) { if(input){input->Release(); input = 0;}}

#if defined(DEBUG) | defined(_DEBUG)
	#ifndef HR
	#define HR(x)                                      \
	{                                                  \
		HRESULT hr = x;                                \
		if(FAILED(hr))                                 \
		{                                              \
			DXTrace(__FILE__, __LINE__, hr, #x, TRUE); \
		}                                              \
	}
	#endif

#else
	#ifndef HR
	#define HR(x) x;
	#endif
#endif

//Particle Code Includes
//===============================================================
// Bounding Volumes

//#pragma warning( push )
//#pragma warning( disable : 2220 )

struct AABB
{
	D3DXVECTOR3 minPt;
	D3DXVECTOR3 maxPt;

	// Initialize to an infinitely small bounding box.
	AABB()
		: minPt(FLT_MAX, FLT_MAX, FLT_MAX),
		maxPt(-FLT_MAX, -FLT_MAX, -FLT_MAX){}

	D3DXVECTOR3 center()const
	{
		return (minPt + maxPt)*0.5f;
	}

	D3DXVECTOR3 extent()const
	{
		return (maxPt - minPt)*0.5f;
	}

	void xform(const D3DXMATRIX& M, AABB& out)
	{
		// Convert to center/extent representation.
		D3DXVECTOR3 c = center();
		D3DXVECTOR3 e = extent();

		// Transform center in usual way.
		D3DXVec3TransformCoord(&c, &c, &M);

		// Transform extent.
		D3DXMATRIX absM;
		D3DXMatrixIdentity(&absM);
		absM(0, 0) = fabsf(M(0, 0)); absM(0, 1) = fabsf(M(0, 1)); absM(0, 2) = fabsf(M(0, 2));
		absM(1, 0) = fabsf(M(1, 0)); absM(1, 1) = fabsf(M(1, 1)); absM(1, 2) = fabsf(M(1, 2));
		absM(2, 0) = fabsf(M(2, 0)); absM(2, 1) = fabsf(M(2, 1)); absM(2, 2) = fabsf(M(2, 2));
		D3DXVec3TransformNormal(&e, &e, &absM);

		// Convert back to AABB representation.
		out.minPt = c - e;
		out.maxPt = c + e;
	}

	
};

//===============================================================
// Randomness

float GetRandomFloat(float a, float b);
void GetRandomVec(D3DXVECTOR3& out);

//===============================================================
// Colors and Materials

const D3DXCOLOR WHITE(1.0f, 1.0f, 1.0f, 1.0f);
const D3DXCOLOR BLACK(0.0f, 0.0f, 0.0f, 1.0f);
const D3DXCOLOR RED(1.0f, 0.0f, 0.0f, 1.0f);
const D3DXCOLOR GREEN(0.0f, 1.0f, 0.0f, 1.0f);
const D3DXCOLOR BLUE(0.0f, 0.0f, 1.0f, 1.0f);
const D3DXCOLOR GREY(0.5f, 0.5f, 0.5f, 1.0f);

//#pragma warning( pop )
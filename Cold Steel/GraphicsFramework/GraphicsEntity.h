#pragma once
#include "ResourceManager.h"
class GraphicsEntity
{
	D3DXVECTOR3 m_Pos;
	D3DXVECTOR3 m_Scale;
	D3DXQUATERNION orientation;
	
	float baseRot;

	UINT m_MeshID;

public:
	
	GraphicsEntity();
	GraphicsEntity(UINT id, D3DXVECTOR3 pos, D3DXVECTOR3 sca, D3DXQUATERNION orient);
	~GraphicsEntity();

	void setPos(D3DXVECTOR3 pos);
	void setScale(D3DXVECTOR3 scale);
	void setOrientation(D3DXQUATERNION orient);
	void setBaseRot(float base);
	
	float getBaseRot();
	D3DXMATRIX getTransform();
	D3DXQUATERNION getOrientation();
	D3DXVECTOR3 getPos();

	UINT getMeshID();

};


#pragma once

#include "../inc/d3dx9.h"

#include <Windows.h>

using namespace std;

enum mouseButtons{LEFT, MIDDLE, RIGHT, BUTTON1, BUTTON2, BUTTON3, BUTTON4, BUTTON5};
enum keyButtons{ A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W, 
				 X, Y, Z, SPACE, TAB, LSHIFT, RSHIFT, LCTRL, RCTRL, LALT, RALT, ONE, TWO, THREE, 
				 FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, ZERO, MNUS, PLUS, LTHAN, GTHAN, 
				 QMARK, COLON, QUOTE, LBRKT, RBRKT, ENTER, FWDSLSH, BSPC, F1, F2, F3, 
				 F4, F5, F6, F7, F8, F9, F10, F11, F12, ESC, LFT, RGT, UP, DWN, PGUP, PGDN,
				 END, HOME, INS, DEL, TILDE};

class RawInput
{
	BYTE* buffer;
	RAWINPUTDEVICE devs[2];

	RAWINPUT raw;

	POINT* mousePos;
	POINT* lastPos;
	float dx, dy;
	bool keys[69];
	bool regMouse;

	D3DXVECTOR3 movement;

	bool mouseButton[8]; //TRUE = DOWN && FALSE = UP
public:
	RawInput();
	~RawInput(void);

	LRESULT handleMessage(UINT msg, WPARAM wParam, LPARAM lParam);
	bool keyPressed(int button);
	bool mousePressed(int button);
	POINT getMousePos();
	void setMousePos(POINT *point);
	void setDX(float dx);
	void setDY(float dy);
	void setRegMouse(bool in);
	bool getregMouse();
	float getLX();
	float getLY();
};
extern RawInput* RAW;
#include "FuzzyVariable.h"

FzSet FuzzyVariable::addTriangularSet(string name, double peak, double left, double right)
{
	///this helps crete the Triangle set, while setting the min/max of the set
	m_MemberSets[name] = new FuzzySet_Triangle(peak, left, right);

	///here I'll set the peak of the set Im creating
	m_MemberSets[name]->SetRep(peak);

	///here we set/adjust the min/max based on our set added in
	if( left < m_Min)
		m_Min = left;
	if( right > m_Max)
		m_Max = right;
	return FzSet(*m_MemberSets[name]);
}

FzSet FuzzyVariable::addLeftShoulderSet(string name, double peak, double left, double right)
{
	///this helps crete the Triangle set, while setting the min/max of the set
	m_MemberSets[name] = new FuzzySet_LeftShoulder(peak, left, right);

	///here I'll set the peak of the set Im creating
	m_MemberSets[name]->SetRep(peak);

	///here we set/adjust the min/max based on our set added in
	if( left < m_Min)
		m_Min = left;
	if( right > m_Max)
		m_Max = right;
	return FzSet(*m_MemberSets[name]);
}

FzSet FuzzyVariable::addRightShoulderSet(string name, double peak, double left, double right)
{
	///this helps crete the Triangle set, while setting the min/max of the set
	m_MemberSets[name] = new FuzzySet_RightShoulder(peak, left, right);

	///here I'll set the peak of the set Im creating
	m_MemberSets[name]->SetRep(peak);

	///here we set/adjust the min/max based on our set added in
	if( left < m_Min)
		m_Min = left;
	if( right > m_Max)
		m_Max = right;
	return FzSet(*m_MemberSets[name]);
}

void FuzzyVariable::Fuzzify(double val)
{
	if(val >= m_Min && val <= m_Max)
	{
		for(auto i = m_MemberSets.begin(); i != m_MemberSets.end(); i++)
		{
			(*i).second->SetDOM((*i).second->CalculateDOM(val));
		}
	}

}

double FuzzyVariable::DefuzzifyMaxAV() const
{
	///reset the numerator and denomenator just in case
	double num = 0.0;
	double den = 0.0;

	for(auto i = m_MemberSets.begin(); i != m_MemberSets.end(); i++)
	{
		///This formula can be found on pg. 436 - 437

		num += (*i).second->GetRep() * (*i).second->GetDOM();
		den += (*i).second->GetDOM();
	}

	if(den == 0.0)
		return 0.0;
	else
		return num / den;
}
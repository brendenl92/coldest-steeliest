#pragma once
#include "DXUtil.h"
#include "MasterEntity.h"
#include <vector>
#include <fstream>
class MapLoader
{
public:
	MapLoader();
	~MapLoader();
	vector<MasterEntity*> loadMap(string map, vector<MasterEntity*> in);
	void saveMap(string map, vector<MasterEntity*> entities);
};


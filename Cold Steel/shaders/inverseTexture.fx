uniform extern float4x4 WVP;
uniform extern float4x4 W;
uniform extern float3 camPos;
uniform extern texture tex;

sampler TexS = sampler_state
{
	Texture = <tex>;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	MipFilter = LINEAR;
	MaxAnisotropy = 16;
	AddressU = WRAP;
	AddressV = WRAP;
	AddressW = Wrap;
};


struct OutputVS
{
	float4 posH    : POSITION0;
	float2 tex0    : TEXCOORD0;
	float3 posW    : TEXCOORD1;
};

OutputVS inverseTexVS(float3 posL : POSITION, float2 tex0 : TEXCOORD0)
{
	OutputVS outVS = (OutputVS)0;

	outVS.posH = mul(float4(posL, 1.0f), WVP);
	outVS.tex0 = tex0;
	outVS.posW = mul(float4(posL, 1.0f), W).xyz;

	return outVS;
}

float4 inverseTexPS(OutputVS IN) : COLOR0
{
	float4 sampledColor = (tex2D(TexS, IN.tex0));
	float z = length(IN.posW - camPos);
	float f = (500 - z) / (500 - 20);
	float4 outputColor = f * sampledColor + (1 - f) * float4(0, 0, 0, 1);
	return float4(outputColor.rgb, 1);
}

technique main
{

	pass P0
	{
		vertexShader = compile vs_3_0 inverseTexVS();
		pixelShader = compile ps_3_0 inverseTexPS();
	}

}
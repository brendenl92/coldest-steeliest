//=============================================================================
// PhongDirLtTex.fx by Frank Luna (C) 2004 All Rights Reserved.
//
// Phong directional light & texture.
//=============================================================================

uniform extern float4x4 World;
uniform extern float4x3 DeferredWorld;
uniform extern float4x4 WorldInvTrans;
uniform extern float4x4 WVP;
uniform extern float4x4 VP;
uniform extern float4x4 Proj;
uniform extern float4x4 View;
uniform extern float4x3 DeferredView;
uniform extern float4x3 InverseView;

uniform extern float4 MatAmbient;
uniform extern float4 MatDiffuse;
uniform extern float4 MatSpecular;
uniform extern float  MatPower;
uniform extern float  Density;
	
uniform extern float4 LightDiffuse;
uniform extern float4 LightSpecular;
uniform extern float3 LightPosition;
uniform extern float3 LightDirection;

uniform extern float3   EyePosW;

uniform extern texture Tex;
uniform extern texture DiffuseMap;
uniform extern texture NormalMap;
uniform extern texture PositionMap;
uniform extern texture DepthMap;

uniform extern bool lit;
uniform extern bool black;

sampler TexS = sampler_state
{
	Texture = <Tex>;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	MipFilter = LINEAR;
	MaxAnisotropy = 16;
	AddressU  = WRAP;
   	AddressV  = WRAP;
	AddressW  = Wrap;
};

struct OutputVS
{
	float4 posH    : POSITION0;
	float3 normalW : TEXCOORD0;
	float3 toEyeW  : TEXCOORD1;
	float2 tex0    : TEXCOORD2;
};

OutputVS PhongDirLtTexVS(float3 posL : POSITION0, float3 normalL : NORMAL0, float2 tex0: TEXCOORD0)
{

	OutputVS outVS = (OutputVS)0;
	
	outVS.normalW = mul(float4(normalL, 0.0f), WorldInvTrans).xyz;
	float3 posW  = mul(float4(posL, 1.0f), World).xyz;
	outVS.toEyeW = EyePosW - posW;
	outVS.posH = mul(float4(posL, 1.0f), WVP);
	outVS.tex0 = tex0;

	return outVS;
}

float4 PhongDirLtTexPS(float3 normalW : TEXCOORD0, float3 toEyeW  : TEXCOORD1, float2 tex0 : TEXCOORD2) : COLOR
{
	// Interpolated normals can become unnormal--so normalize.
	normalW = normalize(normalW);
	toEyeW  = normalize(toEyeW);
	
	// Light vector is opposite the direction of the light.
	float3 lightVecW = -LightDirection;
	
	// Compute the reflection vector.
	float3 r = reflect(-lightVecW, normalW);
	
	// Determine how much (if any) specular light makes it into the eye.
	float t  = pow(max(dot(r, toEyeW), 0.0f), MatPower);

	// Determine the diffuse light intensity that strikes the vertex.
	float s = max(dot(lightVecW, normalW), 0.0f);
	
	// Compute the ambient, diffuse and specular terms separatly. 
	float3 spec = t*(MatSpecular * LightSpecular).rgb;
	float3 diffuse = s*(MatDiffuse * (LightDiffuse * LightDiffuse)).rgb;
	float3 ambient = MatAmbient.rgb * LightDiffuse.rgb;
	
	// Get the texture color.
	float4 texColor = tex2D(TexS, tex0);
	
	// Combine the color from lighting with the texture color.
	float3 color = (diffuse + diffuse) * texColor.rgb + spec;
		
	// Sum all the terms together and copy over the diffuse alpha.
	if (lit == true && black == false)
	{
		return float4(color, MatDiffuse.a * texColor.a);
	}
	else if (lit == false && black == false)
	{
		return float4(texColor.rgb, 1);
	}
	else
	{
		return float4(0, 0, 0, 1);
	}

}

technique one
{
    pass P0
    {
        // Specify the vertex and pixel shader associated with this pass.
        vertexShader = compile vs_2_0 PhongDirLtTexVS();
        pixelShader  = compile ps_2_0 PhongDirLtTexPS();
    }
}
//=============================================================================
// smoke.fx by Jose (C) 2014 All Rights Reserved. (Modified Sprinkler.fx)
//=============================================================================

uniform extern float4x4 gWVP;
uniform extern texture  gTex;
uniform extern float3   gEyePosL;
uniform extern float3   gAccel;
uniform extern float    gTime;
uniform extern int      gViewportHeight;

uniform extern float3   gPosition; //Instanced Data
uniform extern float3   gVelocity; //Instanced Data
uniform extern float3   gSTL;	   //Instanced Data

sampler TexS = sampler_state
{
	Texture = <gTex>;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	MipFilter = POINT;
	AddressU  = CLAMP;
    AddressV  = CLAMP;
};
 
struct OutputVS
{
    float4 posH  : POSITION0;
    float2 tex0  : TEXCOORD0; // D3D fills in for point sprites.
    float size   : PSIZE; // In pixels.
	float time : TEXCOORD1;
};

OutputVS ExplosionVS(float3 posL    : POSITION0, 
                     float3 vel     : TEXCOORD0,
                     float size     : TEXCOORD1,
                     float time     : TEXCOORD2,
                     float lifeTime : TEXCOORD3,
                     float mass     : TEXCOORD4,
                     float4 color   : COLOR0)
{
    // Zero out our output.
	OutputVS outVS = (OutputVS)0;
	
	//Instanced Data input
	posL = gPosition;
	vel = gVelocity;
	size = gSTL.x;
	time = gSTL.y;
	lifeTime = gSTL.z;

	// Get age of particle from creation time.
	float t = gTime - time;
	outVS.time = t;
	// Constant acceleration.
	posL = posL + vel*t + 0.5f * gAccel * t * t;
	
	// Transform to homogeneous clip space.
	outVS.posH = mul(float4(posL, 1.0f), gWVP);
		
	// Compute size as a function of the distance from the camera,
	// and the viewport heights.  The constants were found by 
	// experimenting.
	float d = distance(posL, gEyePosL);
	outVS.size = gViewportHeight*size/(1.0f + 10.0f*d);

	// Done--return the output.
    return outVS;
}

float4 ExplosionPS(float2 tex0 : TEXCOORD0, OutputVS IN) : COLOR
{
    float4 f4TexColor = tex2D(TexS , tex0);
	//float4 f4OutColor = (1-IN.time) * f4TexColor;
	f4TexColor.a -= (IN.time) * f4TexColor.a / 2;
	return f4TexColor;
}

technique ExplosionTech
{
    pass P0
    {
        vertexShader = compile vs_2_0 ExplosionVS();
        pixelShader  = compile ps_2_0 ExplosionPS();
        
        PointSpriteEnable = true;
        AlphaBlendEnable = true;
	    SrcBlend     = SrcAlpha;
	    DestBlend    = InvSrcAlpha;
		AlphaRef = 100;
		AlphaFunc = GreaterEqual;
		ZFunc = lessequal;
	    
	    
	    // Don't write to depth buffer; that is, particles don't obscure
	    // the objects behind them.
	    //ZWriteEnable = false;
    }
}

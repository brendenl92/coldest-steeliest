#pragma once

#include "FuzzyTerm.h"

class FuzzyRule
{
private:
	///antecedent
	const FuzzyTerm* m_Precede;

	///consequence
	FuzzyTerm* m_Follow;

	/*///we don't want to copy rules
	FuzzyRule(const FuzzyRule&);
	FuzzyRule& operator= (const FuzzyRule&);*/

public:

	FuzzyRule(FuzzyTerm& precede, FuzzyTerm& follow) : m_Precede(precede.Clone()), m_Follow(follow.Clone())
	{
	}

	~FuzzyRule()
	{
		delete m_Precede; 
		delete m_Follow;
	}

	///Check function making sure the following action hasn't been
	///precalculated before we start the defuzzing process.
	void SetToZero()
	{
		m_Follow->ClearDOM();
	}

	///this method will update the DOM of the followed action
	///with the preceding conditions.
	void Calculate()
	{
		m_Follow->ORwithDOM(m_Precede->GetDOM());
	}
};
#include "GameWorld.h"
#include "D3DAPP.h"
#include "HUD.h"
#include "MapLoader.h"
#include "Vertex.h"

GameWorld::GameWorld()
{
	frames = 0;
	p.x = g_D3DAPP->getWidth() / 2;
	p.y = g_D3DAPP->getHeight() / 2;
	ClientToScreen(g_D3DAPP->getMainWnd(), &p);
	editor = false;
	InitAllVertexDeclarations();
	ParticleManager->CreateRain(1000, 0.03f, true);
	ParticleManager->CreateFire(D3DXVECTOR3(0, 0, 0), 3);

}

GameWorld::~GameWorld()
{
	destroy();
}

void GameWorld::init()
{

	if (editor)
		cout << "Controls:\nP: Increment chosen building\nO: Decrement chosen building\nI: Move object in negative Z\nJ: Move object in negative X\n" <<
				"K: Move object in positive Z\nL: Move object in positive X\n,: Delete current item selected\nSix(6): Input number and add building of that number\n" <<
				"W: Move camera forward\nA: Move camera to the left\nS: Move camera backwards\nD: Move camera to the right\n' ': Move camera in positive Y\n" <<
				"Z: Move camera in negative\nV: Rotate to the left\nB: Rotate to the right\nMesh ID 22 - 36: Commercial Buildings\nMesh ID 37 - 55: Historical Buildings\n" <<
				"Mesh ID 56 - 61: Industrial Buildings\nMesh ID 62 - 98: Residential Buildings\nMesh ID 99 - 101: Luxury Residential Buildings\nMesh ID 102 - 103: Sky Scrapers\n" <<
				"Mesh ID 104 - 113: Garages\nMesh ID 114 - 125: Roads\n";
	else
		cout << "Controls:\nW: Move camera forward\nA: Move camera to the left\nS: Move camera backwards\nD: Move camera to the right\nSpace: Move camera updwards\n" <<
				"Z: Move camera Downward\nHold Q to move tank\n";

	ML = MapLoader();
	moveSize = 0.0f;
	g_Paused = false;

	renderer = new Renderer();

	string meshes[4];


	meshes[0] = "../media/models/blueSphere.x";
	meshes[1] = "../media/models/building-scyscrapers_41.x";
	meshes[2] = "../media/models/greenSphere.x";
	meshes[3] = "../media/models/groundplane.x";

	RM->ImportMeshes(meshes, 4, 0);

	//tanks
	entities.push_back(new MasterEntity(0, 1, entities.size(), D3DXVECTOR3(7, 0, -16), D3DXVECTOR3(1, 1, 1), *D3DXMatrixIdentity(&D3DXMATRIX()), *D3DXMatrixIdentity(&D3DXMATRIX())));
	entities.push_back(new MasterEntity(0, 3, entities.size(), D3DXVECTOR3(0, 0, 0), D3DXVECTOR3(5, 5, 5), *D3DXMatrixIdentity(&D3DXMATRIX()), *D3DXMatrixIdentity(&D3DXMATRIX())));

	tv = new MasterEntity(0, 0, entities.size(), D3DXVECTOR3(0, 0, 0), D3DXVECTOR3(5, 5, 5), *D3DXMatrixIdentity(&D3DXMATRIX()), *D3DXMatrixIdentity(&D3DXMATRIX()));
	

	
	fps = 0;  
	elapsed = 0.0f;

	CAM->setState(FREE);
	CAM->setSpeed(100);
	CAM->setSensitivity(.05f);
	CAM->setPos(D3DXVECTOR3(0, 75, 75));
	CAM->setTarget(D3DXVECTOR3(0, 10, 0));

	RAW->setDX(0);
	RAW->setDY(0);
	
	gameOver = false;
	finishTime = FLT_MAX;
	finishTimer = 0;

	int returnInt = 100;
	while (returnInt > 0)
		returnInt = ShowCursor(FALSE);

	SOUND->startSong("../media/music/OPC.mp3");

}
void GameWorld::destroy()
{
	AMI->ShutDown();

	for (UINT i = 0; i < entities.size(); i++)
		delete entities[i];

	entities.clear();
	if (renderer)
	{
		delete renderer;
		renderer = NULL;
	}
	RM->clearAll();
	Hud->ClearHud();
	SOUND->endSong();
}
void GameWorld::update(double dt)
{

	SetCursorPos(p.x, p.y);

	SOUND->update();

	frames += 1;
	elapsed += (float)dt;
	if (elapsed >= 1.0f)
	{
		fps = (int)frames;
		elapsed = 0.0;
		frames = 0;
	}

	sprintf_s(FPSBuffer, "Score: %i", entities[1]->getScore());


	static D3DXVECTOR3 input;
	input = { 0, 0, 0 };
	if (RAW->keyPressed(W) == true)
	{
		if (CAM->getState() == FIRST || CAM->getState() == THIRD || RAW->keyPressed(Q))
			entities[1]->getRigidBody()->AddVelocity(V3(entities[1]->getBaseRot().m[0][0], entities[1]->getBaseRot().m[1][0], entities[1]->getBaseRot().m[2][0]), .6f);
		else
			input += CAM->getForward();
	}

	if (RAW->keyPressed(S) == true)
	{
		if (CAM->getState() == FIRST || CAM->getState() == THIRD || RAW->keyPressed(Q))
			entities[1]->getRigidBody()->AddVelocity(V3(entities[1]->getBaseRot().m[0][0], entities[1]->getBaseRot().m[1][0], entities[1]->getBaseRot().m[2][0]), .3f);
		else
			input -= CAM->getForward();
	}

	if (RAW->keyPressed(A) == true)
	{
		if (CAM->getState() == FIRST || CAM->getState() == THIRD || RAW->keyPressed(Q))
			entities[1]->getRigidBody()->AddRotation(1, .02f);
		else
			input -= CAM->getRight();
	}

	if (RAW->keyPressed(D) == true)
	{
		if (CAM->getState() == FIRST || CAM->getState() == THIRD || RAW->keyPressed(Q))
			entities[1]->getRigidBody()->AddRotation(1, -.02f);
		else
			input += CAM->getRight();
	}

	if (RAW->keyPressed(SPACE) == true)
		if (CAM->getState() == FREE)
			input += D3DXVECTOR3(0, 1, 0);

	if (RAW->keyPressed(I) == true)
		entities[0]->setPosition(entities[0]->getPosition() + D3DXVECTOR3(1, 0, 0));
	if (RAW->keyPressed(K) == true)
		entities[0]->setPosition(entities[0]->getPosition() + D3DXVECTOR3(-1, 0, 0));
	if (RAW->keyPressed(J) == true)
		entities[0]->setPosition(entities[0]->getPosition() + D3DXVECTOR3(0, 0, 1));
	if (RAW->keyPressed(L) == true)
		entities[0]->setPosition(entities[0]->getPosition() + D3DXVECTOR3(0, 0, -1));


	if (RAW->keyPressed(Z) == true)
		if (CAM->getState() == FREE)
			input -= D3DXVECTOR3(0, 1, 0);
	if (CAM->getState() == FREE)
		CAM->update((float)dt, input);
	else
		CAM->update((float)dt, D3DXVECTOR3(0, 0, 0));
	
}
void GameWorld::render()
{
	renderer->BeginRendering();

	renderer->Render(entities, tv);

	renderer->EndRendering();
	
}

void GameWorld::handleInput(RAWINPUT *in)
{
	/*
	0 = 0x30
	1 = 0x31
	2 = 0x32
	3 = 0x33
	4 = 0x34
	5 = 0x35
	6 = 0x36
	7 = 0x37
	8 = 0x38
	9 = 0x39

	A = 0x41
	B = 0x42
	C = 0x43
	D = 0x44
	E = 0x45
	F = 0x46
	G = 0x47
	H = 0x48
	I = 0x49
	J = 0x4A
	K = 0x4B
	L = 0x4C
	M = 0x4D
	N = 0x4E
	O = 0x4F
	P = 0x50
	Q = 0x51
	R = 0x52
	S = 0x53
	T = 0x54
	U = 0x55
	V = 0x56
	W = 0x57
	X = 0x58
	Y = 0x59
	Z = 0x5A
	*/

	if (in->data.keyboard.VKey == 0x58)
		cout << CAM->getViewProjMat()._11 << ", " << CAM->getViewProjMat()._12 << ", " << CAM->getViewProjMat()._13 << ", " << CAM->getViewProjMat()._14 << "\n " <<
		CAM->getViewProjMat()._21 << ", " << CAM->getViewProjMat()._22 << ", " << CAM->getViewProjMat()._23 << ", " << CAM->getViewProjMat()._24 << "\n " <<
		CAM->getViewProjMat()._31 << ", " << CAM->getViewProjMat()._32 << ", " << CAM->getViewProjMat()._33 << ", " << CAM->getViewProjMat()._34 << "\n " <<
		CAM->getViewProjMat()._41 << ", " << CAM->getViewProjMat()._42 << ", " << CAM->getViewProjMat()._43 << ", " << CAM->getViewProjMat()._44 << "\n " << endl;

}

void GameWorld::onLostDevice()
{
	ParticleManager->onLostDevice();
	renderer->onLostDevice(false);
	RM->onLostDevice();
}

void GameWorld::onResetDevice()
{
	ParticleManager->onResetDevice();
	renderer->onResetDevice(false);
	RM->onResetDevice();
}
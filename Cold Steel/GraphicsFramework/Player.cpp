#include "Player.h"

Player::Player(short ID, D3DXVECTOR3 pos)
{
	this->playerID = ID;
	D3DXQUATERNION temp;
	D3DXQuaternionIdentity(&temp);
	this->player = new  GraphicsEntity(3, pos, D3DXVECTOR3(2, 2, 2), temp);
}

Player::~Player()
{
	delete player;
}

GraphicsEntity* Player::getEntity()
{
	return player;
}

short Player::getID()
{
	return playerID;
}

void Player::update(float dt)
{
	float dist = D3DXVec3Dot(&player->getPos(), &target);
	if (dist > .01)
	{
		D3DXVECTOR3 nPos = target - player->getPos();
		D3DXVec3Normalize(&nPos, &nPos);
		nPos *= (float).3;
		player->setPos(player->getPos() + nPos);
		
	}
	else
		player->setPos(target);
}

void Player::setTarget(D3DXVECTOR3 pos)
{
	this->target = pos;
}
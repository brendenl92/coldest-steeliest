#ifndef FIREEMITTER_H
#define FIREEMITTER_H

#include "DXUtil.h"
#include "Vertex.h"
#include <vector>

class FireEmitter
{
	public:
	FireEmitter();

	virtual ~FireEmitter();

	// Access Methods
	float getTime();
	void  setTime(float t);
	const AABB& getAABB()const;
	int getAliveParticleCount();
	void setScale(float scale);
	float getScale();
	void setPaused(bool pause);
	
	//Instancing
	void BufferCode();

	void setWorldMtx(const D3DXMATRIX& world);
	//void addParticle();

	//Editor Access Methods
	void SetPosition(D3DXVECTOR3 pos);
	D3DXVECTOR3 GetPosition();

	virtual void onLostDevice();
	virtual void onResetDevice();

	//virtual void initParticle(Particle& out) = 0;
	virtual void update(float dt);
	virtual void draw();	

	//std::vector<FireVertex> mParticles;
	//std::vector<FireVertex*> mAliveParticles;
	//std::vector<FireVertex*> mDeadParticles; 

protected:
	// In practice, some sort of ID3DXEffect and IDirect3DTexture9 manager should
	// be used so that you do not duplicate effects/textures by having several
	// instances of a particle system.
	ID3DXEffect* mFX;
	D3DXHANDLE mhTech;
	D3DXHANDLE mhWVP;
	D3DXHANDLE mhTex;
	D3DXHANDLE mhNoiseTex;
	D3DXHANDLE mhAlphaTex;

	D3DXHANDLE mhFrameTime;
	D3DXHANDLE mhScrollSpeeds;
	D3DXHANDLE mhScales;
	D3DXHANDLE mhDistortion1;
	D3DXHANDLE mhDistortion2;
	D3DXHANDLE mhDistortion3;
	D3DXHANDLE mhDistortionScale;
	D3DXHANDLE mhDistortionBias;

	//Static Instancing
	D3DXHANDLE mhInsPos; //Position

	IDirect3DTexture9* mTex;
	IDirect3DTexture9* mAlphaTex;
	IDirect3DTexture9* mNoiseTex;

	IDirect3DVertexBuffer9* mVB;
	IDirect3DIndexBuffer9* mIB;

	D3DXMATRIX mWorld;
	D3DXMATRIX mInvWorld;
	float mTime;
	D3DXVECTOR3 mAccel;
	AABB mBox;
	int mMaxNumParticles;
	float mTimePerParticle;

	//New for Manager and Manipulation
	D3DXVECTOR3 mPosition; //Starting Position of Particle (Edit in Creation of Particle or during Update)
	float mScale;

	float timeAccum;
	bool mRestart;
	int mSpawned;
	float mStartDelay;
	bool isAlive(); //Check if this Particle Alive?
	bool mFinished;	//Has all the particles died out?

	bool mPaused;//Pause Particles in place
};

#endif
//=============================================================================
// snow.fx by Jose SanRoman (C) 2014 All Rights Reserved.
//
// Falling snow.
//=============================================================================

uniform extern float4x4 gWVP;
uniform extern texture  gTex;
uniform extern float3   gEyePosL;
uniform extern float3   gAccel;
uniform extern float    gTime;
uniform extern int      gViewportHeight;

uniform extern float3   gPosition; //Instanced Data
uniform extern float3   gVelocity; //Instanced Data
uniform extern float3   gSTL;	   //Instanced Data

sampler TexS = sampler_state
{
	Texture = <gTex>;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	MipFilter = POINT;
	AddressU  = CLAMP;
    AddressV  = CLAMP;
};
 
struct OutputVS
{
    float4 posH  : POSITION0;
    float2 tex0  : TEXCOORD0; // D3D fills in for point sprites.
    float size   : PSIZE; // In pixels.
};

OutputVS SnowVS(float3 posL    : POSITION0, 
                float3 vel     : TEXCOORD0,
                float size     : TEXCOORD1,
                float time     : TEXCOORD2,
                float lifeTime : TEXCOORD3,
                float mass     : TEXCOORD4,
                float4 color   : COLOR0)
{
    // Zero out our output.
	OutputVS outVS = (OutputVS)0;

	//Instanced Data input
	posL = gPosition;
	vel = gVelocity;
	size = gSTL.x;
	time = gSTL.y;
	lifeTime = gSTL.z;

	// Get age of particle from creation time.
	float t = gTime - time;
	
	// Constant acceleration.
	posL = posL + vel*t + 0.5f * gAccel * t * t;
	
	// Transform to homogeneous clip space.
	outVS.posH = mul(float4(posL, 1.0f), gWVP);
		
	// Do not scale rain drops as a function of distance.
	// Compute size as a function of the viewport height.  
	// The constant was found by experimenting.
	outVS.size = 0.0035f*gViewportHeight*size;

	// Done--return the output.
    return outVS;
}

float4 SnowPS(float2 tex0 : TEXCOORD0) : COLOR
{
	return tex2D(TexS, tex0);
}

technique SnowTech
{
    pass P0
    {
        vertexShader = compile vs_2_0 SnowVS();
        pixelShader  = compile ps_2_0 SnowPS();
        
        PointSpriteEnable = true;
	AlphaBlendEnable = true;
  	SrcBlend     = One;
	DestBlend    = One;
    }
}

#pragma once

#include "dxutil.h"

typedef D3DXVECTOR3 V3;
typedef D3DXQUATERNION Qu;
typedef D3DXMATRIX M4;

class MasterEntity;
class BoundingVolumeNode;

class RigidBody
{

protected:
	//Variables
	bool	mInverted;
	int		mShape;
	float	mMaxSpeed;
	float	mMaxTurn;
	float	mDampening;
	float	mInvMass;
	V3		mVelocity;
	V3		mOmega;
	M4		mTransform;
	M4		mOffsetM;
	Qu      mHeading;
	Qu		mOffsetHeading;

	BoundingVolumeNode* mVolume;

	MasterEntity* mMaster;



public:

	//Constructor
	RigidBody(){}
	RigidBody(V3 Position, V3 Heading, float MaxSpeed = 1000, float MaxRotation = 10);
	RigidBody(MasterEntity* Master, V3 Position, V3 Heading, float MaxSpeed = 100, float MaxRotation = 10, float Mass = 1);

	~RigidBody();


	//Methods
	virtual void Update(float DeltaTime, V3* PositionOut, V3* VelocityOut, M4* TransformOut);
	void AddVelocity(V3 Velocity);
	void AddVelocity(float AngleX, float AngleY, float Anglez, float Magnitude);
	void AddVelocity(V3 head, float Magnitude);
	void AddRotation(int Axis, float Theta);
	void ResetTransform();
	void RotateBoutanAxis(int Axis, float Theta);


	//Accesors
	Qu GetQuaternion();
	V3 Heading();
	float Mass();
	int GetID();
	V3 GetAxis(int Axis);
	V3 GetHalfSize();
	V3 GetPosition();
	V3 Velocity(){return mVelocity;}
	M4 Transform(){return mTransform;}
	MasterEntity* Master(){ return mMaster;}

	void SetPosition(V3 Position);
	void SetVolume(BoundingVolumeNode* BoundingVolumeNode){ mVolume = BoundingVolumeNode;}
	void SetVelocity(V3 Velocity){};
	void Transform(Qu Quatern, V3 Position);
};
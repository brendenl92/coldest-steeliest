#pragma once
#include "DXUtil.h"
#include "AIManager.h"
#include "PhysicsInterface.h"
#include "ParticleManager.h"
#include "Tank.h"

enum MESHID
{
			TANK1, TANK2 = 2, TANK3 = 4, TANK4 = 6, TANK5 = 8, TANK6 = 10, TANK7 = 12, TANK8 = 14, GROUND = 16, SPHERE, BOX, BLUELASER, REDLASER, GREENLASER,
			BC12, BC13, BC14, BC15, BC16, BC17, BC18, BC19, BC20, BC21, BC22, BC23, BC24, BC25, BC26, BH1, BH4, BH5, BH6, BH10, BH11, BH12, BH17, BH18, BH19,
			BH20, BH21, BH22, BH23, BH24, BH25, BH26, BH27, BH28, BI8, BI9, BI12, BI13, BI14, BI15, BR3, BR5, BR6, BR7, BR8, BR9, BR10, BR11, BR12, BR14, BR15,
			BR16, BR17, BR18, BR19, BR20, BR21, BR22, BR24, BR26, BR27, BR28, BR29, BR30, BR32, BR34, BR35, BR36, BR37, BR38, BR39, BR41, BR43, BR44, BR45, BR50,
			BR51, RL4, RL6, RL8, BS26, BS41, GR1, GR2, GR3, GR4, GR5, GR6, GR7, GR8, GR9, GR11, RB1, RB2, RB3, RB4, RB5, RB6, RB7, RB8, RB9, RB10, RB11, RBC, 
			BLUESPHERE, REDSPHERE, GREENSPHERE, PURPLESPHERE, ORANGESPHERE, PINKSPHERE
};

enum {
		AIENABLED  = 0x01,
		PHYENABLED = 0x02, 
		GFXENABLED = 0x04
	 };

class MasterEntity
{
	D3DXQUATERNION rotQuat;

	D3DXMATRIX baseRot;
	D3DXMATRIX turrRot;

	D3DXMATRIX baseTrans;
	D3DXMATRIX turrTrans;

	D3DXVECTOR3 position;
	D3DXVECTOR3 velocity;
	D3DXVECTOR3 scale;

	UINT GraphicsMeshID;
	UINT EntityID;
	
	RigidBody* rb;

	BaseAIEntity* bai;

	Tank* tank;

	float mHealth;
	float mRecharge;

	char options;

	bool mAlive;

	UINT score;

	int myMoveEffect, myHoverEffect;

	
public:

	MasterEntity(char options, UINT mid, UINT id, D3DXVECTOR3 position, D3DXVECTOR3 scale, D3DXMATRIX rot, D3DXMATRIX rot2);
	MasterEntity();
	~MasterEntity();

	void update(double dte, vector<MasterEntity*> entities);

	void setBaseRot(D3DXMATRIX base);
	void setTurrRot(D3DXMATRIX turr);
	void setPosition(D3DXVECTOR3 pos);
	void setVelocity(D3DXVECTOR3 vel);
	void setScale(D3DXVECTOR3 sca);
	void addScore(UINT score);
	void setDead(bool dead);
	
	D3DXMATRIX getBaseRot();
	D3DXMATRIX getTurrRot();
	D3DXMATRIX getTrans(int index);
	D3DXVECTOR3 getPosition();
	D3DXVECTOR3 getVelocity();
	D3DXVECTOR3 getScale();

	RigidBody* getRigidBody();
	BaseAIEntity* getBaseAIEntity();

	UINT getMeshID();
	UINT getEntityID();

	bool getDead();

	float getRecharge();
	float getHealth();

	UINT getScore();

	Tank* getTank();

	char getOption();

	bool AIEnt();

	void DamageHealth(double damage);
	void ReplenishHealth();

	void shoot();
};

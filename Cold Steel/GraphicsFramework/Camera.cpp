#include "Camera.h"
#include "DXUtil.h"
#include "XInput.h"
#include "GameApp.h"

Camera* Camera::Instance()
{

	static Camera* temp = new Camera();
	return temp;

}

Camera::Camera(void)
{

	m_TPHeight = 2;
	m_TPDist = 5;
	
	state = FREE;

	m_Position = D3DXVECTOR3(.001f, 100.0f, 100.0f);
	m_Forward = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	m_Right = D3DXVECTOR3(1.0f, 0.0f, 0.0f);
	m_Up = D3DXVECTOR3(0.0f, 1.0f, 0.0f);
	
	recalcVectors();

	buildProjMat();

	D3DXMatrixIdentity(&m_ViewMat);
	D3DXMatrixIdentity(&m_ProjMat);

	dx = 0;
	dy = 0;

	m_Speed = 300;
	m_Sensitivity = 1;

	slerp = 0;
	slerpValue = .0001f;
	D3DXMatrixPerspectiveFovLH(&m_ProjMat, D3DXToRadian(30.0f), 1.0f, 1.0f, 1024.0f);
}
void Camera::menuReset()
{
	m_Position = D3DXVECTOR3(.001f, 100.0f, 100.0f);
}
void Camera::reset(D3DXVECTOR3 pos)
{

	m_TPHeight = 2;
	m_TPDist = 20;

	m_ViewMat.m[0][0] = -1;
	m_ViewMat.m[0][1] = 0.004f;
	m_ViewMat.m[0][2] = 0.022f;
	m_ViewMat.m[0][3] = 0;

	m_ViewMat.m[1][0] = 0;
	m_ViewMat.m[1][1] = 1;
	m_ViewMat.m[1][2] = .13f;
	m_ViewMat.m[1][3] = 0;

	m_ViewMat.m[2][0] = 0.03f;
	m_ViewMat.m[2][1] = 0.13f;
	m_ViewMat.m[2][2] = -1;
	m_ViewMat.m[2][3] = 0;

	m_ViewMat.m[3][0] = 50;
	m_ViewMat.m[3][1] = -10;
	m_ViewMat.m[3][2] = -2.44f;
	m_ViewMat.m[3][3] = 1;

}

Camera::~Camera(void)
{
}
void Camera::BuildViewMatrix()
{

	D3DXVec3Normalize(&m_Forward, &m_Forward);

	D3DXVec3Cross(&m_Up, &m_Forward, &m_Right);
	D3DXVec3Normalize(&m_Up, &m_Up);

	D3DXVec3Normalize(&m_Right, &m_Right);
	float x, y, z;
	if (state == FREE || state == FIRST)
	{

		//Fill in the view matrix entries.
		x = -D3DXVec3Dot(&m_Position, &m_Right);
		y = -D3DXVec3Dot(&m_Position, &m_Up);
		z = -D3DXVec3Dot(&m_Position, &m_Forward);
	}
	else if (state == THIRD)
	{
		D3DXVECTOR3 pos = m_Position - (m_TPDist * m_Forward) + (m_TPHeight * m_Up); 		
		x = -D3DXVec3Dot(&pos, &m_Right);
		y = -D3DXVec3Dot(&pos, &m_Up);
		z = -D3DXVec3Dot(&pos, &m_Forward);
	}

	m_ViewMat(0,0) = m_Right.x; 
	m_ViewMat(1,0) = m_Right.y; 
	m_ViewMat(2,0) = m_Right.z; 
	m_ViewMat(3,0) = x;   

	m_ViewMat(0,1) = m_Up.x;
	m_ViewMat(1,1) = m_Up.y;
	m_ViewMat(2,1) = m_Up.z;
	m_ViewMat(3,1) = y;  

	m_ViewMat(0,2) = m_Forward.x; 
	m_ViewMat(1,2) = m_Forward.y; 
	m_ViewMat(2,2) = m_Forward.z; 
	m_ViewMat(3,2) = z;   

	m_ViewMat(0,3) = 0.0f;
	m_ViewMat(1,3) = 0.0f;
	m_ViewMat(2,3) = 0.0f;
	m_ViewMat(3,3) = 1.0f;

}

D3DXMATRIX Camera::getViewMat()
{

	return m_ViewMat;

}

void Camera::recalcVectors()
{

	m_Right	= D3DXVECTOR3(1.0f, 0.0f, 0.0f);	// the right axis from the camera's position
	m_Up = D3DXVECTOR3(0.0f, 1.0f, 0.0f);	//the up vector, default to Y

	m_Forward = m_Forward - m_Position;
	D3DXVec3Normalize(&m_Forward, &m_Forward);

	D3DXVec3Cross(&m_Right, &m_Up, &m_Forward);
	D3DXVec3Cross(&m_Up, &m_Forward, &m_Right);

}
void Camera::update(float dt, D3DXVECTOR3 input)
{
	if (RAW->getregMouse() == false)
		RAW->setRegMouse(true);
	if (!g_Paused)
	{
		if (SET->getController() == false)
		{
			float x = RAW->getLX() * dt;
			float y = RAW->getLY() * dt;
			if (SET->getInverted() == true)
				y *= -1;
			x *= m_Sensitivity;
			y *= m_Sensitivity;

			dx += x;
			dy += y;
			if (y != 0.0f)
			{
				//rotate Up and Down
				D3DXMATRIX xRot;
				D3DXMatrixRotationAxis(&xRot, &m_Right, y);
				D3DXVec3TransformCoord(&m_Forward, &m_Forward, &xRot);
				D3DXVec3TransformCoord(&m_Up, &m_Up, &xRot);
			}
			if (x != 0.0f)
			{
				//rotate left and right
				D3DXMATRIX yRot;
				D3DXMatrixRotationY(&yRot, x);
				D3DXVec3TransformCoord(&m_Right, &m_Right, &yRot);
				D3DXVec3TransformCoord(&m_Up, &m_Up, &yRot);
				D3DXVec3TransformCoord(&m_Forward, &m_Forward, &yRot);
			}

		}
		else
		{
			float magX;
			float magY;
			if (abs((float)XBOX->GetState().Gamepad.sThumbRX) > 8000)
				magX = max(-1, (float)XBOX->GetState().Gamepad.sThumbRX / 32767);
			else
				magX = 0;
			if (abs((float)XBOX->GetState().Gamepad.sThumbRY) > 8000)
				magY = max(-1, (float)XBOX->GetState().Gamepad.sThumbRY / 32767);
			else
				magY = 0;

			float x = magX;
			float y = magY;

			x *= m_Sensitivity;
			y *= m_Sensitivity;
			if (SET->getInverted() == false)
				y *= -1;

			//rotate Up and Down
			D3DXMATRIX xRot;
			D3DXMatrixRotationAxis(&xRot, &m_Right, y);
			D3DXVec3TransformCoord(&m_Forward, &m_Forward, &xRot);
			D3DXVec3TransformCoord(&m_Up, &m_Up, &xRot);

			//rotate left and right
			D3DXMATRIX yRot;
			D3DXMatrixRotationY(&yRot, x);
			D3DXVec3TransformCoord(&m_Right, &m_Right, &yRot);
			D3DXVec3TransformCoord(&m_Up, &m_Up, &yRot);
			D3DXVec3TransformCoord(&m_Forward, &m_Forward, &yRot);
		}
		m_Position += input * m_Speed * dt;
	}
	

	this->BuildViewMatrix();

	FMOD_VECTOR vel = { 0, 0, 0 };
	FMOD_VECTOR pos = { m_Position.x, m_Position.y, m_Position.z };
	FMOD_VECTOR fwd = { m_Forward.x, m_Forward.y, m_Forward.z };
	FMOD_VECTOR up = { m_Up.x, m_Up.y, m_Up.z };
	SOUND->setPosVel(pos, vel, fwd, up);
	buildProjMat();
}

void Camera::cinematicUpdate(float dt)
{
	if (D3DXVec3LengthSq(&(m_Position - m_CinematicPos)) > .2)
	{
		D3DXQUATERNION current;
		D3DXQUATERNION newQuat;
		D3DXMATRIX newMatrix;
		D3DXQuaternionRotationMatrix(&current, &m_ViewMat);
		D3DXQuaternionSlerp(&newQuat, &current, &m_CinematicOrient, slerp);
		D3DXMatrixRotationQuaternion(&newMatrix, &newQuat);
		D3DXVec3Lerp(&m_Position, &m_Position, &m_CinematicPos, slerp);
		m_Forward = D3DXVECTOR3(newMatrix._13, newMatrix._23, newMatrix._33);

		recalcVectors();
		slerp += slerpValue;
		BuildViewMatrix();
		cinematicMoving = true;
	}
	else
	{
		cinematicMoving = false;
	}
}

void Camera::setCinematicTarget(D3DXVECTOR3 pos, D3DXVECTOR3 tar, float slerpValue) //slerp value between 0 - 1, bigger the value quicker the movement.
{
	m_CinematicPos = pos;
	D3DXVECTOR3 right = D3DXVECTOR3(1.0f, 0.0f, 0.0f);	// the right axis from the camera's position
	D3DXVECTOR3 up = D3DXVECTOR3(0.0f, 1.0f, 0.0f);	//the up vector, default to Y

	D3DXVECTOR3 forward = tar - m_CinematicPos;
	D3DXVec3Normalize(&forward, &forward);

	D3DXVec3Cross(&right, &up, &forward);
	D3DXVec3Cross(&up, &forward, &right);
	D3DXMATRIX mat;
	D3DXMatrixLookAtLH(&mat, &m_CinematicPos, &forward, &up);
	D3DXQuaternionRotationMatrix(&m_CinematicOrient, &mat);
	slerp = 0;
	this->slerpValue = slerpValue;
}

bool Camera::isCinematicMoving()
{
	return cinematicMoving;
}

float Camera::getDX()
{
	return dx;
}

float Camera::getDY()
{
	return dy;
}

void Camera::setPos(D3DXVECTOR3 position)
{

	m_Position = position;

}

void Camera::setTarget(D3DXVECTOR3 target)
{

	this->m_Forward = target;
	recalcVectors();

}

void Camera::setSpeed(float speed)
{

	this->m_Speed = speed;

}
void Camera::setSensitivity(float sen)
{

	this->m_Sensitivity = sen;

}

void Camera::setState(camstate in)
{
	this->state = in;
}

D3DXVECTOR3 Camera::getPos()
{

	return m_Position;

}
D3DXVECTOR3 Camera::getForward()
{

	return m_Forward;

}

D3DXVECTOR3 Camera::getRight()
{

	return m_Right;

}
D3DXVECTOR3 Camera::getUp()
{
	return m_Up;
}
void Camera::menuUpdate(float dt)
{
	m_Position += (m_Right * 4) * dt;
	m_Forward = D3DXVECTOR3(0, 0, 0);
	recalcVectors();
	this->BuildViewMatrix();
}

camstate Camera::getState()
{
	return state;
}
D3DXMATRIX Camera::getProjMat()
{
	return m_ProjMat;
}
D3DXMATRIX Camera::getViewProjMat()
{
	buildProjMat();
	return m_ViewMat * m_ProjMat;
}
void Camera::buildProjMat()
{
	D3DXMatrixPerspectiveFovLH(&m_ProjMat, D3DX_PI * 0.25f, (float)g_D3DAPP->getWidth() / (float)g_D3DAPP->getHeight(), 1, 5000.0f);
}
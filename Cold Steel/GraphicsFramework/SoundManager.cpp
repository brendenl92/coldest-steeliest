#include "SoundManager.h"
#include "Settings.h"
#include "Camera.h"
#include "Clock.h"

void ERRCHECK(FMOD_RESULT result)
{
    if (result != FMOD_OK)
    {
		cout << "FMOD Error: " << FMOD_ErrorString(result);
        exit(-1);
    }
}

SoundManager* SoundManager::Instance()
{

	static SoundManager* ptr = new SoundManager();
	return ptr;

}

void SoundManager::update()
{

	system->update();

	for (UINT i = 0; i < effects.size(); i++)
		if (effects[i].second < CLK->getTime())
		{
			effects[i].first->release();
			effects.erase(effects.begin() + i);
		}

}

SoundManager::SoundManager(void)
{

	initialize();

}


SoundManager::~SoundManager(void)
{
	bool temp;
	musicChannel->isPlaying(&temp);
	if (temp)
		endSong();
	reverb->release();
	for (auto i = effects.begin(); i != effects.end(); i++)
		i->first->release();
	system->release();

}


void SoundManager::initialize()
{

	FMOD_RESULT      result; 
	unsigned int     version; 
	int              numdrivers; 
	FMOD_SPEAKERMODE speakermode; 
	FMOD_CAPS        caps; 
	char             name[256]; 


	result = FMOD::System_Create(&system);
	ERRCHECK(result);

	result = system->getVersion(&version);
	ERRCHECK(result);

	if (version < FMOD_VERSION)
	{    
		cout << "Error!  You are using an old version of FMOD " << version << ".  This program requires " << FMOD_VERSION << ".";
		return;
	}

	result = system->getNumDrivers(&numdrivers);
	ERRCHECK(result);

	if (numdrivers == 0)
	{    
		result = system->setOutput(FMOD_OUTPUTTYPE_NOSOUND);
		ERRCHECK(result);
	}
	else
	{
		result = system->getDriverCaps(0, &caps, 0, &speakermode);
		ERRCHECK(result);
		
		result = system->setSpeakerMode(FMOD_SPEAKERMODE_7POINT1);
		ERRCHECK(result);
		if (caps & FMOD_CAPS_HARDWARE_EMULATED)
		{         
			result = system->setDSPBufferSize(1024, 10);         
			ERRCHECK(result);     
		}  
		result = system->getDriverInfo(0, name, 256, 0);     
		ERRCHECK(result);  
		if (strstr(name, "SigmaTel"))     
		{              
			result = system->setSoftwareFormat(48000, FMOD_SOUND_FORMAT_PCMFLOAT, 0,0, FMOD_DSP_RESAMPLER_LINEAR);        
			ERRCHECK(result);    
		} 
	}  
	result = system->init(100, FMOD_INIT_NORMAL, 0); 
	if (result == FMOD_ERR_OUTPUT_CREATEBUFFER)
	{    
		result = system->setSpeakerMode(FMOD_SPEAKERMODE_STEREO);    
		ERRCHECK(result);  

		result = system->init(100, FMOD_INIT_NORMAL, 0); 
	} 
	ERRCHECK(result); 

	result = system->createReverb(&reverb);
	ERRCHECK(result);
}

void SoundManager::setPosVel(FMOD_VECTOR pos, FMOD_VECTOR vel, FMOD_VECTOR fwd, FMOD_VECTOR up)
{
	system->set3DListenerAttributes(0, &pos, &vel, &fwd, &up);
}


void SoundManager::playSoundEffect(string name)
{

	FMOD_RESULT	result; 
	FMOD::Channel *channel = 0;
	FMOD::Sound *sound = 0;

    result = system->createSound(name.c_str(), FMOD_DEFAULT, 0, &sound);
    ERRCHECK(result);
	UINT length;
	sound->getLength(&length, FMOD_TIMEUNIT_MS);
	effects.push_back(make_pair(sound, CLK->getTime() + length));

	result = system->playSound(FMOD_CHANNEL_FREE, sound, true, &channel);
    ERRCHECK(result);

	result = channel->setVolume(SET->getFXVolume());
	ERRCHECK(result);

	result = channel->setPaused(false);
	ERRCHECK(result);

}

void SoundManager::setReverb(FMOD_REVERB_PROPERTIES reb)
{
	reverb->setProperties(&reb);
}

void SoundManager::activateReverb(bool in)
{
	reverb->setActive(in);
}

void SoundManager::playSoundEffect(string name, FMOD_VECTOR pos, bool loop)
{
	FMOD_RESULT	result;
	FMOD::Channel *channel = 0;
	FMOD::Sound *sound = 0;
	if (loop == true)
		result = system->createSound(name.c_str(), FMOD_LOOP_NORMAL | FMOD_3D, 0, &sound);
	else
		result = system->createSound(name.c_str(), FMOD_3D, 0, &sound);
	ERRCHECK(result);

	result = system->playSound(FMOD_CHANNEL_FREE, sound, true, &channel);
	ERRCHECK(result);

	result = channel->set3DMinMaxDistance(30, 10000);

	UINT length;
	sound->getLength(&length, FMOD_TIMEUNIT_MS);
	
	result = channel->setVolume(SET->getFXVolume());
	ERRCHECK(result);

	result = channel->setPaused(false);
	ERRCHECK(result);

}

void SoundManager::startSong(string name)
{
	system->createStream(name.c_str(), FMOD_HARDWARE | FMOD_LOOP_NORMAL | FMOD_2D, 0, &music);
	system->playSound(FMOD_CHANNEL_FREE, music, true, &musicChannel);
	musicChannel->setVolume(SET->getMusicVolume());
	musicChannel->setPaused(false);
}

void SoundManager::endSong()
{

	musicChannel->stop();
	music->release();

}

void SoundManager::setMusicVolume(float volume)
{

	masterVolume = volume;
	musicChannel->setPaused(true);
	musicChannel->setVolume(volume);
	musicChannel->setPaused(false);
}

#pragma once
//#include "vld.h"
#include "DXUtil.h"
#define MY_HOTKEYID 100 //unique in your window

using namespace std;

class D3DAPP
{

	D3DCAPS9 caps;
	D3DADAPTER_IDENTIFIER9 ID;
	bool networking;

public:
	D3DAPP(HINSTANCE instance, string caption, D3DDEVTYPE type, DWORD requestedVP);
	virtual ~D3DAPP();


	HINSTANCE getInstance();
	HWND getMainWnd();

	virtual void changeState(string name);
	virtual bool checkDeviceCaps();
	virtual void onLostDevice();
	virtual void onResetDevice();
	virtual void updateScene(float dt);
	virtual void drawScene();

	virtual void HandleInput(RAWINPUT* raw);

	D3DCAPS9 getCaps();
	D3DADAPTER_IDENTIFIER9 getID();

	int getHeight();
	int getWidth();
	int getOldHeight();
	int getOldWidth();

	void setNetworking(bool);

	bool getNetworking();

	D3DXVECTOR2 getRes();

	virtual void initMainWindow();
	virtual void initDirect3d();
	virtual int run();
	virtual LRESULT msgProc(UINT msg, WPARAM wParam, LPARAM lParam);

	void enableFullScreen(bool input);
	bool isDeviceLost();


protected:
	
	string					m_MainWndCaption;	
	D3DDEVTYPE				m_DevType;
	DWORD					m_RequestedVP; //vertex processing type
	HINSTANCE				m_AppInst; 
	HWND					m_MainWndInst;
	IDirect3D9*				m_d3Object;
	bool					m_AppPaused;
	D3DPRESENT_PARAMETERS	m_D3dPP;
	int						m_AppHeight;
	int						m_AppLength;
	int						m_ResHeight;
	int						m_ResLength;
	int						m_OldHeight;
	int						m_OldWidth;
	
};


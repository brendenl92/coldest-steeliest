#pragma once

//#include <WinSock2.h>
#include <iostream>
#include <vector>
#include <string>
#include <windows.h>

#pragma comment(lib, "ws2_32.lib")

#include "Packets.h"
#include "Player.h"

using namespace std;

class NetworkCore
{
	WSADATA data;
	SOCKADDR_IN serverInfo;
	

	char clientID;


public:

	NetworkCore(UINT port, string ip);
	~NetworkCore(void);

	void SendPosition(int x, int y, int z);
	void SendShoot();

	void disconnect();

	int getPlayerSize();
	Player* getPlayer(int i);

	char getClientID();
};

#pragma once
#include "DXUtil.h"

using namespace std;

class Light
{
	D3DXVECTOR4 Diffuse;
	D3DXVECTOR4 Specular;
	
	float SpecPower;

	D3DXVECTOR3 Position;
public:
	Light();
	Light(D3DXVECTOR4 D, D3DXVECTOR4 S, float p, D3DXVECTOR3 P); //(Diffuse, Specular, Specular Power, Position);

	D3DXMATRIX getTrans();
	D3DXVECTOR4 getDiffuse();
	D3DXVECTOR4 getSpecular();

	void setPos(D3DXVECTOR3 Pos);
	void setDiffuse(D3DXVECTOR4 Diffuse);
	void setSpecular(D3DXVECTOR4 Specular);
	void setSpecPow(float SpecPower);

	void update(float dt);
	~Light();
};


#include "Settings.h"
#include "XInput.h"

Settings::Settings()
{

	ifstream input;
	input.open("../GraphicsFramework/settings.txt");
	if (!input.good())
	{
		MessageBox(NULL, "Cannot find settings.txt", "Settings load fail", MB_ICONWARNING);
		PostQuitMessage(0);
	}
	string strr;
	getline(input, strr);
	if (strr == "1")
	{
		if (XBOX->IsConnected() == true)
			controller = true;
		else
			controller = false;
	}
	else
		controller = false;
	getline(input, strr);
	if (strr == "1")
		inverted = true;
	else
		inverted = false;
	getline(input, strr);
	if (strr == "1")
		thirdPerson = true;
	else
		thirdPerson = false;
	getline(input, strr);
	fxVolume = (float)atof(strr.c_str());
	getline(input, strr);
	musicVolume = (float)atof(strr.c_str());
	getline(input, strr);
	sensitivity = (float)atof(strr.c_str());
	input.close();

}
Settings* Settings::instance()
{
	static Settings* ptr = new Settings();
	return ptr;
}
void Settings::setSettings(bool controller, bool inverted, bool third, float fxVol, float musicVol, float sensitivity)
{
	if (controller == true)
		if (XBOX->IsConnected() == true)
			this->controller = true;
		else
			this->controller = false;
	this->inverted = inverted;
	this->thirdPerson = third;
	this->fxVolume = fxVol;
	this->musicVolume = musicVol;
	this->sensitivity = sensitivity;
	SOUND->setMusicVolume(musicVolume);

	ofstream out;
	out.open("settings.txt");
	if (!out.good())
		MessageBox(NULL, "Cannot save settings", "Settings save fail", MB_ICONWARNING);

	out << controller << endl << inverted << endl << thirdPerson << endl << fxVolume << endl << musicVolume << endl << sensitivity;
	out.close();
}
bool Settings::getController()
{
	return controller;
}

bool Settings::getThird()
{
	return thirdPerson;
}

bool Settings::getInverted()
{
	return inverted;
}

float Settings::getFXVolume()
{
	return fxVolume;
}
float Settings::getMusicVolume()
{
	return musicVolume;
}

float Settings::getSensitivity()
{
	return sensitivity;
}
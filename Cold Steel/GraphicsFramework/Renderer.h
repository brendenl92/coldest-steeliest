#pragma once

#include "MasterEntity.h"
#include "ResourceManager.h"
#include "skybox.h"
#include "Laser.h"
#include "BasicObjectManager.h"
struct planeVertex {
	float x, y, z, w;
	float u, v;
};

enum fx{BUILD, LIGHT, SKYBOX, FINAL};

class Renderer
{
	IDirect3DTexture9* RT;

	IDirect3DSurface9* backbuffer;
	IDirect3DSurface9* RTS;

	LPD3DXEFFECT fx;
	LPD3DXEFFECT FFX;

	planeVertex planeVerts[4];

	void texturePass(vector<MasterEntity*> entities);
	void finalPass(MasterEntity* tv);
public:
	Renderer();
	~Renderer();

	void Render(vector<MasterEntity*> entities, MasterEntity* tv);
	void ScreenCap();

	void renderFinal();

	void setSky(int index);

	void BeginRendering();
	void EndRendering();

	bool firstRendered();

	void renderStart();
	void showHud(bool yes);

	void renderCountDownB(bool in, double h);

	void onLostDevice(bool destruct);
	void onResetDevice(bool first);
};


# pragma once




#define JUGGERNAUT Juggernaut::Instance()
#define PREDATOR Predator::Instance()
#define SQUIRREL FlyingSquirrel::Instance()
#define ROMAN Roman::Instance()
#define RAPTOR Raptor::Instance()
#define ARMADILLO Armadillo::Instance()
#define TRINITY Trinity::Instance()
#define CLARK Clark::Instance()


class Tank
{

protected:

	int   mTankID;
	float mDamage;
	float mShootRate;
	float mDefense;
	float mHealthRegen;
	float mSpeed;
	float mTurnSpeed;

public:

	virtual void initTank() = 0;
	

	int   TankID(){ return mTankID; }
	float Damage(){ return mDamage; }
	float RateOfFire(){ return mShootRate; }
	float Defense(){ return mDefense; }
	float HealthRegen(){ return mHealthRegen; }
	float Speed(){ return mSpeed; }
	float TurnSpeed(){ return mTurnSpeed; }
};




class Juggernaut : public Tank
{
	Juggernaut();

public:

	void initTank(){};
	static Juggernaut* Instance();
};




class Predator : public Tank
{
	Predator();

public:

	void initTank(){};
	static Predator* Instance();
};



class FlyingSquirrel : public Tank
{
	FlyingSquirrel();

public:

	void initTank(){};
	static FlyingSquirrel* Instance();
};



class Roman : public Tank
{
	Roman();

public:

	void initTank(){};
	static Roman* Instance();
};



class Raptor : public Tank
{
	Raptor();

public:

	void initTank(){};
	static Raptor* Instance();
};




class Armadillo : public Tank
{
	Armadillo();

public:

	void initTank(){};
	static Armadillo* Instance();
};




class Trinity : public Tank
{
	Trinity();

public:

	void initTank(){};
	static Trinity* Instance();
};



class Clark : public Tank
{
	Clark();

public:

	void initTank(){};
	static Clark* Instance();
};
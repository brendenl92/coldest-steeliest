#pragma once

#include <windows.h>
using namespace std;

class State
{
public:
	State(void);
	~State(void);

	virtual void Initialize() = 0;
	virtual void UpdateScene(float dt) = 0;
	virtual void RenderScene() = 0;
	virtual void OnResetDevice() = 0;
	virtual void OnLostDevice() = 0;
	virtual void HandleInput(RAWINPUT* raw) = 0;
	virtual void Leave() = 0;

};


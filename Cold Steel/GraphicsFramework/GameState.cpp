#include "GameState.h"
#include "D3DAPP.h"
#include "Camera.h"
#include "HUD.h"

GameState::GameState(void)
{
	gw = new GameWorld();
}

GameState* GameState::Instance()
{

	static GameState* temp = new GameState();
	return temp;

}
GameState::~GameState(void)
{
	delete gw;
}

void GameState::Initialize()
{
	string temp = "../media/textures/costea.png";
	RM->ImportTextures(&temp, 1);
	HR(g_D3dDevice->Clear(0, 0, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, D3DCOLOR_XRGB(255, 255, 255), 1.0f, 0));
	HR(g_D3dDevice->BeginScene());
	D3DXMATRIX trans;
	RM->getSprite()->Begin(D3DXSPRITE_ALPHABLEND);
	D3DXMatrixTransformation2D(&trans, &D3DXVECTOR2(0, 0), 0, &D3DXVECTOR2(2.83f, 1.8f), NULL, 0, NULL);
	HR(RM->getSprite()->SetTransform(&trans));
	HR(RM->getSprite()->Draw(*RM->getTexture(0), NULL, &D3DXVECTOR3(0, 0, 0), &D3DXVECTOR3(0, 0, 0), D3DXCOLOR(1, 1, 1, 1)));
	RM->getSprite()->Flush();
	RM->getSprite()->End();
	g_D3dDevice->EndScene();
	g_D3dDevice->Present(0, 0, 0, 0);
	RM->clearAll();
	gw->init();
	movement = D3DXVECTOR3(0, 0, 0);
	Hud->Initialize();
	ShowCursor(FALSE);

}

void GameState::UpdateScene(float dt)
{

	if (SET->getController() == false)
	{
		movement = D3DXVECTOR3(0, 0, 0);
	}
	else
	{
		XBOX->update();

		float normLX = max(-1, (float)XBOX->GetState().Gamepad.sThumbLX / 32767);
		float normLY = max(-1, (float)XBOX->GetState().Gamepad.sThumbLY / 32767);

		float deadzoneX = 0.1f;
		float deadzoneY = 0.08f;

		float leftStickX = (abs(normLX) < deadzoneX ? 0 : normLX);
		float leftStickY = (abs(normLY) < deadzoneY ? 0 : normLY);
		leftStickX = (abs(normLX) < deadzoneX ? 0 : (abs(normLX) - deadzoneX) * (normLX / abs(normLX)));
		leftStickY = (abs(normLY) < deadzoneY ? 0 : (abs(normLY) - deadzoneY) * (normLY / abs(normLY)));

		if (deadzoneX > 0) leftStickX /= 1 - deadzoneX;
		if (deadzoneY > 0) leftStickY /= 1 - deadzoneY;

		movement += CAM->getForward() * leftStickY;
		movement += CAM->getRight() * leftStickX;

		if (XBOX->GetState().Gamepad.sThumbLX > 7849)
			movement += CAM->getRight() * (XBOX->GetState().Gamepad.sThumbLX / 32767.0f);
		else if (XBOX->GetState().Gamepad.sThumbLX < -7849)
			movement += CAM->getRight() * (XBOX->GetState().Gamepad.sThumbLX / 32767.0f);

		if (XBOX->GetState().Gamepad.wButtons & XINPUT_GAMEPAD_RIGHT_SHOULDER)
			movement += D3DXVECTOR3(0, 1, 0);
		if (XBOX->GetState().Gamepad.wButtons & XINPUT_GAMEPAD_LEFT_SHOULDER)
			movement -= D3DXVECTOR3(0, 1, 0);
	}

	gw->update(dt);
	movement = D3DXVECTOR3(0, 0, 0);

}

void GameState::RenderScene()
{

	gw->render();

}

void GameState::HandleInput(RAWINPUT* raw)
{
	gw->handleInput(raw);
	Hud->HandleInput(raw);
}

void GameState::OnResetDevice()
{
	gw->onResetDevice();
}

void GameState::OnLostDevice()
{
	gw->onLostDevice();
}

void GameState::Leave()
{

	gw->destroy();
}

#include "Light.h"


Light::Light()
{
	Diffuse = D3DXVECTOR4(1, 1, 1, 1);
	Specular = D3DXVECTOR4(1, 1, 1, 1);
	SpecPower = 5;
	Position = D3DXVECTOR3(0, 0, 0);
}


Light::~Light()
{
}

Light::Light(D3DXVECTOR4 D, D3DXVECTOR4 S, float p, D3DXVECTOR3 P)
{
	Diffuse = D;
	Specular = S;
	SpecPower = p;
	Position = P;
}
D3DXMATRIX Light::getTrans()
{
	D3DXMATRIX mat;
	D3DXMatrixTranslation(&mat, Position.x, Position.y, Position.z);
	return mat;
}
D3DXVECTOR4 Light::getDiffuse()
{
	return Diffuse;
}
D3DXVECTOR4 Light::getSpecular()
{
	return Specular;
}
void Light::setPos(D3DXVECTOR3 Pos)
{
	Position = Pos;
}
void Light::setDiffuse(D3DXVECTOR4 Diff)
{
	Diffuse = Diff;
}
void Light::setSpecular(D3DXVECTOR4 Spec)
{
	Specular = Spec;
}
void Light::setSpecPow(float SpecPow)
{
	SpecPower = SpecPow;
}
void Light::update(float dt)
{
		
}
//AIManager.cpp
#include "MasterEntity.h"
#include "AIManager.h"

AIManager* AIManager::s_Instance = 0;
AIManager* AIManager::Instance()
{
	if(!s_Instance)
		s_Instance = new AIManager;
	return s_Instance;
}

AIManager::~AIManager()
{
}

void AIManager::ShutDown()
{
	for(auto i = m_vAIEntities.begin(); i != m_vAIEntities.end(); i++)
	{
		(*i)->ShutDown();
		delete *i;
	}
	m_vAIEntities.clear();
	vector<BaseAIEntity*>().swap(m_vAIEntities);

	MD->ShutDown();

	s_Instance = NULL;
}

BaseAIEntity* AIManager::createAIObject()
{
	BaseAIEntity* tempAI	= new BaseAIEntity();

	cout << "AI Entity " << tempAI->GetAIID() << " was created. " << endl;

	m_vAIEntities.push_back(tempAI);

	return tempAI;

	//delete tempAI;
}

vector<BaseAIEntity*>& AIManager::GetBaseAIEntity()
{
	return m_vAIEntities;
}
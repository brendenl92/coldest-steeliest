//SteeringManager.cpp
#include "MasterEntity.h"
#include "SteeringManager.h"

D3DXVECTOR3 SteeringManager::Seek(D3DXVECTOR3 TargetPos, MasterEntity* SBase)
{
	D3DXVECTOR3 DesiredVelocity = D3DXVECTOR3(0, 0, 0);
	DesiredVelocity = (TargetPos - SBase->getPosition()) * SBase->getBaseAIEntity()->GetMSpeed() * 2;
	return *D3DXVec3Normalize(&DesiredVelocity, &(DesiredVelocity - SBase->getVelocity()));
}

D3DXVECTOR3 SteeringManager::Seek(MasterEntity* Target, MasterEntity* SBase)
{
	D3DXVECTOR3 DesiredVelocity = (Target->getPosition() - SBase->getPosition()) * (FLOAT)SBase->getBaseAIEntity()->GetMSpeed() * (FLOAT)2;
	return *D3DXVec3Normalize(&DesiredVelocity, &(DesiredVelocity - SBase->getVelocity()));
}

D3DXVECTOR3 SteeringManager::GetSeek(vector<MasterEntity* > m_vMEntities, MasterEntity* SBase)
{
	m_SteeringForce = D3DXVECTOR3(0, 0, 0);

	//Create feelers
	CreateFeeler(SBase);
	
	///Obstacle Avoidance
	//m_SteeringForce += ObstacleAvoidance(SBase);

	///Wall Avoidance
	//m_SteeringForce += WallAvoidance(SBase);

	///Seek
	m_SteeringForce += Seek(SBase->getBaseAIEntity()->Enemy /*m_vMEntities[(UINT)SBase->getBaseAIEntity()->GetEnemyID()]*/, SBase);
	
	///Entity Avoidance
	//m_SteeringForce += EntityAvoidance(m_vMEntities, SBase);

	m_SteeringForce.y = 0;

	return *D3DXVec3Normalize(&m_SteeringForce, &(m_SteeringForce * (m_WallWeight + m_ObstacleWeight + m_SeekWeight)));
}

D3DXVECTOR3 SteeringManager::Flee(D3DXVECTOR3 TargetPos, MasterEntity* SBase)
{
	D3DXVECTOR3 DesiredVelocity = (SBase->getPosition() - TargetPos) * SBase->getBaseAIEntity()->GetMSpeed() * 2;
	return *D3DXVec3Normalize(&DesiredVelocity, &(DesiredVelocity - SBase->getVelocity()));
}

D3DXVECTOR3 SteeringManager::Flee(MasterEntity* Target, MasterEntity* SBase)
{
	D3DXVECTOR3 DesiredVelocity = (SBase->getPosition() - Target->getPosition()) * SBase->getBaseAIEntity()->GetMSpeed() * 2;
	return *D3DXVec3Normalize(&DesiredVelocity, &(DesiredVelocity - SBase->getVelocity()));
}

D3DXVECTOR3 SteeringManager::GetFlee(vector<MasterEntity* > m_vMEntities, MasterEntity* SBase)
{
	m_SteeringForce = D3DXVECTOR3(0, 0, 0);

	//Create feelers
	CreateFeeler(SBase);

	///Obstacle Avoidance
	//m_SteeringForce += ObstacleAvoidance(SBase);

	///Wall Avoidance
	//m_SteeringForce += WallAvoidance(SBase);

	///Flee
	m_SteeringForce += Flee(m_vMEntities[(UINT)SBase->getBaseAIEntity()->GetEnemyID()]->getPosition(), SBase);
	
	///Entity Avoidance
	m_SteeringForce += EntityAvoidance(m_vMEntities, SBase);

	m_SteeringForce.y = 0;

	return *D3DXVec3Normalize(&m_SteeringForce, &(m_SteeringForce * (m_WallWeight + m_ObstacleWeight + m_FleeWeight)));
}

D3DXVECTOR3 SteeringManager::Wander(MasterEntity* SBase)
{
	///need to select a point in between -1 and 1 will determine the 
	///direction I will be traveling in

	///start figuring out my wander point
	m_Wander = *D3DXVec3Normalize(&m_Wander, &(D3DXVECTOR3((float)Rand->Uniform(-1, 1) * GetWanderJitter(), 0 , (float)Rand->Uniform(-1, 1)) * GetWanderJitter()));
	
	///change to world coordinates
	m_Wander.x += SBase->getPosition().x * (FLOAT).2;
	m_Wander.z += SBase->getPosition().z * (FLOAT).2;

	///multiply by radius
	m_Wander *= GetWanderRadius();
	
	///getting distance from AI Objects in direction I'm traveling
	D3DXVECTOR3 n1 = D3DXVECTOR3(0, 0, 0);
	D3DXVec3Normalize(&n1, &SBase->getVelocity());
	n1 = n1 * GetWanderDistance();

	///combine the distance and my wander point
	m_Wander += n1;

	m_Wander.y = 0;

	///then return the steering vector that we are desiring to reach
	return (m_Wander - SBase->getPosition());
}

D3DXVECTOR3 SteeringManager::GetWander(MasterEntity* SBase)
{
	m_SteeringForce = D3DXVECTOR3(0, 0, 0);

	//Create feelers
	CreateFeeler(SBase);

	///Wander
	m_SteeringForce += Wander(SBase) / 2;

	///Wall Avoidance
	//m_SteeringForce += WallAvoidance(SBase);

	///Obstacle Avoidance
	//m_SteeringForce += ObstacleAvoidance(SBase);

	m_SteeringForce.y = 0;

	return *D3DXVec3Normalize(&m_SteeringForce, &(m_SteeringForce * (m_WallWeight + m_ObstacleWeight + m_WanderWeight)));
}

D3DXVECTOR3 SteeringManager::Arrive(MasterEntity* Target, MasterEntity* SBase)
{
	///Get distance from the object
	float distance = D3DXVec3Length(&(Target->getPosition() - SBase->getPosition()));

	///based on distance from point we will decrement the speed of the entity
	if (distance < 21)
	{
		return D3DXVECTOR3(0, 0, 0);
	}
	if (distance < 40)
	{
		///tweak the deceleration
		FLOAT dc = 0.85f;

		///calc speed needed
		FLOAT speed = distance / 10 * dc;

		///make sure velocity doesn't go over max speed
		if (speed > SBase->getBaseAIEntity()->GetMSpeed())
			speed = SBase->getBaseAIEntity()->GetMSpeed();

		D3DXVECTOR3 DesiredVelocity = (Target->getPosition() - SBase->getPosition()) * speed;

		return *D3DXVec3Normalize(&DesiredVelocity, &(DesiredVelocity - SBase->getVelocity()));
	}
	if(distance > 0)
	{
		///tweak the deceleration
		FLOAT dc = 0.45f;

		///calc speed needed
		FLOAT speed = distance / 4 * dc;

		///make sure velocity doesn't go over max speed
		if(speed > SBase->getBaseAIEntity()->GetMSpeed())
			speed = SBase->getBaseAIEntity()->GetMSpeed();

		D3DXVECTOR3 DesiredVelocity = (Target->getPosition() - SBase->getPosition()) * speed;
		
		return *D3DXVec3Normalize(&DesiredVelocity, &(DesiredVelocity - SBase->getVelocity()));
	}

	return D3DXVECTOR3(0, 0, 0);
}

D3DXVECTOR3 SteeringManager::Arrive(D3DXVECTOR3 TargetPos, MasterEntity* SBase)
{
	///Get distance from object
	float distance = D3DXVec3Length(&(TargetPos - SBase->getPosition()));

	///based on distance from point we will decrement the speed of the entity
	if(distance > 0)
	{
		///tweak the deceleration
		FLOAT dc = 0.45f;

		///calc speed needed
		FLOAT speed = distance/4 * dc;

		///make sure velocity doesn't go over max speed
		if(speed > SBase->getBaseAIEntity()->GetMSpeed())
			speed = SBase->getBaseAIEntity()->GetMSpeed();

		D3DXVECTOR3 DesiredVelocity = (TargetPos - SBase->getPosition()) * speed;

		return (DesiredVelocity - SBase->getVelocity());
	}

	return D3DXVECTOR3(0, 0, 0);
}

D3DXVECTOR3 SteeringManager::GetArrive(vector<MasterEntity* > m_vMEntities, MasterEntity* SBase)
{
	m_SteeringForce = D3DXVECTOR3(0, 0, 0);

	//Create feelers
	CreateFeeler(SBase);

	///Wall Avoidance
	//m_SteeringForce += WallAvoidance(SBase);

	///Obstacle Avoidance
	//m_SteeringForce += ObstacleAvoidance(SBase);

	///Arrive
	m_SteeringForce += Arrive(SBase->getBaseAIEntity()->Enemy, SBase);
	//m_SteeringForce += Arrive(m_vMEntities[(UINT)SBase->getBaseAIEntity()->GetEnemyID()]->getPosition(), SBase);

	///Entity Avoidance
	m_SteeringForce += EntityAvoidance(m_vMEntities, SBase);

	m_SteeringForce.y = 0;

	return *D3DXVec3Normalize(&m_SteeringForce, &(m_SteeringForce * (m_WallWeight + m_ObstacleWeight + m_ArriveWeight)));
}

D3DXVECTOR3 SteeringManager::Pursue(MasterEntity* Target, MasterEntity* SBase)
{
	///If the object is looking at us then we can just seek them out..
	D3DXVECTOR3 toTarget = *D3DXVec3Normalize(&toTarget, &(Target->getPosition() - SBase->getPosition()));

	///Dotting the Heading of the objects
	float relativeDirection = D3DXVec3Dot(&SBase->getVelocity(), &Target->getVelocity()); 

	///acos(.95) = 18 deg our fov when chasing out the target
	if((D3DXVec3Dot(&toTarget, &SBase->getVelocity()) > 0) && (relativeDirection < -0.95)) 
	{
		return Seek(Target->getPosition(), SBase);
	}

	///From here on we are saying that the object isn't ahead of us so we 
	///are going to predict were it will go...

	///look ahead time is proportional to the distance between the target 
	///and the pursuer, and is inversly proportional to the sum of the 
	///objects velocities.
	float tmag =  D3DXVec3Length(&Target->getVelocity());
	float lookAhead = D3DXVec3Length(&(toTarget)) / (SBase->getBaseAIEntity()->GetMSpeed() + tmag);

	///Now seek the predicted future position of the target....
	return Seek((Target->getPosition() + Target->getVelocity() * lookAhead), SBase);
}

D3DXVECTOR3 SteeringManager::Evade(MasterEntity* Target, MasterEntity* SBase)
{
	D3DXVECTOR3 toTarget = *D3DXVec3Normalize(&toTarget, &(Target->getPosition() - SBase->getPosition()));

	float tmag = D3DXVec3Length(&Target->getVelocity());
	float lookAhead = D3DXVec3Length(&(toTarget)) / (SBase->getBaseAIEntity()->GetMSpeed() + tmag);

	return Flee((Target->getPosition() + Target->getVelocity() * lookAhead), SBase);
}

D3DXVECTOR3 SteeringManager::Hide(vector<MasterEntity* > m_vMEntities, MasterEntity* SBase)
{
	///First we need to determine where the Enemy is and then find the farthest building from where he is

	///Variants
	FLOAT cushion		= 10.f;
	FLOAT maxDistance	= 0.0f;
	FLOAT distance		= 0.0f;
	UINT  bestID		= 0;
	D3DXVECTOR3 n_v		= D3DXVECTOR3(0, 0, 0);
	
	///now we need to cycle through the buildiung to find the farthest building from our starting position
	for (auto i = m_vMEntities.begin(); i != m_vMEntities.end(); i++)
	{
		if ((*i)->getMeshID() >= 30 || (*i)->getMeshID() <= 130)
		{
			///Calculate max distance 
			distance = D3DXVec3Length(&((*i)->getPosition() - SBase->getPosition()));

			if (distance > maxDistance)
			{
				maxDistance = distance;
				bestID = (*i)->getEntityID();
			}
		}
	}

	///Now that I have found the farthest Entity I need to Arrive at the position w/ a given tolerance level.
	n_v = *D3DXVec3Normalize(&n_v, &(m_vMEntities[bestID]->getPosition() - SBase->getPosition()));
	n_v = n_v * 10;

	D3DXVECTOR3 DesiredVelocity = Arrive(D3DXVECTOR3(m_vMEntities[bestID]->getPosition().x + n_v.x, m_vMEntities[bestID]->getPosition().y + n_v.y, m_vMEntities[bestID]->getPosition().z + n_v.z), SBase);

	return DesiredVelocity;

}

D3DXVECTOR3 SteeringManager::GetHide(vector<MasterEntity* > m_vMEntities, MasterEntity* SBase)
{
	m_SteeringForce = D3DXVECTOR3(0, 0, 0);

	//Create feelers
	CreateFeeler(SBase);

	///Hide
	m_SteeringForce += Hide(m_vMEntities, SBase);
	
	///Entity Avoidance
	//m_SteeringForce += EntityAvoidance(m_vMEntities, SBase);

	m_SteeringForce.y = 0;

	return *D3DXVec3Normalize(&m_SteeringForce, &(m_SteeringForce * (m_WallWeight + m_ObstacleWeight + m_ArriveWeight)));

}

//D3DXVECTOR3 SteeringManager::WallAvoidance(MasterEntity* SBase) const
//{
//	D3DXVECTOR3 WForce;
//
//	double distancetoWall = 0.0;
//	double distancetoClstWall = 999999;
//
//	//index for the walls
//	int closestWall = -1;
//
//	D3DXVECTOR3 point = D3DXVECTOR3(0, 0, 0);
//	D3DXVECTOR3 closestPoint = D3DXVECTOR3(0, 0, 0);
//	D3DXVECTOR3 poc = D3DXVECTOR3(0, 0, 0);
//
//	///examine the feelers, and run through each wall
//	for (auto f = SBase->m_WD.begin(); f != SBase->m_WD.end(); f++)
//	{
//		for (int w = 0; w != WMI->getPlaneList().size(); w++)
//		{
//			if (WMI->getPlaneList()[w]->getPlaneA().getX() > 0
//				&& f->getX() //SBase->Base->getPosition().getX() + SBase->Base->getHalfExtentsX() * 1.4
//			> WMI->getPlaneList()[w]->getPlaneA().getX() - WMI->getPlaneList()[w]->GetOffset())
//			{
//				distancetoWall = (SBase->Base->getPosition() - WMI->getPlaneList()[w]->getPlaneA()).magnitude();
//
//				if (distancetoWall < distancetoClstWall)
//				{
//					distancetoClstWall = distancetoWall;
//
//					closestWall = w;
//
//					closestPoint.setX(SBase->Base->getPosition().getX() + (distancetoWall
//						- (SBase->Base->getHalfExtentsX() + WMI->getPlaneList()[w]->GetOffset())));
//					closestPoint.setY(0);
//					closestPoint.setZ(SBase->Base->getPosition().getZ() - (distancetoWall
//						- (SBase->Base->getHalfExtentsX() + WMI->getPlaneList()[w]->GetOffset())));
//
//					poc += closestPoint;
//				}
//			}
//			if (WMI->getPlaneList()[w]->getPlaneA().getX() < 0
//				&& f->getX() //SBase->Base->getPosition().getX() - SBase->Base->getHalfExtentsX() * 1.4
//				< WMI->getPlaneList()[w]->getPlaneA().getX() + WMI->getPlaneList()[w]->GetOffset())
//			{
//				distancetoWall = (SBase->Base->getPosition() - WMI->getPlaneList()[w]->getPlaneA()).magnitude();
//
//				if (distancetoWall < distancetoClstWall)
//				{
//					distancetoClstWall = distancetoWall;
//
//					closestWall = w;
//
//					closestPoint.setX(SBase->Base->getPosition().getX() - (distancetoWall
//						- (SBase->Base->getHalfExtentsX() + WMI->getPlaneList()[w]->GetOffset())));
//					closestPoint.setY(0);
//					closestPoint.setZ(SBase->Base->getPosition().getZ() + (distancetoWall
//						- (SBase->Base->getHalfExtentsX() + WMI->getPlaneList()[w]->GetOffset())));
//
//					poc += closestPoint;
//				}
//			}
//			if (WMI->getPlaneList()[w]->getPlaneA().getZ() > 0
//				&& f->getZ() //SBase->Base->getPosition().getZ() + SBase->Base->getHalfExtentsZ() * 1.4
//				> WMI->getPlaneList()[w]->getPlaneA().getZ() - WMI->getPlaneList()[w]->GetOffset())
//			{
//				distancetoWall = (SBase->Base->getPosition() - WMI->getPlaneList()[w]->getPlaneA()).magnitude();
//
//				if (distancetoWall < distancetoClstWall)
//				{
//					distancetoClstWall = distancetoWall;
//
//					closestWall = w;
//
//					closestPoint.setX(SBase->Base->getPosition().getX() - (distancetoWall
//						- (SBase->Base->getHalfExtentsX() + WMI->getPlaneList()[w]->GetOffset())));
//					closestPoint.setY(0);
//					closestPoint.setZ(SBase->Base->getPosition().getZ() + (distancetoWall
//						- (SBase->Base->getHalfExtentsX() + WMI->getPlaneList()[w]->GetOffset())));
//
//					poc += closestPoint;
//				}
//			}
//			if (WMI->getPlaneList()[w]->getPlaneA().getZ() < 0
//				&& f->getZ() //SBase->Base->getPosition().getZ() - SBase->Base->getHalfExtentsX() * 1.4
//				< WMI->getPlaneList()[w]->getPlaneA().getZ() + WMI->getPlaneList()[w]->GetOffset())
//			{
//				distancetoWall = (SBase->Base->getPosition() - WMI->getPlaneList()[w]->getPlaneA()).magnitude();
//
//				if (distancetoWall < distancetoClstWall)
//				{
//					distancetoClstWall = distancetoWall;
//
//					closestWall = w;
//
//					closestPoint.setX(SBase->Base->getPosition().getX() + (distancetoWall
//						- (SBase->Base->getHalfExtentsX() + WMI->getPlaneList()[w]->GetOffset())));
//					closestPoint.setY(0);
//					closestPoint.setZ(SBase->Base->getPosition().getZ() - (distancetoWall
//						- (SBase->Base->getHalfExtentsX() + WMI->getPlaneList()[w]->GetOffset())));
//
//					poc += closestPoint;
//				}
//			}
//		}///next wall
//	}
//
//	///if intersection has been detected, calculate a force that will push it away
//	if (closestWall >= 0)
//	{
//		//calculate by what distance the projected position of the Entity will over shoot
//		D3DXVECTOR3 overshoot = (SBase->Base->getPosition() + (SBase->Base->getVelocity().normalize()) * 1.4)
//							- poc;
//
//		//create a force in the direction of the wall, with a magnitude of the overshoot
//		WForce = (WMI->getPlaneList()[closestWall]->getNormal() * overshoot.magnitude() / SBase->GetMSpeed());
//	}
//
//	return WForce;
//}

//D3DXVECTOR3 SteeringManager::ObstacleAvoidance(MasterEntity* SBase) const
//{
//	D3DXVECTOR3 OForce;
//
//	int maxRange = 99999;
//	int id		 = 0;
//
//	///create a detection box porportional to the velocity of the object
//	SBase->getBaseAIEntity()->m_DB = (SBase->getPosition() + *D3DXVec3Normalize(&SBase->getBaseAIEntity()->m_DB, &(SBase->getVelocity())) / SBase->getRigidBody()->Mass());// * SBase->GetMSpeed();
//
//	for(auto i = WMI->getCubeList().begin(); i != WMI->getCubeList().end(); i++)
//	{
//		if((*i)->getIMass() == 0.0f)
//		{
//			if(((*i)->getPosition() - SBase->getPosition()).magnitude() < maxRange)
//			{
//				maxRange = (int)((*i)->getPosition() - SBase->getPosition()).magnitude();
//				id = (*i)->GetID();
//				continue;
//			}
//		}
//	}
//
//	bool result = Test(WMI->getCubeList()[id]);
//
//		if(result == true)
//		{
//			///get penetration
//			D3DXVECTOR3 n1, n2, pen;
//
//			n1	= (SBase->Base->getPosition() - (WMI->getCubeList()[id])->getPosition());
//			n2	= (SBase->Base->getPosition() - SBase->m_DB);
//			pen = n1 - n2;							///vector amount displaying how much space there is between the entity and the obstacle
//
//			pen.setY(0);
//
//			//SBase->Base->getPosition() += pen;
//
//			OForce = (SBase->Base->getVelocity() + pen).normalize();
//			//OForce = ((SBase->Base->getVelocity()).normalize() * -1) + (pen);// * 4);
//			
//			//OForce = OForce.normalize();
//
//			return OForce;
//		}
//
//	return OForce;
//}

bool SteeringManager::Test(MasterEntity* c, MasterEntity* SBase) const
{
	if (c->getMeshID() != 16)
		if(SBase->getBaseAIEntity()->m_DB.x - SBase->getRigidBody()->GetHalfSize().x < (c->getPosition().x + c->getRigidBody()->GetHalfSize().x) &&			///left-side
			SBase->getBaseAIEntity()->m_DB.x + SBase->getRigidBody()->GetHalfSize().x > (c->getPosition().x - c->getRigidBody()->GetHalfSize().x) &&		///right-side
			SBase->getBaseAIEntity()->m_DB.z - SBase->getRigidBody()->GetHalfSize().z < (c->getPosition().z + c->getRigidBody()->GetHalfSize().z) &&		///front
			SBase->getBaseAIEntity()->m_DB.z + SBase->getRigidBody()->GetHalfSize().z > (c->getPosition().z - c->getRigidBody()->GetHalfSize().z))			///back
				return true;
	return false;
}

D3DXVECTOR3 SteeringManager::EntityAvoidance(vector<MasterEntity* > m_vMEntities, MasterEntity* SBase)
{
	D3DXVECTOR3 EForce = D3DXVECTOR3(0, 0, 0);

	FLOAT maxRange	= 99999;
	FLOAT id		= 0;

	bool result		= false;

	///create a detection box porportional to the velocity of the object
	SBase->getBaseAIEntity()->m_DB = *D3DXVec3Normalize(&SBase->getVelocity(), &SBase->getVelocity());
	SBase->getBaseAIEntity()->m_DB = SBase->getBaseAIEntity()->m_DB	* SBase->getBaseAIEntity()->GetTolerance();
	SBase->getBaseAIEntity()->m_DB += SBase->getPosition();

	for (UINT target = 0; target != m_vMEntities.size(); target++)
	{
		if (m_vMEntities[target]->getEntityID() == SBase->getEntityID())
			continue;
		if (m_vMEntities[target]->getMeshID() > 0 && m_vMEntities[target]->getMeshID() < 16 && (m_vMEntities[target]->getRigidBody()->Mass()) > 0.0f)
		{
			if (D3DXVec3Length(&(m_vMEntities[target]->getPosition() - SBase->getPosition())) < maxRange)
			{
				maxRange = D3DXVec3Length(&(m_vMEntities[target]->getPosition() - SBase->getPosition()));
				id = (FLOAT)(m_vMEntities[target]->getEntityID());
			}
		}
	}

	for (UINT i = 0; i != m_vMEntities.size(); i++)
	{
		if (D3DXVec3Length(&(m_vMEntities[i]->getPosition() - SBase->getPosition())) <= maxRange)
		{
			if (m_vMEntities[i]->getEntityID() == SBase->getEntityID())
				continue;
			if (m_vMEntities[i]->getEntityID() == id)
				result = Test(m_vMEntities[(UINT)i], SBase);
		}

		if(result == true)
		{			
			/*D3DXVECTOR3 toTarget = *D3DXVec3Normalize(&toTarget, &(m_vMEntities[i]->getPosition() - SBase->getPosition()));

			float tmag = D3DXVec3Length(&m_vMEntities[i]->getVelocity());
			float lookAhead = D3DXVec3Length(&(toTarget)) / (SBase->getBaseAIEntity()->GetMSpeed() + tmag);

			EForce = Flee((m_vMEntities[i]->getPosition() + m_vMEntities[i]->getVelocity() * lookAhead), SBase);*/
			EForce = Evade(m_vMEntities[(UINT)i], SBase);

			EForce = *D3DXVec3Normalize(&EForce, &EForce);

			return EForce;
		}
	}

	return EForce;
}

void SteeringManager::CreateFeeler(MasterEntity* SBase) const
{
	SBase->getBaseAIEntity()->m_WD.clear();
	vector<D3DXVECTOR3>().swap(SBase->getBaseAIEntity()->m_WD);

	D3DXVECTOR3 direction = D3DXVECTOR3(0, 0, 0);
	int xt = 0;
	int zt = 0;

	if (SBase->getPosition().x > 0)
		xt = 1;
	else
		xt = -1;
	
	if (SBase->getPosition().z > 0)
		zt = 1;
	else
		zt = -1;

	//get direction we are heading in
	direction = *D3DXVec3Normalize(&direction, &SBase->getVelocity());

	//setup points
	D3DXVECTOR3 temp1 = SBase->getPosition() + direction + D3DXVECTOR3((FLOAT)xt, 0, 0);
	D3DXVECTOR3 temp2 = SBase->getPosition() + direction + D3DXVECTOR3(0, 0, (FLOAT)zt);
	D3DXVECTOR3 temp3 = SBase->getPosition() + direction + D3DXVECTOR3((FLOAT)xt/2, 0, (FLOAT)zt/2);

	SBase->getBaseAIEntity()->m_WD.push_back(temp1);
	SBase->getBaseAIEntity()->m_WD.push_back(temp2);
	SBase->getBaseAIEntity()->m_WD.push_back(temp3);
}

FLOAT SteeringManager::GetWanderRadius() const
{
	return wanderRadius;
}

FLOAT SteeringManager::GetWanderJitter() const
{
	return wanderJitter;
}

FLOAT SteeringManager::GetWanderDistance() const
{
	return wanderDistance;
}
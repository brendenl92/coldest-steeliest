#include "Clock.h"


Clock::Clock()
{
	cntsPerSec = 0;
	currTimeStamp = 0;
	prevTimeStamp = 0;
	secsPerCnt = 0;
	dt = 0;
	timeElapsed = 0;
}

Clock::~Clock()
{
}

Clock* Clock::Instance()
{
	static Clock* ptr = new Clock();
	return ptr;
}
void Clock::start()
{
	cntsPerSec = 0;
	QueryPerformanceFrequency((LARGE_INTEGER*)&cntsPerSec);
	secsPerCnt = 1.0f / (float)cntsPerSec;

	prevTimeStamp = 0;
	QueryPerformanceCounter((LARGE_INTEGER*)&prevTimeStamp);

}

void Clock::add(float in)
{
	timeElapsed += in;
}

float Clock::end()
{
	QueryPerformanceCounter((LARGE_INTEGER*)&currTimeStamp);
	dt = (currTimeStamp - prevTimeStamp)*secsPerCnt;
	timeElapsed += dt;
	return dt;
}

void Clock::finish()
{
	prevTimeStamp = currTimeStamp;
}

float Clock::getDT()
{
	return dt;
}

float Clock::getTime()
{
	return timeElapsed;
}
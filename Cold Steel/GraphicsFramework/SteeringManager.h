#pragma once

#include "BaseAIEntity.h"
#include "Random.h"

class SteeringManager
{
private:
	D3DXVECTOR3			m_SteeringForce;
	D3DXVECTOR3			maxForce;
	D3DXVECTOR3			maxTurnRate;
	D3DXVECTOR3			m_Wander;
	D3DXVECTOR3			TargetPos;
	FLOAT				wanderRadius;
	FLOAT				wanderJitter;
	FLOAT				wanderDistance;
	
public:
	SteeringManager()
	{
		maxForce			= D3DXVECTOR3(10, 0, 10);
		maxTurnRate			= D3DXVECTOR3(3, 0, 3);
		m_Wander			= D3DXVECTOR3(0, 0, 0);
		m_SeekWeight		= 4;
		m_FleeWeight		= 4;
		m_WallWeight		= 4;
		wanderRadius		= 10;
		wanderJitter		= 4;
		wanderDistance		= 10;
		m_WanderWeight		= 1;
		m_ArriveWeight		= 4;
		m_SpecialWeight		= (FLOAT).4;
		m_ObstacleWeight	= 4;
	}

public:
	~SteeringManager()
	{
		maxForce			= D3DXVECTOR3(0, 0, 0);
		maxTurnRate			= D3DXVECTOR3(0, 0, 0);
		m_SeekWeight		= 0;
		m_FleeWeight		= 0;
		m_WallWeight		= 0;
		wanderRadius		= 0;
		wanderJitter		= 0;
		wanderDistance		= 0;
		m_WanderWeight		= 0;
		m_ArriveWeight		= 0;
		m_SpecialWeight		= 0;
		m_ObstacleWeight	= 0;
	}

public:
	D3DXVECTOR3 CalculateSForce();

///Steering Algorithms
public:
	///Seek
	D3DXVECTOR3 Seek(D3DXVECTOR3 TargetPos, MasterEntity* SBase);
	D3DXVECTOR3 Seek(MasterEntity* Target, MasterEntity* SBase);
	D3DXVECTOR3 GetSeek(vector<MasterEntity* > m_vMEntities, MasterEntity* SBase);			///Used to return a vector with Seek, OA, & WA 

	///Flee
	D3DXVECTOR3 Flee(D3DXVECTOR3 TargetPos, MasterEntity* SBase);
	D3DXVECTOR3 Flee(MasterEntity* Target, MasterEntity* SBase);
	D3DXVECTOR3 GetFlee(vector<MasterEntity* > m_vMEntities, MasterEntity* SBase);			///Flee

	///Wander
	D3DXVECTOR3 Wander(MasterEntity* SBase);
	D3DXVECTOR3 GetWander(MasterEntity* SBase);												///Wander

	///Arrive
	D3DXVECTOR3 Arrive(D3DXVECTOR3 TargetPos, MasterEntity* SBase);
	D3DXVECTOR3 Arrive(MasterEntity* Target, MasterEntity* SBase);
	D3DXVECTOR3 GetArrive(vector<MasterEntity* > m_vMEntities, MasterEntity* SBase);		///Arrive

	///Pursue
	D3DXVECTOR3 Pursue(MasterEntity* Target, MasterEntity* SBase);							///Pursue
	
	///Evade
	D3DXVECTOR3 Evade(MasterEntity* Target, MasterEntity* SBase);							///Evade

	///Hide
	/*D3DXVECTOR3 Hide(BaseAIEntity* Target);												///Hide
	D3DXVECTOR3 Hide(D3DXVECTOR3 TargetPos);
	D3DXVECTOR3 GetHide();*/
	D3DXVECTOR3 Hide(vector<MasterEntity* > m_vMEntities, MasterEntity* SBase);
	D3DXVECTOR3 GetHide(vector< MasterEntity* > m_vMEntities, MasterEntity* SBase);

	///Wall Avoidance
	D3DXVECTOR3 WallAvoidance(MasterEntity* SBase) const;									///Wall

	///Obstacle Avoidance
	D3DXVECTOR3 ObstacleAvoidance(MasterEntity* SBase) const;								///Obstacle
	bool	Test(MasterEntity* c, MasterEntity* SBase) const;

	///Entity Avoidance
	D3DXVECTOR3 EntityAvoidance(vector<MasterEntity* > m_vMEntities, MasterEntity* SBase);	///Entity

	///Create Feelers
	void	CreateFeeler(MasterEntity* SBase) const;

	///Wander helpers
	FLOAT  GetWanderRadius() const;
	FLOAT  GetWanderJitter() const;
	FLOAT  GetWanderDistance() const;

///Steering Weights
private:
	FLOAT	m_SeekWeight;
	FLOAT	m_FleeWeight;
	FLOAT	m_WallWeight;
	FLOAT	m_ArriveWeight;
	FLOAT	m_HideWeight;
	FLOAT	m_WanderWeight;
	FLOAT	m_SpecialWeight;
	FLOAT	m_ObstacleWeight;
};
xof 0303txt 0032
template Frame {
 <3d82ab46-62da-11cf-ab39-0020af71e433>
 [...]
}

template Matrix4x4 {
 <f6f23f45-7686-11cf-8f52-0040333594a3>
 array FLOAT matrix[16];
}

template FrameTransformMatrix {
 <f6f23f41-7686-11cf-8f52-0040333594a3>
 Matrix4x4 frameMatrix;
}

template ObjectMatrixComment {
 <95a48e28-7ef4-4419-a16a-ba9dbdf0d2bc>
 Matrix4x4 objectMatrix;
}

template Vector {
 <3d82ab5e-62da-11cf-ab39-0020af71e433>
 FLOAT x;
 FLOAT y;
 FLOAT z;
}

template MeshFace {
 <3d82ab5f-62da-11cf-ab39-0020af71e433>
 DWORD nFaceVertexIndices;
 array DWORD faceVertexIndices[nFaceVertexIndices];
}

template Mesh {
 <3d82ab44-62da-11cf-ab39-0020af71e433>
 DWORD nVertices;
 array Vector vertices[nVertices];
 DWORD nFaces;
 array MeshFace faces[nFaces];
 [...]
}

template MeshNormals {
 <f6f23f43-7686-11cf-8f52-0040333594a3>
 DWORD nNormals;
 array Vector normals[nNormals];
 DWORD nFaceNormals;
 array MeshFace faceNormals[nFaceNormals];
}

template Coords2d {
 <f6f23f44-7686-11cf-8f52-0040333594a3>
 FLOAT u;
 FLOAT v;
}

template MeshTextureCoords {
 <f6f23f40-7686-11cf-8f52-0040333594a3>
 DWORD nTextureCoords;
 array Coords2d textureCoords[nTextureCoords];
}

template ColorRGBA {
 <35ff44e0-6c7c-11cf-8f52-0040333594a3>
 FLOAT red;
 FLOAT green;
 FLOAT blue;
 FLOAT alpha;
}

template ColorRGB {
 <d3e16e81-7835-11cf-8f52-0040333594a3>
 FLOAT red;
 FLOAT green;
 FLOAT blue;
}

template Material {
 <3d82ab4d-62da-11cf-ab39-0020af71e433>
 ColorRGBA faceColor;
 FLOAT power;
 ColorRGB specularColor;
 ColorRGB emissiveColor;
 [...]
}

template MeshMaterialList {
 <f6f23f42-7686-11cf-8f52-0040333594a3>
 DWORD nMaterials;
 DWORD nFaceIndexes;
 array DWORD faceIndexes[nFaceIndexes];
 [Material <3d82ab4d-62da-11cf-ab39-0020af71e433>]
}

template TextureFilename {
 <a42790e1-7810-11cf-8f52-0040333594a3>
 STRING filename;
}


Frame building_residential_36 {
 

 FrameTransformMatrix relative {
  1.000000,0.000000,0.000000,0.000000,0.000000,1.000000,0.000000,0.000000,0.000000,0.000000,1.000000,0.000000,0.000000,0.000000,0.000000,1.000000;;
 }

 ObjectMatrixComment object {
  1.000000,0.000000,0.000000,0.000000,0.000000,1.000000,0.000000,0.000000,0.000000,0.000000,1.000000,0.000000,0.000000,0.000000,0.000000,1.000000;;
 }

 Mesh mesh_b {
  20;
  -18.424230;14.610102;7.946474;,
  -18.424230;14.610102;-7.946474;,
  18.424230;14.610102;-7.946474;,
  18.424230;14.610102;7.946474;,
  18.424230;14.610102;7.946474;,
  18.424230;14.610102;-7.946474;,
  18.424230;0.000000;-7.946474;,
  18.424230;0.000000;7.946474;,
  -18.424230;14.610102;7.946474;,
  18.424230;14.610102;7.946474;,
  18.424230;0.000000;7.946474;,
  -18.424230;0.000000;7.946474;,
  -18.424230;14.610102;-7.946474;,
  -18.424230;14.610102;7.946474;,
  -18.424230;0.000000;7.946474;,
  -18.424230;0.000000;-7.946474;,
  18.424230;14.610102;-7.946474;,
  -18.424230;14.610102;-7.946474;,
  -18.424230;0.000000;-7.946474;,
  18.424230;0.000000;-7.946474;;
  10;
  3;0,2,1;,
  3;2,0,3;,
  3;4,6,5;,
  3;6,4,7;,
  3;8,10,9;,
  3;10,8,11;,
  3;12,14,13;,
  3;14,12,15;,
  3;16,18,17;,
  3;18,16,19;;

  MeshNormals normals {
   20;
   0.000000;1.000000;0.000000;,
   0.000000;1.000000;0.000000;,
   0.000000;1.000000;0.000000;,
   0.000000;1.000000;0.000000;,
   1.000000;0.000000;0.000000;,
   1.000000;0.000000;0.000000;,
   1.000000;0.000000;0.000000;,
   1.000000;0.000000;0.000000;,
   0.000000;0.000000;1.000000;,
   0.000000;0.000000;1.000000;,
   0.000000;0.000000;1.000000;,
   0.000000;0.000000;1.000000;,
   -1.000000;0.000000;0.000000;,
   -1.000000;0.000000;0.000000;,
   -1.000000;0.000000;0.000000;,
   -1.000000;0.000000;0.000000;,
   0.000000;0.000000;-1.000000;,
   0.000000;0.000000;-1.000000;,
   0.000000;0.000000;-1.000000;,
   0.000000;0.000000;-1.000000;;
   10;
   3;0,2,1;,
   3;2,0,3;,
   3;4,6,5;,
   3;6,4,7;,
   3;8,10,9;,
   3;10,8,11;,
   3;12,14,13;,
   3;14,12,15;,
   3;16,18,17;,
   3;18,16,19;;
  }

  MeshTextureCoords tc0 {
   20;
   0.748751;0.446134;,
   0.748751;0.539376;,
   0.879466;0.539376;,
   0.879466;0.446134;,
   0.602280;0.752794;,
   0.285388;0.752794;,
   0.285388;1.006358;,
   0.602280;1.006358;,
   0.602280;0.752794;,
   0.001711;0.752794;,
   0.001711;1.006358;,
   0.602280;1.006358;,
   0.602280;0.752794;,
   0.285388;0.752794;,
   0.285388;1.006358;,
   0.602280;1.006358;,
   0.602280;0.752794;,
   0.001711;0.752794;,
   0.001711;1.006358;,
   0.602280;1.006358;;
  }

  MeshMaterialList mtls {
   1;
   10;
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0;

   Material Dflt_Material {
    0.588235;0.588235;0.588235;1.000000;;
    9.999999;
    0.900000;0.900000;0.900000;;
    0.000000;0.000000;0.000000;;

    TextureFilename Diffuse {
     "buildz1.tga";
    }
   }
  }
 }
}
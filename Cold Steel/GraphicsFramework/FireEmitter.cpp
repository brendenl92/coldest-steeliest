//=============================================================================
// ParticleEmitter.cpp by Frank Luna (C) 2004 All Rights Reserved.
//=============================================================================

#include "FireEmitter.h"
#include "Camera.h"
#include "d3dApp.h"
#include "DXUtil.h"
#include "Vertex.h"
#include <cassert>

FireEmitter::FireEmitter()
{
	mStartDelay = 0.0f;

	timeAccum = 0.0f;
	mRestart = true;
	mSpawned = 0;
	mFinished = false;
	mTime = 0.0f;
	mPaused = false;

	AABB stuff;
	
	mMaxNumParticles = 1;
	mScale = 1.0f;

	// Allocate memory for maximum number of particles.
	//mParticles.resize(mMaxNumParticles);
	//mAliveParticles.reserve(mMaxNumParticles);
	//mDeadParticles.reserve(mMaxNumParticles);

	D3DXMatrixIdentity(&mWorld);
	D3DXMatrixIdentity(&mInvWorld);

	// Create the texture.
	HR(D3DXCreateTextureFromFile(g_D3dDevice, "../media/images/fire01.dds", &mTex));
	HR(D3DXCreateTextureFromFile(g_D3dDevice, "../media/images/alpha01.dds", &mAlphaTex));
	HR(D3DXCreateTextureFromFile(g_D3dDevice, "../media/images/noise01.dds", &mNoiseTex));

	// Create the FX.
	ID3DXBuffer* errors = 0;
	HR(D3DXCreateEffectFromFile(g_D3dDevice, "../shaders/fire.fx", 0, 0, D3DXSHADER_DEBUG, 0, &mFX, &errors));
	if( errors )
		MessageBox(0, (char*)errors->GetBufferPointer(), 0, 0);

	mhTech    = mFX->GetTechniqueByName("FireTechnique");
	mhWVP     = mFX->GetParameterByName(0, "gWVP");
	mhTex     = mFX->GetParameterByName(0, "fireTexture");
	mhAlphaTex     = mFX->GetParameterByName(0, "alphaTexture");
	mhNoiseTex     = mFX->GetParameterByName(0, "noiseTexture");
	mhFrameTime = mFX->GetParameterByName(0, "frameTime");
	mhScrollSpeeds = mFX->GetParameterByName(0, "scrollSpeeds");
	mhScales = mFX->GetParameterByName(0, "scales");
	mhDistortion1 = mFX->GetParameterByName(0, "distortion1");
	mhDistortion2 = mFX->GetParameterByName(0, "distortion2");
	mhDistortion3 = mFX->GetParameterByName(0, "distortion3");
	mhDistortionScale = mFX->GetParameterByName(0, "distortionScale");
	mhDistortionBias = mFX->GetParameterByName(0, "distortionBias");

	//Uniform Instance
	mhInsPos  = mFX->GetParameterByName(0, "gPosition");
	
	// We don't need to set these every frame since they do not change.
	HR(mFX->SetTechnique(mhTech));
	HR(mFX->SetTexture(mhTex, mTex));
	HR(mFX->SetTexture(mhAlphaTex, mAlphaTex));
	HR(mFX->SetTexture(mhNoiseTex, mNoiseTex));
	
	BufferCode();

	D3DXMatrixIdentity(&mWorld);
}

FireEmitter::~FireEmitter()
{
	ReleaseCOM(mFX);
	ReleaseCOM(mTex);
	ReleaseCOM(mAlphaTex);
	ReleaseCOM(mNoiseTex);
	ReleaseCOM(mVB);
	ReleaseCOM(mIB);
}

float FireEmitter::getTime()
{
	return mTime;
}

void FireEmitter::setTime(float t)
{
	mTime = t;
}

const AABB& FireEmitter::getAABB()const
{
	return mBox;
}

void FireEmitter::setWorldMtx(const D3DXMATRIX& world)
{
	mWorld = world;

	// Compute the change of coordinates matrix that changes coordinates 
	// relative to world space so that they are relative to the particle
	// system's local space.
	D3DXMatrixInverse(&mInvWorld, 0, &mWorld);
}

void FireEmitter::onLostDevice()
{
	HR(mFX->OnLostDevice());

	// Default pool resources need to be freed before reset.
	ReleaseCOM(mVB);
	ReleaseCOM(mIB);
}

void FireEmitter::onResetDevice()
{
	HR(mFX->OnResetDevice());

	// Default pool resources need to be recreated after reset.
	if(mVB == 0)
	{
		BufferCode();
	}
}

void FireEmitter::update(float dt)
{
	if (mPaused == false)
		mTime += dt;

	if(mTime > 1000.0f)
		mTime = 0.0f;
}

void FireEmitter::draw()
{
	D3DXMATRIX FirePos;
	D3DXMatrixTranslation(&FirePos, mPosition.x, mPosition.y, mPosition.z);

	D3DXMATRIX FireScale;
	D3DXMatrixScaling(&FireScale, mScale, mScale, mScale);
	
	float angle, rotation;
	//Rotate so it faces camera all the time
	// Calculate the rotation that needs to be applied to the billboard model to face the current camera position using the arc tangent function.
	angle = atan2(mPosition.x - CAM->getPos().x, mPosition.z - CAM->getPos().z) * (180.0f / D3DX_PI);

	// Convert rotation into radians.
	rotation = angle * 0.0174532925f;
	
	//D3DXMATRIX worldMatrix;
	//worldMatrix = mWorld;
	D3DXMatrixRotationY(&mWorld, rotation);

	D3DXMatrixMultiply(&mWorld, &mWorld, &FirePos); 

	// Set FX parameters.
	HR(mFX->SetMatrix(mhWVP, &(FireScale*mWorld* CAM->getViewProjMat())));
	HR(mFX->SetFloat(mhFrameTime, mTime));

		// Set the three scrolling speeds for the three different noise textures.
	D3DXVECTOR3 scrollSpeeds = D3DXVECTOR3(1.3f, 2.1f, 2.3f);
	HR(mFX->SetValue(mhScrollSpeeds, &scrollSpeeds, sizeof(D3DXVECTOR3)));

	// Set the three scales which will be used to create the three different noise octave textures.
	D3DXVECTOR3 scales = D3DXVECTOR3(1.0f, 2.0f, 3.0f);
	HR(mFX->SetValue(mhScales, &scales, sizeof(D3DXVECTOR3)));

	// Set the three different x and y distortion factors for the three different noise textures.
	D3DXVECTOR2 distortion1 = D3DXVECTOR2(0.1f, 0.2f);
	D3DXVECTOR2 distortion2 = D3DXVECTOR2(0.1f, 0.3f);
	D3DXVECTOR2 distortion3 = D3DXVECTOR2(0.1f, 0.1f);
	HR(mFX->SetValue(mhDistortion1, &distortion1, sizeof(D3DXVECTOR2)));
	HR(mFX->SetValue(mhDistortion2, &distortion2, sizeof(D3DXVECTOR2)));
	HR(mFX->SetValue(mhDistortion3, &distortion3, sizeof(D3DXVECTOR2)));


	// The the scale and bias of the texture coordinate sampling perturbation.
	float distortionScale = 0.8f;
	float distortionBias = 0.5f;

	HR(mFX->SetFloat(mhDistortionBias, distortionBias));
	HR(mFX->SetFloat(mhDistortionScale, distortionScale));

	UINT numPasses = 0;
	HR(mFX->Begin(&numPasses, 0));
	HR(mFX->BeginPass(0));

	HR(g_D3dDevice->SetStreamSource(0, mVB, 0, sizeof(FireVertex)));
	HR(g_D3dDevice->SetIndices(mIB));
	HR(g_D3dDevice->SetVertexDeclaration(FireVertex::Decl));

	HR(g_D3dDevice->SetRenderState(D3DRS_FILLMODE, D3DFILL_SOLID));
	HR(g_D3dDevice->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, 4, 0, 2));

	HR(mFX->EndPass());
	HR(mFX->End());
}

void FireEmitter::BufferCode()
{
	// Obtain a pointer to a new vertex buffer.
	HR(g_D3dDevice->CreateVertexBuffer(4 * sizeof(FireVertex), D3DUSAGE_WRITEONLY, 0, D3DPOOL_MANAGED, &mVB, 0));
		
	// Now lock it to obtain a pointer to its internal data, and write the
	// cube's vertex data.
	FireVertex* v = 0;
	HR(mVB->Lock(0, 0, (void**)&v, 0));

	D3DXVECTOR3 pos(-1.0f, -1.0f, -1.0f);
	v[0].initialPos = pos;
	v[0].mTexture = D3DXVECTOR2(1.0f, 1.0f);

	pos = D3DXVECTOR3(-1.0f,  1.0f, -1.0f);
	v[1].initialPos = pos;
	v[1].mTexture = D3DXVECTOR2(1.0f, 0.0f);

	pos = D3DXVECTOR3( 1.0f,  1.0f, -1.0f);
	v[2].initialPos = pos;
	v[2].mTexture = D3DXVECTOR2(0.0f, 0.0f);

	pos = D3DXVECTOR3( 1.0f, -1.0f, -1.0f);
	v[3].initialPos = pos;
	v[3].mTexture = D3DXVECTOR2(0.0f, 1.0f);

	HR(mVB->Unlock());

	// Obtain a pointer to a new index buffer.
	HR(g_D3dDevice->CreateIndexBuffer(6 * sizeof(WORD), D3DUSAGE_WRITEONLY, D3DFMT_INDEX16, D3DPOOL_MANAGED, &mIB, 0));

	// Now lock it to obtain a pointer to its internal data, and write the
	// cube's index data.

	WORD* k = 0;

	HR(mIB->Lock(0, 0, (void**)&k, 0));

	// Front face.
	k[0] = 0; k[1] = 1; k[2] = 2;
	k[3] = 0; k[4] = 2; k[5] = 3;

	HR(mIB->Unlock());

}

int FireEmitter::getAliveParticleCount()
{
	return mMaxNumParticles;
}

void FireEmitter::SetPosition(D3DXVECTOR3 pos)
{
	mPosition = pos;

	mPosition.y = pos.y + mScale;
}

D3DXVECTOR3 FireEmitter::GetPosition()
{
	return mPosition;
}

void FireEmitter::setScale(float scale)
{
	mPosition.y -= mScale;
	mPosition.y += scale;

	mScale = scale;
}

float FireEmitter::getScale()
{
	return mScale;
}

void FireEmitter::setPaused(bool paused)
{
	mPaused = paused;
}
#pragma once

#include <vector>
#include "Mail.h"

using namespace std;

#define MD MailDispatch::Instance()

class MailDispatch
{
private:
	static MailDispatch* s_Instance;

private:
	MailDispatch() {}

public:
	static MailDispatch* Instance();

public:
	~MailDispatch();

private:
	void CallHandler(const Mail &letter);

public:
	void SendToState(int r, int s, double d, int msg, void* info);
	void CalcShortestDelay(double dt);
	void ShutDown();

private:
	vector<Mail> m_vPackage;
};
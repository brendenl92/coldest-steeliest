#include "Tank.h"


Juggernaut* Juggernaut::Instance()
{
	static Juggernaut* T = new Juggernaut();
	return T;
}


Juggernaut::Juggernaut()
{
	mTankID = 0;
	mDamage = 0.4f;
	mShootRate = 0.4f;
	mSpeed = 0.2f;
	mTurnSpeed = 0.2f;
	mDefense = 1.0f;
	mHealthRegen = .4f;
}





Predator* Predator::Instance()
{
	static Predator* T = new Predator();
	return T;
}


Predator::Predator()
{
	mTankID = 1;
	mDamage = 1.0f;
	mShootRate = 0.4f;
	mSpeed = 0.5f;
	mTurnSpeed = 0.4f;
	mDefense = 0.5f;
	mHealthRegen = 0.4f;
}





FlyingSquirrel* FlyingSquirrel::Instance()
{
	static FlyingSquirrel* T = new FlyingSquirrel();
	return T;
}


FlyingSquirrel::FlyingSquirrel()
{
	mTankID = 2;
	mDamage = 0.3f;
	mShootRate = 0.9f;
	mSpeed = 1.0f;
	mTurnSpeed = 1.0f;
	mDefense = 0.3f;
	mHealthRegen = 0.4f;
}






Roman* Roman::Instance()
{
	static Roman* T = new Roman();
	return T;
}


Roman::Roman()
{
	mTankID = 3;
	mDamage = 0.6f;
	mShootRate = 0.5f;
	mSpeed = 0.2f;
	mTurnSpeed = 0.2f;
	mDefense = 0.5f;
	mHealthRegen = 0.4f;
}





Raptor* Raptor::Instance()
{
	static Raptor* T = new Raptor();
	return T;
}


Raptor::Raptor()
{
	mTankID = 4;
	mDamage = 0.6f;
	mShootRate = 0.6f;
	mSpeed = 0.6f;
	mTurnSpeed = 0.6f;
	mDefense = 0.3f;
	mHealthRegen = 0.3f;
}





Armadillo* Armadillo::Instance()
{
	static Armadillo* T = new Armadillo();
	return T;
}


Armadillo::Armadillo()
{
	mTankID = 5;
	mDamage = 0.3f;
	mShootRate = 0.4f;
	mSpeed = 0.6f;
	mTurnSpeed = 0.6f;
	mDefense = 0.5f;
	mHealthRegen = 0.6f;
}





Trinity* Trinity::Instance()
{
	static Trinity* T = new Trinity();
	return T;
}


Trinity::Trinity()
{
	mTankID = 6;
	mDamage = 0.5f;
	mShootRate = 0.5f;
	mSpeed = 0.5f;
	mTurnSpeed = 0.5f;
	mDefense = 0.5f;
	mHealthRegen = 0.5f;
}





Clark* Clark::Instance()
{
	static Clark* T = new Clark();
	return T;
}


Clark::Clark()
{
	mTankID = 7;
	mDamage = 1.0f;
	mShootRate = 1.0f;
	mSpeed = 1.0;
	mTurnSpeed = 1.0f;
	mDefense = 1.0;
	mHealthRegen = 1.0f;
}
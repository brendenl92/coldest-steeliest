#pragma once

#include "ResourceManager.h"
#include <vector>

using namespace std;

class MasterEntity;
class Radar
{
public:
	Radar( );
	~Radar( );
	void Update(vector <MasterEntity*> Entities),
		Draw(),
		OnLostDevice(),
		OnResetDevice();
	inline void	SetPaused(bool isPaused){ this->isPaused = isPaused; }

private:
	D3DXVECTOR3 mRadarPos,
		mTankBasePos;

	float mTankBaseAngle;

	D3DXMATRIX mMatrix;

	ID3DXLine* mLine;

	void DrawRadarEntities(),
		DrawRadarTank(),
		DrawRadarTurret();

	vector <MasterEntity*> EntitiesVec;

	bool isPaused;
};
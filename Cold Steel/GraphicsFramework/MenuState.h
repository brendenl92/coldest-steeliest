#pragma once

#include "UIElements.h"
#include "TankSelectBack.h"
#include "state.h"
#include "MainMenuBackground.h"
//#include "SpriteManager.h"
class MenuState :
	public State
{
	MenuState(void);
	vector<Button> buttons;
	vector<Slider> sliders;
	vector<CheckBox> cboxes;
	vector<Text> texts;
	vector<Image> images;
	TankSelectBack* back;

	int chosen;
	bool options;
	float selectPause;

public:
	static MenuState* Instance();
	~MenuState(void);
	virtual void Initialize();
	virtual void UpdateScene(float dt);
	virtual void RenderScene();
	virtual void HandleInput(RAWINPUT* raw);

	virtual void OnResetDevice();
	virtual void OnLostDevice();
	virtual void Leave();
	
	void constructMain();
	void destructMain();
	void constructSettings();
	void destructSettings();
};


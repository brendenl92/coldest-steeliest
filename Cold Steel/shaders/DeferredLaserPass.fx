uniform extern float4x4 WVP;
uniform extern float4x4 World;
uniform extern texture Tex;
uniform extern float texSize;
uniform extern float3 CameraPos;

float Glowness = 10.0f;

struct VSIn
{
	float4 position : POSITION0;
	float3 tex0 : TEXCOORD0;
};

struct VSOut
{

	float4 pos : POSITION0;
	float2 tex0 : TEXCOORD0;
};

sampler LaserSampler = sampler_state
{
	Texture = <Tex>;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	MipFilter = LINEAR;
	AddressU = CLAMP;
	AddressV = CLAMP;
};

VSOut LaserVS(VSIn i)
{
	VSOut o = (VSOut)0;

	o.pos = mul(i.position, WVP);
	o.tex0 = i.position.xy;

	return o;
}

float4 LaserPS(VSOut input) : COLOR
{
	//float4 scn = float4(tex2D(LaserSampler, input.tex0).rgb, 1.0);
	//float3 glow = 10.0f * tex2D(LaserSampler, input.tex0).xyz;
	
	//return float4(scn.xyz + glow, scn.w);

	return float4(tex2D(LaserSampler, input.tex0).rgb, 1.0);
	//return float4(0.2f, 0.5f, 1.0f, 1.0f);
}

technique main
{
	pass p0
	{
		VertexShader = compile vs_2_0 LaserVS();
		PixelShader = compile ps_2_0 LaserPS();
		CullMode = none;
		ZWriteEnable = true;
		ZFunc = lessequal;
	}

};
#include "PhysicsInterface.h"
#include "collision function.h"
#include "Contact.h"
#include "MasterEntity.h"

enum Axis{ Xa, Ya, Za };

PhysicsInterface* PhysicsInterface::s_Instance = 0;

PhysicsInterface* PhysicsInterface::Instance()
{
	if (!s_Instance)
		s_Instance = new PhysicsInterface();

	return s_Instance;
}


//CTOR
PhysicsInterface::PhysicsInterface()
{
	
}

//Create Tank Object
RigidBody* PhysicsInterface::CreateTank(V3 pos)
{
	V3 Heading(1, 0, 0);
//	PhyTank* Tank = new PhyTank(pos, Heading);

	RigidBody* Canon = new RigidBody();

	return Canon;
}

RigidBody* PhysicsInterface::CreateRigidBody(MasterEntity* Master, V3 Position, float Radius, V3 Halfsize, float mass)
{
 	V3 Heading(1, 0, 0);
	BoundingVolume* BV = new BoundingVolume(Radius, Master->getMeshID() > 15 ? 1 : 0, Halfsize);
	RigidBody* temp = new RigidBody(Master, Position, Heading, 100.0f, 10.f, mass);
	BoundingVolumeNode* BVN = new BoundingVolumeNode(temp, BV);
	temp->SetVolume(BVN);
	mCollisionVolumes.push_back(BVN);
	return temp;
}

//Remove Collision Volume
void PhysicsInterface::RemoveCollisionVolume(RigidBody* Body)
{
	for (unsigned i = 0; i < mCollisionVolumes.size(); i++)
	{
		if (mCollisionVolumes[i]->mBody = Body)
		{
			mCollisionVolumes[i] = NULL;
			delete mCollisionVolumes[i];
			mCollisionVolumes.erase(mCollisionVolumes.begin() + i);
		}
	}
}

//Update Physics Engine
void PhysicsInterface::UpdatePhysics(float dt)
{

	/*if (PEntities.size() > 0)
	{
		for (auto it = PEntities.begin(); it != PEntities.end(); ++it)
			it->second->Update(dt);
	}*/

	GenertateContacts();
	ResolveContacts();
}


//Add linear motion to tank
void PhysicsInterface::AddLinearMotion()
{
	//((PhyTank*)PEntities[0])->mLeftThruster += 0.5f;
	//((PhyTank*)PEntities[0])->mRightThruster += 0.5f;
}


//Add left turn
void PhysicsInterface::AddRotationLeft()
{
//	((PhyTank*)PEntities[0])->mRightThruster += 0.5f;
}

//Add right turn
void PhysicsInterface::AddRotationRight()
{
//	((PhyTank*)PEntities[0])->mLeftThruster += 0.5f;
}



void PhysicsInterface::RotateAroundTheYAxis90DegreesCounterClockwise(RigidBody* rb)
{
	//Qu temp(PEntities[0]->GetAxis(Ya), 90);

	float rightangle = 90.0f;

	Qu temp;
	//D3DXQuaternionToAxisAngle(&temp, &PEntities[0]->GetAxis(Ya), &rightangle);
	D3DXQuaternionRotationAxis(&temp, &rb->GetAxis(Ya), rightangle);

	Qu Current(rb->GetQuaternion());

	Current *= temp;

	rb->Transform(Current, V3(0, 0, 0));
}

void PhysicsInterface::Check()
{
	//((PhyTank*)PEntities[0])->GetPosition();
}

//Generate Contacts
void PhysicsInterface::GenertateContacts()
{
	/*for (unsigned i = 0; i < mCollisionVolumes.size() - 1; i++)
		for (unsigned j = i + 1; j < mCollisionVolumes.size(); j++)
		if (i != j)
			if (mCollisionVolumes[i]->mVolume->environment != 1 || mCollisionVolumes[j]->mVolume->environment != 1)
				if (mCollisions.DetectCollision(mCollisionVolumes[i], mCollisionVolumes[j]))
					mCollisions.GenerateContact(mCollisionVolumes[i], mCollisionVolumes[j]);*/
}	


//Resolve Contacts
void PhysicsInterface::ResolveContacts()
{
	mCollisions.RsolveContacts();
}


//Shoot ray
bool PhysicsInterface::ShootRay(V3& Rp, V3& Rn, V3& q/*, MasterEntity* Me*/)
{
	V3 ClosestTarget(0,0,0);
	V3 poc(0,0,0);
	bool found = false;
	Contact* Ct;

	cout << "\nShoot!\n";

	//Check ray against all OBB volumes
	//Test Ray to OBB, If collision 
	//If there has been no collision yet set to closest
	//else check to see if the poc found is smaller than the current closest point
	if (mCollisionVolumes.size() > 1)
	{
		for (unsigned i = 0; i < mCollisionVolumes.size(); i++)
			if (mCollisionVolumes[i]->mVolume->mShape == VSPHERE)
			if (mCollisions.mDetector.RayToSphere(Rp, Rn, mCollisionVolumes[i]))
				{

				cout << "\nHit!\n";

					found = true;

					Ct = mCollisions.mGenerator.RayToSphere(Rp, Rn, mCollisionVolumes[i]);

					poc = Ct->mPoint;

					if (ClosestTarget == V3(0, 0, 0))
					{
						ClosestTarget = poc;
						//Me = mCollisionVolumes[i]->mBody->Master();
					}
					else if (D3DXVec3LengthSq(&poc) < D3DXVec3LengthSq(&ClosestTarget))
					{
						ClosestTarget = poc;
						//Me = mCollisionVolumes[i]->mBody->Master();
					}
					else
						poc;
				}






		/*if (mCollisionVolumes[i]->mVolume->mShape == VCUBE)
		if (POCRayToOBB(Rp, Rn, mCollisionVolumes[i], poc))
		{
			found = true;
			cout << "HIT!!!!!!\n";

			if (ClosestTarget == V3(0, 0, 0))
			{
				ClosestTarget = poc;
				Me = mCollisionVolumes[i]->mBody->Master();
			}
			else if (D3DXVec3LengthSq(&poc) < D3DXVec3LengthSq(&ClosestTarget))
			{
				ClosestTarget = poc;
				Me = mCollisionVolumes[i]->mBody->Master();
			}
			else
				poc;

		}*/
	}

			q = ClosestTarget;
			return found;
}


//Bullet collision detection
bool PhysicsInterface::BulletHit(V3& SPosition, float Radius, V3& poc, MasterEntity& Me)
{
	bool found = false;

	/*If there is more than one volumes
	to compare collision detection 
	against the bullet
	
	If there is collision detected
	find point of contact and 
	Master entity it hit*/
	if (mCollisionVolumes.size() > 1)
	{
		for (unsigned i = 0; i < mCollisionVolumes.size(); i++)
		{
			//If comparing to a box
			if (mCollisionVolumes[i]->mVolume->mShape == VCUBE)
			{
				BoundingVolume* BV = new BoundingVolume(Radius, 0);
				BoundingVolumeNode* BVN = new BoundingVolumeNode(NULL, BV);
				Contact* Ct = NULL;

				//Sphere Sphere test first
				/*if (mCollisions.mDetector.SphereToSphere(BVN, mCollisionVolumes[i]))
				{*/

					//Detect Collision
				if (SphereToOBBDetect(BVN, mCollisionVolumes[i], SPosition))
					{

						cout << "\nHit!\n";

						found = true;

						//Create Contact
						Ct = SphereToOBBGenerate(BVN, mCollisionVolumes[i], SPosition);

						poc = Ct->mPoint;
						Me = *(mCollisionVolumes[i]->mBody->Master());
					}

					//Clear mess
					delete BVN;
					delete Ct;
				//}
			}

			if (found)
				return true;
		}
	}

	return found;
}
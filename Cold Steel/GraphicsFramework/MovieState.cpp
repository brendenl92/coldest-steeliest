#include "MovieState.h"
#include "D3DAPP.h"
#include "DXUtil.h"

MovieState* MovieState::Instance()
{

	static MovieState* ptr = new MovieState();
	return ptr;

}

MovieState::MovieState(void)
{

}


MovieState::~MovieState(void)
{
	if (m_graphBuilder)
	{
		m_graphBuilder->Release();
		delete m_graphBuilder;
	}
	if (m_mediaControl)
	{
		m_mediaControl->Release();
		delete m_mediaControl;
	}
	if (m_mediaEvent)
	{
		m_mediaEvent->Release();
		delete m_mediaEvent;
	}
	if (m_videoWindow)
	{
		m_videoWindow->Release();
		delete m_videoWindow;
	}
}

void MovieState::Initialize()
{
	m_videoWindow = NULL;
	CoInitialize(NULL);
	CoCreateInstance (CLSID_FilterGraph, NULL, CLSCTX_INPROC_SERVER, IID_IGraphBuilder, (void **)&m_graphBuilder);
	m_graphBuilder->QueryInterface(IID_IMediaControl, (void **)&m_mediaControl);
	m_graphBuilder->QueryInterface(IID_IVideoWindow, (void **)&m_videoWindow);
	m_graphBuilder->QueryInterface (IID_IMediaEvent, (void **)&m_mediaEvent);
	m_graphBuilder->RenderFile(L"../media/movies/temp.avi", NULL);
	m_videoWindow->put_Owner((OAHWND)g_D3DAPP->getMainWnd());
	m_videoWindow->put_WindowStyle(WS_CHILD | WS_CLIPCHILDREN);
	RECT rect;
	GetClientRect(g_D3DAPP->getMainWnd(), &rect);
	m_videoWindow->SetWindowPosition(rect.left, rect.top, rect.right, rect.bottom);
	m_videoWindow->put_Visible(OATRUE);
	m_mediaControl->Run();
	
}

void MovieState::OnResetDevice()
{



}

void MovieState::OnLostDevice()
{



}

void MovieState::Leave()
{

	m_mediaControl->Stop();
	m_mediaControl->Release();
	m_graphBuilder->Release();
	m_mediaEvent->Release();
}
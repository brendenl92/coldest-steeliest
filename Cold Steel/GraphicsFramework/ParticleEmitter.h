//=============================================================================
// ParticleEmitter.h by Frank Luna (C) 2004 All Rights Reserved.
//=============================================================================

#ifndef PSYSTEM_H
#define PSYSTEM_H

#include "DXUtil.h"
#include "Vertex.h"
#include <vector>

//===============================================================
class ParticleEmitter
{
public:
	ParticleEmitter(
		const std::string& fxName,
		const std::string& techName,
		const std::string& texName,
		const D3DXVECTOR3& accel,
		const AABB& box,
		int maxNumParticles,
		float timePerParticle);

	virtual ~ParticleEmitter();

	// Access Methods
	float getTime();
	void  setTime(float t);
	const AABB& getAABB()const;
	int getAliveParticleCount();
	//Instancing
	void BufferCode();

	void setWorldMtx(const D3DXMATRIX& world);
	void addParticle();
	//Editor Access Methods
	void SetPosition(D3DXVECTOR3 pos);
	D3DXVECTOR3 GetPosition();
	//Editor Sets
	void SetMinimumMaxSizes(float min, float max);
	void SetMinimumSize(float min);
	void SetMaximumSize(float max);
	void SetDelay(float delay);
	void SetLifeMin(float min);
	void SetLifeMax(float max);
	void SetMaxparticles(int Maxparticles); //No More than 500
	void SetVelXMin(float min);
	void SetVelXMax(float max);
	void SetVelX(D3DXVECTOR3);
	void SetVelYMin(float min);
	void SetVelYMax(float max);
	void SetVelY(D3DXVECTOR3);
	void SetVelZMin(float min);
	void SetVelZMax(float max);
	void SetVelZ(D3DXVECTOR3);
	void SetStartDelay(float time);
	void SetAcceleration(D3DXVECTOR3 acc);
	void Reset();
	void SetEmitterType(int type);
	void setRotationMtx(D3DXMATRIX rot);
	void setParticleID(int id);
	void setOffset(D3DXVECTOR3 offSet);
	void setPaused(bool paused);
	
	//Editor Accessors
	float GetMinimumSize();
	float GetMaximumSize();
	D3DXVECTOR3 GetSize(); //X = Min, Y = Max
	float GetDelay();
	D3DXVECTOR3 GetLife();//X = Min, Y = Max
	int GetMaxParticles();
	D3DXVECTOR3 GetVelocityX();//X = Min, Y = Max
	D3DXVECTOR3 GetVelocityY();//X = Min, Y = Max
	D3DXVECTOR3 GetVelocityZ();//X = Min, Y = Max
	float GetStartDelay();
	bool getRestart();
	int getEmitterType();
	int getParticleID();
	D3DXVECTOR3 getOffset();

	void SetRestart(bool restart);
	int getType();
	void setType(int);

	bool isFinished(); //Check if this Particle Alive, if dead Manager will delete from vector

	virtual void onLostDevice();
	virtual void onResetDevice();

	virtual void initParticle(Particle& out) = 0;
	virtual void update(float dt);
	virtual void draw();

	std::vector<Particle> mParticles;
	std::vector<Particle*> mAliveParticles;
	std::vector<Particle*> mDeadParticles;

	void CircleParticleOut(); //Creates effect of circle from center to out	on X		  //= 1
	void CircleParticleOutZ(); //Creates effect of circle from center to out on Z		  //= 3
	void CircleParticleIn();	//Create effect of circle from biggest form to the center //= 2
	void CircleParticleInCenter(); //Creates the same as In but towards center down		  //= 4
	void FireWorkStyle();	//Initializes a batch to shoot in random directions			  //= 5
protected:
	// In practice, some sort of ID3DXEffect and IDirect3DTexture9 manager should
	// be used so that you do not duplicate effects/textures by having several
	// instances of a particle system.
	ID3DXEffect* mFX;
	D3DXHANDLE mhTech;
	D3DXHANDLE mhWVP;
	D3DXHANDLE mhEyePosL;
	D3DXHANDLE mhTex;
	D3DXHANDLE mhTime;
	D3DXHANDLE mhAccel;
	D3DXHANDLE mhViewportHeight;
	//Static Instancing
	D3DXHANDLE mhInsPos; //Position
	D3DXHANDLE mhInsVel; //Velocity
	D3DXHANDLE mhInsSTL; //Size, Time, LifeTime
	D3DXHANDLE mhPosH; //Size, Time, LifeTime

	IDirect3DTexture9* mTex;
	IDirect3DVertexBuffer9* mVB;
	D3DXMATRIX mWorld;
	D3DXMATRIX mInvWorld;
	float mTime;
	D3DXVECTOR3 mAccel;
	AABB mBox;
	int mMaxNumParticles;
	float mTimePerParticle;

	//New for Manager and Manipulation
	D3DXVECTOR3 mPosition; //Starting Position of Particle (Edit in Creation of Particle or during Update)
	//Size floats for Slider Interactivity - Initial Set is done in Creation
	//After some consideration, I will be having it 100 max and 1 min
	float mMinimumSize;//Will start initially at 1
	float mMaximumSize;//Will start initially at 100
	float mLifeMax;
	float mLifeMin;
	D3DXVECTOR3 mVelocityX;
	D3DXVECTOR3 mVelocityY;
	D3DXVECTOR3 mVelocityZ;
	D3DXVECTOR3 mAcceleration;
	D3DXVECTOR3 mOldAcc;

	int mType; //What type of Particle Emitter is this?
	float timeAccum;
	bool mRestart;
	int mSpawned;
	float mStartDelay;
	
	bool mFinished;	//Has all the particles died out?
	int mEmitType; // 0 = linear, 1 = Circle Out, 2 = Circle In , 3 = Circle Out To Position
	float angle;

	D3DXMATRIX mRotation;
	D3DXMATRIX mPos;
	D3DXMATRIX mPos2;
	D3DXVECTOR3 mOffSet; // Initial offset of emitter (so when updating position, it still stays intact to the position
	bool mPaused; //Do we need to freeze the effects?

	int mID; //ID specific for use of accessors and mutators on MultiEffect particles, set to 3500 (too big)
};

#endif // P_SYSTEM
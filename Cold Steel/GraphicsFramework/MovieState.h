#pragma once
#include <dshow.h>
#include <Windows.h>

using namespace std;

#include "state.h"
class MovieState :
	public State
{

	IGraphBuilder *m_graphBuilder;
	IMediaControl *m_mediaControl;
	IMediaEvent   *m_mediaEvent;
	IVideoWindow  *m_videoWindow;
	MovieState(void);
public:
	static MovieState* Instance();
	~MovieState(void);
	virtual void Initialize();
	virtual void UpdateScene(float dt){}
	virtual void RenderScene(){}
	virtual void OnResetDevice();
	virtual void OnLostDevice();
	virtual void HandleInput(RAWINPUT* raw) {}

	virtual void Leave();
};


#include "MenuState.h"
#include "RawInput.h"
#include "D3DAPP.h"
#include "GameApp.h"
MenuState* MenuState::Instance()
{

	static MenuState* ptr = new MenuState();
	return ptr;

}

MenuState::MenuState(void)
{
	
	g_D3DAPP->setNetworking(false);

}

MenuState::~MenuState(void)
{
	if (back)
		delete back;
}
void MenuState::Initialize()
{
	string names[14];
	names[0] = "../media/buttons/Play.png";
	names[1] = "../media/buttons/Movie.png";
	names[2] = "../media/buttons/NameScroll.png";
	names[3] = "../media/buttons/Options.png";
	names[4] = "../media/buttons/Quit.png";
	names[5] = "../media/buttons/Back.png";
	names[6] = "../media/buttons/Save.png";
	names[7] = "../media/buttons/SlideBack.png";
	names[8] = "../media/buttons/Slider.png";
	names[9] = "../media/buttons/CheckBox.png";
	names[10] = "../media/images/SettingsBack.png";
	names[11] = "../media/images/ColdSteel.png";
	names[12] = "../media/buttons/Multiplayer.png";
	names[13] = "../media/buttons/credits.png";
	RM->ImportTextures(names, 14);

	RM->createFont("Arial", 40, 18, 300, false);
	RM->createFont("Avenir", 100, 70, 700, false);

	CAM->menuReset();
	CAM->setTarget(D3DXVECTOR3(0, 0, 0));
	back = new TankSelectBack();
	constructMain();
	
	SOUND->startSong("../media/music/Gamepunk.mp3");
	buttons[0].setChosen(true);
	ShowCursor(TRUE);
	selectPause = 0;
	chosen = 0;
	options = false;
}
void MenuState::HandleInput(RAWINPUT* raw)
{

	if (raw->data.keyboard.VKey == 0x42)
		if (raw->data.keyboard.Flags == 0)
			back->nextTank();

	if(raw->data.keyboard.VKey == 0x56)
		if (raw->data.keyboard.Flags == 0)
			back->prevTank();


	if (raw->data.mouse.ulButtons & RI_MOUSE_LEFT_BUTTON_UP)
		for (UINT i = 0; i < buttons.size(); i++)
		{
			if (RAW->getMousePos().x <= buttons[i].getPos().x + (buttons[i].getHalfExtents().x * 2) && RAW->getMousePos().x >= buttons[i].getPos().x &&
				RAW->getMousePos().y <= buttons[i].getPos().y + (buttons[i].getHalfExtents().y * 2) && RAW->getMousePos().y >= buttons[i].getPos().y)
			{

				if (buttons[i].getName() == "MaintoGame")
				{

					SOUND->playSoundEffect("../media/soundeffect/Button_Click.wav");
					g_D3DAPP->changeState("TANKSELECTION");

				}

				else if (buttons[i].getName() == "MaintoMulti")
				{

					SOUND->playSoundEffect("../media/soundeffect/Button_Click.wav");
					g_D3DAPP->changeState("CREDITS");

				}

				else if (buttons[i].getName() == "MaintoNameScroll")
				{

					SOUND->playSoundEffect("../media/soundeffect/Button_Click.wav");
					g_D3DAPP->changeState("TEXT");

				}

				else if (buttons[i].getName() == "MaintoMovie")
				{

					SOUND->playSoundEffect("../media/soundeffect/Button_Click.wav");
					g_D3DAPP->changeState("MOVIE");

				}

				else if (buttons[i].getName() == "MaintoSettings")
				{

					SOUND->playSoundEffect("../media/soundeffect/Button_Click.wav");
					destructMain();
					constructSettings();
				}

				else if (buttons[i].getName() == "SettingstoMain")
				{

					SOUND->playSoundEffect("../media/soundeffect/Button_Click.wav");
					destructSettings();
					constructMain();
				}

				else if (buttons[i].getName() == "SettingstoSave")
				{
					SOUND->playSoundEffect("../media/soundeffect/Button_Click.wav");
					SET->setSettings(cboxes[0].getChecked(), cboxes[1].getChecked(), cboxes[2].getChecked(), sliders[0].getPosition(), sliders[1].getPosition(), sliders[2].getPosition() * .5f);
					CAM->setSensitivity(sliders[2].getPosition());
					Sleep(200);
					destructSettings();
					constructMain();
				}

				else if (buttons[i].getName() == "MaintoQuit")
				{
					SOUND->playSoundEffect("../media/soundeffect/Button_Click.wav");
					DestroyWindow(g_D3DAPP->getMainWnd());
					exit(0);
				}
			}
		}

}
void MenuState::UpdateScene(float dt)
{	
	XBOX->update();
	if(SET->getController() == true)
	{
		if (XBOX->GetState().Gamepad.sThumbLY > 10000 || XBOX->GetState().Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_UP || XBOX->GetState().Gamepad.sThumbLY < -10000 || XBOX->GetState().Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_DOWN)
		{
			if (CLK->getTime() > selectPause)
			{
				if (options == false)
				{
					switch (chosen)
					{
					case 0:
						chosen = 1;
						buttons[0].setChosen(false);
						buttons[1].setChosen(true);
						break;
					case 1:
						chosen = 0;
						buttons[0].setChosen(true);
						buttons[1].setChosen(false);
						break;
					case 2:
						chosen = 3;
						buttons[2].setChosen(false);
						buttons[3].setChosen(true);
						break;
					case 3:
						chosen = 2;
						buttons[2].setChosen(true);
						buttons[3].setChosen(false);
						break;
					};
				}
				selectPause = CLK->getTime() + (float).3;
				XBOX->Vibrate(65535, 65535, (float).15);
			}
		}
		else if (XBOX->GetState().Gamepad.sThumbLX > 10000 || XBOX->GetState().Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_RIGHT || XBOX->GetState().Gamepad.sThumbLX < -10000 || XBOX->GetState().Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_LEFT)
		{
			if (CLK->getTime() > selectPause)
			{
				if (options == false)
				{
					switch (chosen)
					{
					case 0:
						chosen = 2;
						buttons[0].setChosen(false);
						buttons[2].setChosen(true);
						break;
					case 1:
						chosen = 3;
						buttons[3].setChosen(true);
						buttons[1].setChosen(false);
						break;
					case 2:
						chosen = 0;
						buttons[2].setChosen(false);
						buttons[0].setChosen(true);
						break;
					case 3:
						chosen = 1;
						buttons[1].setChosen(true);
						buttons[3].setChosen(false);
						break;
					};
				}
				else if (options == true)
				{
					switch (chosen)
					{
					case 0:
						chosen = 1;
						buttons[0].setChosen(false);
						buttons[1].setChosen(true);
						break;
					case 1:
						chosen = 0;
						buttons[0].setChosen(true);
						buttons[1].setChosen(false);
						break;
					};
				}
				selectPause = CLK->getTime() + (float).3;
				XBOX->Vibrate(65535, 65535, (float).15);
			}
		}
		if (XBOX->GetState().Gamepad.wButtons & XINPUT_GAMEPAD_A)
		{
			POINT* pt = new POINT();
			pt->x = (LONG)(buttons[chosen].getPos().x + (buttons[chosen].getHalfExtents().x / 2));
			pt->y = (LONG)(buttons[chosen].getPos().y + (buttons[chosen].getHalfExtents().y / 2));
			RAW->setMousePos(pt);
			RAWINPUT* raw = new RAWINPUT();
			raw->data.mouse.ulButtons = RI_MOUSE_LEFT_BUTTON_UP;
			HandleInput(raw);
			delete pt;
			delete raw;
		}
	}
	back->update(dt);

	for(UINT i = 0; i < buttons.size(); i++)
		buttons[i].update(dt);

	for(UINT i = 0; i < sliders.size(); i++)
		sliders[i].update(dt);

	for(UINT i = 0; i < cboxes.size(); i++)
		cboxes[i].update(dt);

	SOUND->update();

}

void MenuState::RenderScene()
{
	HR(g_D3dDevice->Clear(0, 0, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, D3DCOLOR_XRGB(84, 84, 84), 1.0F, 0));
	HR(g_D3dDevice->BeginScene());
	back->render();
	D3DXMATRIX temp;
	RM->getSprite()->Begin(D3DXSPRITE_ALPHABLEND);
	
	for(UINT i = 0; i < images.size(); i++)
	{
		D3DXMatrixTransformation2D(&temp, NULL, 0.0, &images[i].getScale(), NULL, NULL, &images[i].getPos());
		RM->getSprite()->SetTransform(&temp);
		RM->getSprite()->Draw(*RM->getTexture(images[i].getTextID()), NULL, NULL, NULL, 0xFFFFFFFF);
		RM->getSprite()->Flush();
	}

	for(UINT i = 0; i < buttons.size(); i ++)
	{
		
		D3DXMatrixTransformation2D(&temp, NULL, 0.0, &(buttons[i].getScale()), &(buttons[i].getPos() + buttons[i].getHalfExtents()), 0, &buttons[i].getPos());
		RM->getSprite()->SetTransform(&temp);
		RM->getSprite()->Draw(*RM->getTexture(buttons[i].getID()), &buttons[i].getRect(), NULL, NULL, 0xFFFFFFFF);
		RM->getSprite()->Flush();


	}
	for(UINT i = 0; i < sliders.size(); i++)
	{
		
		D3DXMatrixTransformation2D(&temp, NULL, (float)0.0, &D3DXVECTOR2((float)1, (float)1), &(sliders[i].getPos() + D3DXVECTOR2((float)(RM->getTextureX(sliders[i].getID()) / 2), (float)(RM->getTextureY(sliders[i].getID()) / 2))), (float)0, &sliders[i].getPos());
		RM->getSprite()->SetTransform(&temp);
		RM->getSprite()->Draw(*RM->getTexture(sliders[i].getID()), NULL, NULL, NULL, 0xFFFFFFFF);
		RM->getSprite()->Flush();

		D3DXMatrixTransformation2D(&temp, NULL, 0.0, &D3DXVECTOR2(1, 1), &(sliders[i].getButton().getPos() + sliders[i].getButton().getHalfExtents()), 0, &sliders[i].getButton().getPos());
		RM->getSprite()->SetTransform(&temp);
		RM->getSprite()->Draw(*RM->getTexture(sliders[i].getButton().getID()), &sliders[i].getButton().getRect(), NULL, NULL, 0xFFFFFFFF);
		RM->getSprite()->Flush();
	}
	for(UINT i = 0; i < cboxes.size(); i++)
	{
		D3DXMatrixTransformation2D(&temp, NULL, (float)0.0, &D3DXVECTOR2((float).6, (float).6), &(cboxes[i].getPos() + cboxes[i].getHalfExtents()), 0, &cboxes[i].getPos());
		RM->getSprite()->SetTransform(&temp);
		RM->getSprite()->Draw(*RM->getTexture(cboxes[i].getID()), &cboxes[i].getRect(), NULL, NULL, 0xFFFFFFFF);
		RM->getSprite()->Flush();
	}
	
	RM->getSprite()->End();

	for(UINT i = 0; i < texts.size(); i++)
	{
		RM->getFont(texts[i].getFont())->DrawTextA(0, texts[i].getText().c_str(), -1, &texts[i].getRect(), DT_CENTER | DT_VCENTER, D3DCOLOR_XRGB(255, 255, 255));
	}
	
	g_D3dDevice->EndScene();
	g_D3dDevice->Present(0, 0, 0, 0);
	
}

void MenuState::OnResetDevice()
{

	RM->onResetDevice();
	back->onResetDevice();
}

void MenuState::OnLostDevice()
{
	
	RM->onLostDevice();
	back->onLostDevice();
	if (g_D3DAPP->getOldHeight() != 0 || g_D3DAPP->getOldWidth() != 0)
	{
		float x = (float)g_D3DAPP->getWidth() / (float)g_D3DAPP->getOldWidth();
		float y = (float)g_D3DAPP->getHeight() / (float)g_D3DAPP->getOldHeight();

		for (UINT i = 0; i < buttons.size(); i++)
		{
			buttons[i].setPos(D3DXVECTOR2(buttons[i].getPos().x * x, buttons[i].getPos().y * y));
			buttons[i].setScale(D3DXVECTOR2(buttons[i].getScale().x * x, buttons[i].getScale().y * y));
		}
	}

}

void MenuState::Leave()
{
	delete back;
	back = NULL;
	buttons.clear();
	sliders.clear();
	cboxes.clear();
	images.clear();
	texts.clear();
	RM->clearAll();


}

void MenuState::constructMain()
{
	ShowCursor(TRUE);
	options = false;
	chosen = 0;
	string names[4];
	names[0] = "MaintoGame";
	names[1] = "MaintoMulti";
	names[2] = "MaintoSettings";
	names[3] = "MaintoQuit";
	buttons.push_back(Button(0, D3DXVECTOR2(30, (float)g_D3DAPP->getHeight() - 300), names[0]));
	buttons.push_back(Button(13, D3DXVECTOR2(30, (float)g_D3DAPP->getHeight() - 150), names[1]));
	buttons.push_back(Button(3, D3DXVECTOR2((float)(g_D3DAPP->getWidth() - 542), (float)g_D3DAPP->getHeight() - 300), names[2]));
	buttons.push_back(Button(4, D3DXVECTOR2((float)(g_D3DAPP->getWidth() - 542), (float)g_D3DAPP->getHeight() - 150), names[3]));
	images.push_back(Image(D3DXVECTOR2((float)((g_D3DAPP->getWidth() / 2) - ((RM->getTextureX(11) / 2) + 70)), (float)30), D3DXVECTOR2((float)1, (float)1), 11));
	OnLostDevice();
}
void MenuState::destructMain()
{
	buttons.clear();
	images.clear();
}
void MenuState::constructSettings()
{
	ShowCursor(TRUE);
	options = true;
	chosen = 0; 
	sliders.push_back(Slider(Button(8, D3DXVECTOR2((float)(g_D3DAPP->getWidth() / 3 - (RM->getTextureX(7) + 20)), 600), "Slider"), 7, 
		D3DXVECTOR2((float)(g_D3DAPP->getWidth() / 4) + 230, 600)));
	sliders[0].setSlidePosition(SET->getFXVolume());

	sliders.push_back(Slider(Button(8, D3DXVECTOR2((float)(g_D3DAPP->getWidth() / 3 - (RM->getTextureX(7) + 20)), 800), "Slider"), 7, 
		D3DXVECTOR2((float)(g_D3DAPP->getWidth() / 4) + 230, 800)));
	sliders[1].setSlidePosition(SET->getMusicVolume());

	sliders.push_back(Slider(Button(8, D3DXVECTOR2((float)(g_D3DAPP->getWidth() / 3 - (RM->getTextureX(7) + 20)), 1100), "Slider"), 7, 
		D3DXVECTOR2((float)(g_D3DAPP->getWidth() / 4) + 230, 1000)));
	sliders[2].setSlidePosition(SET->getSensitivity() / 20);

	buttons.push_back(Button(5, D3DXVECTOR2((float)((g_D3DAPP->getWidth() / 2) - (RM->getTextureX(5) - 20)), (float)1200), "SettingstoMain"));
	buttons.push_back(Button(6, D3DXVECTOR2((float)((g_D3DAPP->getWidth() / 2) + 20), (float)1200), "SettingstoSave"));
	cboxes.push_back(CheckBox(9, D3DXVECTOR2((float)(g_D3DAPP->getWidth() / 2 + 300), (float)600)));
	cboxes.push_back(CheckBox(9, D3DXVECTOR2((float)(g_D3DAPP->getWidth() / 2 + 300), (float)800)));
	cboxes.push_back(CheckBox(9, D3DXVECTOR2((float)(g_D3DAPP->getWidth() / 2 + 300), (float)1000)));
	cboxes[0].setChecked(SET->getController());
	cboxes[1].setChecked(SET->getInverted());
	cboxes[2].setChecked(SET->getThird());
	texts.push_back(Text(0, D3DXVECTOR2((float)(g_D3DAPP->getWidth() / 2 + 200), (float)545), "Controller"));
	texts.push_back(Text(0, D3DXVECTOR2((float)(g_D3DAPP->getWidth() / 2 + 230), (float)745), "Inverted"));
	texts.push_back(Text(0, D3DXVECTOR2((float)(g_D3DAPP->getWidth() / 3) + 140, 545), "FX Volume"));
	texts.push_back(Text(0, D3DXVECTOR2((float)(g_D3DAPP->getWidth() / 3) + 120, 745), "Music Volume"));
	texts.push_back(Text(0, D3DXVECTOR2((float)(g_D3DAPP->getWidth() / 3) + 130, 945), "Sensitivity"));
	texts.push_back(Text(0, D3DXVECTOR2((float)(g_D3DAPP->getWidth() / 2 + 180), (float)945), "Third Person"));
	texts.push_back(Text(1, D3DXVECTOR2((float)(g_D3DAPP->getWidth() / 2 - 300), (float)200), "Settings"));
	//images.push_back(Image(D3DXVECTOR2((float)50, (float)50), D3DXVECTOR2((float)1.35, (float).8), 10));
	OnLostDevice();
}
void MenuState::destructSettings()
{
	buttons.clear();
	sliders.clear();
	cboxes.clear();
	texts.clear();
	images.clear();
}
#include "XInput.h"

XBOXController::XBOXController()
{
	controllerNum = 0;
	ZeroMemory(&controllerState, sizeof(XINPUT_STATE));
}

XBOXController* XBOXController::Instance()
{

	static XBOXController* ptr = new XBOXController();
	return ptr;

}

XINPUT_STATE XBOXController::GetState()
{

	return controllerState;

}

bool XBOXController::IsConnected()
{
	// Zeroise the state
	ZeroMemory(&controllerState, sizeof(XINPUT_STATE));

	// Get the state
	DWORD Result = XInputGetState(controllerNum, &controllerState);

	if (Result == ERROR_SUCCESS)
	{
		return true;
	}
	else
	{
		return false;
	}
}
 
void XBOXController::update()
{

	ZeroMemory(&controllerState, sizeof(XINPUT_STATE));

	XInputGetState(controllerNum, &controllerState);

	if (CLK->getTime() >= vibEndTime)
	{
		ZeroMemory(&Vibration, sizeof(XINPUT_VIBRATION));
		XInputSetState(controllerNum, &Vibration);
	}
	
}

void XBOXController::Vibrate(int leftVal, int rightVal, float durration)
{

	ZeroMemory(&Vibration, sizeof(XINPUT_VIBRATION));

	// Set the Vibration Values
	Vibration.wLeftMotorSpeed = leftVal;
	Vibration.wRightMotorSpeed = rightVal;

	// Vibrate the controller
	XInputSetState(controllerNum, &Vibration);

	vibEndTime = CLK->getTime() + durration;
}
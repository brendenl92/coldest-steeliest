#include "GameApp.h"
#include <iostream>
#include <time.h>
#include "RawInput.h"
//#include "vld.h"
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE prevInstance, PSTR cmdLine, int showCmd)
{

	GameApp app(hInstance, "Cold Steel", D3DDEVTYPE_HAL, D3DCREATE_HARDWARE_VERTEXPROCESSING);
	g_D3DAPP = &app;
	RAW = new RawInput();
	app.initializeState();

	srand((unsigned int)time(NULL));

	SET->instance();
	return g_D3DAPP->run();
	

}
GameApp::~GameApp()
{
	delete Machine;
}
void GameApp::initializeState()
{

	Machine->setCurrentState(GameState::Instance());

}

void GameApp::HandleInput(RAWINPUT* raw)
{
	Machine->handleInput(raw);
}

void GameApp::changeState(string name)
{

	if(name == "MENU")
		Machine->changeState(MenuState::Instance());
	else if (name == "GAME")
		Machine->changeState(GameState::Instance());
	else if (name == "CREDITS")
		Machine->changeState(CreditsState::Instance());
	else if (name == "TANKSELECTION")
		Machine->changeState(TankSelectionState::Instance());

}
void main()
{

	HWND a = GetConsoleWindow();
	MoveWindow(a, 1, 1, 400, 300, 1);
	WinMain(GetModuleHandle(NULL), NULL, "", 0);

}

GameApp::GameApp(HINSTANCE instance, string caption, D3DDEVTYPE type, DWORD requestedVP):D3DAPP(instance, caption, type, requestedVP)
{

	Machine = new FSM();
	
}

void GameApp::onLostDevice()
{

	Machine->getState()->OnLostDevice();

}

void GameApp::onResetDevice()
{

	Machine->getState()->OnResetDevice();

}

void GameApp::updateScene(float dt)
{

	Machine->update(dt);

}

int GameApp::run()
{

	MSG message;
	message.message = WM_NULL;

	CLK->start();

	while(message.message != WM_QUIT)
	{
		if(PeekMessage(&message, m_MainWndInst, NULL, NULL, PM_REMOVE))
		{
			TranslateMessage(&message);
			DispatchMessage(&message);
		}
		else
		{
			if(m_AppPaused)
			{
				Sleep(20);
				continue;
			}

			if(!isDeviceLost())
			{
				float dt = CLK->end();
				updateScene(dt);
				drawScene();
				CLK->finish();
			}
		}
	}

	return 0;

}

void GameApp::drawScene()
{

	Machine->getState()->RenderScene();

}

#pragma once

#include "FuzzySet.h"
#include <vector>

using namespace std;

///My initial plan was to set this up, utilizing the public access
///of FuzzySet, FuzzyTerm : public FuzzySet and from there I would 
///make copies of each of the left shoulder, mid, and right shoulder
///but using the diagram in the book found on pg. 447 I realized that
///FuzzyTerm was a base class that was mimicing the options of FuzzySet 
///while allowing for conjecture from the operands such as the 
///OR, AND rules.
class FuzzyTerm
{
public:
	virtual ~FuzzyTerm() {}

	virtual void		ClearDOM() const = 0;
	virtual void		ORwithDOM(double val) = 0;
	virtual double		GetDOM() const = 0;
	virtual FuzzyTerm*	Clone() const = 0;
};

///handles exceptions between fuzzy terms, and thier opposing sets
class FzSet : public FuzzyTerm
{
private:
	FuzzySet& _fcSet;

public:
	///The FzSet is a base class mimicing the Fuzzy Set class but
	///allows interaction between the Fuzz "sets" and the "operands"
	///pg. 447

	FzSet(FuzzySet& f) : _fcSet(f) 
	{
	}

	~FzSet() {}

	FzSet* Clone() const
	{
		return new FzSet(*this);
	}

	///pg. 447 shows that in this function calls the set, stored within
	///this class, getDom() method.
	void	ClearDOM() const
	{
		_fcSet.ClearDOM();
	}

	void	ORwithDOM(double val)
	{
		_fcSet.ORwithDOM(val);
	}

	double	GetDOM() const
	{
		return _fcSet.GetDOM();
	}

	double GetRep() const
	{
		return _fcSet.GetRep();
	}
};

///takes in two Fuzzy Terms
class FzAND : public FuzzyTerm
{
private:
	vector<FuzzyTerm*> m_Pair;
	
public:
	///The FzAND is a base class mimicing the FcSet class but accepts two variables
	///and allows interaction between the Fuzz "sets" and the "operands", returning lowest DOM
	///pg. 447

	///He, the author, describes how to go about setting up these methods on pg. 421
	FzAND(FuzzyTerm& f1, FuzzyTerm& f2)
	{
		m_Pair.push_back(f1.Clone());
		m_Pair.push_back(f2.Clone());
	}

	~FzAND() {}

	FzAND* Clone() const
	{
		return new FzAND(*this);
	}

	///pg. 447 shows that in this function calls the set, stored within
	///this class, getDom() method.
	void	ClearDOM() const
	{
		///clear both sets
		for(auto i = m_Pair.begin(); i != m_Pair.end(); i++)
		{
			(*i)->ClearDOM();
		}
	}

	void	ORwithDOM(double val)					
	{
		///check both sets to see if val passed in is greater than 
		///thier individual DOMs
		for(auto i = m_Pair.begin(); i != m_Pair.end(); i++)
		{
			(*i)->ORwithDOM(val);
		}
	}

	double	GetDOM() const							
	{
		double lowestDOM = 999999;
		double temp;
		///cycle through and find the set with the lowest DOM
		for(auto i = m_Pair.begin(); i != m_Pair.end(); i++)
		{
			if((*i)->GetDOM() < lowestDOM)
			{
				temp = (*i)->GetDOM();
				lowestDOM = (*i)->GetDOM();
			}
		}

		///return smallest DOM between sets
		return lowestDOM;
	}
};
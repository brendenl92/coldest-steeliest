#include "Renderer.h"
#include "D3DAPP.h"
#include "HUD.h"

Renderer::Renderer()
{
	if (g_D3dDevice->GetRenderTarget(0, &backbuffer) != D3D_OK)
		int broken = 0;
	float width = (float)g_D3DAPP->getWidth();
	float height = (float)g_D3DAPP->getHeight();
	planeVerts[0] = { 0.0f, 0.0f, .5f, 1, 0 + .5f / width, 0.0f + .5f / height };
	planeVerts[1] = { width, 0.0f, .5f, 1.0f, 1.0f + .5f / width, 0.0f + .5f / height };
	planeVerts[2] = { width, height, .5f, 1.0f, 1.0f + .5f / width, 1.0f + .25f / height };
	planeVerts[3] = { 0.0f, height, .5f, 1.0f, 0 + .5f / width, 1.0f + .5f / height };

	ID3DXBuffer* errors = 0;

	D3DXCreateEffectFromFileA(g_D3dDevice, "../shaders/inverseTexture.fx", 0, 0, D3DXFX_NOT_CLONEABLE | D3DXSHADER_SKIPOPTIMIZATION | D3DXSHADER_DEBUG, 0, &fx, &errors);
	if (errors)
		cout << (char*)errors->GetBufferPointer();

	if (errors)
		errors->Release();
	//delete errors;
	fx->SetTechnique("main");

	onResetDevice(true);

}


Renderer::~Renderer()
{
	onLostDevice(true);
}

void Renderer::ScreenCap()
{
	SYSTEMTIME time;
	GetLocalTime(&time);
	char name[100];
	sprintf_s(name, "../Screenshots/%i-%i-%i_%i-%i-%i-%i.bmp", time.wMonth, time.wDay, time.wYear, time.wHour, time.wMinute, time.wSecond, time.wMilliseconds);
	D3DXSaveSurfaceToFile(name, D3DXIFF_BMP, RTS, NULL, NULL);
}

void Renderer::texturePass(vector<MasterEntity*> entities)
{

	//g_D3dDevice->ColorFill(RTS, NULL, D3DXCOLOR(0, 0, 1, 1));
	//g_D3dDevice->SetRenderTarget(0, RTS);

	static UINT numPasses = 0;

	for (UINT i = 0; i < entities.size(); i++)
	{
		fx->SetMatrix("WVP", &(entities[i]->getTrans(0) * CAM->getViewProjMat()));
		fx->SetMatrix("W", &entities[i]->getTrans(0));
		fx->SetValue("camPos", &CAM->getPos(), sizeof(D3DXVECTOR3));

		HR(fx->Begin(&numPasses, D3DXFX_DONOTSAVESTATE));

		for (int j = 0; j < RM->getMesh(entities[i]->getMeshID())->getNumMaterials(); j++)
		{

			fx->SetTexture("tex", RM->getMesh(entities[i]->getMeshID())->getTexture(j));
			HR(fx->BeginPass(0));
			//fx->
			HR(RM->getMesh(entities[i]->getMeshID())->getMesh()->DrawSubset(j));
			HR(fx->EndPass());

		}
		HR(fx->End());
	}
}

void Renderer::finalPass(MasterEntity* tv)
{
	g_D3dDevice->SetRenderTarget(0, backbuffer);


}

void Renderer::BeginRendering()
{

	HR(g_D3dDevice->Clear(0, NULL, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER | D3DCLEAR_STENCIL, 0x50000, 1.0f, 0));
	HR(g_D3dDevice->BeginScene());
}

void Renderer::EndRendering()
{
	HR(g_D3dDevice->EndScene());
	HR(g_D3dDevice->Present(0, 0, 0, 0));
}


void Renderer::Render(vector<MasterEntity*> entities, MasterEntity* tv)
{

	texturePass(entities);
	//finalPass(tv);


}

void Renderer::onLostDevice(bool destruct)
{
	RT->Release();
	RT = NULL;

	backbuffer->Release();
	backbuffer = NULL;

	RTS->Release();
	RTS = NULL;

	fx->OnLostDevice();
	FFX->OnLostDevice();

	RM->onLostDevice();

	if (destruct)
	{
		fx->Release();
		FFX->Release();
	}


}

void Renderer::onResetDevice(bool first)
{
	cout << "width: " << g_D3DAPP->getWidth() << " Height: " << g_D3DAPP->getHeight() << endl;

	HRESULT temp = g_D3dDevice->CreateTexture(g_D3DAPP->getWidth(), g_D3DAPP->getHeight(), 1, D3DUSAGE_RENDERTARGET, D3DFMT_A8R8G8B8, D3DPOOL_DEFAULT, &RT, NULL);

	RT->GetSurfaceLevel(0, &RTS);
	g_D3dDevice->GetBackBuffer(0, 0, D3DBACKBUFFER_TYPE_MONO, &backbuffer);

	//SHFX = RM->getEffect(SHADOWFX);
	//SHFX->SetTechnique("main");
	if (!first)
	{
		RM->onResetDevice();
		fx->OnResetDevice();
		FFX->OnResetDevice();
	}
}

void Renderer::setSky(int index)
{
}



//#include "Renderer.h"
//#include "D3DAPP.h"
//#include "HUD.h"
//
//Renderer::Renderer()
//{
//	createProjectionMatrix();
//
//	g_D3dDevice->GetRenderTarget(0, &backbuffer);
//	float width = (float)g_D3DAPP->getWidth();
//	float height = (float)g_D3DAPP->getHeight();
//	planeVerts[0] = { 0.0f, 0.0f, .5f, 1, 0 + .5f / width, 0.0f + .5f / height };
//	planeVerts[1] = { width, 0.0f, .5f, 1.0f, 1.0f + .5f / width, 0.0f + .5f / height };
//	planeVerts[2] = { width, height, .5f, 1.0f, 1.0f + .5f / width, 1.0f + .25f / height };
//	planeVerts[3] = { 0.0f, height, .5f, 1.0f, 0 + .5f / width, 1.0f + .5f / height };
//
//	firstRenderDone = false;
//
//	SMSize = 512;
//
//	LightDiffuse = D3DXVECTOR4(1, 1, 1, 0);
//	LightSpecular = D3DXVECTOR4(1, 0, 1, 1);
//	LightPosition = D3DXVECTOR4(0, 300, 200, 0);
//	LightDirection = D3DXVECTOR4(0, -.8f, -.5f, 0);
//	
//	ID3DXBuffer* errors = 0;
//
//	D3DXCreateEffectFromFileA(g_D3dDevice, "../shaders/DeferredBuildPass.fx", 0, 0, D3DXFX_NOT_CLONEABLE | D3DXSHADER_SKIPOPTIMIZATION | D3DXSHADER_DEBUG, 0, &BFX, &errors);
//
//	D3DXCreateEffectFromFileA(g_D3dDevice, "../shaders/DeferredLightPass.fx", 0, 0, D3DXFX_NOT_CLONEABLE | D3DXSHADER_SKIPOPTIMIZATION | D3DXSHADER_DEBUG, 0, &LFX, &errors);
//	if (errors)
//		cout << (char*)errors->GetBufferPointer();
//
//	D3DXCreateEffectFromFileA(g_D3dDevice, "../shaders/DeferredSkyPass.fx", 0, 0, D3DXFX_NOT_CLONEABLE | D3DXSHADER_SKIPOPTIMIZATION | D3DXSHADER_DEBUG, 0, &SFX, &errors);
//	if (errors)
//		cout << (char*)errors->GetBufferPointer();
//
//	D3DXCreateEffectFromFileA(g_D3dDevice, "../shaders/DeferredFinalPass.fx", 0, 0, D3DXFX_NOT_CLONEABLE | D3DXSHADER_SKIPOPTIMIZATION | D3DXSHADER_DEBUG, 0, &FFX, &errors);
//	if (errors)
//		cout << (char*)errors->GetBufferPointer();
//
//	D3DXCreateEffectFromFileA(g_D3dDevice, "../shaders/DeferredLaserPass.fx", 0, 0, D3DXFX_NOT_CLONEABLE | D3DXSHADER_SKIPOPTIMIZATION | D3DXSHADER_DEBUG, 0, &LAFX, &errors);
//	if (errors)
//		cout << (char*)errors->GetBufferPointer();
//
//	D3DXCreateEffectFromFileA(g_D3dDevice, "../shaders/inverseTexture.fx", 0, 0, D3DXFX_NOT_CLONEABLE | D3DXSHADER_SKIPOPTIMIZATION | D3DXSHADER_DEBUG, 0, &EFX, &errors);
//	if (errors)
//		cout << (char*)errors->GetBufferPointer();
//
//
//	if (errors)
//		errors->Release();
//	delete errors;
//	BFX->SetTechnique("main");
//	LFX->SetTechnique("main");
//	SFX->SetTechnique("main");
//	FFX->SetTechnique("main");
//	LAFX->SetTechnique("main");
//	EFX->SetTechnique("main");
//
//	onResetDevice(true);
//
//	D3DXVECTOR3 forward, up, right;
//	up = D3DXVECTOR3(0, 1, 0);
//	forward = forward - D3DXVECTOR3(0, 30, 20);
//	D3DXVec3Normalize(&forward, &forward);
//
//	D3DXVec3Cross(&right, &up, &forward);
//	D3DXVec3Cross(&up, &forward, &right);
//
//	LV.m[0][0] = -1;
//	LV.m[0][1] = 0;
//	LV.m[0][2] = 0;
//	LV.m[0][3] = 0;
//
//	LV.m[0][0] = 0;
//	LV.m[1][1] = .5f;
//	LV.m[2][2] = .85f;
//	LV.m[3][3] = 0;
//
//	LV.m[0][0] = 0;
//	LV.m[1][1] = -.85f;
//	LV.m[2][2] = -.5f;
//	LV.m[3][3] = 0;
//
//	LV.m[0][0] = 1.72f;
//	LV.m[1][1] = 14.24f;
//	LV.m[2][2] = 361.79f;
//	LV.m[3][3] = 1;
//
//	D3DXMatrixPerspectiveFovLH(&LP, D3DXToRadian(30.0f), 1.0f, 1.0f, 1024.0f);
//	LPV = LV * LP;
//	box = new Skybox();
//
//	renderHud = true;
//}
//
//
//Renderer::~Renderer()
//{
//	onLostDevice(true);
//	delete box;
//}
//
//void Renderer::ScreenCap()
//{
//	SYSTEMTIME time ;
//	GetLocalTime(&time);
//	char name[100];	
//	sprintf_s(name, "../Screenshots/%i-%i-%i_%i-%i-%i-%i.bmp", time.wMonth, time.wDay, time.wYear, time.wHour, time.wMinute, time.wSecond, time.wMilliseconds);
//	D3DXSaveSurfaceToFile(name, D3DXIFF_BMP, LightSur, NULL, NULL);
//}
//
//void Renderer::buildPass(vector<MasterEntity*> entities)
//{
//
//	g_D3dDevice->ColorFill(PosSur, NULL, D3DXCOLOR(0, 0, 1, 1));
//	g_D3dDevice->ColorFill(ShadowSur, NULL, D3DXCOLOR(0, 0, 1, 1));
//	g_D3dDevice->ColorFill(NormSur, NULL, D3DXCOLOR(0, 1, 0, 1));
//	g_D3dDevice->ColorFill(DiffSur, NULL, D3DXCOLOR(0, .75f, 1, 1));
//	g_D3dDevice->ColorFill(LightSur, NULL, D3DXCOLOR(0, 0, 0, 1));
//
//	g_D3dDevice->SetRenderTarget(0, DiffSur);
//	g_D3dDevice->SetRenderTarget(1, NormSur);
//	g_D3dDevice->SetRenderTarget(2, PosSur);
//	g_D3dDevice->SetRenderTarget(3, ShadowSur);
//
//	static UINT numPasses = 0;
//	D3DXMATRIX invview = CAM->getViewMat();
//	BFX->SetMatrix("View", &invview);
//	D3DXMatrixInverse(&invview, 0, &invview);
//
//	BFX->SetMatrix("Proj", &m_ProjMat);
//	BFX->SetMatrix("VP", &CAM->getViewProjMat());
//
//
//	D3DXMATRIX inv;
//
//	for (UINT i = 0; i < entities.size(); i++)
//	{
//		if (entities[i]->getMeshID() <= TANK5)
//		{
//			for (int r = 0; r < 2; r++)
//			{
//				BFX->SetMatrix("World", &entities[i]->getTrans(r)); 
//				BFX->SetMatrix("lightWVP", &(entities[i]->getTrans(r) * LPV));
//				D3DXMatrixInverse(&inv, 0, &entities[i]->getTrans(r));
//				HR(LFX->SetVector("MatAmbient", (D3DXVECTOR4*)&(RM->getMesh(entities[i]->getMeshID() + r)->getMaterial()->Ambient)));
//				LFX->SetVector("MatDiffuse", (D3DXVECTOR4*)&(RM->getMesh(entities[i]->getMeshID() + r)->getMaterial()->Diffuse));
//				LFX->SetFloat("c_fSpecularPower", RM->getMesh(entities[i]->getMeshID() + r)->getMaterial()->Power);
//				LFX->SetValue("EyeVecW", CAM->getPos(), sizeof(D3DXVECTOR3));
//				BFX->SetMatrix("WorldInvTrans", &inv);
//
//				HR(BFX->Begin(&numPasses, D3DXFX_DONOTSAVESTATE));
//
//				for (int j = 0; j < RM->getMesh(entities[i]->getMeshID() + r)->getNumMaterials(); j++)
//				{
//
//					BFX->SetTexture("Tex", RM->getMesh(entities[i]->getMeshID() + r)->getTexture(j));
//					HR(BFX->BeginPass(0));
//					HR(RM->getMesh(entities[i]->getMeshID() + r)->getMesh()->DrawSubset(j));
//					HR(BFX->EndPass());
//
//				}
//				HR(BFX->End());
//			}
//		}
//		else
//		{
//			BFX->SetMatrix("World", &entities[i]->getTrans(0));
//			BFX->SetMatrix("lightWVP", &(entities[i]->getTrans(0) * LPV));
//			D3DXMatrixInverse(&inv, 0, &entities[i]->getTrans(0));
//			LFX->SetVector("MatAmbient", (D3DXVECTOR4*)&(RM->getMesh(entities[i]->getMeshID())->getMaterial()->Ambient));
//			LFX->SetVector("MatDiffuse", (D3DXVECTOR4*)&(RM->getMesh(entities[i]->getMeshID())->getMaterial()->Diffuse));
//			LFX->SetFloat("MatPower", RM->getMesh(entities[i]->getMeshID())->getMaterial()->Power);
//			LFX->SetValue("EyeVecW", CAM->getPos(), sizeof(D3DXVECTOR3));
//			BFX->SetMatrix("WorldInvTrans", &inv);
//
//			HR(BFX->Begin(&numPasses, D3DXFX_DONOTSAVESTATE));
//
//			for (int j = 0; j < RM->getMesh(entities[i]->getMeshID())->getNumMaterials(); j++)
//			{
//
//				BFX->SetTexture("Tex", RM->getMesh(entities[i]->getMeshID())->getTexture(j));
//
//				HR(BFX->BeginPass(0));
//				HR(RM->getMesh(entities[i]->getMeshID())->getMesh()->DrawSubset(j));
//				HR(BFX->EndPass());
//
//			}
//			HR(BFX->End());
//		}
//		
//	}
//	for (UINT i = 0; i < BOM->sphereVecSize(); i++)
//	{
//
//		BFX->SetMatrix("World", &BOM->getSpheres(i)->getTrans());
//		BFX->SetMatrix("lightWVP", &(BOM->getSpheres(i)->getTrans() * LPV));
//		D3DXMatrixInverse(&inv, 0, &BOM->getSpheres(i)->getTrans());
//		LFX->SetVector("MatAmbient", (D3DXVECTOR4*)&(RM->getMesh(BOM->getSpheres(i)->getID())->getMaterial()->Ambient));
//		LFX->SetVector("MatDiffuse", (D3DXVECTOR4*)&(RM->getMesh(BOM->getSpheres(i)->getID())->getMaterial()->Diffuse));
//		LFX->SetFloat("c_fSpecularPower", RM->getMesh(BOM->getSpheres(i)->getID())->getMaterial()->Power);
//		LFX->SetValue("EyeVecW", CAM->getPos(), sizeof(D3DXVECTOR3));
//		BFX->SetMatrix("WorldInvTrans", &inv);
//
//		HR(BFX->Begin(&numPasses, D3DXFX_DONOTSAVESTATE));
//
//		for (int j = 0; j < RM->getMesh(BOM->getSpheres(i)->getID())->getNumMaterials(); j++)
//		{
//
//			BFX->SetTexture("Tex", RM->getMesh(BOM->getSpheres(i)->getID())->getTexture(j));
//			HR(BFX->BeginPass(0));
//			HR(RM->getMesh(BOM->getSpheres(i)->getID())->getMesh()->DrawSubset(j));
//			HR(BFX->EndPass());
//
//		}
//		HR(BFX->End());
//	}
//	g_D3dDevice->SetRenderTarget(0, DiffSur);
//
//}
//
//void Renderer::lightPass()
//{
//	g_D3dDevice->SetRenderTarget(0, LightSur);
//
//	if (RAW->keyPressed(E) == true)
//		LFX->SetFloat("FogDensity", .003f);
//	else
//		LFX->SetFloat("FogDensity", 0);
//
//	LFX->SetVector("LightPosition", &LightPosition);
//	LFX->SetVector("LightDirection", &LightDirection);
//	LFX->SetVector("LightDiffuse", &LightDiffuse);
//	LFX->SetVector("LightSpecular", &LightSpecular);
//
//	LFX->SetTexture("DiffuseMap", RTDiffuse);
//	LFX->SetTexture("NormalMap", RTNormal);
//	LFX->SetTexture("PositionMap", RTPos);
//	LFX->SetTexture("DepthMap", RTShadow);
//
//	UINT numPass;
//	LFX->Begin(&numPass, 0);
//	for (UINT i = 0; i < numPass; i++)
//	{
//		LFX->BeginPass(i);
//		g_D3dDevice->SetFVF(D3DFVF_XYZRHW | D3DFVF_TEX1);
//		g_D3dDevice->DrawPrimitiveUP(D3DPT_TRIANGLEFAN, 2, planeVerts, sizeof(planeVertex));
//		LFX->EndPass();
//	}
//	LFX->End();
//
//}
//
//void Renderer::renderFinal(UINT winner)
//{
//	renderWinner = true;
//	this->winner = winner;
//}
//
//void Renderer::shadowPass(vector<MasterEntity*> entities)
//{
//	
//	for (UINT i = 0; i < entities.size(); i++)
//	{
//
//	}
//
//}
//
//void Renderer::laserPass(vector<Laser> lasers)
//{
//	D3DXVECTOR2 texSize((float)(1 / g_D3DAPP->getHeight()), (float)(1 / g_D3DAPP->getWidth()));
//	D3DXMATRIX vp = CAM->getViewProjMat();
//	UINT numPasses;
//	LAFX->SetValue("texSize", texSize, sizeof(D3DXVECTOR2));
//	for (UINT l = 0; l < lasers.size(); l++)
//	{
//		LAFX->SetMatrix("World", &lasers[l].getMat());
//		LAFX->SetMatrix("WVP", &(lasers[l].getMat() * vp));
//		LAFX->Begin(&numPasses, D3DXFX_DONOTSAVESTATE);
//
//		for (int j = 0; j < RM->getMesh(lasers[l].getID())->getNumMaterials(); j++)
//		{
//
//			LAFX->SetTexture("Tex", RM->getMesh(lasers[l].getID())->getTexture(j));
//
//			HR(LAFX->BeginPass(0));
//			HR(RM->getMesh(lasers[l].getID())->getMesh()->DrawSubset(j));
//			HR(LAFX->EndPass());
//
//		}
//		HR(LAFX->End());
//	}
//}
//void Renderer::skyPass()
//{
//	UINT numPasses;
//
//	SFX->SetMatrix("WVP", &(box->getWorld() * CAM->getViewProjMat()));
//	HR(SFX->SetTexture("Tex", box->getTex(skyboxTexture)));
//
//	HR(SFX->Begin(&numPasses, D3DXFX_DONOTSAVESTATE));
//	HR(SFX->BeginPass(0));
//	HR(box->getMesh()->DrawSubset(0));
//	HR(SFX->EndPass());
//
//	HR(SFX->End());
//
//}
//
//void Renderer::finalPass()
//{
//	g_D3dDevice->SetRenderTarget(0, backbuffer);
//	FFX->SetTexture("LightMap", RTLights);
//	if (g_Paused || Hud->isScoreEnabled())
//		FFX->SetFloat("dim", .2f);
//	else
//		FFX->SetFloat("dim", 0);
//
//	UINT numPass;
//	HR(FFX->Begin(&numPass, 0));
//	for (UINT i = 0; i < numPass; i++)
//	{
//		FFX->BeginPass(i);
//		g_D3dDevice->SetFVF(D3DFVF_XYZRHW | D3DFVF_TEX1);
//		g_D3dDevice->DrawPrimitiveUP(D3DPT_TRIANGLEFAN, 2, planeVerts, sizeof(planeVertex));
//		FFX->EndPass();
//	}
//	FFX->End();
//
//	if (firstRenderDone == false)
//		firstRenderDone = true;
//}
//
//void Renderer::BeginRendering()
//{
//
//	HR(g_D3dDevice->Clear(0, NULL, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER | D3DCLEAR_STENCIL, 0x50000, 1.0f, 0));
//	HR(g_D3dDevice->BeginScene());
//}
//
//void Renderer::EndRendering()
//{
//	HR(g_D3dDevice->EndScene());
//	HR(g_D3dDevice->Present(0, 0, 0, 0));
//}
//
//void Renderer::educational(vector<MasterEntity*> entities)
//{
//	UINT numPass;
//	EFX->Begin(&numPass, 0);
//	for (UINT k = 0; k < entities.size(); k++)
//	{
//		EFX->SetMatrix("WVP", &(entities[k]->getTrans(0) * CAM->getViewProjMat()));
//		for (int i = 0; i < RM->getMesh(entities[k]->getMeshID())->getNumMaterials(); i++)
//		{
//			EFX->SetTexture("tex", RM->getMesh(entities[k]->getMeshID())->getTexture(i));
//			for (UINT j = 0; j < numPass; j++)
//			{
//				EFX->BeginPass(j);
//				RM->getMesh(entities[k]->getMeshID())->getMesh()->DrawSubset(i);
//				EFX->EndPass();
//			}
//		}
//	}
//	EFX->End();
//
//}
//
//void Renderer::Render(vector<MasterEntity*> entities, vector<Laser> lasers)
//{
//
//	/*buildPass(entities);
//	laserPass(lasers);
//	ParticleManager->draw();
//	lightPass();
//	skyPass();
//	finalPass();*/
//
//	//if (renderHud)
//	//	Hud->Draw();
//	//renderStart();
//	educational(entities);
//
//
//}
//void Renderer::showHud(bool yes)
//{
//	renderHud = yes;
//}
//void Renderer::renderStart()
//{
//
//	if (renderCountDown)
//	{
//		D3DXMATRIX trans;
//		D3DXMatrixTransformation2D(&trans, NULL, 0, &D3DXVECTOR2(1, 1), NULL, 0, &D3DXVECTOR2(0.0f, -200.0f));
//		RM->getSprite()->Begin(D3DXSPRITE_ALPHABLEND);
//		HR(RM->getSprite()->SetTransform(&trans));
//		if (countDown > 4)
//			RM->getSprite()->Draw(*RM->getTexture(19), NULL, &D3DXVECTOR3(0, 0, 0), &D3DXVECTOR3((float)(g_D3DAPP->getWidth() / 2 - 64), (float)(g_D3DAPP->getHeight() / 2 - 64), 0), D3DXCOLOR(1, 1, 1, 1));
//		else if (countDown > 3)
//			RM->getSprite()->Draw(*RM->getTexture(18), NULL, &D3DXVECTOR3(0, 0, 0), &D3DXVECTOR3((float)(g_D3DAPP->getWidth() / 2 - 64), (float)(g_D3DAPP->getHeight() / 2 - 64), 0), D3DXCOLOR(1, 1, 1, 1));
//		else if (countDown > 2)
//			RM->getSprite()->Draw(*RM->getTexture(17), NULL, &D3DXVECTOR3(0, 0, 0), &D3DXVECTOR3((float)(g_D3DAPP->getWidth() / 2 - 64), (float)(g_D3DAPP->getHeight() / 2 - 64), 0), D3DXCOLOR(1, 1, 1, 1));
//		else if (countDown > 1)
//			RM->getSprite()->Draw(*RM->getTexture(16), NULL, &D3DXVECTOR3(0, 0, 0), &D3DXVECTOR3((float)(g_D3DAPP->getWidth() / 2 - 64), (float)(g_D3DAPP->getHeight() / 2 - 64), 0), D3DXCOLOR(1, 1, 1, 1));
//		else if (countDown > 0)
//			RM->getSprite()->Draw(*RM->getTexture(15), NULL, &D3DXVECTOR3(0, 0, 0), &D3DXVECTOR3((float)(g_D3DAPP->getWidth() / 2 - 64), (float)(g_D3DAPP->getHeight() / 2 - 64), 0), D3DXCOLOR(1, 1, 1, 1));
//		else
//			RM->getSprite()->Draw(*RM->getTexture(20), NULL, &D3DXVECTOR3(0, 0, 0), &D3DXVECTOR3((float)(g_D3DAPP->getWidth() / 2 - 64), (float)(g_D3DAPP->getHeight() / 2 - 64), 0), D3DXCOLOR(1, 1, 1, 1));
//
//		RM->getSprite()->Flush();
//		RM->getSprite()->End();
//	}
//
//}
//
//void Renderer::renderCountDownB(bool in, double H)
//{
//	countDown = H;
//	renderCountDown = in;
//}
//
//void Renderer::createProjectionMatrix()
//{
//	D3DXMatrixPerspectiveFovLH(&m_ProjMat, D3DX_PI * 0.25f, (float)g_D3DAPP->getWidth() / (float)g_D3DAPP->getHeight(), 1, 5000.0f);
//}
//
//bool Renderer::firstRendered()
//{
//	return firstRenderDone;
//}
//
//void Renderer::onLostDevice(bool destruct)
//{
//	RTPos->Release();
//	RTPos = NULL;
//	RTShadow->Release();
//	RTShadow = NULL;
//	RTDiffuse->Release();
//	RTDiffuse = NULL;
//	RTNormal->Release();
//	RTNormal = NULL;
//	RTLights->Release();
//	RTLights = NULL;
//
//	backbuffer->Release();
//	backbuffer = NULL;
//
//	PosSur->Release();
//	PosSur = NULL;
//	NormSur->Release();
//	NormSur = NULL;
//	ShadowSur->Release();
//	ShadowSur = NULL;
//	DiffSur->Release();
//	ShadowSur = NULL;
//	LightSur->Release();
//	LightSur = NULL;
//	ShadowStencil->Release();
//	ShadowStencil = NULL;
//
//	BFX->OnLostDevice();
//	LFX->OnLostDevice();
//	SFX->OnLostDevice();
//	FFX->OnLostDevice();
//	LAFX->OnLostDevice();
//
//	RM->onLostDevice();
//
//	if (destruct)
//	{
//		BFX->Release();
//		LFX->Release();
//		SFX->Release();
//		FFX->Release();
//		LAFX->Release();
//	}
//		
//
//}
//
//void Renderer::onResetDevice(bool first)
//{
//	cout << "width: " << g_D3DAPP->getWidth() << " Height: " << g_D3DAPP->getHeight() << endl;
//
//	HRESULT temp = g_D3dDevice->CreateTexture(g_D3DAPP->getWidth(), g_D3DAPP->getHeight(), 1, D3DUSAGE_RENDERTARGET, D3DFMT_A8R8G8B8, D3DPOOL_DEFAULT, &RTDiffuse, NULL);
//	//if (temp == S_OK)
//	//	cout << "OK!\n";
//	//else if (temp == D3DERR_INVALIDCALL)
//	//	cout << "Invalid Call!\n";
//	//else if (temp == D3DERR_OUTOFVIDEOMEMORY)
//	//	cout << "Out of VMEM!\n";
//	//else if (temp == E_OUTOFMEMORY)
//	//	cout << "Out of Mem!\n";
//
//	temp = g_D3dDevice->CreateTexture(g_D3DAPP->getWidth(), g_D3DAPP->getHeight(), 1, D3DUSAGE_RENDERTARGET, D3DFMT_X8R8G8B8, D3DPOOL_DEFAULT, &RTNormal, NULL);
//	//if (temp == S_OK)
//	//	cout << "OK!\n";
//	//else if (temp == D3DERR_INVALIDCALL)
//	//	cout << "Invalid Call!\n";
//	//else if (temp == D3DERR_OUTOFVIDEOMEMORY)
//	//	cout << "Out of VMEM!\n";
//	//else if (temp == E_OUTOFMEMORY)
//	//	cout << "Out of Mem!\n";
//
//	temp = g_D3dDevice->CreateTexture(g_D3DAPP->getWidth(), g_D3DAPP->getHeight(), 1, D3DUSAGE_RENDERTARGET, D3DFMT_X8R8G8B8, D3DPOOL_DEFAULT, &RTPos, NULL);
//	//if (temp == S_OK)
//	//	cout << "OK!\n";
//	//else if (temp == D3DERR_INVALIDCALL)
//	//	cout << "Invalid Call!\n";
//	//else if (temp == D3DERR_OUTOFVIDEOMEMORY)
//	//	cout << "Out of VMEM!\n";
//	//else if (temp == E_OUTOFMEMORY)
//	//	cout << "Out of Mem!\n";
//
//	temp = g_D3dDevice->CreateTexture(g_D3DAPP->getWidth(), g_D3DAPP->getHeight(), 1, D3DUSAGE_RENDERTARGET, D3DFMT_R32F, D3DPOOL_DEFAULT, &RTShadow, NULL);
//	//if (temp == S_OK)
//	//	cout << "OK!\n";
//	//else if (temp == D3DERR_INVALIDCALL)
//	//	cout << "Invalid Call!\n";
//	//else if (temp == D3DERR_OUTOFVIDEOMEMORY)
//	//	cout << "Out of VMEM!\n";
//	//else if (temp == E_OUTOFMEMORY)
//	//	cout << "Out of Mem!\n";
//
//	temp = g_D3dDevice->CreateTexture(g_D3DAPP->getWidth() - 10, g_D3DAPP->getHeight() - 30, 1, D3DUSAGE_RENDERTARGET, D3DFMT_A8R8G8B8, D3DPOOL_DEFAULT, &RTLights, NULL);
//	//if (temp == S_OK)
//	//	cout << "OK!\n";
//	//else if (temp == D3DERR_INVALIDCALL)
//	//	cout << "Invalid Call!\n";
//	//else if (temp == D3DERR_OUTOFVIDEOMEMORY)
//	//	cout << "Out of VMEM!\n";
//	//else if (temp == E_OUTOFMEMORY)
//	//	cout << "Out of Mem!\n";
//
//	g_D3dDevice->CreateDepthStencilSurface(512, 512, D3DFMT_D24S8, D3DMULTISAMPLE_NONE, 0, TRUE, &ShadowStencil, NULL);
//
//
//	RTPos->GetSurfaceLevel(0, &PosSur);
//	RTNormal->GetSurfaceLevel(0, &NormSur);
//	RTShadow->GetSurfaceLevel(0, &ShadowSur);
//	RTDiffuse->GetSurfaceLevel(0, &DiffSur);
//	RTLights->GetSurfaceLevel(0, &LightSur);
//	g_D3dDevice->GetBackBuffer(0, 0, D3DBACKBUFFER_TYPE_MONO, &backbuffer);
//	
//	//SHFX = RM->getEffect(SHADOWFX);
//	//SHFX->SetTechnique("main");
//	if (!first)
//	{
//		RM->onResetDevice();
//		BFX->OnResetDevice();
//		LFX->OnResetDevice();
//		LAFX->OnResetDevice();
//		SFX->OnResetDevice();
//		FFX->OnResetDevice();
//	}
//}
//
//void Renderer::setSky(int index)
//{
//	skyboxTexture = index;
//}

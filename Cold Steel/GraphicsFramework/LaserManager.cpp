#include "LaserManager.h"


LaserManager::LaserManager()
{
}


LaserManager::~LaserManager()
{
	lasers.clear();
}

LaserManager* LaserManager::thenameofthefunctiondoesntreallymattericannameitanythingeventhoughweusuallycallitinstance()
{
	static LaserManager* thisoneisusuallycalledptrbutimgoingtotomakeitsomethingmorerediculous = new LaserManager();
	return thisoneisusuallycalledptrbutimgoingtotomakeitsomethingmorerediculous;
}

void LaserManager::addLaser(D3DXVECTOR3 pos, D3DXVECTOR3 vel, UINT mID, MasterEntity* me)
{
	lasers.push_back(Laser(pos, vel, mID, me));
	FMOD_VECTOR temp;
	temp.x = pos.x;
	temp.y = pos.y;
	temp.z = pos.z;
	SOUND->playSoundEffect("../media/soundeffect/TankLaser1.wav", temp, false);
}	

MasterEntity* LaserManager::getME(UINT index)
{
	return lasers[index].getMasterEntity();
}


void LaserManager::delLaser(UINT index)
{
	lasers.erase(lasers.begin() + index);
}

void LaserManager::update(double dt)
{
	for (int i = lasers.size() - 1; i >= 0; i--)
	{
		if (CLK->getTime() > lasers[i].getEnd())
			lasers.erase(lasers.begin() + i);
		else
			lasers[i].update(dt);
	}
}

void LaserManager::leave()
{
	lasers.clear();
}

vector<Laser> LaserManager::getLasers()
{
	return lasers;
}
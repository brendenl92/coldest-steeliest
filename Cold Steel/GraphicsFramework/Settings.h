#pragma once

#include <fstream>
#include <iostream>
#include <Windows.h>
#include "SoundManager.h"
#include <string>
using namespace std;

#define SET Settings::instance()

class Settings
{
	bool controller,
		 inverted,
		 thirdPerson;

	float fxVolume,
		   musicVolume;

	float sensitivity;
		   
	Settings();
public:
	static Settings* instance();

	void setSettings(bool controller, bool inverted, bool third, float fxVol, float musicVol, float sensitivity);
	
	bool getController();
	bool getThird();
	bool getInverted();
	float getFXVolume();
	float getMusicVolume();
	float getSensitivity();

};
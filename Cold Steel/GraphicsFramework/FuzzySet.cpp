#include "FuzzySet.h"


double FuzzySet_Triangle::CalculateDOM(double val) const
{
	///test to see wether the left or right offsets are zero
	///to prevent equalling zero we will divide by zero
	if( (m_Left == 0.0 && m_Peak == val) || (m_Right == 0.0 && m_Peak == val) )
		return 1.0;

	///find if DOM is left of peak
	if( (val <= m_Peak) && (val >= m_Left) )
	{
		double grad = 1.0 / m_Left;
		return grad * ( val - ( m_Peak - m_Left) );
	}

	///find if DOM is right of peak
	else if( (val > m_Peak) && (val <= m_Right) )
	{
		double grad = 1.0 / m_Right;
		return grad * ( val - ( m_Right - m_Peak) );
	}

	///if out of range of this FLV, return 0
	else
	{
		return 0.0;
	}
}

double FuzzySet_LeftShoulder::CalculateDOM(double val) const
{
	///check to see which offset will be zero
	if( (m_Right == 0.0 && m_Peak == val) )
		return 1.0;

	///find Dom if left of peak
	if( val < m_Peak )
	{
		return 1.0;
	}

	///find Dom if right of peak
	else if( (val >= m_Peak) && (val < m_Right) )
	{
		double grad = 1.0 / m_Right;
		return grad * ( val - ( m_Right - m_Peak) );
	}

	///out of range return 0
	else
	{
		return 0.0;
	}
}

double FuzzySet_RightShoulder::CalculateDOM(double val) const
{
	///check to see which offset will be zero
	if( (m_Left == 0.0 && m_Peak == val) )
		return 1.0;

	///find Dom if left of peak
	if( (val <= m_Peak) && (val > m_Left) )
	{
		double grad = 1.0 / m_Left;
		return grad * ( val - ( m_Peak - m_Left) );
	}

	///find Dom if right of peak
	else if( val > m_Peak )
	{
		return 1.0;
	}

	///out of range return 0
	else
	{
		return 0.0;
	}
}
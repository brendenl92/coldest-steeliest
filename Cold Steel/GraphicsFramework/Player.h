#pragma once
#include "GraphicsEntity.h"
class Player
{
	short playerID;
	GraphicsEntity* player;
	D3DXVECTOR3 target;
public:
	Player(short playerID, D3DXVECTOR3 pos);
	GraphicsEntity* getEntity();
	short getID();
	void update(float dt);
	void setTarget(D3DXVECTOR3 pos);
	~Player();
};


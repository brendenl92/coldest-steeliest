#pragma once
#include "ResourceManager.h"
class MasterEntity;
class Laser
{
	UINT mID;
	double endTime;
	D3DXVECTOR3 vel;
	D3DXVECTOR3 pos;
	D3DXMATRIX transform;
	MasterEntity* me;

public:
	Laser(D3DXVECTOR3 pos, D3DXVECTOR3 vel, UINT mID, MasterEntity* me);
	~Laser();
	void update(double dt);
	UINT getID();
	D3DXMATRIX getMat();
	MasterEntity* getMasterEntity();
	double getEnd();
	D3DXVECTOR3 getPosition(){ return pos; }

};


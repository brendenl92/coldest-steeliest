#pragma once
#include "DXUtil.h"

enum COLOR { blue, red, green, purple, orange, pink };

class Sphere
{
	D3DXMATRIX mat;
	LPD3DXMESH mesh;
	UINT meshID;
	UINT color;
	
public:
	Sphere(D3DXVECTOR3 pos, float size, UINT meshID);
	~Sphere();

	void setPos(D3DXVECTOR3 in);
	void setColor(UINT color);

	D3DXMATRIX getTrans();
	UINT getColor();
	UINT getID();

};


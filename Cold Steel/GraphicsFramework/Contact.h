#pragma once

#include <vector>
#include "BoundingVolume.h"



enum Priority_Type{LHS, RHS};

//////////////////////////
//Contact
/////////////////////////
class Contact
{
public:

	Contact(BoundingVolumeNode* lhs, BoundingVolumeNode* rhs, V3 PointOfContact, V3 Normal, float PenetrationDepth, float Epsilon = 1.0f);

	//Variables

	BoundingVolumeNode* mVolumes[2];
	float mDepth;
	float mEpsilon;
	V3 mPoint;
	V3 mNormal;
};



////////////////////////
//Collision Detector
////////////////////////
struct CollisionDetector
{
	bool DetectCollisions(BoundingVolumeNode* lhs, BoundingVolumeNode* rhs);
//	int  Priority(BoundingVolumeNode* lhs, BoundingVolumeNode* rhs);
	bool SphereToSphere(BoundingVolumeNode* lhs, BoundingVolumeNode* rhs);
	bool OBBToOBB(BoundingVolumeNode* lhs, BoundingVolumeNode* rhs);
	bool SphereToOBB(BoundingVolumeNode* Sphere, BoundingVolumeNode* Box);
	bool RayToOBB(V3& Rp, V3& Rn, BoundingVolumeNode* rhs);
	bool RayToSphere(V3& Rp, V3& Rn, BoundingVolumeNode* rhs);
};



//////////////////////
//Contact Generator
/////////////////////
struct ContactGenerator
{
	Contact* GenerateContact(BoundingVolumeNode* lhs, BoundingVolumeNode* rhs);
	Contact* SphereToSphere(BoundingVolumeNode* Sphere, BoundingVolumeNode* Box);
	Contact* SphereToOBB(BoundingVolumeNode* lhs, BoundingVolumeNode* rhs);
	Contact* OBBToOBB(BoundingVolumeNode* lhs, BoundingVolumeNode* rhs);
	Contact* RayToOBB(V3& Rp, V3& Rn, BoundingVolumeNode* rhs);
	Contact* RayToSphere(V3& Rp, V3& Rn, BoundingVolumeNode* rhs);
};




/////////////////////////
//Contact Resolver
////////////////////////
struct ContactResolver
{
	void InterpenetrationFix(Contact* Contact);
};



///////////////////////////
//Collision Data
//////////////////////////
//this will work, i can feel it in my loins. 
class CollisionData
{
public:

	CollisionData(int MaxContacts = 10, int MaxItterations = 10);

	CollisionDetector mDetector;
	ContactGenerator mGenerator;
	ContactResolver mResolver;


	//VAriables
	std::vector<Contact*> mContacts;

	int mMaxContacts;
	int mMaxItterations;

	//Methods
	void AddContact(Contact* contact);
	bool DetectCollision(BoundingVolumeNode* lhs, BoundingVolumeNode* rhs);
	Contact* GenerateContact(BoundingVolumeNode* lhs, BoundingVolumeNode* rhs);
	void RsolveContacts();
};
#pragma once

#include <cmath>
#include <algorithm>
#include <cassert>
#include <time.h>
#include <stdlib.h>

using namespace std;

#define Rand Random::Instance()

class Random 
{
private:
	float ZN, ZN1, A, C, M, xMin, xMax;

private:
	Random()
	{
		A		= 16807;
		C		= 0;
		M		= (float)2147483647;
		xMin	= 0;
		xMax	= 1;
	}

public:
	static Random* Instance()
	{
		static Random* ptr = new Random;

		return ptr;
	}

	///should be set at the beginning
	void SetSeed(float newSeed)
	{
		ZN = newSeed;
	}

	void SetMin(float n1)
	{
		xMin = n1;
	}

	void SetMax(float n2)
	{
		xMax = n2;
	}

	///returns a number between 0 & 1
	double get()
	{
		ZN1 = fmod((A * ZN + C), M);
		ZN = ZN1;
		return ZN/M;
	}

	double Uniform(double xMin, double xMax)
	{
		//assert(xMin < xMax);
		return ((xMax - xMin) * get()) + xMin;
	}
};
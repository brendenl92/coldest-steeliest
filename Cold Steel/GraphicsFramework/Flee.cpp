//Flee.cpp
#include "Flee.h"
#include "BaseAIEntity.h"
#include "AIManager.h"

Flee* Flee::s_Instance = 0;
Flee* Flee::Instance()
{
	if(!s_Instance)
		s_Instance = new Flee;
	return s_Instance;
}

Flee::Flee() 
	: BaseState()
{
	BaseName	= "FLEE";
	numMsg1		= 0;
	numMsg2		= 0;
	numMsg3		= 0;
}

Flee::Flee(ASM* fStatem, int id)
	: BaseState(fStatem)
{
	BaseName	= "FLEE";
	m_ownerID	= id;
	numMsg1		= 0;
	numMsg2		= 0;
	numMsg3		= 0;
}

Flee::~Flee()
{
}

void Flee::Enter()
{
	///change objects color here
}

void Flee::Execute(double dt)
{
	
}

void Flee::Exit()
{

}

void Flee::HandleMsg(Mail message)
{
	switch (message.m_Msg)
	{
	case _OnPatrol:
		{
			m_fsm->Transition("WANDER");
			//MD->SendToState(message.m_Reciever, message.m_Reciever, 0, _OnPatrol, (void*)0);
			AMI->GetBaseAIEntity()[message.m_Reciever]->SetState(1);
			break;
		}
	case _LockedOn:
		{
			break;
		}
	case _FallingBack:
		{
			if (AMI->GetBaseAIEntity()[message.m_Reciever]->Enemy == NULL)
			{
				m_fsm->Transition("WANDER");
				AMI->GetBaseAIEntity()[message.m_Reciever]->SetState(1);
			}
			else if (AMI->GetBaseAIEntity()[message.m_Reciever]->GetCurrentHP() > 50)
			{
				m_fsm->Transition("ATTACK");
				//MD->SendToState(message.m_Reciever, message.m_Reciever, 0, _LockedOn, (void*)0);
				AMI->GetBaseAIEntity()[message.m_Reciever]->SetState(2);
			}
			else if (AMI->GetBaseAIEntity()[message.m_Reciever]->GetCurrentHP() < 50)
			{
				m_fsm->Transition("FLEE");
				//MD->SendToState(message.m_Reciever, message.m_Reciever, 0, _FallingBack, (void*)0);
				AMI->GetBaseAIEntity()[message.m_Reciever]->SetState(3);
			}
		}
	}
}

void Flee::ShutDown()
{
	s_Instance = NULL;
}
#pragma once

#include <map>
#include <string>
#include <vector>
#include "FuzzyRule.h"
#include "FuzzyVariable.h"

using namespace std;

class FuzzyModule
{
private:
	typedef map< string, FuzzyVariable* > VarMap;

private:
	//a map of all the fuzzy variables
	VarMap				m_Variables;

	//a vector containing the rules
	vector<FuzzyRule*>	m_Rules;

	//zeros the DOMs of each rule; used in defuzzify
	void	SetToZero();

public:
	~FuzzyModule()
	{
		m_Variables.clear();
		m_Rules.clear();
	}

	//create a new "empty" fuzzy Variable and returns a reference to it.
	FuzzyVariable&	CreateFLV(const string& VarName);

	//adds a rule
	void			AddRule(FuzzyTerm& precede, FuzzyTerm& follow);

	//fuzzify's the FLVs
	void			Fuzzify(const string& FLV, double val);

	double			Defuzzify(const string& key);
};
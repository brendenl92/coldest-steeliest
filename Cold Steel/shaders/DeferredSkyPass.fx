uniform extern float4x4 WVP;
uniform extern float4x4 World;
uniform extern texture Tex;

uniform extern float3 CameraPos;

struct VSIn
{
	float4 position : POSITION0;
	float3 tex0 : TEXCOORD0;
};

struct VSOut
{

	float4 pos : POSITION0;
	float3 tex0 : TEXCOORD0;
};

sampler SkySampler = sampler_state
{
	Texture = <Tex>;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	MipFilter = LINEAR;
	AddressU = CLAMP;
	AddressV = CLAMP;
};

VSOut SkyVS(VSIn i)
{
	VSOut o = (VSOut)0;

	o.pos = mul(i.position, WVP).xyww;
	
	o.tex0 = i.position.xyz;

	return o;
}

float4 SkyPS(VSOut input) : COLOR
{
	float4 output = texCUBE(SkySampler, input.tex0);
	output.a = 1;
	return output;

}

technique main
{
	pass p0
	{
		VertexShader = compile vs_2_0 SkyVS();
		PixelShader = compile ps_2_0 SkyPS();
		CullMode = none;
		ZWriteEnable = true;
		ZFunc = lessequal;

	}
	
};
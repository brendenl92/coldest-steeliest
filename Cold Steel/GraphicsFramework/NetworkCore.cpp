#include "NetworkCore.h"

SOCKET Socket;
vector<Player*> players;
char* buffer;

void update()
{
	buffer = new char[256];
	cout << "Joined thread\n";
	while (1)
	{
		if (recv(Socket, buffer, sizeof(Server_UpdatePosition), NULL) > 0)
		{
			void* data = (void*)buffer;
			switch (buffer[0])
			{
			case 0:
			{
				cout << "Packet 0 Recieved";
				Server_ClientJoin* pack = (Server_ClientJoin*)data;
				cout << "Player " << pack->clientID << " Connected...";
				players.push_back(new Player(pack->clientID, D3DXVECTOR3((float)pack->data.x, (float)pack->data.y, (float)pack->data.z)));
				break;
			}
			case 2:
			{
				Client_UpdatePosition *pack = (Client_UpdatePosition*)data;
				cout << "Packet 2 Recieved from " << pack->clientID << " with position (" << pack->x << ", " << pack->y << ", " << pack->z << ")\n";
				for (UINT i = 0; i < players.size(); i++)
					if (players[i]->getID() == pack->clientID)
					{
						//cout << "Player " << i << "'s position updated to (" << pack->x << ", " << pack->y << ", " << pack->z << ").\n";
						players[i]->setTarget(D3DXVECTOR3((float)pack->x, (float)pack->y, (float)pack->z));
					}
				break;
			}
			case 3:
			{
				Client_Shoot* shot = (Client_Shoot*)data;
				cout << "Packet 3 recieved from " << shot->clientID << ".\n";
				for (UINT i = 0; i < players.size(); i++)
					if (players[i]->getID() == shot->clientID)
					{
						FMOD_VECTOR temp = { players[i]->getEntity()->getPos().x, players[i]->getEntity()->getPos().y, players[i]->getEntity()->getPos().z };
						SOUND->playSoundEffect("../media/soundeffect/Shot.wav", temp, false);
					}
			}
			case 'L':
				cout << "l recieved\n";
				break;
			}
		}
	}
}

void NetworkCore::disconnect()
{
	char* buffer = new char[1];
	buffer[0] = 'D';
	send(Socket, buffer, sizeof(buffer), 0);
}

NetworkCore::NetworkCore(UINT port, string ip)
{
	if (WSAStartup(MAKEWORD(2, 2), &data) == 0)
		cout << "System Started Up.\n";

	Socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (Socket == INVALID_SOCKET)
		cout << "Failed to Connect!";
	else
		cout << "Socket creation success!\n";

	ioctlsocket(Socket, FIONBIO, (u_long*)1);
	in_addr hostaddr;
	hostaddr.s_addr = inet_addr(ip.c_str());
	struct hostent *host;
	if ((host = gethostbyaddr((const char*)&hostaddr, sizeof(in_addr), AF_INET)) == NULL)
		cout << "failed to resolve host\n";
	else
		cout << "Succeeded to resolve hostname\n";

	serverInfo.sin_port = htons(port);
	serverInfo.sin_family = AF_INET;
	serverInfo.sin_addr.s_addr = *((unsigned long*)host->h_addr);

	int answer = connect(Socket, (SOCKADDR*)(&serverInfo), sizeof(serverInfo));
	if (answer != 0)
		cout << "error connecting";
	else
		cout << "Connection successful!\n";

	char* buffer = new char[256];
	
	if (recv(Socket, buffer, sizeof(Server_ClientJoin), 0) > 0)
	{
		void* data = (void*)buffer;
		Server_ClientJoin* pack = (Server_ClientJoin*)data;
		cout << "Recieved player information: " << pack->clientID << " - (" << pack->data.x << ", " << pack->data.y << ", " << pack->data.z << ")\n";
		this->clientID = pack->clientID;
		players.push_back(new Player(pack->clientID, D3DXVECTOR3((float)pack->data.x, (float)pack->data.y, (float)pack->data.z)));
		CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)update, NULL, 0, (LPDWORD)0);
	}
} 

NetworkCore::~NetworkCore(void)
{
}

void NetworkCore::SendShoot()
{
	Client_Shoot* shot = new Client_Shoot;
	shot->clientID = this->clientID;
	shot->packetID = 3;
	void* data = (void*)shot;
	if (send(Socket, (char*)data, sizeof(Client_Shoot), 0) > 0)
		cout << "Sent shot\n";
}

void NetworkCore::SendPosition(int x, int y, int z)
{
	Client_UpdatePosition* pack = new Client_UpdatePosition;
	pack->packetID = 2;
	pack->clientID = clientID;
	pack->x = x;
	pack->y = y;
	pack->z = z;
	void* data = (void*)pack;
	send(Socket, (char*)data, sizeof(Client_UpdatePosition), NULL);
}

Player* NetworkCore::getPlayer(int i)
{
	return players[i];
}

char NetworkCore::getClientID()
{
	return clientID;
}
int NetworkCore::getPlayerSize()
{
	return players.size();
}

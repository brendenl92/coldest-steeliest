#include "TankSelectionState.h"
Tank* g_PlayerTank = NULL;
TankSelectionState::TankSelectionState()
{

}


TankSelectionState::~TankSelectionState()
{
	if (back)
		delete back;
}

TankSelectionState* TankSelectionState::Instance()
{
	static TankSelectionState* ptr = new TankSelectionState();
	return ptr;
}

void TankSelectionState::Initialize()
{
	string names[10];
	
	names[0] = "../media/images/leftSelection.png";
	names[1] = "../media/images/rightSelection.png";
	names[2] = "../media/buttons/select.png";

	names[3] = "../media/images/agilityInfo.png";
	names[4] = "../media/images/attackInfo.png";
	names[5] = "../media/images/defenseInfo.png";
	names[6] = "../media/images/StatsCover.png";

	names[7] = "../media/images/HealthBar.png";
	names[8] = "../media/images/RechargeMeter.png";
	names[9] = "../media/images/DefenseBar.png";

	RM->ImportTextures(names, 10);

	RM->createFont("Arial", 20, 9, 300, false);
	RM->createFont("Avenir", 50, 35, 700, false);

	CAM->menuReset();
	CAM->setTarget(D3DXVECTOR3(0, 0, 0));
	back = new TankSelectBack();

	ConstructSelection();

	buttons[0].setChosen(true);
	ShowCursor(TRUE);
}

void TankSelectionState::HandleInput(RAWINPUT* raw)
{
	if (raw->data.mouse.ulButtons & RI_MOUSE_LEFT_BUTTON_UP)
		for (UINT i = 0; i < buttons.size(); i++)
		{
			if (RAW->getMousePos().x <= buttons[i].getPos().x + (buttons[i].getHalfExtents().x * 2) && RAW->getMousePos().x >= buttons[i].getPos().x &&
				RAW->getMousePos().y <= buttons[i].getPos().y + (buttons[i].getHalfExtents().y * 2) && RAW->getMousePos().y >= buttons[i].getPos().y)
			{
				SOUND->playSoundEffect("../media/soundeffect/Button_Click.wav");

				if (buttons[i].getName() == "LeftArrow")
				{
						back->prevTank();
				}

				else if (buttons[i].getName() == "RightArrow")
				{
						back->nextTank();
				}

				else if (buttons[i].getName() == "SelectButton")
				{
					if (back->getTankIndex() == 0)
					{
						switch (back->getTankIndex())
						{
						case 0:
							g_PlayerTank = JUGGERNAUT;
							break;
						case 1:
							g_PlayerTank = PREDATOR;
							break;
						case 2:
							g_PlayerTank = SQUIRREL;
							break;
						case 3:
							g_PlayerTank = ROMAN;
							break;
						case 4:
							g_PlayerTank = RAPTOR;
							break;
						case 5:
							g_PlayerTank = ARMADILLO;
							break;
						case 6:
							g_PlayerTank = TRINITY;
							break;
						case 7:
							g_PlayerTank = CLARK;
							break;
						}
						g_D3DAPP->changeState("GAME");
					}
				}
			}
		}
	if (raw->data.keyboard.VKey == VK_ESCAPE)
		if (raw->data.keyboard.Flags == 0)
			g_D3DAPP->changeState("MENU");

	if (raw->data.keyboard.VKey == 0x56)
		if (raw->data.keyboard.Flags == 0)
			back->prevTank();
	if (raw->data.keyboard.VKey == 0x42)
		if (raw->data.keyboard.Flags == 0)
			back->nextTank();
}

void TankSelectionState::UpdateScene(float dt)
{
	back->update(dt);

	for (UINT i = 0; i < buttons.size(); i++)
		buttons[i].update(dt);

	for (UINT i = 0; i < HoverButtons.size(); ++i)
		HoverButtons[i].update(dt);

	switch (back->getTankIndex())
	{
	case 0:
		HoverButtons[0].setRight(JUGGERNAUT->Damage());
		HoverButtons[1].setRight(JUGGERNAUT->Speed());
		HoverButtons[2].setRight(JUGGERNAUT->Defense());
		break;
	case 1:
		HoverButtons[0].setRight(PREDATOR->Damage());
		HoverButtons[1].setRight(PREDATOR->Speed());
		HoverButtons[2].setRight(PREDATOR->Defense());
		break;
	case 2:
		HoverButtons[0].setRight(SQUIRREL->Damage());
		HoverButtons[1].setRight(SQUIRREL->Speed());
		HoverButtons[2].setRight(SQUIRREL->Defense());
		break;
	case 3:
		HoverButtons[0].setRight(ROMAN->Damage());
		HoverButtons[1].setRight(ROMAN->Speed());
		HoverButtons[2].setRight(ROMAN->Defense());
		break;
	case 4:
		HoverButtons[0].setRight(RAPTOR->Damage());
		HoverButtons[1].setRight(RAPTOR->Speed());
		HoverButtons[2].setRight(RAPTOR->Defense());
		break;
	case 5:
		HoverButtons[0].setRight(ARMADILLO->Damage());
		HoverButtons[1].setRight(ARMADILLO->Speed());
		HoverButtons[2].setRight(ARMADILLO->Defense());
		break;
	case 6:
		HoverButtons[0].setRight(TRINITY->Damage());
		HoverButtons[1].setRight(TRINITY->Speed());
		HoverButtons[2].setRight(TRINITY->Defense());
		break;
	case 7:
		HoverButtons[0].setRight(CLARK->Damage());
		HoverButtons[1].setRight(CLARK->Speed());
		HoverButtons[2].setRight(CLARK->Defense());
		break;
	}

	SOUND->update();
}

void TankSelectionState::RenderScene()
{
	HR(g_D3dDevice->Clear(0, 0, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, D3DCOLOR_XRGB(84, 84, 84), 1.0F, 0));
	HR(g_D3dDevice->BeginScene());

	back->render();

	D3DXMATRIX temp;

	RM->getSprite()->Begin(D3DXSPRITE_ALPHABLEND);

	for (UINT i = 0; i < buttons.size(); i++)
	{
		D3DXMatrixTransformation2D(&temp, NULL, 0.0, &buttons[i].getScale(), &(buttons[i].getPos() + buttons[i].getHalfExtents()), 0, &buttons[i].getPos());
		RM->getSprite()->SetTransform(&temp);
		RM->getSprite()->Draw(*RM->getTexture(buttons[i].getID()), &buttons[i].getRect(), NULL, NULL, 0xFFFFFFFF);
	}

	for (UINT i = 0; i < HoverButtons.size(); i++)
	{
		D3DXMatrixTransformation2D(&temp, NULL, 0.0, &(D3DXVECTOR2(0.4f, 0.5f) * 2), NULL, 0, NULL);
		RM->getSprite()->SetTransform(&temp);
		RM->getSprite()->Draw(*RM->getTexture(HoverButtons[i].getID()), &HoverButtons[i].getRect(), NULL, &D3DXVECTOR3(HoverButtons[i].getPos().x, HoverButtons[i].getPos().y, 0), 0xFFFFFFFF);
	}

	D3DXMatrixTransformation2D(&temp, NULL, 0.0, &(images[0].getScale() * 2), NULL, NULL, &images[0].getPos());
	RM->getSprite()->SetTransform(&temp);
	RM->getSprite()->Draw(*RM->getTexture(images[0].getTextID()), NULL, NULL, NULL, 0xFFFFFFFF);

	RM->getSprite()->End();

	for (UINT i = 0; i < texts.size(); i++)
		RM->getFont(texts[i].getFont())->DrawTextA(0, texts[i].getText().c_str(), -1, &texts[i].getRect(), DT_CENTER | DT_VCENTER, D3DCOLOR_XRGB(255, 255, 255));

	if (back->getTankIndex() != 0)
	{
		RECT temp = { g_D3DAPP->getWidth() / 2 - 320, g_D3DAPP->getHeight() / 2 - 100, g_D3DAPP->getWidth() / 2 + 320, g_D3DAPP->getHeight() / 2 + 100 };
		RM->getFont(1)->DrawTextA(NULL, "Tank not unlocked!", -1, &temp, DT_CENTER | DT_VCENTER, D3DCOLOR_XRGB(255, 255, 255));
	}

	g_D3dDevice->EndScene();
	g_D3dDevice->Present(0, 0, 0, 0);
}

void TankSelectionState::OnResetDevice()
{
	RM->onResetDevice();
	back->onResetDevice();
}

void TankSelectionState::OnLostDevice()
{
	RM->onLostDevice();
	back->onLostDevice();
}

void TankSelectionState::Leave()
{
	delete back;
	back = NULL;

	SOUND->endSong();

	buttons.clear();
	texts.clear();
	HoverButtons.clear();
	images.clear();
	
	RM->clearAll();

}

void TankSelectionState::ConstructSelection()
{
	options = false;
	chosen = 0;
	string names[6];
	names[0] = "LeftArrow";
	names[1] = "RightArrow";
	names[2] = "SelectButton";
	names[3] = "AttackBar";
	names[4] = "AgilityBar";
	names[5] = "DefenseBar";

	buttons.push_back(Button(0, D3DXVECTOR2(30, (float)g_D3DAPP->getHeight() - 140), names[0]));
	buttons.push_back(Button(1, D3DXVECTOR2((float)g_D3DAPP->getWidth() - 300, (float)g_D3DAPP->getHeight() - 140), names[1]));
	buttons.push_back(Button(2, D3DXVECTOR2((float)(g_D3DAPP->getWidth() / 2) - 180, (float)g_D3DAPP->getHeight() - 140), names[2]));

	HoverButtons.push_back(Button2(7, D3DXVECTOR2((float)(g_D3DAPP->getWidth() / 2) + 140, 0 + 50), names[3]));
	HoverButtons.push_back(Button2(9, D3DXVECTOR2((float)(g_D3DAPP->getWidth() / 2) + 140, 0 + 115), names[4]));
	HoverButtons.push_back(Button2(8, D3DXVECTOR2((float)(g_D3DAPP->getWidth() / 2) + 140, 0 + 180), names[5]));

	images.push_back(Image(D3DXVECTOR2((float)(g_D3DAPP->getWidth() / 2 + 150) - (g_D3DAPP->getWidth() / 8), 0), D3DXVECTOR2(0.5f, 0.5f), 6));

	images.push_back(Image(D3DXVECTOR2((float)0.0f, (float)0.0f), D3DXVECTOR2(1, 1), 3));

	OnLostDevice();
}
//=============================================================================
// PhongDirLtTex.fx by Frank Luna (C) 2004 All Rights Reserved.
//
// Phong directional light & texture.
//=============================================================================

uniform extern float4x4 World;
uniform extern float4x3 DeferredWorld;
uniform extern float4x4 WorldInvTrans;
uniform extern float4x4 WVP;
uniform extern float4x4 VP;
uniform extern float4x4 Proj;
uniform extern float4x4 View;
uniform extern float4x3 DeferredView;
uniform extern float4x3 InverseView;

uniform extern float4 MatAmbient;
uniform extern float4 MatDiffuse;
uniform extern float4 MatSpecular;
uniform extern float  MatPower;
uniform extern float  Density;
	
uniform extern float4 LightDiffuse;
uniform extern float4 LightSpecular;
uniform extern float3 LightPosition;
uniform extern float3 LightDirection;

uniform extern float3   EyePosW;

uniform extern texture Tex;
uniform extern texture DiffuseMap;
uniform extern texture NormalMap;
uniform extern texture PositionMap;
uniform extern texture DepthMap;

uniform extern bool lit;

sampler TexS = sampler_state
{
	Texture = <Tex>;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	MipFilter = LINEAR;
	MaxAnisotropy = 16;
	AddressU  = WRAP;
   	AddressV  = WRAP;
	AddressW  = Wrap;
};

sampler DiffuseSampler = sampler_state
{
	Texture = <DiffuseMap>;
	MagFilter = Point;
	MinFilter = Point;
};

sampler NormalSampler = sampler_state
{
	Texture = <NormalMap>;
	MagFilter = Point;
	MinFilter = Point;
};

sampler PositionSampler = sampler_state
{
	Texture = <PositionMap>;
	MagFilter = Point;
	MinFilter = Point;
};

sampler DepthSampler = sampler_state
{
	Texture = <DepthMap>;
	MagFilter = Point;
	MinFilter = Point;
};

struct OutputVS
{
	float4 posH    : POSITION0;
	float3 normalW : TEXCOORD0;
	float3 toEyeW  : TEXCOORD1;
	float2 tex0    : TEXCOORD2;
};

OutputVS PhongDirLtTexVS(float3 posL : POSITION0, float3 normalL : NORMAL0, float2 tex0: TEXCOORD0)
{

	OutputVS outVS = (OutputVS)0;
	
	outVS.normalW = mul(float4(normalL, 0.0f), WorldInvTrans).xyz;
	float3 posW  = mul(float4(posL, 1.0f), World).xyz;
	outVS.posH = mul(float4(posL, 1.0f), WVP);

	return outVS;
}

float4 PhongDirLtTexPS(float3 normalW : TEXCOORD0, float3 toEyeW  : TEXCOORD1, float2 tex0 : TEXCOORD2) : COLOR
{
	return float4(0, 0, 0, 0);
}

technique one
{
    pass P0
    {
        vertexShader = compile vs_2_0 PhongDirLtTexVS();
        pixelShader  = compile ps_2_0 PhongDirLtTexPS();
    }
}
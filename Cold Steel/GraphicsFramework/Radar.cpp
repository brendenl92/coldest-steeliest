#include "Radar.h"
#include "D3DAPP.h"
#include "MasterEntity.h"

Radar::Radar()
{
	mTankBaseAngle = 0.0f;

	float width = (float)g_D3DAPP->getWidth();
	float height = (float)g_D3DAPP->getHeight();

	mTankBasePos = D3DXVECTOR3(width - 220, 190, 0);

	width *= 2;
	width -= 200;

	height /= 2;
	height -= 150;

	mRadarPos = D3DXVECTOR3(width/3 + 350, height/3 + 50, 0);

	isPaused = false;

}


Radar::~Radar()
{

}

void Radar::Update(vector <MasterEntity*> Entities)
{
	EntitiesVec = Entities;
}

void Radar::Draw()
{

	D3DXMatrixTransformation2D(&mMatrix, &D3DXVECTOR2(128, 128), 0, &D3DXVECTOR2(1.3f, 1.3f), NULL, 0, NULL);
	HR(RM->getSprite()->SetTransform(&mMatrix));
	HR(RM->getSprite()->Draw(*RM->getTexture(2), NULL, &D3DXVECTOR3(256, 256, 0), &mRadarPos, D3DXCOLOR(1, 1, 1, 1)));
	
	DrawRadarTurret();

	DrawRadarTank();

	DrawRadarEntities();
}

void Radar::OnLostDevice()
{

}

void Radar::OnResetDevice()
{
	float width = (float)g_D3DAPP->getWidth();
	float height = (float)g_D3DAPP->getHeight();

	mTankBasePos = D3DXVECTOR3(width - 100, (height / 2) - 225, 0);

	width *= 2;
	width -= 200;

	height /= 2;
	height -= 150;

	mRadarPos = D3DXVECTOR3(width, height, 0);
}

void Radar::DrawRadarEntities()
{
	if (EntitiesVec.size() < 2 || isPaused)
		return;

	for (UINT x = 2; x < EntitiesVec.size(); ++x)
	{
		if (EntitiesVec[x]->getMeshID() == TANK1)
		{
			D3DXVECTOR3 PlayerPos = EntitiesVec[1]->getPosition(),
				EnemyPos = EntitiesVec[x]->getPosition();

			D3DXVECTOR3 dir = EnemyPos - PlayerPos;

			float dist = D3DXVec3Length(&dir);
			float maxdist = 150.0f;

			if (dist >= maxdist)
				continue;

			D3DXVECTOR3 vec = D3DXVECTOR3(0, 0, 1);

			D3DXVECTOR3 forward = CAM->getForward();

			forward = D3DXVECTOR3(forward.x, 0, forward.z);

			float dot = D3DXVec3Dot(&vec, &forward),
				lengthA = D3DXVec3Length(&vec),
				lengthB = D3DXVec3Length(&forward);

			float rotation;

			if (forward.x < 0.0f)
				rotation = -acos(dot / (lengthA * lengthB));
			else
				rotation = acos(dot / (lengthA * lengthB));

			rotation *= -1;

			//rotation *= (180 / 3.14159f);
			//float angle = rotation * (3.14159f / 180);//0 is where the angle of degrees goes.

			dir = D3DXVECTOR3(dir.x, dir.y, dir.z * -1);

			D3DXMatrixTransformation2D(&mMatrix, NULL, 0, NULL, &D3DXVECTOR2(mTankBasePos.x, mTankBasePos.y), rotation, NULL);
			HR(RM->getSprite()->SetTransform(&mMatrix));
			HR(RM->getSprite()->Draw(*RM->getTexture(9), NULL, &D3DXVECTOR3(0, 0, 0), &D3DXVECTOR3(mTankBasePos.x + dir.x, mTankBasePos.y + dir.z, 0), D3DXCOLOR(1, 1, 1, 1)));
		}
	}
}

void Radar::DrawRadarTank()
{
	D3DXVECTOR3 fwd(EntitiesVec[1]->getBaseRot()._31, 0, EntitiesVec[1]->getBaseRot()._33);

	/*if (fwd.x > 0.0f)
		fwd = D3DXVECTOR3(fwd.x * -1, 0, fwd.z * -1);*/

	D3DXVECTOR3 vec = D3DXVECTOR3(CAM->getForward().x, 0, CAM->getForward().z);

	//vec = D3DXVECTOR3(vec.x *= -1, 0, vec.z *= -1);

	float dot = D3DXVec3Dot(&vec, &fwd),
		lengthA = D3DXVec3Length(&vec),
		lengthB = D3DXVec3Length(&fwd);

	if (vec.x <= 0.0f)
		mTankBaseAngle = -acos(dot / (lengthA * lengthB));
	else
		mTankBaseAngle = acos(dot / (lengthA * lengthB));

	D3DXMatrixTransformation2D(&mMatrix, NULL, 0, NULL, &D3DXVECTOR2(mTankBasePos.x, mTankBasePos.y), mTankBaseAngle, NULL);
	HR(RM->getSprite()->SetTransform(&mMatrix));
	HR(RM->getSprite()->Draw(*RM->getTexture(7), NULL, &D3DXVECTOR3(16, 16, 0), &mTankBasePos, D3DXCOLOR(1, 1, 1, 1)));
}

void Radar::DrawRadarTurret()
{
	D3DXMatrixTransformation2D(&mMatrix, NULL, 0, NULL, NULL, NULL, NULL);
	HR(RM->getSprite()->SetTransform(&mMatrix));
	HR(RM->getSprite()->Draw(*RM->getTexture(8), NULL, &D3DXVECTOR3(16, 16, 0), &mTankBasePos, D3DXCOLOR(1, 1, 1, 1)));
}
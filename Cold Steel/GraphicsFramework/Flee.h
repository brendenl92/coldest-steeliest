#pragma once

#include "ASM.h"

#define FL Flee::Instance()

class Flee : public BaseState
{
private:
	static Flee* s_Instance;
	int numMsg1, numMsg2, numMsg3;

private:
	Flee();

public:
	static Flee* Instance();

public:
	Flee(ASM* fsm, int id);
	~Flee();

public:
	void Enter();					//enters into attack state
	void Execute(double dt);		//executes the code in attack state, steering forces will go here
	void Exit();					//will exit the attack state
	void HandleMsg(Mail message);	//handles msgs sent to the attack state
	void ShutDown();
};